import 'dart:async';

import 'package:date_util/date_util.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:happy_mama/entity/push_notification.dart';
import 'package:happy_mama/page/home.dart';
import 'package:happy_mama/service/firebase/notification/notification_service.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_provider.dart';

class NotificationBloc extends BlocBase {
  List<PushNotification> _notifications;
  List<PushNotification> _dayScheduleNotifications;
  List<PushNotification> _monthScheduleNotifications;
  List<PushNotification> _attentionScheduleNotifications;
  List<PushNotification> _standardScheduleNotifications;
  BehaviorSubject<List<PushNotification>> notifications = BehaviorSubject();
  BehaviorSubject<int> unreadAmount = BehaviorSubject();
  BehaviorSubject<bool> firstLoad = BehaviorSubject();
  String _childId;
  DateTime _birthdayDate;
  DateTime _creationDate;
  DateTime _notificationTime;
  Timer _newPushTimer;
  String _name;
  bool _male = false;
  bool wasOpenedFromPush = false;
  bool _silentMode = false;
  String _sound;

  @override
  void dispose() {
    _newPushTimer?.cancel();
    firstLoad?.close();
    notifications?.close();
    unreadAmount?.close();
  }

  init(String childId, DateTime date,
      {String name = 'Кроха',
      bool male = false,
      @required DateTime creation,
      int hour = 11,
      int minute = 0,
      bool silent = false,
      String sound = ''}) {
    _childId = childId;
    _birthdayDate = date;
    _name = name[0].toUpperCase() + name.substring(1).toLowerCase();
    _male = male;
    _creationDate = creation;
    _notificationTime = DateTime(0, 0, 0, hour, minute);
    _silentMode = silent;
    _sound = sound;
    _asyncInit(fromInit: true);
  }

  _asyncInit({bool fromInit = false}) async {
    firstLoad.add(true);
    // print('Start ${DateTime.now()}');
    if (fromInit) {
      await StorageService.clearScheduleLists(_childId);
      await flutterLocalNotificationsPlugin.cancelAll();
    }

    _notifications = await _getNotificationList();
    //print('Start ${DateTime.now()}');
    await Future.wait(<Future>[
      _getScheduleNotifications(),
      _getScheduleNotifications(monthSchedule: true),
      _getAttentionNotification(),
      _getStandardNotifications()
    ]).then((values) {
      _dayScheduleNotifications = values[0];
      _monthScheduleNotifications = values[1];
      _attentionScheduleNotifications = values[2];
      _standardScheduleNotifications = values[3];
    });
    //print('Start day schedule ${DateTime.now()}');
    //_dayScheduleNotifications = await _getScheduleNotifications();
    //print('Start month schedule ${DateTime.now()}');
//    _monthScheduleNotifications =
//        await _getScheduleNotifications(monthSchedule: true);
//
//    ///new
//    ///
//    //print('Start attention schedule ${DateTime.now()}');
//    _attentionScheduleNotifications = await _getAttentionNotification();
//    //print('attention list $_attentionScheduleNotifications');
//    //print('Start standard schedule ${DateTime.now()}');
//    _standardScheduleNotifications = await _getStandardNotifications();

    ///CHECK THIS
    ///
    //print('Start write to storage ${DateTime.now()}');
    await StorageService.writeEveryDayScheduleList(
        _dayScheduleNotifications, _childId);
    await StorageService.writeEveryMonthScheduleList(
        _monthScheduleNotifications, _childId);

    ///new
    ///
    await StorageService.writeAttentionScheduleList(
        _attentionScheduleNotifications, _childId);
    await StorageService.writeStandardScheduleList(
        _standardScheduleNotifications, _childId);
    //_notifications.sort((a, b) => (a?.date?.compareTo(b.date)) ?? 1);
    var pendingPush =
        (await flutterLocalNotificationsPlugin.pendingNotificationRequests())
            .map((f) => f.id)
            .toList();
    notifications.add(_notifications);
    unreadAmount.add(_countUnread());
    //.c.////print('PENDING PUSH $pendingPush');
    //print('Start day init ${DateTime.now()}');
    for (var noty in _dayScheduleNotifications) {
      //.c.////print('NOTY ID ${noty.id}');
      if (pendingPush.contains(noty.id)) continue;

      if (noty.date.year == DateTime.now().year &&
          noty.date.month == DateTime.now().month &&
          noty.date.hour == DateTime.now().hour &&
          noty.date.minute == DateTime.now().minute &&
          (DateTime.now().difference(noty.date).inSeconds > 9)) continue;
      await _initScheduleNotification(
          channelId: 'everydayId',
          channelName: 'everyday',
          channelDescription: 'everyday notifications',
          date: noty.date,
          //.add(Duration(minutes: _notificationTime.minute)),
          id: noty.id,
          title: noty.title,
          body: noty.body);
    }
    //print('Start month init ${DateTime.now()}');
    for (var noty in _monthScheduleNotifications) {
      //.c.////print('NOTY ID ${noty.id}');
      if (pendingPush.contains(noty.id)) continue;

      if (noty.date.year == DateTime.now().year &&
          noty.date.month == DateTime.now().month &&
          noty.date.hour == DateTime.now().hour &&
          noty.date.minute == DateTime.now().minute &&
          (DateTime.now().difference(noty.date).inSeconds > 9)) continue;
      await _initScheduleNotification(
          channelId: 'everydmonthId',
          channelName: 'everymonth',
          channelDescription: 'everymonth notifications',
          //TODO ADD NOTIFICATIONS TIME FROM SETTINGS
          date: noty.date,
          //.add(Duration(minutes: _notificationTime.minute)),
          id: noty.id,
          title: noty.title,
          body: noty.body);
    }
    //print('Start attention init ${DateTime.now()}');
    for (var noty in _attentionScheduleNotifications) {
      //.c.////print('NOTY ID ${noty.id}');
      if (pendingPush.contains(noty.id)) continue;

      if (noty.date.year == DateTime.now().year &&
          noty.date.month == DateTime.now().month &&
          noty.date.hour == DateTime.now().hour &&
          noty.date.minute == DateTime.now().minute &&
          (DateTime.now().difference(noty.date).inSeconds > 9)) continue;
      //print('init attention');
      await _initScheduleNotification(
          channelId: 'attentionId',
          channelName: 'attention',
          channelDescription: 'attention notifications',
          date: noty.date,
          //.add(Duration(minutes: _notificationTime.minute)),
          id: noty.id,
          title: noty.title,
          body: noty.body);
    }
    //print('Start standard init ${DateTime.now()}');
    for (var noty in _standardScheduleNotifications) {
      //.c.////print('NOTY ID ${noty.id}');
      if (pendingPush.contains(noty.id)) continue;

      if (noty.date.year == DateTime.now().year &&
          noty.date.month == DateTime.now().month &&
          noty.date.hour == DateTime.now().hour &&
          noty.date.minute == DateTime.now().minute &&
          (DateTime.now().difference(noty.date).inSeconds > 9)) continue;
      //print('init standard');
      await _initScheduleNotification(
          channelId: 'standardId',
          channelName: 'standard',
          channelDescription: 'standard notifications',
          date: noty.date,
          //.add(Duration(minutes: _notificationTime.minute)),
          id: noty.id,
          title: noty.title,
          body: noty.body);
    }
    //print('Start generator ${DateTime.now()}');
    await _generateLostPush();
    _initNotificationChecker();
  }

  _initForNewDate() async {
    await disableNotifications();
    _asyncInit();
  }

  _initScheduleNotification(
      {@required channelId,
      @required channelName,
      @required channelDescription,
      @required DateTime date,
      @required int id,
      @required String title,
      @required String body}) async {
    var size =
        (await flutterLocalNotificationsPlugin.pendingNotificationRequests())
                ?.length ??
            0;
    //.c.////print(id);
    var scheduledNotificationDateTime = date;
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        (channelId ?? 'defaultId') +
            '${_silentMode ? 'SilentMod' : ''}' +
            'Time:${_notificationTime.hour}:${_notificationTime.minute}' +
            'Sound:${_sound ?? 'system'}',
        channelName ?? 'defaultChannelName',
        channelDescription ?? 'defaultChannelDescription',
        importance: Importance.High,
        priority: Priority.High,
        playSound: !_silentMode,
        sound: (_sound != null && _sound.isNotEmpty && _sound != 'системный')
            ? (_sound)
            : null,
        onlyAlertOnce: false);
    //print('$_sound');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails(
      presentSound: !_silentMode,
      sound: (_sound != null && _sound.isNotEmpty && _sound != 'системный')
          ? (_sound + '.aiff')
          : null,
    );
    NotificationDetails platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(id ?? size, title, body,
        scheduledNotificationDateTime, platformChannelSpecifics,
        androidAllowWhileIdle: true, payload: '$id');
    ////.c.////print(
    // (await flutterLocalNotificationsPlugin.pendingNotificationRequests()));
  }

  Future<List<PushNotification>> _getNotificationList() async {
    var temp = await StorageService.readPushList(_childId) ?? [];

    ///for generator check
    ///
    ///temp.removeAt(0);

    ///generate old push
//    if (temp.isEmpty &&
//        (DateTime.now().difference(_creationDate).inDays >= 1.0)) {
//      var startDate = _birthdayDate.millisecondsSinceEpoch >
//              _creationDate.millisecondsSinceEpoch
//          ? _birthdayDate
//          : _creationDate;
//      //.c.////print('generate');
//      temp = _generateOldPush(startDate);
//    }
    if (temp.isNotEmpty && temp.length > 1) {
      temp.sort((a, b) => (a?.date?.millisecondsSinceEpoch ?? 0)
          .compareTo(((b?.date?.millisecondsSinceEpoch ?? 0))));
      temp = temp.reversed.toList();
    }

    ///for check
//    if (temp.isEmpty) {
//      temp.add(PushNotification(
//          id: 219,
//          iconColor: ColorStyle.codGray,
//          titleTextColor: ColorStyle.codGray,
//          //TODO CHANGE INFO AT FIELD "date" TO DateTime(now.year, now.month, now.day, hour, minute).add(Duration(days: i)),
//          date: DateTime(_birthdayDate.year, _birthdayDate.month,
//                  _birthdayDate.day, 11, 30, 00)
//              .add(Duration(days: 219)),
////              DateTime.now().add(Duration(seconds: i*5 + 1)),
//          title: 'День 220',
//          body: _getTextForPush(id: 5)));
//      temp.add(PushNotification(
//          id: 100,
//          iconColor: ColorStyle.codGray,
//          titleTextColor: ColorStyle.codGray,
//          //TODO CHANGE INFO AT FIELD "date" TO DateTime(now.year, now.month, now.day, hour, minute).add(Duration(days: i)),
//          date: DateTime(_birthdayDate.year, _birthdayDate.month,
//                  _birthdayDate.day, 11, 30, 00)
//              .add(Duration(days: 0)),
////              DateTime.now().add(Duration(seconds: i*5 + 1)),
//          title: 'День 1',
//          body: _getTextForPush(id: 0)));
//    }
    return temp;
  }

  _getScheduleNotifications({bool monthSchedule = false}) async {
    List<PushNotification> temp = [];
    if (monthSchedule) {
      temp.addAll(
          (await StorageService.readEveryMonthScheduleList(_childId)) ?? []);
      if (temp.length == 0) {
        //for (var i = 0; i < 5; i++) {
        var now = DateTime.now();
        int month = now.month;
        int hour = _notificationTime.hour;
        int minute = _notificationTime.minute;
        int seconds = 0;
        //if (now.month == month) {
        if (now.day > _birthdayDate.day) {
          month = now.month + 1;
        } else if (now.day == _birthdayDate.day) {
          if (now
              .isAfter(DateTime(now.year, now.month, now.day, hour, minute))) {
            hour = now.hour;
            minute = now.minute;
            seconds = now.second + 5;
          }
        }
        //  }

        DateTime date = _getNextMonthBirthday(
            monthNumber: month, year: now.year, hour: hour, minute: minute);
        int monthAmount = _countMonths(date);
        if (!(_notifications.map((n) => n.id).toList().contains(monthAmount))) {
          Map map = await _getMonthlyPushById(id: monthAmount);
          if (map?.isNotEmpty ?? false) {
            temp.add(PushNotification(
                id: monthAmount,
                iconColor: ColorStyle.codGray,
                titleTextColor: ColorStyle.codGray,
                date: date.add(Duration(seconds: seconds)),
//              DateTime.now().add(Duration(minutes: 1)),
                title: 'Поздравляем!',
                category: 'birthday',
                body: map['body'],
                buttonName: map['button_name'],
                buttonPath: map['button_path']));
            // }
          }
        }
      }
    } else {
      temp.addAll(
          (await StorageService.readEveryDayScheduleList(_childId)) ?? []);
      int dayCapacity = temp.length;
      if (dayCapacity < 3) {
        for (var i = 0; i < 7; i++) {
          int day =
              _countDays(DateTime.now().add(Duration(days: dayCapacity + i)));

          ///THERE WE CHECK THAT THIS PUSH NOT AT SCHEDULE LIST
          ////.c.////print(day);
          ////.c.////print(temp.map((f) => f.id).toList());
          ///for check
          /// //print('temo');
          ///  //print(temp.map((f) => f.id).toList());
          ///  //print('notifications');
          ///  //print(_notifications.map((n) => n.id).toList());
          if (temp.map((f) => f.id).toList().contains(day + 100)) continue;
          if (_notifications.map((n) => n.id).toList().contains(day + 100))
            continue;

          ///

          int hour = _notificationTime.hour;
          int minute = _notificationTime.minute;
          int seconds = 0;
          var now = DateTime.now();
          if (i == 0 &&
              now.isAfter(
                  DateTime(now.year, now.month, now.day, hour, minute))) {
            hour = now.hour;
            minute = now.minute;
            seconds = now.second + 5;
          }
          var body = await _getTextForEverydayPush(id: day);
          if (body?.isNotEmpty ?? false) {
            temp.add(PushNotification(
                id: 100 + day,
                iconColor: ColorStyle.codGray,
                titleTextColor: ColorStyle.codGray,
                date: DateTime(
                        now.year, now.month, now.day, hour, minute, seconds)
                    .add(Duration(days: dayCapacity + i)),
//              DateTime.now().add(Duration(seconds: i*5 + 1)),
                title: 'День ${day + 1}',
                body: body));
          }
        }
      }
    }
    return temp;
  }

  int _countDays(DateTime date) {
    return date.difference(_birthdayDate).inDays;
  }

  int _countMonths(DateTime date) {
    if (_birthdayDate.year == date.year) {
      return date.month - _birthdayDate.month;
    } else {
      return ((date.year - _birthdayDate.year) * 12 +
          (date.month - _birthdayDate.month));
    }
  }

  int _countUnread() {
    return _notifications.where((n) => n.notRead).length;
  }

  _checkForNewNotification() {
    //.c.////print('NEW SCHEDULE $_dayScheduleNotifications');
    if ((_dayScheduleNotifications == null ||
            _dayScheduleNotifications.isEmpty) &&
        (_monthScheduleNotifications == null ||
            _monthScheduleNotifications.isEmpty) &&
        (_attentionScheduleNotifications == null ||
            _attentionScheduleNotifications.isEmpty) &&
        (_standardScheduleNotifications == null ||
            _standardScheduleNotifications.isEmpty)) {
      if (firstLoad?.value ?? true) firstLoad.add(false);
      return;
    }

    ///for check
    /// //print(_dayScheduleNotifications.map((d) => d.id).toList());
    var now = DateTime.now();
    List<PushNotification> newPush = List();
    _dayScheduleNotifications?.retainWhere((n) {
      if (n.date.isBefore(now)) {
        newPush.add(n);
        return false;
      } else {
        return true;
      }
    });
    _monthScheduleNotifications?.retainWhere((n) {
      if (n.date.isBefore(now)) {
        newPush.add(n);
        return false;
      } else {
        return true;
      }
    });
    _attentionScheduleNotifications?.retainWhere((n) {
      if (n.date.isBefore(now)) {
        newPush.add(n);
        return false;
      } else {
        return true;
      }
    });
    _standardScheduleNotifications?.retainWhere((n) {
      if (n.date.isBefore(now)) {
        newPush.add(n);
        return false;
      } else {
        return true;
      }
    });
    ////.c.////print('NEW SCHEDULE $_dayScheduleNotifications');
    //.c.////print('NEW PUSHES $newPush');
    if (newPush.isNotEmpty) {
      newPush.sort((a, b) => (a?.date?.millisecondsSinceEpoch ?? 0)
          .compareTo((b?.date?.millisecondsSinceEpoch ?? 0)));
      newPush.forEach((push) => _notifications.insert(0, push));
      unreadAmount.add(_countUnread());
      notifications.add(_notifications);
      StorageService.writePushList(_notifications, _childId);
      StorageService.writeEveryDayScheduleList(
          _dayScheduleNotifications, _childId);
      StorageService.writeEveryMonthScheduleList(
          _monthScheduleNotifications, _childId);
      StorageService.writeAttentionScheduleList(
          _attentionScheduleNotifications, _childId);
      StorageService.writeStandardScheduleList(
          _standardScheduleNotifications, _childId);
    }
    if (firstLoad?.value ?? true) firstLoad.add(false);
  }

  readNotification(PushNotification push) {
//    //.c.////print(DateTime(DateTime.now().year, DateTime.now().month,
//        DateTime.now().day, _notificationTime.hour, _notificationTime.minute));
//    _birthdayDate = DateTime(2018, 1, 31);
//    for (var i = 1; i < 12; i++) {
//      //.c.////print(_getNextMonthBirthday(monthNumber: 12 + i, year: 2019));
//    }
    if (push != null) {
      var index = _notifications.indexOf(push);
      _notifications[index].notRead = false;
      notifications.add(_notifications);
      unreadAmount.add(_countUnread());
      flutterLocalNotificationsPlugin.cancel(_notifications[index].id);
      StorageService.writePushList(_notifications, _childId);
    }
  }

  getUnreadNotifications() {
    return _notifications?.where((f) => f.notRead)?.toList() ?? [];
  }

  addToBookmark(PushNotification push) {
    if (push != null) {
      var index = _notifications.indexOf(push);
      _notifications[index].bookmark = !_notifications[index].bookmark;
      notifications.add(_notifications);
      StorageService.writePushList(_notifications, _childId);
    }
  }

  updateBirthday(DateTime date, DateTime creationDate) {
    _creationDate = creationDate;
    _birthdayDate = date;
    _initForNewDate();
  }

  DateTime _getNextMonthBirthday(
      {int monthNumber = 1, int year = 1970, int hour = 11, int minute = 0}) {
    if (monthNumber > 12) {
      monthNumber = monthNumber - 12;
      year++;
    }
    if (_birthdayDate.day > 28) {
      var dateUtil = DateUtil();
      if (_birthdayDate.day > dateUtil.daysInMonth(monthNumber, year)) {
        return DateTime(year, monthNumber,
            dateUtil.daysInMonth(monthNumber, year), hour, minute);
      } else {
        return DateTime(year, monthNumber, _birthdayDate.day, hour, minute);
      }
    } else {
      return DateTime(year, monthNumber, _birthdayDate.day, hour, minute);
    }
  }

  void _initNotificationChecker() {
    _newPushTimer?.cancel();
    _checkForNewNotification();
    _newPushTimer = Timer.periodic(Duration(seconds: 10), (_) {
      _checkForNewNotification();
    });
  }

  disableNotifications() async {
    _notifications = null;
    _dayScheduleNotifications = null;
    _monthScheduleNotifications = null;
    _standardScheduleNotifications = null;
    _attentionScheduleNotifications = null;
    _birthdayDate = null;
    _creationDate = null;
    notifications.add(_notifications);
    unreadAmount.add(null);
    _newPushTimer?.cancel();
    await StorageService.clearScheduleLists(_childId);
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  deleteById(String id) async {
    await StorageService.clearNotifications(id);
  }

  _generateLostPush() async {
    ///Generate old push for last period which user lost when not join to app
    if (_notifications == null || _notifications.isEmpty) return;
    var everyDaySchedule = _dayScheduleNotifications
        .where((f) => f.id > 99)
        .map((f) => f.id)
        .toList();
    var monthSchedule = _monthScheduleNotifications
        .where((f) => f.id < 100)
        .map((f) => f.id)
        .toList();
    everyDaySchedule ??= [];
    monthSchedule ??= [];
    List<PushNotification> lostPushTemp = [];

    ///Generate everyday push
    ///
    DateTime date;
    int id;
    var firstNoty = _notifications.lastWhere((f) => (f.id > 99 && f.id < 1000),
        orElse: () => null);
    ////print('first noty ${firstNoty.id}');
    if (firstNoty != null) {
      date = firstNoty.date.add(Duration(days: 1));
      id = firstNoty.id + 1;
      while ((DateTime.now().day != date.day ||
              DateTime.now().month != date.month) &&
          DateTime.now().isAfter(date)) {
        if (!everyDaySchedule.contains(id) &&
            !_notifications.map((f) => f.id).contains(id)) {
          var body = await _getTextForEverydayPush(id: id - 100);
          if (body?.isNotEmpty ?? false) {
            lostPushTemp.add(PushNotification(
                id: id,
                title: 'День ${id - 99}',
                body: body,
                iconColor: ColorStyle.codGray,
                titleTextColor: ColorStyle.codGray,
                date: date));
          }
        }
        date = date.add(Duration(days: 1));
        id++;
      }
    }

    ///Generate birthday push
    ///
    firstNoty = _notifications.lastWhere((f) => f.id < 100, orElse: () => null);
    if (firstNoty != null) {
      date = _getNextMonthBirthday(
          monthNumber: firstNoty.date.month + 1,
          year: firstNoty.date.year,
          hour: _notificationTime.hour,
          minute: _notificationTime.minute);
      int monthAmount = _countMonths(date);
      id = firstNoty.id + 1;
      while ((DateTime.now().day != date.day ||
              DateTime.now().month != date.month) &&
          DateTime.now().isAfter(date)) {
        if (!monthSchedule.contains(monthAmount) &&
            !_notifications.map((f) => f.id).contains(monthAmount)) {
          Map map = await _getMonthlyPushById(id: monthAmount);
          if (map?.isNotEmpty ?? false) {
            lostPushTemp.add(PushNotification(
                id: monthAmount,
                iconColor: ColorStyle.codGray,
                titleTextColor: ColorStyle.codGray,
                date: date,
                title: 'Поздравляем!',
                category: 'birthday',
                body: map['body'],
                buttonName: map['button_name'],
                buttonPath: map['button_path']));
          }
        }
        date = _getNextMonthBirthday(
            monthNumber: date.month + 1,
            year: date.year,
            hour: _notificationTime.hour,
            minute: _notificationTime.minute);
        monthAmount = _countMonths(date);
        id++;
      }
    }
    firstNoty = _notifications.lastWhere((f) => (f.id > 999 && f.id < 2000),
        orElse: () => null);
    if (firstNoty != null) {
      date = firstNoty.date.add(Duration(days: 1));
      id = firstNoty.id + 1;
      while ((DateTime.now().day != date.day ||
              DateTime.now().month != date.month) &&
          DateTime.now().isAfter(date)) {
        if ( //!Schedule.contains(id) &&
            !_notifications.map((f) => f.id).contains(id)) {
          Map push = await _getAttentionPushById(id: id - 1000);
          if (push == null || (push?.isEmpty ?? true)) {
            date = date.add(Duration(days: 1));
            id++;
            continue;
          }
          lostPushTemp.add(PushNotification(
            id: id,
            iconColor: ColorStyle.codGray,
            titleTextColor: ColorStyle.codGray,
            date: date,
//              DateTime.now().add(Duration(seconds: i*5 + 1)),
            title: 'Обратите внимание!',
            category: 'attention',
            body: push['body'],
            buttonPath: push['button_path'],
            buttonName: push['button_name'],
          ));
        }
        date = date.add(Duration(days: 1));
        id++;
      }
    }
    if (lostPushTemp.isNotEmpty) {
//      lostPushTemp.sort((a, b) => (a?.date?.millisecondsSinceEpoch ?? 0)
//          .compareTo((b?.date?.millisecondsSinceEpoch ?? 0)));
//      lostPushTemp.forEach((push) => _notifications.insert(0, push));
      _notifications.addAll(lostPushTemp);
      _notifications.sort((a, b) => (a?.date?.millisecondsSinceEpoch ?? 0)
          .compareTo(((b?.date?.millisecondsSinceEpoch ?? 0))));
      _notifications = _notifications.reversed.toList();
      unreadAmount.add(_countUnread());
      notifications.add(_notifications);
      StorageService.writePushList(_notifications, _childId);
    }
  }

  Future<String> _getTextForEverydayPush({int id}) async {
    ///FIREBASE EVERYDAY PUSHES IDs STARTS FROM 1, BUT AT APP IDs STARTS FROM 0
    String body;
    //id = id % (DateUtil().leapYear(DateTime.now().year) ? 366 : 365);
    try {
      body = await new NotificationService().fetchEverydayPush(id + 1);
    } catch (e) {
      //print('ERROR OCCURED WHEN GET EVERYDAY PUSH FOR ID $id');
//      body =
//          '''Многие люди не могут похвастаться очень разнообразным опытом.\nНа их жизненном графике не хватает координат, чтобы, соединив их, построить правильную траекторию движения. \n \nБольшинство не видит проблему во всём её многообразии, поэтому их решения весьма прямолинейны. Чем выше культура человека, позволяющая ему шире использовать опыт человечества, тем лучше его конструкторские решения.''';
    }
    return body;
  }

  Future<Map> _getMonthlyPushById({int id}) async {
    Map body;
    //id = id % 13;
    try {
      body = await new NotificationService().fetchMonthlyPush(id, male: _male);
      if (body['body'] != null) {
        if (body['body'].contains(RegExp(r'<name:.?name>'))) {
          body['body'] =
              body['body'].replaceAll(RegExp(r'<name:.?name>'), _name);
        }
      }
    } catch (e) {
      //print('$e');
      //print('ERROR OCCURED WHEN GET MONTHLY PUSH FOR ID $id');
//      String msg1 =
//          'Поздравляем! Сегодня родил${_male ? 'cя' : 'ась'} ${_name[0].toUpperCase()}${_name.substring(1).toLowerCase()}!\n\nНадеемся наше приложение поможет вам запомнить все о своем ребенке в его 1 год жизни, а также подскажет вам самые необходимые советы.';
//      String msg2 =
//          '${_name[0].toUpperCase()}${_name.substring(1).toLowerCase()} сегодня отмечает $id ${Formatter.monthSuffix(id)}!\n\nА помните как${_male ? 'им' : 'ой'} он${_male ? '' : 'а'} был${_male ? '' : 'а'} когда ${id == 1 ? 'только родил${_male ? 'cя' : 'ась'}' : '${_male ? 'ему' : 'ей'} был всего 1 месяц'}? 😮';
//      body['body'] = id == 0 ? msg1 : msg2;
    }
    return body;
  }

  Future<Map> _getAttentionPushById({int id}) async {
    ///FIREBASE ATTENTION PUSHES IDs STARTS FROM 1, BUT AT APP IDs STARTS FROM 0
    Map body;
    //id = id % (DateUtil().leapYear(DateTime.now().year) ? 366 : 365);
    try {
      body = await new NotificationService().fetchAttentionPush(id + 1);
      //print('attention_id=$id body $body');
      //print(body['body']);
    } catch (e) {
      //print('ERROR OCCURED WHEN GET ATTENTION PUSH FOR ID $id');
    }
    return body;
  }

  Future<Map> _getStandardPushById(int id) async {
    Map body;
    try {
      body = await new NotificationService().fetchStandardPush(id);
      //print('standard_id=$id body $body');
      //print(body['body']);
    } catch (e) {
      //print('ERROR OCCURED WHEN GET STANDARD PUSH FOR ID $id');
    }
    return body;
  }

  Future<List<PushNotification>> _getAttentionNotification() async {
    List<PushNotification> temp = [];
    temp.addAll(
        (await StorageService.readAttentionScheduleList(_childId)) ?? []);
    int dayCapacity = temp.length;
    if (dayCapacity < 3) {
      for (var i = 0; i < 7; i++) {
        int day =
            _countDays(DateTime.now().add(Duration(days: dayCapacity + i)));
        if (temp.map((f) => f.id).toList().contains(day + 1000)) continue;
        if (_notifications.map((n) => n.id).toList().contains(day + 1000))
          continue;

        int hour = _notificationTime.hour;
        int minute = _notificationTime.minute;
        int seconds = 0;
        var now = DateTime.now();
        if (i == 0 &&
            now.isAfter(DateTime(now.year, now.month, now.day, hour, minute))) {
          hour = now.hour;
          minute = now.minute;
          seconds = now.second + 5;
        }

        var push = await _getAttentionPushById(id: day);
        //print('attention $day push $push');
        if (push == null || (push?.isEmpty ?? true)) continue;
        temp.add(PushNotification(
          id: 1000 + day,
          iconColor: ColorStyle.codGray,
          titleTextColor: ColorStyle.codGray,
          date: DateTime(now.year, now.month, now.day, hour, minute, seconds)
              .add(Duration(days: dayCapacity + i)),
//              DateTime.now().add(Duration(seconds: i*5 + 1)),
          title: 'Обратите внимание!',
          category: 'attention',
          body: push['body'],
          buttonPath: push['button_path'],
          buttonName: push['button_name'],
        ));
      }
    }
    return temp;
  }

  Future<List<PushNotification>> _getStandardNotifications() async {
    List<PushNotification> temp = [];
    temp.addAll(
        (await StorageService.readStandardScheduleList(_childId)) ?? []);
    Map pushList = await _getPlanningNotifications();
    if (pushList != null && pushList.isNotEmpty) {
      DateTime now = DateTime.now();
      pushList.removeWhere((k, v) =>
          DateTime(now.year, now.month, now.day, 0, 0)
              .isAfter(Formatter.parseFormattedStringDate(v)));
      //print('pushList: $pushList');
      if (pushList.isNotEmpty) {
        for (var pushKey in pushList.keys) {
          DateTime pushDate =
              Formatter.parseFormattedStringDate(pushList[pushKey]);
          int id = int.tryParse(pushKey.toString().substring(3));

          if (id == null) continue;

          Map push = await _getStandardPushById(id);
          if (push != null && push.isNotEmpty) {
            String title = push['title'];
            if (title == null) continue;

            String body = push['body'];
            if (body == null) continue;

            if (temp.map((f) => f.id).toList().contains(id + 2000)) continue;

            if (_notifications.map((n) => n.id).toList().contains(id + 2000))
              continue;

            int hour = _notificationTime.hour;
            int minute = _notificationTime.minute;
            int seconds = 0;

            var now = DateTime.now();
            if (now.isAfter(DateTime(
                pushDate.year, pushDate.month, pushDate.day, hour, minute))) {
              hour = now.hour;
              minute = now.minute;
              seconds = now.second + 5;
            }

            temp.add(PushNotification(
              id: 2000 + id,
              iconColor: ColorStyle.codGray,
              titleTextColor: ColorStyle.codGray,
              date: DateTime(pushDate.year, pushDate.month, pushDate.day, hour,
                  minute, seconds),
//              DateTime.now().add(Duration(seconds: i*5 + 1)),
              title: title,
              category: 'standard',
              body: body,
              buttonPath: push['button_path'],
              buttonName: push['button_name'],
            ));
          }
        }
      }
    }
    return temp;
  }

  Future<Map> _getPlanningNotifications() async {
    Map planningMap;
    try {
      planningMap = await NotificationService().fetchPlanner();
      //print('planning push $planningMap');
    } catch (e) {
      //print('ERROR OCCURED WHEN GET PLANNER LIST');
    }
    return planningMap;
  }

  changePushTime({int hour = 11, int minute = 00}) {}
}
