import 'package:happy_mama/application.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/entity/child_jaw.dart';
import 'package:happy_mama/entity/doctor.dart';
import 'package:happy_mama/entity/height_entity.dart';
import 'package:happy_mama/entity/user.dart';
import 'package:happy_mama/entity/vaccine.dart';
import 'package:happy_mama/entity/weight_entity.dart';
import 'package:happy_mama/service/firebase/baby/baby_height_service.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/service/firebase/baby/baby_teeth_service.dart';
import 'package:happy_mama/service/firebase/baby/baby_weight_service.dart';
import 'package:happy_mama/service/firebase/facade/auth_facade.dart';
import 'package:happy_mama/service/firebase/photo/photo_service.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/util/cast.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_provider.dart';
import 'notification_bloc.dart';

class ProfileBloc extends BlocBase {
  User parent;
  List<Child> children;
  BehaviorSubject<bool> childrenUpdated = BehaviorSubject.seeded(true);
  int _currentChildIndex;
  BehaviorSubject<Child> child = BehaviorSubject();
  BehaviorSubject<String> skills = BehaviorSubject();
  BehaviorSubject<List<Doctor>> doctors = BehaviorSubject();
  BehaviorSubject<List<Vaccine>> vaccines = BehaviorSubject();
  BehaviorSubject<ChildJaw> childJaw = BehaviorSubject();
  BehaviorSubject<int> teethAmount = BehaviorSubject();
  BehaviorSubject<List<WeightEntity>> weights = BehaviorSubject();
  BehaviorSubject<List<HeightEntity>> heights = BehaviorSubject();
  NotificationBloc notifyBloc = NotificationBloc();
  Map<String, dynamic> _pushSettings;

  Map<String, dynamic> get pushSettings => _pushSettings;

  final _userService = UserService();
  final _teethService = BabyTeethService();
  final _weightService = BabyWeightService();
  final _heightService = BabyHeightService();
  final _babyInfoService = BabyInfoService();
  final _photoService = PhotoService();

  @override
  void dispose() {
    child.close();
    skills.close();
    doctors.close();
    vaccines.close();
    childJaw.close();
    teethAmount.close();
    weights.close();
    heights.close();
    childrenUpdated.close();
  }

  exitFromProfile() async {
    if (child.value == null || !child.value.wasBorn) {
      await notifyBloc.disableNotifications();
    }
    await StorageService.clearStorage();
    AuthFacade facade = AuthFacade();
    drainStreams();
    await facade.clear();
  }

  drainStreams() {
    child.add(null);
    skills.add(null);
    doctors.add(null);
    vaccines.add(null);
    childJaw.add(null);
    teethAmount.add(null);
  }

  Future<void> init(User user, {DateTime mainBabyBirthday}) async {
    parent = user;
    children = await _userService.fetchKids(user);
    //print('ALL CHILDREN: $children');
    List<Future> imageFutures = [];
    children.forEach((child) {
      imageFutures.add(() async {
        //print('IMAGE/ NAME IS ${child.name}');
        if (child.mainImageId != null) {
          //print('IMAGE/ MAIN IMAGE ID FOR ${child.name} IS ${child.mainImageId}');
          var links = await _photoService.getLinksFromDatabase(child.id);
          //print('IMAGE/ LINKS FOR ${child.name}: IS $links');
          var res = await _photoService.getImage(
              child.id, child.mainImageId, links[child.mainImageId]);
          //print('IMAGE/ FOR ${child.name} IS $res');
        }
      }());
    });
    await Future.wait(imageFutures);
    String id = await StorageService.getChildListId(parent.identifier);
    if (id != null) {
      _currentChildIndex = children.indexOf(children
          .firstWhere((child) => child.id == id, orElse: () => children[0]));
    } else {
      _currentChildIndex = null;
    }
    //print('CURRENT CHILD ID IS $_currentChildIndex, FROM STORAGE: $id');
    if (mainBabyBirthday != null) {
      int maybeIndex =
          children.indexWhere((child) => child.birthDate == mainBabyBirthday);
      if (maybeIndex != null) {
        _currentChildIndex = maybeIndex;
        StorageService.saveChildListId(
            parent.identifier, children[_currentChildIndex].id);
      }
    }
    _currentChildIndex ??= (children == null || children.isEmpty) ? null : 0;
    String _childId = children[_currentChildIndex].id;
    bool isPink = await StorageService.getColor(_childId);
    //print("IS PINK FROM STORAGE = $isPink");
    isPink ??= children[_currentChildIndex].gender != 'male';
    if (children[_currentChildIndex].gender == null ||
        children[_currentChildIndex].gender.isEmpty) {
      isPink = false;
    }
    mainThemeIsPink.add(isPink);
    //_children[_currentChildIndex].jaw = await _getTeeth();
    await _initStreams();
    _initNotifications();
    doctors.where((doctors) => doctors != null).listen((doctors) {
      BabyInfoService service = BabyInfoService();
      service.postDoctors(child.value.id, doctors);
    });
    vaccines.where((vaccines) => vaccines != null).listen((vaccines) {
      BabyInfoService service = BabyInfoService();
      service.postVaccines(child.value.id, vaccines);
    });
    childrenUpdated.add(true);
  }

  Future _initStreams() async {
    if (_currentChildIndex != null) {
      parent = await _userService.fetchInfo();
      children = await _userService.fetchKids(parent);
      //print(
          //"Fetched children: ${children.map((child) => child.name).join(", ")}");
      getTeeth().then((some) {
        children[_currentChildIndex].jaw = some;
        teethAmount
            .add(children[_currentChildIndex]?.jaw?.getTeethAmount() ?? 0);
      });
      weights.add([]);
      fetchWeights().then((data) => weights.add(cast(data)));
      heights.add([]);
      fetchHeights().then((data) => heights.add(cast(data)));
      child.add(children[_currentChildIndex]);
      //if (_currentChild.skills != null && _currentChild.skills.isNotEmpty) {
      String res = children[_currentChildIndex].skills.join(', ');
      if (res.isNotEmpty) {
        res = 'Умеет $res';
      }
      skills.add(res);
      //}
      //if (_currentChild.doctors != null && _currentChild.doctors.isNotEmpty) {
      doctors.add(children[_currentChildIndex].doctors);
      //}
      //if (_currentChild.vaccines != null && _currentChild.vaccines.isNotEmpty) {
      vaccines.add(children[_currentChildIndex].vaccines);
      //}
      childJaw.add(children[_currentChildIndex].jaw);

      childJaw.listen((jaw) {
        children[_currentChildIndex].jaw = jaw;
        teethAmount.add(jaw?.getTeethAmount() ?? 0);
      });
      _pushSettings = await StorageService.getPushSettings();
    }
  }

  _initNotifications() {
    if ((child?.value?.birthDate ?? null) != null) {
      //print(_pushSettings);
      //print(_pushSettings['time']);
      notifyBloc.init(
        child.value.id,
        child.value.birthDate,
        name: children[_currentChildIndex].name,
        male: children[_currentChildIndex].gender == 'male',
        creation: parent.creationDate,
        hour: _pushSettings['time'][0],
        minute: _pushSettings['time'][1],
        silent: !_pushSettings['enable'],
        sound: _pushSettings['sound'],
      );
      //notifyBloc.updateBirthday(child.value.birthDate);
    }
  }

  Future getTeeth() async {
    children[_currentChildIndex].jaw =
        await _teethService.getTeeth(children[_currentChildIndex].id);
    childJaw.add(children[_currentChildIndex].jaw);
    teethAmount.add(childJaw?.value?.getTeethAmount() ?? 0);
    return childJaw.value;
  }

  setTeeth() async {
    await _teethService.setTeeth(children[_currentChildIndex].id,
        children[_currentChildIndex].jaw.toJson());
  }

  changeChildInfo(
    String name,
    String gender,
    bool wasBorn,
    DateTime birthDate, {
    int mainImageId,
    bool setMainImageToNull = false,
  }) {
    children[_currentChildIndex].name = name;
    children[_currentChildIndex].gender = gender;
    children[_currentChildIndex].birthDate = birthDate;
    children[_currentChildIndex].skills = [];
    children[_currentChildIndex].mainImageId = mainImageId ??
        (setMainImageToNull ? null : children[_currentChildIndex].mainImageId);
    child.add(children[_currentChildIndex]);
    childJaw.add(children[_currentChildIndex].jaw);
    skills.add('');
    _initNotifications();
    childrenUpdated.add(true);
  }

  updateName(String name) {
    children[_currentChildIndex].name = name;
    child.add(children[_currentChildIndex]);
  }

  changeChild(String id) async {
    _currentChildIndex = (children == null || children.isEmpty)
        ? null
        : children.indexOf(children.firstWhere((f) => f.id == id));
    //_children[_currentChildIndex].jaw = await _getTeeth();
    //print("ALL KIDS: ${children.map((child) => child.name).join(", ")}");
    //print("CURRENT CHILD ID IS $_currentChildIndex");
    await _initStreams();
    await notifyBloc.disableNotifications();
    _initNotifications();
    doctors.where((doctors) => doctors != null).listen((doctors) {
      BabyInfoService service = BabyInfoService();
      service.postDoctors(child.value.id, doctors);
    });
    vaccines.where((vaccines) => vaccines != null).listen((vaccines) {
      BabyInfoService service = BabyInfoService();
      service.postVaccines(child.value.id, vaccines);
    });
    StorageService.saveChildListId(
        parent.identifier, children[_currentChildIndex].id);
    bool isPink = await StorageService.getColor(id);
    //print("IS PINK FROM STORAGE = $isPink");
    isPink ??= children[_currentChildIndex].gender != 'male';
    if (children[_currentChildIndex].gender == null ||
        children[_currentChildIndex].gender.isEmpty) {
      isPink = false;
    }
    mainThemeIsPink.add(isPink);
    childrenUpdated.add(true);
  }

  deleteChild(Child child) async {
    BabyInfoService service = BabyInfoService();
    await service.deleteChild(parent.identifier, child.id);
    children.remove(child);
    await notifyBloc.deleteById(child.id);
    changeChild(children[0].id);
    childrenUpdated.add(true);
  }

  Future fetchWeights() async {
    return await _weightService.fetch(children[_currentChildIndex].id) ?? [];
  }

  loadWeightsToServer() async {
    await _weightService.post(children[_currentChildIndex].id, weights.value);
  }

  Future fetchHeights() async {
    return await _heightService.fetch(children[_currentChildIndex].id) ?? [];
  }

  loadHeightsToServer() async {
    await _heightService.post(children[_currentChildIndex].id, heights.value);
  }

  updatePushSettings(
      {int hour = 11, int minute = 0, bool silent = false, String sound = ''}) {
    notifyBloc.disableNotifications();
    StorageService.setPushSettings(
        enable: !silent, sound: sound, hour: hour, minute: minute);
    _pushSettings = {
      'enable': !silent,
      'time': [hour, minute],
      'sound': sound,
    };
    if ((child?.value?.birthDate ?? null) != null) {
      notifyBloc.init(
        child.value.id,
        child.value.birthDate,
        name: children[_currentChildIndex].name,
        male: children[_currentChildIndex].gender == 'male',
        creation: parent.creationDate,
        hour: hour,
        minute: minute,
        silent: silent,
        sound: sound,
      );
    }
  }

  ///FOR PREMIUM
  ///check does user have premium status
  ///return bool
  bool havePremiumStatus() {
    return false;
  }
}
