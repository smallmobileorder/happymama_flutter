import 'package:date_util/date_util.dart';

DateTime getNextMonthBirthday( DateTime birthday,
    {int monthNumber = 1, int year = 1970,}) {
  if (monthNumber > 12) {
    monthNumber = monthNumber - 12;
    year++;
  }
  if (birthday.day > 28) {
    var dateUtil = DateUtil();
    if (birthday.day > dateUtil.daysInMonth(monthNumber, year)) {
      return DateTime(year, monthNumber,
          dateUtil.daysInMonth(monthNumber, year));
    } else {
      return DateTime(year, monthNumber, birthday.day);
    }
  } else {
    return DateTime(year, monthNumber, birthday.day);
  }
}