import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/widgets/image_viewer.dart';

class LoadingImageWidget extends StatefulWidget {
  final bool enablePhotoView;
  final String url;
  final BoxFit fit;

  const LoadingImageWidget(
      {Key key, this.enablePhotoView = false, @required this.url, this.fit})
      : super(key: key);

  createState() => _LoadingImageWidgetState();
}

class _LoadingImageWidgetState extends State<LoadingImageWidget> {
  Widget image;
  String tag;

  @override
  void initState() {
    super.initState();
    image = networkImage();
    tag = (widget?.url ?? 'default') + Random().nextInt(100000).toString();
  }

  @override
  Widget build(BuildContext context) {
    return (widget?.enablePhotoView ?? false)
        ? Hero(
            tag: tag,
            child: InkWell(
              child: image,
              onTap: openImageViewer,
            ))
        : image;
  }

  networkImage() {
    if (widget?.url?.contains('.svg') ?? false) {
      return SvgPicture(
        AdvancedNetworkSvg(
          '${widget?.url ?? ''}',
          SvgPicture.svgByteDecoder,
          useDiskCache: true,
        ),
        // fit: widget?.fit,
      );
    } else {
      return Image(
        image: AdvancedNetworkImage(
          '${widget?.url ?? ''}',
          useDiskCache: true,
        ),
        // fit: widget?.fit,
      );
    }
  }

  void openImageViewer() {
    Navigator.of(context).push(CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (_) => ImageViewer(
              image: image,
              heroTag: tag,
            )));
  }
}
