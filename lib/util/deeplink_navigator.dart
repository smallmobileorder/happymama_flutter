import 'package:flutter/cupertino.dart';
import 'package:happy_mama/page/baby/evolution_steps.dart';
import 'package:happy_mama/page/baby/motor_skills/motor_skills.page.dart';
import 'package:happy_mama/page/doctors/all_doctors/all_doctors.page.dart';
import 'package:happy_mama/page/height/height_page.dart';
import 'package:happy_mama/page/home.dart';
import 'package:happy_mama/page/jaw/jaw.dart';
import 'package:happy_mama/page/vaccines/all_vaccines/all_vaccines.page.dart';
import 'package:happy_mama/page/weight/weight_page.dart';

/// Example:
///    import from home - global
/// for main: use deeplinkNavigator.parsePathAndNavigate('main/height/')();
/// others: use deeplinkNavigator.parsePathAndNavigate('calendar/1');
///

final List<int> history = [];

class DeeplinkNavigator {
  final List<String> pathHistory = [];

  final _profileSections = {
    //'skills': ,
    'evolution': EvolutionStepsPage(),
    'height': HeightPage(),
    'weight': WeightPage(),
    'doctors': AllDoctorsPage(),
    'vaccines': AllVaccinesPage(),
    'teeth': Jaw(),
    'motorskills': MotorSkillsPage()
    //'foto': ,
    //'profile': ,
  };

  get path {
    String temp = _path ?? '';
    //print('deeplink get path from tab ${tabBarController.index}');
    if (((_path?.contains('knowledge') ?? false) &&
            tabBarController.index == 2) ||
        ((_path?.contains('calendar') ?? false) &&
            tabBarController.index == 3) ||
        ((_path?.contains('main') ?? false) && tabBarController.index == 0)) {
      // //print('get path');
      _path = '';
    }
    return temp;
  }

  get pathNoCleaning {
    String temp = _path ?? '';
    //print('deeplink get path from tab ${tabBarController.index}');
    if (((_path?.contains('knowledge') ?? false) &&
            tabBarController.index == 2) ||
        ((_path?.contains('calendar') ?? false) &&
            tabBarController.index == 3) ||
        ((_path?.contains('main') ?? false) && tabBarController.index == 0)) {
      // //print('get path');
    }
    return temp;
  }

  String _path;

  parsePathAndNavigate(String path, {int prevTab}) {
    if (path == null || path.isEmpty)
      return (context) {
        return;
      };
    if (prevTab != null) {
      history.add(prevTab);
    }
    List temp = (path.trim().split('/'));
    temp.removeWhere((f) => f.isEmpty || f == null);
    //print(temp);
    Function function = () {};
    switch (temp[0]) {
      case 'main':
        //print('go to main page');
        function = () {
          tabBarController.index = 0;
          if (temp.length == 1) return;
          if (_profileSections.containsKey(temp[1])) {
            babyTab.currentState.push(
                CupertinoPageRoute(builder: (_) => _profileSections[temp[1]]));
          } else {
            _path = path;
          }
        };
        break;
      case 'notifications':
        tabBarController.index = 1;
        break;
      case 'knowledge':
        if (knowledgeTab?.currentState?.canPop() ?? false) {
          knowledgeTab.currentState.popUntil((route) => route.isFirst);
        }
//        if (history.contains(2)&&tabBarController.index!=2) {
//          if (knowledgeTab?.currentState?.canPop() ?? false) {
//            knowledgeTab.currentState.popUntil((route) => route.isFirst);
//          }
//        } else if(tabBarController.index==2){
//          knowledgeTab.currentState.pop();
//        }
//        if (tabBarController.index == 2) {
//          //history.removeLast();
//          knowledgeTab.currentState.maybePop();
//        }
        if (temp.length > 1) _path = path;
        tabBarController.index = 2;
        break;
      case 'calendar':
        if (temp.length > 1) _path = path;
        tabBarController.index = 3;
        /*function = (context) async {
          if (temp.length == 1) return;
          var month = int.tryParse(temp[1]);
          if (month == null || month < 0 || month > 12) {
            return;
          }
          */
        /*calendarTab.currentState.push(CupertinoPageRoute(
            builder: (_) => CalendarPage(),
          ));*/
        /*
        };*/
        break;
      default:
        break;
    }
    return function;
  }

  addKnowledgePath(String path) {
    //print('Deeplink. addKnowledgePath:' + path);
    List temp = (path.trim().split('/'));
    if (temp.length < 2) return;
    if (pathHistory.isNotEmpty) {
      if (pathHistory.last.split('/')[1] != temp[1]) {
        pathHistory.add(path.trim());
      } else {
        //print('Deeplink. addKnowledgePath remove');
        pathHistory.removeLast();
      }
    } else {
      pathHistory.add(path.trim());
    }
    //print('Deeplink. addKnowledgePath history: $pathHistory');
  }

  clearKnowledgePaths() {
    //print('Deeplink. clear paths');
    pathHistory.clear();
  }

  setKnowledgePathIfNecessary(int tab) {
    //print('Deeplink. setPath');
    if (tab != 2) return;
    if (pathHistory?.isEmpty ?? true) return;
    if ((pathHistory?.length ?? 0) > 1) {
      //print('Deeplink. setPath remove');
      pathHistory?.removeLast();
    }
    //print('Deeplink. setPath history $pathHistory');
    String temp = pathHistory?.last ?? '';
    pathHistory?.removeLast();
    if (temp.isNotEmpty) _path = temp;
  }

  addArticleIdToKnowledgePath(int id) {
    //print('Deeplink. add to path article id $id');
    if (pathHistory?.isEmpty ?? true) return;
    pathHistory.last = pathHistory.last.split('/')[0] +
        '/' +
        pathHistory.last.split('/')[1] +
        '/$id';
  }
}
