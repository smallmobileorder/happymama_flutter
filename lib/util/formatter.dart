import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class Formatter {
//   static int countIntMonth({
//    @required DateTime curDate,
//    @required DateTime childDate,
//  }) {
//    assert(curDate != null);
//    assert(childDate != null);
//    //assert(curDate.isAfter(childDate));
//    if (childDate.year == curDate.year) {
//      return curDate.month - childDate.month;
//    } else {
//      return (curDate.year - childDate.year) * 12 +
//          (curDate.month - childDate.month);
//    }
//  }

  static String formatDate(DateTime date) {
    initializeDateFormatting('ru_RU', null);
    if (date == null) {
      return '';
    }
    final parseDate = DateFormat.yMd('ru').format(date);
    return parseDate.replaceAll('г.', '');
  }

  static String formatTime(DateTime date) {
    initializeDateFormatting('ru_RU', null);
    if (date == null) {
      return '';
    }
    final time = DateFormat.Hm('ru').format(date);
    return time;
  }

  static DateTime parseFormattedStringDate(String date) {
    initializeDateFormatting('ru_RU', null);
    return DateFormat.yMd('ru').parse(date);
  }

  static String weekSuffix(int count) {
    switch (count) {
      //case 1:
        //return 'неделя';
      //case 2:
      case 3:
        return 'я неделя';
      //case 4:
        //return 'недели';
      default:
        return 'ая неделя';
    }
  }

  static String monthSuffix(int count) {
    switch (count) {
      case 1:
        return 'месяц';
      case 2:
      case 3:
      case 4:
        return 'месяца';
      default:
        return 'месяцев';
    }
  }

  static String doubleToString(double d) {
    return d.toStringAsFixed(d.truncateToDouble() == d ? 0 : 1);
  }
}
