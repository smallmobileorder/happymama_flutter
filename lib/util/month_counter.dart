import 'package:date_util/date_util.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/date_symbol_data_local.dart';

int monthCounter({@required DateTime end, @required DateTime start}) {
  initializeDateFormatting('ru_RU', null);
  if (end == null ||
      start == null ||
      end.millisecondsSinceEpoch < start.millisecondsSinceEpoch) {
    return -1;
  }
  var tmp;
  if (end.year == start.year) {
    tmp = end.month - start.month;
  } else {
    tmp = ((end.year - start.year) * 12 + (end.month - start.month));
  }
  if (end.day < start.day) {
    if (start.day > 28) {
      var dateUtil = DateUtil();
      if (start.day > dateUtil.daysInMonth(end.month, end.year)) {
        if (end.day != dateUtil.daysInMonth(end.month, end.year)) {
          tmp--;
        }
      } else {
        tmp--;
      }
    } else {
      tmp--;
    }
  }
  return tmp;
}
