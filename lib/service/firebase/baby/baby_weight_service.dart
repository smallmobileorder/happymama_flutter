import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/weight_entity.dart';
import 'package:happy_mama/util/cast.dart';

class BabyWeightService {
  Future<void> post(String identifier, List<WeightEntity> weights) async {
    Map<String, dynamic> temp = Map();
    for (var i = 0; i < weights.length; i++) {
      temp['$i'] = {
        'id': weights[i].id,
        'date': weights[i].date,
        'weight': weights[i].weight
      };
    }
    return FirebaseDatabase.instance
        .reference()
        .child('weights/$identifier')
        .set(temp);
  }

  Future<List<WeightEntity>> fetch(String identifier) {
    return FirebaseDatabase.instance
        .reference()
        .child('weights/$identifier')
        .once()
        .then((snapshot) {
      return snapshot;
    }).then((value) => cast<List<dynamic>>(value.value)
            ?.map((elem) => WeightEntity.fromJson(elem))
            ?.where((elem) => elem != null)
            ?.toList()??[]);
  }
}
