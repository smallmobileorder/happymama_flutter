import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/child_jaw.dart';

class BabyTeethService {
  Future<void> setTeeth(String identifier, Map<String, String> mapa) async {
    return FirebaseDatabase.instance
        .reference()
        .child('teeth/$identifier')
        .set(mapa.map((key, value) => MapEntry(key.toString(), value)));
  }

  Future<ChildJaw> getTeeth(String identifier) {
    return FirebaseDatabase.instance
        .reference()
        .child('teeth/$identifier')
        .once()
        .then((snapshot) {
      return snapshot;
    }).then((value) =>
            ChildJaw.fromJson(value.value));
  }
}
