import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/entity/doctor.dart';
import 'package:happy_mama/entity/vaccine.dart';
import 'package:happy_mama/util/cast.dart';
import 'package:happy_mama/util/formatter.dart';

class BabyInfoService {
  Future<Child> fetchInfo(String identifier) {
    return FirebaseDatabase.instance
        .reference()
        .child('babies/$identifier/info')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      ////print('Baby info json: ${json.value}');
      return json;
    }).then((json) => Child.fromJson(identifier, cast(json.value)));
  }

  Future<void> postInfo(
    String identifier,
    String name,
    String gender,
    String date, {
    int mainImageId,
  }) {
    return FirebaseDatabase.instance
        .reference()
        .child('babies/$identifier/info')
        .update({
      'birth_date': date,
      'gender': gender,
      'name': name,
      'mainImageId': mainImageId,
    });
  }

  Future<List<Doctor>> fetchDoctors(String identifier) {
    //print('Fetch doctors with id=$identifier');
    return FirebaseDatabase.instance
        .reference()
        .child('doctors/$identifier')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      ////print('Doctors json: ${json.value}');
      return json;
    }).then((json) => (json.value as List<dynamic>)
            .map((elem) => Doctor.fromJson(elem))
            .where((elem) => elem != null)
            .toList());
  }

  Future<void> postDoctors(String identifier, List<Doctor> doctors) async {
    //print('Post doctors with id=$identifier');
    //TODO fix request to firebase
    await FirebaseDatabase.instance
        .reference()
        .child('doctors/$identifier')
        .remove();
    List<Map<String, String>> jsons =
        doctors.map((doctor) => doctor.toMap()).toList();
    Map<String, Map<String, String>> res = {};
    for (int i = 0; i < jsons.length; i++) {
      res[i.toString()] = jsons[i];
    }
    FirebaseDatabase.instance
        .reference()
        .child('doctors/$identifier')
        .update(res);
  }

  Future<List<Vaccine>> fetchVaccines(String identifier) {
    //print('Fetch vaccines with id=$identifier');
    return FirebaseDatabase.instance
        .reference()
        .child('vaccines/$identifier')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      ////print('Vaccines json: ${json.value}');
      return json;
    }).then((json) => (json.value as List<dynamic>)
            .map((elem) => Vaccine.fromJson(elem))
            .where((elem) => elem != null)
            .toList());
  }

  Future<void> postVaccines(String identifier, List<Vaccine> vaccines) async {
    //print('Post vaccines with id=$identifier');
    //TODO fix request to firebase
    await FirebaseDatabase.instance
        .reference()
        .child('vaccines/$identifier')
        .remove();
    List<Map<String, String>> jsons =
        vaccines.map((vaccines) => vaccines.toMap()).toList();
    Map<String, Map<String, String>> res = {};
    for (int i = 0; i < jsons.length; i++) {
      res[i.toString()] = jsons[i];
    }
    FirebaseDatabase.instance
        .reference()
        .child('vaccines/$identifier')
        .update(res);
  }

  Future<void> postSkills(String identifier, Map<int, DateTime> skills) async {
    var data = skills.map((k, v) => MapEntry(
        k.toString() + '_skill', v == null ? null : Formatter.formatDate(v)));
    //FirebaseStorage.instance.ref().child('').
    await FirebaseDatabase.instance
        .reference()
        .child('babies/$identifier/info/skills')
        .update(data);
  }

  Future<Map<int, DateTime>> fetchSkills(String identifier) async {
    return FirebaseDatabase.instance
        .reference()
        .child('babies/$identifier/info/skills')
        .once()
        .then((json) {
      if (json?.value == null) return {};
      ////print('Skills json: $json');
      var res = json.value as Map<dynamic, dynamic>;
      ////print('Casted skills: $res');
      try {
        return res?.map((k, v) => MapEntry(
            int.parse((k as String).split('_')[0]),
            Formatter.parseFormattedStringDate(v)));
      } catch (e) {
        //print('Error on fetching skills: $e');
        return {};
      }
    });
  }

  Future deleteChild(String parentId, String childId) async {
    Map<String, dynamic> values = {
      'babies/$childId': null,
      'doctors/$childId': null,
      'heights/$childId': null,
      'weights/$childId': null,
      'teeth/$childId': null,
      'photos/$childId': null,
      'vaccines/$childId': null,
      'users/$parentId/$childId': null,
    };
    return FirebaseDatabase.instance.reference().update(values);
  }

  Future changeBirthday(String childId, DateTime birthday) {
    // change info on baby info
    FirebaseDatabase.instance.reference().child('babies/$childId/info').update({
      'birth_date': Formatter.formatDate(birthday),
    });
    // delete all doctors and vaccines
    fetchDoctors(childId).then((doctors) {
      doctors = doctors
          .map((doctor) => doctor.date.isBefore(birthday) ? null : doctor)
          .toList();
      postDoctors(childId, doctors);
    });
    fetchVaccines(childId).then((vaccines) {
      vaccines = vaccines
          .map((vaccine) => vaccine.date.isBefore(birthday) ? null : vaccine)
          .toList();
      postVaccines(childId, vaccines);
    });
    // delete all photos


    // delete all motor skills
    // delete all teeth
    // delete heights and weights
  }
}
