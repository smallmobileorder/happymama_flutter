import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/advice.dart';

class CalendarService {
  Future<List<Advice>> fetchAdvices(int monthNumber) {
    //print('Fetch advices for month $monthNumber');
    return FirebaseDatabase.instance
        .reference()
        .child('calendar/month_$monthNumber')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as List<dynamic>)
        .map((elem) => Advice.fromJson(elem))
        .where((elem) => elem != null)
        .toList());
  }
}