import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/height_entity.dart';
import 'package:happy_mama/entity/weight_entity.dart';
import 'package:happy_mama/service/firebase/baby/baby_height_service.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/service/firebase/baby/baby_teeth_service.dart';
import 'package:happy_mama/service/firebase/baby/baby_weight_service.dart';
import 'package:happy_mama/service/firebase/photo/photo_service.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/util/month_counter.dart';

class ChangeDateService {
  BabyInfoService babyInfoService;
  PhotoService photoService;
  BabyTeethService babyTeethService;
  BabyHeightService babyHeightService;
  BabyWeightService babyWeightService;

  ChangeDateService() {
    babyInfoService = BabyInfoService();
    photoService = PhotoService();
    babyTeethService = BabyTeethService();
    babyHeightService = BabyHeightService();
    babyWeightService = BabyWeightService();
  }

  Future<int> changeDate(
    String id,
    DateTime birthday,
    DateTime oldBirthday,
    int mainImageId,
  ) {
    int newMonth = monthCounter(end: DateTime.now(), start: birthday);
    int oldMonth = monthCounter(end: DateTime.now(), start: oldBirthday);

    // delete all photos
    Future photos = photoService.getLinksFromDatabase(id).then((links) async {
      int diff = newMonth - oldMonth;
      List<int> monthes = links.keys.toList();
      monthes.sort();
      for (int imageMonth in monthes) {
        var link = links[imageMonth];
        int newImageMonth = imageMonth + diff;
        //print(
          //  'IMAGE WAS ON $imageMonth MONTH, BUT NOW IT ON $newImageMonth MONTH');
        if (newImageMonth > 12 || newImageMonth < 0) {
          await photoService.deleteImage(id, imageMonth);
        } else {
          await photoService.deleteFromCache(id, imageMonth);
          await photoService.deleteLinkFromDatabase(id, imageMonth);
          await photoService.saveLinkToDatabase(id, newImageMonth, link);
        }
      }
    });

    int newMainImageId;
    if (mainImageId != null) {
      newMainImageId = mainImageId + (newMonth - oldMonth);
      if (newMainImageId < 0 || newMainImageId > 12) {
        newMainImageId = null;
      }
    }
    //print('IMAGE ID WAS $mainImageId, AND BECAME $newMainImageId');

    // change info on baby info
    Future info =
        FirebaseDatabase.instance.reference().child('babies/$id/info').update({
      'birth_date': Formatter.formatDate(birthday),
      'mainImageId': newMainImageId,
    });

    // delete all doctors and vaccines
    Future doctors = babyInfoService.fetchDoctors(id).then((doctors) {
      doctors =
          doctors.where((doctor) => !doctor.date.isBefore(birthday)).toList();
      babyInfoService.postDoctors(id, doctors);
    }).catchError((e) {
      //print("Error on fetching doctors: $e");
    });

    Future vaccines = babyInfoService.fetchVaccines(id).then((vaccines) {
      vaccines = vaccines
          .where((vaccine) => !vaccine.date.isBefore(birthday))
          .toList();
      babyInfoService.postVaccines(id, vaccines);
    }).catchError((e) {
      //print("Error on fetching vaccines: $e");
    });

    // delete all motor skills
    Future skills = babyInfoService.fetchSkills(id).then((skills) {
      skills = skills.map(
          (k, v) => v.isBefore(birthday) ? MapEntry(k, null) : MapEntry(k, v));
      babyInfoService.postSkills(id, skills);
    });

    // delete all teeth
    Future teeth = babyTeethService.getTeeth(id).then((jaw) {
      Map<String, String> map = jaw.toJson();
      for (String key in map.keys) {
        if (Formatter.parseFormattedStringDate(map[key]).isBefore(birthday)) {
          map[key] = null;
        }
      }
      babyTeethService.setTeeth(id, map);
    });

    // delete heights
    Future heights = babyHeightService.fetch(id).then((heights) {
      heights = heights
          .map((height) =>
              Formatter.parseFormattedStringDate(height.date).isBefore(birthday)
                  ? HeightEntity()
                  : height)
          .toList();
      babyHeightService.post(id, heights);
    });

    // delete weights
    Future weights = babyWeightService.fetch(id).then((weights) {
      weights = weights
          .map((weight) =>
              Formatter.parseFormattedStringDate(weight.date).isBefore(birthday)
                  ? WeightEntity()
                  : weight)
          .toList();
      babyWeightService.post(id, weights);
    });

    return Future.wait([
      info,
      doctors,
      vaccines,
      photos,
      skills,
      teeth,
      heights,
      weights,
    ]).then((_) {
      return newMainImageId;
    });
  }
}
