import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/user.dart';

class BabyRegisterService {

  Future<String> register(final String parentIdentifier, final String birthDate, final String gender, final String name) {
    final reference = FirebaseDatabase
        .instance
        .reference()
        .child('babies')
        .push();

    return
        reference
        .child('info')
        .set({'birth_date': birthDate, 'gender': gender, 'name': name})
        .then((_) => FirebaseDatabase.instance.reference().child('users/$parentIdentifier').update({reference.key: true}))
        .then((_) => reference.key);
  }

}