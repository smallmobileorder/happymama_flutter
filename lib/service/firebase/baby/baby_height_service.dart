import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/height_entity.dart';
import 'package:happy_mama/util/cast.dart';

class BabyHeightService {
  Future<void> post(String identifier, List<HeightEntity> heights) async {
    Map<String, dynamic> temp = Map();
    for (var i = 0; i < heights.length; i++) {
      temp['$i'] = {
        'id': heights[i].id,
        'date': heights[i].date,
        'height': heights[i].height
      };
    }
    return FirebaseDatabase.instance
        .reference()
        .child('heights/$identifier')
        .set(temp);
  }

  Future<List<HeightEntity>> fetch(String identifier) {
    return FirebaseDatabase.instance
        .reference()
        .child('heights/$identifier')
        .once()
        .then((snapshot) {
      return snapshot;
    }).then((value) => cast<List<dynamic>>(value.value)
            ?.map((elem) => HeightEntity.fromJson(elem))
            ?.where((elem) => elem != null)
            ?.toList()??[]);
  }
}
