import 'package:firebase_database/firebase_database.dart';

class NotificationService {
  Future<String> fetchEverydayPush(int id) {
    //print('Fetch everyday push body for day $id');
    return FirebaseDatabase.instance
        .reference()
        .child('notifications/everyday/id_$id')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as Map)['body'].toString());
  }

  Future<Map> fetchMonthlyPush(int id, {bool male = false}) {
    //print('Fetch monthly push body for ${male?'boy':'girl'} month $id ');
    return FirebaseDatabase.instance
        .reference()
        .child('notifications/monthly/${male?'boy':'girl'}/id_$id')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as Map));
  }

  Future<Map> fetchAttentionPush(int id) {
    //print('Fetch attention push body for day $id');
    return FirebaseDatabase.instance
        .reference()
        .child('notifications/attention/id_$id')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as Map));
  }

  Future<Map> fetchStandardPush(int id) {
    //print('Fetch standard push by id $id');
    return FirebaseDatabase.instance
        .reference()
        .child('notifications/standard/id_$id')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as Map));
  }

  Future<Map> fetchPlanner() {
    //print('Fetch standard push map');
    return FirebaseDatabase.instance
        .reference()
        .child('notifications/push_planner')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
      // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as Map));
  }
}
