import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';

class PhotoService {
  /// Name of photo in storage: $dir/[month]/[link]
  /// So we can get image from db by link and delete it by month
  /// May return NULL if file is not exists and [create] = false
  /// If [create] is true, then clear dir before saving
  Future<File> getFile(String childId, int month, String link,
      {bool create = false}) async {
    // clear dir first
    Directory dir = await getDirectory(childId, month);
    if (dir.existsSync() &&
        dir.listSync(recursive: true).isNotEmpty &&
        create) {
      dir.deleteSync(recursive: true);
    }
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/$childId/$month/'
        '${getPhotoNameInCache(link)}.txt');
    if (!file.existsSync()) {
      if (create) {
        file.createSync(recursive: true);
      } else {
        return null;
      }
    }
    return file;
  }

  Future<File> getFileWithoutLink(String childId, int month) async {
    // clear dir first
    Directory dir = await getDirectory(childId, month);
    var filePath = dir.listSync()[0]?.path;
    final file = File('$filePath');
    if (!file.existsSync()) {
      return null;
    }
    return file;
  }

  Future<Directory> getDirectory(String childId, int month) async {
    final directory = await getApplicationDocumentsDirectory();
    final dir = Directory('${directory.path}/$childId/$month/');
    return dir;
  }

  String getPhotoName(int month) {
    return '${month}_';
  }

  String getPhotoNameInCache(String link) {
    return link
        .split('/')
        .last
        .replaceAll(RegExp(r'''[%&-+*^$#@!?=.,()\[\]:;"'`\/]'''), '_');
  }

  Future<void> saveImage(String id, int month, Uint8List image) async {
    ////print("SAVING IMAGE FOR $month MONTH: $image");
    // save to store
    String link = await saveToStore(id, month, image);
    // save to cache
    await saveToCache(id, link, month, image);
    ////print(link);
    // save to database
    await saveLinkToDatabase(id, month, link);
  }

  Future<Uint8List> getImage(String id, int month, String link) async {
    ////print("GETTING IMAGE FOR $link");
    if (link == null) {
      return null;
    }
    // get from cache
    Uint8List data = await getFromCache(id, month, link);
    if (data == null) {
      ////print("GET IMAGE FOR $link FROM STORE!!!");
      // get from store
      data = await getFromStore(id, link);
      if (data != null) {
        await saveToCache(id, link, month, data);
      }
    }
    ////print("FINALLY GOT IMAGE FOR $link; BYTES = $data");
    return data;
  }

  Future<Uint8List> getImageByMonth(String id, int month) async {
    try {
      File file = await getFileWithoutLink(id, month);
      Uint8List data = file?.readAsBytesSync();
      return data;
    } catch (e) {
      //print('CAN NOT READ FILE FOR  MOTH $month without link');
      //print(e);
      return null;
    }
    /*String link = await FirebaseDatabase.instance
        .reference()
        .child('photos/$id/${month}_')
        .once()
        .then((json) {
      if (json == null || json.value == null) {
        return null;
      }
      var link = (json.value as String);
      return link;
    });
    if (link == null) {
      return Uint8List.fromList([]);
    }
    Uint8List data = await getFromCache(id, month, link);
    if (data == null) {
      //print("GET IMAGE FOR $link FROM STORE!!!");
      // get from store
      data = await getFromStore(id, link);
      if (data != null) {
        await saveToCache(id, link, month, data);
      }
    }
    return data;*/
  }

  Future<void> deleteImage(String id, int month) async {
    //print('deleting image for $month month');
    // delete from cache
    await deleteFromCache(id, month);
    // delete from store
    String link = await FirebaseDatabase.instance
        .reference()
        .child('photos/$id/${month}_')
        .once()
        .then((snapshot) {
      if (snapshot?.value != null) {
        return snapshot.value as String;
      }
      return null;
    });
    if (link != null) {
      await deleteFromStore(id, link);
    } else {
      //print("ERROR! LINK FROM DATABASE FOR MONTH $month IS NULL");
    }
    // delete from database
    await deleteLinkFromDatabase(id, month);
  }

  Future<void> saveToCache(
      String id, String link, int month, Uint8List data) async {
    //print("SAVING LINK FOR $month MONTH: $link");
    /*Database db = await getDb();
    await StoreRef.main().record('$id/$link').put(db, data.toList());*/
    try {
      File file = await getFile(id, month, link, create: true);
      file.writeAsBytesSync(data);
    } catch (e) {
      //print('CAN NOT SAVE FILE FOR  MOTH $month LINK "$link"');
      //print(e);
    }
  }

  Future<Uint8List> getFromCache(String id, int month, String link) async {
    /* Database db = await getDb();
    var list = await StoreRef.main().record('$id/$link').get(db);
    if (list != null) {
      list = (list as List<dynamic>).map((elem) => elem as int).toList();
      return Uint8List.fromList(list);
    }
    return null;*/
    try {
      File file = await getFile(id, month, link);
      Uint8List data = file?.readAsBytesSync();
      return data;
    } catch (e) {
      //print('CAN NOT READ FILE FOR  MOTH $month "$link"');
      //print(e);
      return null;
    }
  }

  Future<void> deleteFromCache(String id, int month) async {
    /*Database db = await getDb();
    await StoreRef.main().record('$id/$month').delete(db);*/
    try {
      Directory dir = await getDirectory(id, month);
      dir.deleteSync(recursive: true);
    } catch (e) {
      //print('CAN NOT DELETE FILE FOR MOTH $month LINK');
      //print(e);
    }
  }

  Future<String> saveToStore(String id, int month, Uint8List data) async {
    StorageTaskSnapshot snapshot = await FirebaseStorage.instance
        .ref()
        .child('photos/$id/${getPhotoName(month)}')
        .putData(data)
        .onComplete;
    String link = await snapshot.ref.getDownloadURL();
    return link;
    ////print("Image uploaded with link: $link");
  }

  Future<Uint8List> getFromStore(String id, String link) async {
    /*String url;
    try {
      var downloadUrl = await FirebaseStorage.instance
          .ref()
          .child('photos/$id/${getPhotoName(month)}')
          .getDownloadURL();
      ////print('Download URL: $downloadUrl}');
      url = downloadUrl.toString();
    } catch (e) {
      //print('Exception on downloading image for $month month: $e');
      return null;
    }*/
    Client client = Client();
    var request = await client.get(Uri.parse(link));
    return request.bodyBytes;
  }

  Future<void> deleteFromStore(String id, String link) async {
    StorageReference ref =
        await FirebaseStorage.instance.getReferenceFromUrl(link);
    await ref.delete();
    /*await FirebaseStorage.instance
        .ref()
        .child('photos/$id/${getPhotoName(month)}')
        .delete();*/
  }

  Future<void> saveLinkToDatabase(String id, int month, String link) async {
    await FirebaseDatabase.instance
        .reference()
        .child('photos/$id')
        .update({getPhotoName(month): link});
  }

  Future<Map<int, String>> getLinksFromDatabase(String id) async {
    return FirebaseDatabase.instance
        .reference()
        .child('photos/$id')
        .once()
        .then((json) {
      if (json == null || json.value == null) {
        return {};
      }
      var links = (json.value as Map).cast<String, String>();
      return links.map(
          (k, v) => MapEntry<int, String>(int.parse(k.replaceAll('_', '')), v));
    });
  }

  Future<void> deleteLinkFromDatabase(String id, int month) async {
    await FirebaseDatabase.instance
        .reference()
        .child('photos/$id')
        .update({getPhotoName(month): null});
  }
}
