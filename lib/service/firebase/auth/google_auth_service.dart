import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleAuthService {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<AuthResult> signIn() {
    return _googleSignIn.signIn().then((user) {
      if (user == null) {
        throw StateError('User declined authorization');
      }
      return user.authentication;
    }).then((authentication) {
      return GoogleAuthProvider.getCredential(
        accessToken: authentication.accessToken,
        idToken: authentication.idToken,
      );
    })
        // .catchError((e) => //print('GOOGLE ERROR $e'))
        .then((credential) {
      return _auth.signInWithCredential(credential);
    });
  }

  Future<void> logout() {
    return _googleSignIn.signOut();
  }
}
