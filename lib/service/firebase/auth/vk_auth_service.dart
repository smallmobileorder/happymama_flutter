import 'package:flutter/services.dart';
import 'package:flutter_vk_sdk/flutter_vk_sdk.dart';

class VkAuthService {

  Future<String> signIn() async {
    String email;
    PlatformException error;
    await FlutterVKSdk.init(appId: '7160949', apiVersion: '5.90');
    await FlutterVKSdk.login(scope: 'email', onSuccess: (some) => email = some.email, onError: (error) => error = error);
    //print('result');
    if (email != null) {
      //print('email is $email');
      return email;
    } else if (error != null) {
      //print('error is ${error?.message}');
      throw error;
    } else {
      throw StateError('Непредвиденная ошибка');
    }
  }

  Future<void> logout() {
    return FlutterVKSdk.logout();
  }

}