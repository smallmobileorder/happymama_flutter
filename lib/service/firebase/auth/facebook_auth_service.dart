import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class FacebookAuthService {

  final FacebookLogin _facebookLogin = FacebookLogin();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<AuthResult> signIn() {
    return
    _facebookLogin
        .logIn(['email'])
        .then((result) {
          if (result.status == FacebookLoginStatus.loggedIn) {
            return FacebookAuthProvider.getCredential(accessToken: result.accessToken.token);
          } else {
            throw StateError('User declined authorization');
          }
        })
        .then((credential) => _auth.signInWithCredential(credential));
  }

  Future<void> logout() {
    return _facebookLogin.logOut();
  }

}