import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AppleAuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<AuthResult> signIn() {
    //print('sign');
    return AppleSignIn.performRequests([
      AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
    ]).then((result) {
      //print('hello');
      if (result.status == AuthorizationStatus.authorized) {
        //print('auth');
        return OAuthProvider(providerId: "apple.com").getCredential(
          idToken:
          String.fromCharCodes(result.credential.identityToken),
          accessToken:
          String.fromCharCodes(result.credential.authorizationCode),
        );
      } else {
        //print(result.status.toString());
        //print(result.error.toString());
        //print('there');
        throw StateError('User declined authorization');
      }
    }).then((credential) => _auth.signInWithCredential(credential));
  }

//  Future<void> logout() {
//  }
}
