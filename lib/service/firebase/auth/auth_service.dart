import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:happy_mama/service/firebase/auth/apple_auth_service.dart';
import 'package:happy_mama/service/firebase/auth/auth_error.dart';
import 'package:happy_mama/service/firebase/auth/email_auth_service.dart';
import 'package:happy_mama/service/firebase/auth/facebook_auth_service.dart';
import 'package:happy_mama/service/firebase/auth/google_auth_service.dart';
import 'package:happy_mama/service/firebase/auth/vk_auth_service.dart';
import 'package:happy_mama/util/cast.dart';

class AuthService {

  final _auth = FirebaseAuth.instance;
  final _emailAuthService = EmailAuthService();
  final _googleAuthService = GoogleAuthService();
  final _facebookAuthService = FacebookAuthService();
  final _vkAuthService = VkAuthService();
  final _appleAuthService = AppleAuthService();

  Future<bool> get isAuthorized => _auth.currentUser().then((user) => user != null);

  Future<FirebaseUser> signIn({@required String email, @required String password}) {
    final response = _emailAuthService.signIn(email: email, password: password);
    return _handleAuth(response);
  }

  Future<FirebaseUser> register({@required String email, @required String password}) {
    final response = _emailAuthService.register(email: email, password: password);
    return _handleAuth(response);
  }

  Future<void> resetPassword({@required String email}) {
    return _auth.sendPasswordResetEmail(email: email);
  }

  Future<FirebaseUser> googleSignIn() {
    final response = _googleAuthService.signIn();
    return _handleAuth(response);
  }

  Future<FirebaseUser> facebookSignIn() {
    final response = _facebookAuthService.signIn();
    return _handleAuth(response);
  }

  Future<FirebaseUser> appleSignIn() {
    try {
      final response = _appleAuthService.signIn();
      return _handleAuth(response);
    }catch(e){
      print(e);
    }

  }

  Future<FirebaseUser> vkSignIn() {
    final email = _vkAuthService.signIn();
    final response = email
        .then(
            (email) =>
                _emailAuthService.register(email: 'vk$email', password: 'vk$email')
                .catchError((error) {
                  final exception = cast<PlatformException>(error);
                  if (exception != null) {
                    switch (exception.code) {
                      case 'ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL':
                      case 'ERROR_EMAIL_ALREADY_IN_USE':
                        return _emailAuthService.signIn(email: 'vk$email', password: 'vk$email');

                      default: throw exception;
                    }
                  }
                  throw error;
                })
        );
    return _handleAuth(response);
  }

  Future<void> logout() {
    return
      Future.wait([
        _auth.signOut(),
        _googleAuthService.logout(),
        _facebookAuthService.logout(),
        _vkAuthService.logout()
      ]);
  }

  Future<FirebaseUser> _handleAuth(Future<AuthResult> future) {
    return
      future
        .then((result) => result.user)
        .catchError((error) {
          final exception = cast<PlatformException>(error);
          if (exception != null) {
            switch (exception.code) {
              case 'ERROR_USER_DISABLED':
                throw AuthError('Пользователь недоступен');

              case 'ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL':
              case 'ERROR_EMAIL_ALREADY_IN_USE':
                throw AuthError('Пользователь с таким логином уже существует');

              case 'ERROR_INVALID_EMAIL':
                throw AuthError('Неверный e-mail');

              case 'ERROR_USER_NOT_FOUND':
              case 'ERROR_WRONG_PASSWORD':
                throw AuthError('Неверный логин или пароль');

              case 'ERROR_WEAK_PASSWORD':
                throw AuthError('Ненадежный пароль');

              default:
                //print('ERROR CODE IS ${exception.code}');
                throw AuthError('Повторите позже');
            }
          }

          throw error;
        });
  }

}