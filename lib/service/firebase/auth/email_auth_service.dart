import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

class EmailAuthService {

  final _auth = FirebaseAuth.instance;

  Future<AuthResult> signIn({@required String email, @required String password}) {
    return _auth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<AuthResult> register({@required String email, @required String password}) {
    return _auth.createUserWithEmailAndPassword(email: email, password: password);
  }

}