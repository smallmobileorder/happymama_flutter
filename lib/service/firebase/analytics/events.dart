import 'package:flutter/foundation.dart';

class AnalyticEvent {
  final String name;
  final Map<String, dynamic> parameters;

  AnalyticEvent(this.name, this.parameters);

  factory AnalyticEvent.createBookmarkEvent(
      {@required String section,
      @required String category,
      @required String objectTitle}) {
    return AnalyticEvent('add_bookmark', {
      'section': section,
      'category': category,
      'object_title': objectTitle,
    });
  }

  factory AnalyticEvent.createShareEvent(
      {@required String section, @required String objectTitle}) {
    return AnalyticEvent('share_content', {
      'section': section,
      'object_title': objectTitle,
    });
  }

  factory AnalyticEvent.createStartPurchaseEvent({@required String section}) {
    return AnalyticEvent('start_purchase', {
      'section': section,
    });
  }

  factory AnalyticEvent.addPhotoEvent({@required int month}) {
    return AnalyticEvent('add_photo', {
      'age': month,
    });
  }

  factory AnalyticEvent.changePhotoEvent({@required int month}) {
    return AnalyticEvent('change_photo', {
      'age': month,
    });
  }

  factory AnalyticEvent.deletePhotoEvent({@required int month}) {
    return AnalyticEvent('delete_photo', {
      'age': month,
    });
  }

  factory AnalyticEvent.setMainPhotoEvent({@required int month}) {
    return AnalyticEvent('set_main_photo', {
      'age': month,
    });
  }

  factory AnalyticEvent.openCreateCollageDialogEvent({@required int month}) {
    return AnalyticEvent('open_create_collage_dialog', {
      'age': month,
    });
  }

  factory AnalyticEvent.generateCollageEvent({@required String color}) {
    return AnalyticEvent('generate_collage', {'color': color});
  }

  factory AnalyticEvent.shareCollageEvent({@required bool havePremium}) {
    return AnalyticEvent('share_collage', {'premium': havePremium});
  }

}
