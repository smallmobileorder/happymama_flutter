import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';

class KnowledgeService {
  Future<List<KnowledgeSectionEntity>> fetchSections() {
    //print('Fetch knowledge sections');
    return FirebaseDatabase.instance
        .reference()
        .child('knowledge')
        .once()
        .then((json) {
      if (json?.value == null) throw StateError('Wrong JSON');
     // //print('knowledge section json: ${json.value}');
      return json;
    }).then((json) => (json.value as List<dynamic>)
        .map((elem) => KnowledgeSectionEntity.fromJson(elem))
        .where((elem) => elem != null)
        .toList());
  }

}