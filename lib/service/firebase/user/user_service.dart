import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/entity/user.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/util/cast.dart';
import 'package:happy_mama/util/formatter.dart';

class UserService {
  final _babyInfoService = BabyInfoService();
  final _auth = FirebaseAuth.instance;

  Future<FirebaseUser> get user => _auth.currentUser().then((firebaseUser) {
        if (firebaseUser == null) throw StateError('Cant receive user');
        return firebaseUser;
      });

  Future<User> fetchInfo() {
    return user.then((firebaseUser) async {
      final userRecord = await FirebaseDatabase.instance
          .reference()
          .child('users/${firebaseUser.uid}')
          .once();

      final Map<dynamic, dynamic> kidsMap = cast(userRecord.value);

      final kids =
          kidsMap?.keys?.map((some) => cast<String>(some))?.toList() ?? [];

      return User(
          identifier: firebaseUser.uid,
          email: firebaseUser.email,
          kids: kids,
          creationDate: firebaseUser.metadata.creationTime);
    });
  }

  Future<List<Child>> fetchKids(final User parent) async {
    try {
      final futures = parent.kids.map(
          (childIdentifier) => _babyInfoService.fetchInfo(childIdentifier));
      final children = await Future.wait(futures);

      final now = DateTime.now();

      children.sort((left, right) =>
          left.birthDate?.compareTo(right.birthDate ?? now) ?? 1);

      return children.toList();
    } catch (e) {
      return [];
    }
  }

  Future postFeedback(String id, String email, String text) async {
    return FirebaseDatabase.instance
        .reference()
        .child('feedback/$id')
        .push()
        .set({
      'email': email,
      'text': text,
      'date': Formatter.formatDate(DateTime.now()),
    });
  }

  Future rateApp(String id, int rate, {String comment}) async {
    var data = {
      'rate': rate,
      'date': Formatter.formatDate(DateTime.now()),
    };
    if (comment != null && comment.isNotEmpty) {
      data['comment'] = comment;
    }
    return FirebaseDatabase.instance
        .reference()
        .child('rates/$id')
        .update(data);
  }

  Future<bool> checkIfRated(String id) async {
    return FirebaseDatabase.instance
        .reference()
        .child('rates/$id')
        .once()
        .then((snapshot) => snapshot.value != null);
  }
}
