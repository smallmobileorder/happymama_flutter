import 'package:happy_mama/service/firebase/auth/auth_service.dart';

class AuthFacade {
  
  final _authService = AuthService();

  Future<void> clear() {
    return _authService.logout();
  }
  
}