import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:happy_mama/entity/push_notification.dart';
import 'package:happy_mama/entity/user.dart';
import 'package:happy_mama/util/cast.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';

const String PUSH_KEY = 'push';
const String EVERYDAY_KEY = 'everyday_push';
const String EVERYMONTH_KEY = 'everymonth_push';
const String ATTENTION_KEY = 'attention_push';
const String STANDARD_KEY = 'standard_push';
const String USER_KEY = 'user';
const String PHOTO_KEY = 'photos';
const String KNOWLEDGE_BOOKMARKS = 'knowledge_bookmarks';
const String NOTIFICATION_DB = 'notify.db';
const String PREMIUM_KEY = 'premium';

class StorageService {
  static Future<DateTime> getLastRateTime() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo.openDatabase(dir + '/last_rate_time');
    var store = StoreRef.main();
    String time = await store.record('last_rate_time').get(db);
    await db.close();
    if (time == null) {
      return null;
    }
    int parsed = int.tryParse(time);
    if (parsed == null) {
      return null;
    }
    return DateTime.fromMillisecondsSinceEpoch(parsed);
  }

  static Future saveLastRateTime() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo.openDatabase(dir + '/last_rate_time');
    var store = StoreRef.main();
    DateTime now = DateTime.now();
    //print('SAVING ${now.toIso8601String()}; ${now.millisecondsSinceEpoch}');
    await store
        .record('last_rate_time')
        .put(db, now.millisecondsSinceEpoch.toString());
    await db.close();
  }

  static Future<DateTime> getLastMarketRateTime() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo.openDatabase(dir + '/last_rate_time');
    var store = StoreRef.main();
    String time = await store.record('first_entrance_time').get(db);
    await db.close();
    if (time == null) {
      return null;
    }
    int parsed = int.tryParse(time);
    if (parsed == null) {
      return null;
    }
    return DateTime.fromMillisecondsSinceEpoch(parsed);
  }

  static Future setLastMarketRateTime() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo.openDatabase(dir + '/last_rate_time');
    var store = StoreRef.main();
    await store
        .record('first_entrance_time')
        .put(db, DateTime.now().millisecondsSinceEpoch.toString());
    await db.close();
  }

  static Future<bool> isFirstEntrance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool res = prefs.getBool('flag');
    return res == null;
  }

  static Future<void> setFirstEntrance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('flag', false);
  }

  static Future saveChildListId(String parentId, String childId) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo.openDatabase(dir + '/main_child_id');
    var store = StoreRef.main();
    await store.record(parentId).put(db, childId);
    await db.close();
  }

  static Future<String> getChildListId(String parentId) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo.openDatabase(dir + '/main_child_id');
    var store = StoreRef.main();
    var res = await store.record(parentId).cast<String, String>().get(db);
    await db.close();
    return res;
  }

  static Future saveColor(String id, bool isBlue) async {
    //print('SAVING COLOR $isBlue with id=$id');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('$id/color', isBlue);
  }

  static Future<bool> getColor(String id) async {
    //print('GETTING COLOR with id=$id');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('$id/color');
  }

  static Future _writePushList(
      List<PushNotification> items, String key, String childId) async {
    //SharedPreferences storage = await SharedPreferences.getInstance();
    List<String> jsonPush = List();
    for (var param in items) {
      jsonPush.add(jsonEncode(param.toJson()));
    }

    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo
        .openDatabase(dir + '/' + childId + '_' + NOTIFICATION_DB);
    var store = StoreRef.main();
    //await storage.setStringList(key, jsonPush);
    await store.record(key).put(db, jsonPush);
    await db.close();
  }

  static Future<List<PushNotification>> _readPushListFromStorage(
      String key, String childId) async {
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('child id $childId');
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo
        .openDatabase(dir + '/' + childId + '_' + NOTIFICATION_DB);
    var store = StoreRef.main();
    List<PushNotification> items = List<PushNotification>();
    var jsonList =
        cast(await store.record(key).get(db)); //prefs.getStringList(key);
    if (jsonList == null) {
      return items;
    }
    for (var param in jsonList) {
      items.add(PushNotification.fromJson(jsonDecode(param)));
    }
    await db.close();
    return items;
  }

  static writeEveryDayScheduleList(
      List<PushNotification> list, String childId) async {
    await _writePushList(list, EVERYDAY_KEY, childId);
  }

  static writeEveryMonthScheduleList(
      List<PushNotification> list, String childId) async {
    await _writePushList(list, EVERYMONTH_KEY, childId);
  }

  static writeAttentionScheduleList(
      List<PushNotification> list, String childId) async {
    await _writePushList(list, ATTENTION_KEY, childId);
  }

  static writeStandardScheduleList(
      List<PushNotification> list, String childId) async {
    await _writePushList(list, STANDARD_KEY, childId);
  }

  static writePushList(List<PushNotification> list, String childId) async {
    await _writePushList(list, PUSH_KEY, childId);
  }

  static writeUserInfo(User user) async {
    final storage = FlutterSecureStorage();
    return await storage.write(key: USER_KEY, value: jsonEncode(user.toJson()));
  }

  static writeKnowledgeBookmarks(Map<int, List<int>> bkmrks) async {
    final storage = FlutterSecureStorage();
    Map<String, List<int>> json = {};
    bkmrks.forEach((k, v) => json.putIfAbsent('$k', () => v));
    ////print('json $json');
    return await storage.write(
        key: KNOWLEDGE_BOOKMARKS, value: jsonEncode(json));
  }

  static Future<List<PushNotification>> readEveryDayScheduleList(
      String childId) async {
    return await _readPushListFromStorage(EVERYDAY_KEY, childId) ?? [];
  }

  static Future<List<PushNotification>> readEveryMonthScheduleList(
      String childId) async {
    return await _readPushListFromStorage(EVERYMONTH_KEY, childId) ?? [];
  }

  static Future<List<PushNotification>> readAttentionScheduleList(
      String childId) async {
    return await _readPushListFromStorage(ATTENTION_KEY, childId) ?? [];
  }

  static Future<List<PushNotification>> readStandardScheduleList(
      String childId) async {
    return await _readPushListFromStorage(STANDARD_KEY, childId) ?? [];
  }

  static Future<List<PushNotification>> readPushList(String childId) async {
    return await _readPushListFromStorage(PUSH_KEY, childId) ?? [];
  }

  static Future<User> readUserInfo() async {
    final storage = FlutterSecureStorage();
    return User.fromJson(jsonDecode(await storage.read(key: USER_KEY)));
  }

  static Future<Map<int, List<int>>> readKnowledgeBookmarks() async {
    final storage = FlutterSecureStorage();
    Map<int, List<int>> map = {};
    try {
      Map temp = jsonDecode(await storage.read(key: KNOWLEDGE_BOOKMARKS));
      ////print('temp $temp');
      temp.forEach((k, v) => map.putIfAbsent(int.parse(k),
          () => (v as List).map((f) => cast(f)).toList().cast<int>()));
      ////print('map $map');
    } catch (e) {
      //print('ERROR OCCURED WHEN TRY TO LOAD BOOKMARKS $e');
    }
    return map;
  }

  static Future clearStorage() async {
    final storage = FlutterSecureStorage();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    storage.deleteAll();
    bool flag = prefs.getBool('flag');
    await prefs.clear();
    if (flag != null) {
      await prefs.setBool('flag', flag);
    }
  }

  static Future clearNotifications(String childId) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    await databaseFactoryIo
        .deleteDatabase(dir + '/' + childId + '_' + NOTIFICATION_DB);
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.clear();
  }

  static clearScheduleLists(String childId) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    Database db = await databaseFactoryIo
        .openDatabase(dir + '/' + childId + '_' + NOTIFICATION_DB);
    var store = StoreRef.main();
    await store.records(
        [EVERYDAY_KEY, EVERYMONTH_KEY, ATTENTION_KEY, STANDARD_KEY]).delete(db);
    await db.close();
  }

  static Future<Map<String, dynamic>> getPushSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> set = prefs.getStringList('push_settings');
    if (set != null && set.isNotEmpty) {
      return {
        'enable': set[0] == 'true',
        'time': set[1].split(':').map((f) => int.parse(f)).toList(),
        'sound': set[2],
      };
    }
    return {
      'enable': true,
      'time': [11, 00],
      'sound': '',
    };
  }

  static Future setPushSettings(
      {bool enable, int hour = 11, int minute = 00, String sound = ''}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(
        'push_settings', [enable.toString(), '$hour:$minute', sound]);
  }

  static Future<bool> getPremiumStatus() async {
    final storage = FlutterSecureStorage();
    return (await storage.read(key: PREMIUM_KEY) ?? 'false') == 'true';
  }

  static Future setPremiumStatus(bool premium) async {
    final storage = FlutterSecureStorage();
    return await storage.write(key: PREMIUM_KEY, value: premium.toString());
  }
}
