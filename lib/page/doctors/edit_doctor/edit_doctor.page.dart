import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/page/doctors/doctor_names.dart';

import 'package:happy_mama/entity/doctor.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/event_date_builder.dart';
import 'package:happy_mama/widgets/event_name_selector.dart';

class EditDoctorPage extends StatefulWidget {
  final Doctor doctor;

  const EditDoctorPage({this.doctor});

  @override
  State<StatefulWidget> createState() => _EditDoctorPageState();
}

class _EditDoctorPageState extends State<EditDoctorPage> {
  Doctor doctor;
  ProfileBloc bloc;
  final TextEditingController commentCon = TextEditingController();
  final TextEditingController nameCon = TextEditingController();
  final String ownDoctorName = 'Добавить врача или обследование вручную';
  String title;
  String doctorNameButtonValue;

  OutlineInputBorder border;
  bool buttonSaveEnabled;
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    title = widget.doctor == null ? 'Добавить врача' : widget.doctor.name;
    doctor = widget.doctor ?? Doctor(name: null, date: null);
    if (doctor.name != null) {
      buttonSaveEnabled = true;
      if (doctorNames.contains(doctor.name)) {
        doctorNameButtonValue = doctor.name;
      } else {
        nameCon.text = doctor.name;
        doctorNameButtonValue = ownDoctorName;
      }
    } else {
      buttonSaveEnabled = false;
      doctorNameButtonValue = '';
    }
    // doctorNameButtonValue = widget.ownDoctorName;
    commentCon.text = doctor.comment;
    commentCon.addListener(() {
      scrollController.jumpTo(0);
    });

    /// Init border
    border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(
        width: 2,
        color: ColorStyle.tint, //Theme.of(context).primaryColor,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    nameCon.dispose();
    commentCon.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              controller: scrollController,
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(24, 16, 24, 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EventDateBuilder(doctor.date, onTap: selectDate),
                    Container(height: 16),

                    /// Button with doctor name
                    doctor.name == null
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            child: CupertinoButton.filled(
                              padding: EdgeInsets.all(12),
                              child: Text(
                                'Выбрать врача',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              onPressed: selectDoctor,
                            ),
                          )
                        : Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                width: 2,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                            child: CupertinoButton(
                              padding: EdgeInsets.all(12),
                              child: Text(
                                doctorNameButtonValue,
                                style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black,
                                  fontWeight:
                                      doctorNameButtonValue == ownDoctorName
                                          ? FontWeight.w500
                                          : FontWeight.w400,
                                ),
                              ),
                              onPressed: selectDoctor,
                            ),
                          ),

                    /// Button with doctor name if we selected own name
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: doctorNameButtonValue == ownDoctorName
                          ? TextField(
                              controller: nameCon,
                              decoration: InputDecoration(
                                enabledBorder: border,
                                focusedBorder: border,
                                hintText: 'Название врача',
                                contentPadding:
                                    EdgeInsets.fromLTRB(16, 17, 16, 18),
                                counter: Container(),
                              ),
                              style: TextStyle(
                                fontSize: 13,
                              ),
                              maxLength: 150,
                              onChanged: changeName,
                            )
                          : Container(),
                    ),
                    Container(height: 16),
                    TextField(
                      controller: commentCon,
                      maxLines: 6,
                      maxLength: 500,
                      decoration: InputDecoration(
                        enabledBorder: border,
                        focusedBorder: border,
                        hintText: 'Ваш комментарий',
                        contentPadding: EdgeInsets.all(16),
                        counter: Container(),
                      ),
                      style: TextStyle(
                        fontSize: 13,
                      ),
                      onChanged: changeComment,
                    ),
                    Container(height: 42),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: Center(
                child: CupertinoButton.filled(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                  child: Text('Сохранить'),
                  onPressed:
                      (doctor.date == null) || (doctor?.name?.isEmpty ?? true)
                          ? null
                          : saveDoctor,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void saveDoctor() {
    bool dateEmpty = doctor.date == null;
    bool nameEmpty = doctor?.name?.isEmpty ?? true;
    if (!nameEmpty && !dateEmpty) {
      FocusScope.of(context).requestFocus(FocusNode());
      Navigator.pop(context, doctor);
    } else {
      String text;
      if (nameEmpty) {
        if (doctorNameButtonValue == ownDoctorName) {
          text = 'Введите врача или обследование';
        } else {
          text = 'Выберите врача или обследование';
        }
      } else if (dateEmpty) {
        text = 'Выберите дату посещения';
      }
      showDialog(
        context: context,
        builder: (_) => InfoDialog(
          title: text,
        ),
      );
    }
  }

  void selectDoctor() {
    showDialog<String>(
      context: context,
      builder: (_) => EventNameSelector(
        text: 'Выбор врача',
        ownButtonText: ownDoctorName,
        names: doctorNames,
      ),
    ).then((name) {
      if (name == null) {
        return;
      }
      setState(() {
        doctorNameButtonValue = name;
        if (name != ownDoctorName) {
          doctor = doctor.copyWith(name: name);
        } else {
          doctor = doctor.copyWith(name: '');
        }
      });
    });
  }

  void selectDate() async {
    DateTime now = DateTime.now();
    DateTime initDate = doctor.date == null
        ? DateTime.utc(now.year, now.month, now.day)
        : DateTime.utc(
            doctor.date.year,
            doctor.date.month,
            doctor.date.day,
          );
    DateTime date = await showDatePicker(
      context: context,
      initialDate: initDate,
      firstDate: bloc.child.value.birthDate,
      lastDate: DateTime.utc(now.year, now.month, now.day, 11, 59),
    );
    if (date == null) {
      return;
    }
    date = DateTime.utc(
      date.year,
      date.month,
      date.day,
    );
    changeDate(date);
  }

  void changeDate(DateTime date) {
    setState(() => doctor = doctor.copyWith(date: date));
  }

  void changeName(String name) {
    setState(() => doctor = doctor.copyWith(name: name));
  }

  void changeComment(String comment) {
    setState(() => doctor = doctor.copyWith(comment: comment));
  }
}
