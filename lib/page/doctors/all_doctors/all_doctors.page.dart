import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/page/doctors/edit_doctor/edit_doctor.page.dart';

import 'package:happy_mama/entity/doctor.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/widgets/confirm_dialog.dart';
import 'package:happy_mama/widgets/description_text.dart';

import '../../home.dart';

class AllDoctorsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AllDoctorsPageState();
}

CupertinoNavigationBar _bar = CupertinoNavigationBar(
  middle: Text('Врачи и осмотры'),
  border: Border(
    bottom: BorderSide(color: ColorStyle.tint),
  ),
);

class _AllDoctorsPageState extends State<AllDoctorsPage> {
  ProfileBloc bloc;
  List<Doctor> doctors;
  bool loading = true;

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    doctors = [];
    BabyInfoService service = BabyInfoService();
    service.fetchDoctors(bloc.child.value.id).then((doctors) {
      this.doctors = doctors;
      setState(() => loading = false);
    }).catchError((error) {
      //print('Error on fetching doctors: ${error.toString()}');
      setState(() => loading = false);
    });
    /*doctors = bloc.doctors.value ?? [];*/
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Врачи и осмотры'),
        border: Border(
          bottom: BorderSide(color: ColorStyle.tint),
        ),
      ),
      body: loading
          ? Center(child: CircularProgressIndicator())
          : Stack(
              fit: StackFit.expand,
              children: <Widget>[
                SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: 128),
                  physics: BouncingScrollPhysics(),
                  child: _DoctorTileList(
                    doctors,
                    onPressed: onDoctorPressed,
                    onDelete: onDeletePressed,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 128,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: loading
          ? Container()
          : Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 48),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        _AddButton(onPressed: onAddPressed),
                        Container(height: 16),
                        _InfoButton(onPressed: onInfoPressed),
                      ],
                    ),
                  ),
                ),
                doctors.length == 0
                    ? Positioned(
                        bottom: 145,
                        right: 50,
                        child: Text(
                          'Но никогда не поздно добавить :)',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    : Container(),
                doctors.length == 0
                    ? Positioned(
                        bottom: 87,
                        right: 10,
                        child: SvgPicture.asset(
                          'assets/vectors/arrow.svg',
                          height: 65,
                          color: Theme.of(context).primaryColor,
                        ),
                      )
                    : Container(),
              ],
            ),
    );
  }

  void onAddPressed() {
    showDialog<Doctor>(
      context: context,
      builder: (_) => EditDoctorPage(),
    ).then((doctor) {
      if (doctor != null) {
        doctors.add(doctor);
        sortDoctors();
        bloc.doctors.add(doctors);
        setState(() {});
      }
    });
  }

  void onDoctorPressed(int index) {
    showDialog<Doctor>(
      context: context,
      builder: (_) => EditDoctorPage(doctor: doctors[index]),
    ).then((doctor) {
      doctors[index] = doctor ?? doctors[index];
      sortDoctors();
      bloc.doctors.add(doctors);
      setState(() {});
    });
  }

  void onDeletePressed(int index) {
    showDialog<bool>(
      context: context,
      builder: (_) => ConfirmDialog(
        title: 'Вы действительно хотите удалить данную запись?',
        destructive: true,
      ),
    ).then((delete) {
      if (delete == true) {
        setState(() {
          doctors.removeAt(index);
          bloc.doctors.add(doctors);
        });
      }
    });
  }

  void onInfoPressed() {
    deeplinkNavigator.parsePathAndNavigate('/knowledge/11/', prevTab: 0)();
  }

  void sortDoctors() {
    doctors.sort((a, b) {
      int res = b.date.compareTo(a.date);
      if (res == 0) {
        return a.name.compareTo(b.name);
      }
      return res;
    });
  }
}

class _AddButton extends StatelessWidget {
  final Function onPressed;

  const _AddButton({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 48,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        child: Text(
          'Добавить посещение врача',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class _InfoButton extends StatelessWidget {
  final Function onPressed;

  const _InfoButton({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 48,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          width: 2,
          color: Theme.of(context).primaryColor,
        ),
      ),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        padding: EdgeInsets.zero,
        child: Text(
          'Подробнее',
          style: TextStyle(fontSize: 14),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class _DoctorTileList extends StatelessWidget {
  final List<Doctor> doctors;
  final Function(int) onPressed;
  final Function(int) onDelete;

  const _DoctorTileList(this.doctors, {this.onPressed, this.onDelete});

  @override
  Widget build(BuildContext context) {
    if (doctors.length == 0) {
      return Center(child: _NoEventsScreen());
    }
    List<Widget> list = [];
    for (int i = 0; i < doctors.length; i++) {
      list.add(
        Container(
          margin: EdgeInsets.symmetric(vertical: 18),
          child: _DoctorTile(
            doctors[i],
            i,
            onPressed: onPressed,
            onDelete: onDelete,
          ),
        ),
      );
      list.add(Divider(
        height: 0,
        thickness: 1.0,
        indent: 24,
        endIndent: 24,
        color: Theme.of(context).primaryColor.withOpacity(0.25),
      ));
    }
    list.removeLast();
    return Column(
      children: list,
    );
  }
}

class _NoEventsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData query = MediaQuery.of(context);
    return SizedBox(
      height: query.size.height - _bar.preferredSize.height - 180 - 50,
      width: query.size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 25,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'Нет записей',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontFamily: 'AmaticSC',
                  fontSize: 52,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 75,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: SvgPicture.asset('assets/vectors/baby_happy.svg'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _DoctorTile extends StatelessWidget {
  final Doctor doctor;
  final Function(int) onPressed;
  final Function(int) onDelete;
  final int index;

  const _DoctorTile(this.doctor, this.index, {this.onPressed, this.onDelete});

  final TextStyle style = const TextStyle(
    fontSize: 11,
    color: Colors.black,
    fontFamily: 'Montserrat',
  );
  final TextStyle styleBig = const TextStyle(
    fontSize: 15,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Montserrat',
  );

  @override
  Widget build(BuildContext context) {
    //TODO fix it
    ProfileBloc bloc = BlocProvider.of<ProfileBloc>(context);
    DateTime date = bloc?.child?.value?.birthDate;
    int month;
    if (date != null) {
      if (date.year == doctor.date.year) {
        month = doctor.date.month - date.month;
      } else {
        month = (doctor.date.year - date.year) * 12 +
            (doctor.date.month - date.month);
      }
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          month == null
                              ? Container()
                              : Text('$month ${Formatter.monthSuffix(month)}:',
                                  style: styleBig),
                          Text(doctor.name, style: style),
                        ],
                      ),
                    ),
                    Container(width: 20),
                    Text(Formatter.formatDate(doctor.date), style: styleBig),
                    Container(width: 16),
                  ],
                ),
              ),
              CupertinoButton(
                minSize: 35,
                padding: EdgeInsets.zero,
                child: SvgPicture.asset('assets/vectors/pen.svg', width: 18),
                onPressed: () => onPressed(index),
              ),
              CupertinoButton(
                minSize: 35,
                padding: EdgeInsets.zero,
                child: SvgPicture.asset('assets/vectors/bucket.svg', width: 26),
                onPressed: () => onDelete(index),
              ),
            ],
          ),
          DescriptionTextWidget(
            text: doctor.comment,
            style: style,
            key: ValueKey(doctor.comment),
          ),
        ],
      ),
    );
  }
}
