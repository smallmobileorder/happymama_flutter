import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child_jaw.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';
import 'package:happy_mama/widgets/loading_container.dart';
import 'package:positioned_tap_detector/positioned_tap_detector.dart';

import '../home.dart';

class Jaw extends StatefulWidget {
  @override
  _JawState createState() => _JawState();
}

class _JawState extends State<Jaw> {
  ProfileBloc bloc;
  ChildJaw jaw;

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    initBloc();
  }

  initBloc() async {
    bloc = BlocProvider.of<ProfileBloc>(context);
    jaw = bloc.childJaw.value ?? ChildJaw();
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //print(width);
    return WillPopScope(
      onWillPop: ()async{
        bloc.setTeeth();
        return Future.value(true);
      },
      child: Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Зубы'),
          backgroundColor: Colors.white,
          border: Border(
              bottom: BorderSide(
                  color: ColorStyle.tint,
                  width: 0.0,
                  style: BorderStyle.solid)),
        ),
        body: isLoading
            ? Center(child: LoadingContainer())
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    'Верхняя челюсть',
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  PositionedTapDetector(
                    child: Container(
                      width: width,
                      height: width / 4 * 3.3,
                      child: SvgPicture.string(jaw?.getJawSvgString(
                                  width,
                                  bloc?.child?.value?.birthDate ??
                                      DateTime.now()) ??
                              '') ??
                          null,
                    ),
                    onTap: (position) => teethQualifier(position.relative),
                  ),
                  Text(
                    'Нижняя челюсть',
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  Container(height: 48),
                ],
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: Container(
          height: 48,
          width: double.infinity,
          margin: EdgeInsets.only(left: 48, right: 48),
          child: InkWell(
            borderRadius: BorderRadius.circular(15),
            onTap: () => openAboutJawPage(),
            child: Ink(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: ColorStyle.tint, width: 2),
                  borderRadius: BorderRadius.circular(15)),
              child: Center(
                child: Text(
                  'Подробнее',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 14,
                      color: ColorStyle.codGray,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  teethQualifier(Offset position) async {
    //print(position);
    int number = jaw.getToothNumber(position);
    if (number != 0) {
      if (jaw.isToothMarked(number)) {
        showDialog(
            context: context,
            builder: (_) => CustomCupertinoAlertDialog(
                  content: Text('Изменить информацию о зубе'),
                  actions: <Widget>[
                    CustomCupertinoDialogAction(
                        isDestructiveAction: true,
                        child: Text('Убрать отметку'),
                        onPressed: () {
                          unMarkTooth(number);
                          Navigator.of(context, rootNavigator: true).pop();
                        }),
                    CustomCupertinoDialogAction(
                      child: Text('Изменить дату'),
                      onPressed: () async {
                        await markTooth(number);
                        Navigator.of(context, rootNavigator: true).pop();
                      },
                    ),
                    CustomCupertinoDialogAction(
                      isDefaultAction: true,
                      child: Text('Отмена'),
                      onPressed: () =>
                          Navigator.of(context, rootNavigator: true).pop(),
                    )
                  ],
                ));
      } else {
        markTooth(number);
      }
    }
  }

  openAboutJawPage() {
    deeplinkNavigator.parsePathAndNavigate('/knowledge/6/', prevTab: 0)();
  }

  markTooth(int number) async {
    DateTime date = await showDatePicker(
      context: context,
      initialDate: jaw.getToothDate(number) ?? DateTime.now(),
      firstDate: bloc.child?.value?.birthDate ?? DateTime.now(),
      lastDate: DateTime.now(),
    );
    if (date != null) {
      jaw.markToothByNumber(number, markDate: date);
      bloc.childJaw.add(jaw);
      setState(() {});
    }
  }

  unMarkTooth(int number) {
    jaw.unMarkToothByNumber(number);
    bloc.childJaw.add(jaw);
    setState(() {});
  }
}
