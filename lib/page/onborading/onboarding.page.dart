import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/page/auth/login_page.dart';
import 'package:happy_mama/page/onborading/dots_indicator.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/style/color_style.dart';

class OnboardingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  final PageController controller = PageController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  /*final double defaultHeight = 584;
  final double defaultWidth = 286;*/
  final double defaultHeight = 812.0 - 44.0 - 34.0;
  final double defaultWidth = 375.0;

  double getH(double height, double curHeight) =>
      height / defaultHeight * curHeight;

  double getW(double width, double curWidth) => width / defaultWidth * curWidth;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context)
        .removePadding(removeTop: true, removeBottom: true)
        .size;
    EdgeInsets picMargin = EdgeInsets.only(
      left: getW(45, size.width),
      right: getW(44, size.width),
    );
    final widthSize = size.width - getW(90, size.width);
    final heightSize = size.height - getH(80, size.height);
    final ratio = heightSize / widthSize;
    final defaultRatio = defaultHeight / defaultWidth;
    //print('$ratio, $defaultRatio');
    final BoxFit fit =
        ratio < defaultRatio ? BoxFit.fitHeight : BoxFit.fitWidth;
    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            color: ColorStyle.cyan.withOpacity(0.35),
          ),
        ),
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          child: SvgPicture.asset('assets/vectors/toys.svg'),
        ),
        Positioned.fill(
          top: getH(80, size.height),
          bottom: getH(90, size.height),
          left: 0,
          //getW(45, size.width),
          right: 0,
          //getW(44, size.width),
          child: Container(
            child: PageView(
              controller: controller,
              children: <Widget>[
                Container(
                  margin: picMargin,
                  child: Image.asset(
                    'assets/images/onboarding/1.png',
                    fit: fit,
                  ),
                ),
                Container(
                  margin: picMargin,
                  child: Image.asset(
                    'assets/images/onboarding/2.png',
                    fit: fit,
                  ),
                ),
                Container(
                  margin: picMargin,
                  child: Image.asset(
                    'assets/images/onboarding/3.png',
                    fit: fit,
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Container(
                        margin: picMargin.copyWith(
                          bottom: getH(102, size.height),
                        ),
                        child: Image.asset(
                          'assets/images/onboarding/4.png',
                          fit: fit,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: getH(45, size.height),
                      left: getW(28, size.width),
                      child: SvgPicture.asset(
                        'assets/vectors/arrow_left.svg',
                        height: getH(80, size.height),
                        color: ColorStyle.cyan,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        margin: EdgeInsets.only(
                          left: getW(76, size.width),
                          right: getW(76, size.width),
                        ),
                        child: CupertinoButton.filled(
                            child: Text(
                              'Давайте приступим!',
                              textAlign: TextAlign.center,
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 8),
                            onPressed: () async {
                              await StorageService.setFirstEntrance();
                              Navigator.pushReplacement(
                                context,
                                CupertinoPageRoute(
                                    builder: (context) => LoginPage(),
                                    settings:
                                        RouteSettings(name: 'Авторизация')),
                              );
                            }),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 24,
          left: 0,
          right: 0,
          child: Center(
            child: DotsIndicator(
              controller: controller,
              itemCount: 4,
            ),
          ),
        ),
      ],
    );
  }
}
