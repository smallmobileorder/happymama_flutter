import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/page/auth/login_page.dart';
import 'package:happy_mama/page/home.dart';
import 'package:happy_mama/page/onborading/onboarding.page.dart';
import 'package:happy_mama/page/welcome/welcome_page.dart';
import 'package:happy_mama/service/firebase/auth/auth_service.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/style/color_style.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final _authService = AuthService();
  final _userService = UserService();
  bool connectionProblem = false;

  @override
  void initState() {
    Future.delayed(Duration(seconds: 7)).whenComplete(() =>
        mounted ? setState(() => connectionProblem = true) : print('unmount'));
    _authService.isAuthorized.then((hasUser) async {
      bool first = await StorageService.isFirstEntrance();
      if (first == true) {
        print('FIRST ENTRANCE');
        Navigator.of(context).pushReplacement(
            CupertinoPageRoute(builder: (context) => OnboardingPage()));
      } else {
        if (hasUser) {
          print('HAS USER');
          final user = await _userService.fetchInfo();
          if (connectionProblem) setState(() => connectionProblem = false);
          if (user.kids.isEmpty) {
            print('KIDS EMPTY');
            Navigator.of(context).pushReplacement(CupertinoPageRoute(
                builder: (context) => WelcomePage(parent: user),settings: RouteSettings(name: 'Экран приветствия')));
          } else {
            print('KIDS NOT EMPTY');
            final bloc = BlocProvider.of<ProfileBloc>(context);
            await bloc.init(user);
            Navigator.of(context).pushReplacement(CupertinoPageRoute(
                builder: (context) => HomePage(),
                settings: RouteSettings(name: 'Домашняя страница')));
          }
        } else {
          print('LOGIN PAGE');
          Navigator.of(context).pushReplacement(
              CupertinoPageRoute(builder: (context) => LoginPage(), settings: RouteSettings(name: 'Авторизация')));
        }
      }
    }).catchError((e) async {
      print('CATCHED ERROR: $e');
      bool first = await StorageService.isFirstEntrance();
      if (first == true) {
        Navigator.of(context).pushReplacement(
            CupertinoPageRoute(builder: (context) => OnboardingPage()));
      } else {
        Navigator.of(context).pushReplacement(
            CupertinoPageRoute(builder: (context) => LoginPage(), settings: RouteSettings(name: 'Авторизация')));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorStyle.cyan,
        body: Stack(
          children: <Widget>[
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              child: SvgPicture.asset('assets/vectors/splash/toys.svg'),
            ),
            Center(
              child: FlareActor(
                "assets/flare/baby.flr",
                alignment: Alignment.center,
                fit: BoxFit.contain,
                isPaused: !mounted,
                animation: 'Last',
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: MediaQuery.of(context).size.height / 5.5,
              child: SvgPicture.asset('assets/vectors/splash/happy_mama.svg'),
            ),
            if (connectionProblem)
              Positioned(
                  left: 0,
                  right: 0,
                  bottom: 16,
                  child: Text(
                    'Проверьте подключение к интернету..',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'AmaticSC',
                      color: ColorStyle.sunglow,
                    ),
                    textAlign: TextAlign.center,
                  )),
          ],
        ));
  }
}

class DualAnimationLoopController implements FlareController {
  final String _startAnimationName;
  final String _loopAnimationName;
  final double _mix;

  DualAnimationLoopController(this._startAnimationName, this._loopAnimationName,
      [this._mix = 1.0]);

  bool _looping = false;
  double _duration = 0.0;
  ActorAnimation _startAnimation;
  ActorAnimation _loopAnimation;

  @override
  void initialize(FlutterActorArtboard artboard) {
    _startAnimation = artboard.getAnimation(_startAnimationName);
    _loopAnimation = artboard.getAnimation(_loopAnimationName);
  }

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    _duration += elapsed;

    if (!_looping) {
      if (_duration < _startAnimation?.duration ?? 0) {
        _startAnimation.apply(_duration, artboard, _mix);
      } else {
        _looping = true;
        _duration -= _startAnimation?.duration ?? 0;
      }
    }
    if (_looping) {
      _duration %= _loopAnimation?.duration ?? 0;
      _loopAnimation.apply(_duration, artboard, _mix);
    }
    return true;
  }

  @override
  ValueNotifier<bool> isActive = ValueNotifier(true);

  @override
  void setViewTransform(Mat2D viewTransform) {}
}
