import 'dart:io';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/album/collage_share_dialog.dart';
import 'package:happy_mama/page/album/custom_dialog.dart' as custom;
import 'package:happy_mama/service/firebase/analytics/events.dart';
import 'package:happy_mama/widgets/loading_container.dart';
import 'package:rxdart/rxdart.dart';
import 'package:screenshot/screenshot.dart';

import '../home.dart';
import 'collage.dart';

class CollageReady extends StatelessWidget {
  final List<ImageProvider> images;
  final Color color;
  final Child child;
  final ProfileBloc bloc;
  final BehaviorSubject<bool> disableImage;

  const CollageReady(
    this.images,
    this.child,
    this.bloc,
    this.disableImage, {
    this.color = blueColor,
  });

  @override
  Widget build(BuildContext context) {
    return custom.CustomDialog(
      elevation: 0,
      padding: 13,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          border: Border.all(width: 2, color: Theme.of(context).primaryColor),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 24, 16, 16),
                child: Text(
                  'Ваш коллаж готов. Хотите им поделиться?',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Collage(
                  images,
                  child,
                  bloc,
                  color: color,
                  withWatermark: !premiumService.isPremium(),
                ),
              ),
              Container(height: 24),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: CupertinoButton.filled(
                    padding: EdgeInsets.all(16),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        'Поделиться коллажем',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    onPressed: () => shareCollage(context)),
              ),
              Container(height: 24),
            ],
          ),
        ),
      ),
    );
  }

  shareCollage(BuildContext context) {
    if (premiumService.isPremium()) {
      ScreenshotController controller = noWatermarkController;
      _share(controller).asStream().listen((_) {
        Navigator.pop(context);
        Navigator.pop(context);
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => LoadingContainer());
    } else {
      showDialog(
        context: context,
        builder: (_) => CollageShareDialog(disableImage: disableImage),
      );
    }
  }

  _share(ScreenshotController controller) async {
    disableImage.add(true);
    //enableShareReceiving();
    File file = await controller.capture(
      pixelRatio: 4,
      delay: Duration(seconds: 3),
    );
//    //print(file.path);
//    //print(file.exists());
//    //print(file.readAsBytesSync());
    //await Future.delayed(Duration(seconds: 2));
    disableImage.add(false);
    await Share.file('Поделиться коллажем', 'collage.png',
        file.readAsBytesSync(), 'image/*');
    var event = AnalyticEvent.shareCollageEvent(
        havePremium: premiumService.isPremium());
    analytics.logEvent(name: event.name, parameters: event.parameters);
  }
}
