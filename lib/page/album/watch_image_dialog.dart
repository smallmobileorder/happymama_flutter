import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:happy_mama/page/album/custom_dialog.dart';

class WatchImageDialog extends StatelessWidget {
  final ImageProvider image;
  final Function(ImageActionType) onPressed;

  const WatchImageDialog(this.image, this.onPressed);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double mainWidth = width - 72;
    return CustomDialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(width: 2, color: Theme.of(context).primaryColor),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: mainWidth,
              height: mainWidth,
              margin: EdgeInsets.only(top: 24),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              child: Image(
                fit: BoxFit.cover,
                image: image,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 16, 24, 12),
              child: CupertinoButton.filled(
                padding: EdgeInsets.all(16),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'Сделать основным фото профиля',
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                ),
                onPressed: () async {
                  await onPressed(ImageActionType.SET_MAIN);
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
              child: CupertinoButton.filled(
                padding: EdgeInsets.all(16),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'Изменить фото',
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                ),
                onPressed: () async {
                  await onPressed(ImageActionType.CHANGE);
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 12, 24, 16),
              child: CupertinoButton.filled(
                padding: EdgeInsets.all(16),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'Удалить',
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                ),
                onPressed: () async {
                  await onPressed(ImageActionType.DELETE);
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

enum ImageActionType { CHANGE, SET_MAIN, DELETE }
