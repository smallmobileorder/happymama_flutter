
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/widgets/loading_container.dart';

class ImageHolder extends StatelessWidget {
  final Size size;
  final bool active;
  final MemoryImage image;
  final int month;
  final Function onPressed;
  final Widget noImageHolder;

  const ImageHolder({
    @required this.size,
    this.image,
    this.onPressed,
    @required this.noImageHolder,
    this.active = false,
    @required this.month,
  });

  ImageHolder withImage(MemoryImage image) {
    return ImageHolder(
      size: this.size,
      image: image,
      onPressed: this.onPressed,
      noImageHolder: this.noImageHolder,
      active: this.active,
      month: this.month,
    );
  }

  @override
  Widget build(BuildContext context) {
    var _image;
    var _holder = noImageHolder;
    Widget _loading = Container();
    if (image != null) {
      _image = DecorationImage(
        fit: BoxFit.cover,
        //image: widget.image,
        image: image,
      );
      _loading = LoadingContainer();
      _holder = Container(
        width: size.width,
        height: size.height,
        color: Colors.transparent,
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: EdgeInsets.only(right: 2, bottom: 2),
          child: Container(
            width: 24,
            height: 24,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Theme.of(context).primaryColor,
            ),
            child: Center(
              child: Text(
                month.toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ),
      );
    }
    return FlatButton(
      padding: EdgeInsets.zero,
      onPressed: active ? onPressed : null,
      splashColor: Colors.transparent,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _loading,
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: _image == null
                  ? Border.all(
                      color: active
                          ? Theme.of(context).primaryColor
                          : Color(0xFFC4C4C4),
                      width: 2,
                    )
                  : null,
              image: _image,
            ),
            child: _holder,
          ),
        ],
      ),
    );
  }
}

/*class ImageHolder extends StatefulWidget {
  final Size size;
  final bool active;
  final bool shadow;
  final ImageProvider image;
  final FutureOr<Uint8List> byteImage;
  final Function onPressed;
  final Widget noImageHolder;

  const ImageHolder({
    Key key,
    @required this.size,
    this.image,
    this.byteImage,
    this.onPressed,
    @required this.noImageHolder,
    this.active = false,
    this.shadow = false,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ImageHolderState();
}

class _ImageHolderState extends State<ImageHolder> {
  Uint8List byteImage;
  bool loading = false;

  @override
  void initState() {
    //print("INIT STATE FOR IMAGE HOLDER");
    if (widget.byteImage != null) {
      setState(() => loading = true);
      Future.wait([
        () async {
          Uint8List bytes = await widget.byteImage;
          if(bytes == null) {
            setState(() => loading = false);
            return;
          }
          */ /*var result = await FlutterImageCompress.compressWithList(
            bytes.toList(),
            minHeight: widget.size.width.toInt(),
            minWidth: widget.size.height.toInt(),
            quality: 80,
          );*/ /*
          loading = false;
          setState(() => byteImage = Uint8List.fromList(bytes));
        }()
      ]);
      */ /*Future.wait<Uint8List>([widget.byteImage]).then((res) async {
        var result = await FlutterImageCompress.compressWithList(
          res[0].toList(),
          minHeight: widget.size.width.toInt(),
          minWidth: widget.size.height.toInt(),
          quality: 80,
        );
        loading = false;
        setState(() => byteImage = Uint8List.fromList(result));
      });*/ /*
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _image;
    var _holder = widget.noImageHolder;
    if (byteImage != null) {
      _image = DecorationImage(
        fit: BoxFit.fitWidth,
        //image: widget.image,
        image: MemoryImage(byteImage),
      );
      _holder = Container();
    }
    return FlatButton(
      padding: EdgeInsets.zero,
      onPressed: loading ? null : widget.onPressed,
      splashColor: Colors.transparent,
      child: Container(
        width: widget.size.width,
        height: widget.size.height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: _image == null
              ? Border.all(
                  color: widget.active
                      ? Theme.of(context).primaryColor
                      : Color(0xFFC4C4C4),
                  width: 2,
                )
              : null,
          image: _image,
        ),
        child: Stack(
          children: <Widget>[
            _holder,
            loading ? LoadingContainer() : Container(),
          ],
        ),
      ),
    );
  }
}*/
