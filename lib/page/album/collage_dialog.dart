import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/album/collage_ready.dart';
import 'package:happy_mama/service/firebase/analytics/events.dart';
import 'package:rxdart/rxdart.dart';

class CollageDialog extends StatelessWidget {
  final List<ImageProvider> images;
  final Child child;
  final ProfileBloc bloc;
  final BehaviorSubject<bool> disableImage;

  const CollageDialog(this.images, this.child, this.bloc, this.disableImage);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          border: Border.all(width: 2, color: Theme.of(context).primaryColor),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(16, 24, 16, 24),
              child: Text(
                'Какого цвета вы бы хотели коллаж?',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            CupertinoButton(
              onPressed: () {
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (_) => CollageReady(images, child, bloc, disableImage, color: pinkColor),
                );
                var event = AnalyticEvent.generateCollageEvent(color: 'Розовый');
                analytics.logEvent(
                    name: event.name, parameters: event.parameters);
              },
              child: Text(
                'Розовый',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: pinkColor),
              ),
            ),
            CupertinoButton(
              onPressed: () {
                Navigator.pop(context);
                showDialog(
                  context: context,
                  builder: (_) => CollageReady(images, child, bloc, disableImage, color: blueColor),
                );

                var event = AnalyticEvent.generateCollageEvent(color: 'Голубой');
                analytics.logEvent(
                    name: event.name, parameters: event.parameters);
              },
              child: Text(
                'Голубой',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: blueColor),
              ),
            ),
            Container(height: 16),
          ],
        ),
      ),
    );
  }
}
