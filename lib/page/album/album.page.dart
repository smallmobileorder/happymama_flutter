import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/album/collage_dialog.dart';
import 'package:happy_mama/page/album/image_holder.dart';
import 'package:happy_mama/page/album/load_image_dialog.dart';
import 'package:happy_mama/page/album/watch_image_dialog.dart';
import 'package:happy_mama/service/firebase/analytics/events.dart';
import 'package:happy_mama/service/firebase/photo/photo_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/month_counter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/rxdart.dart';

class AlbumPage extends StatefulWidget {
  final double screenWidth;

  const AlbumPage(this.screenWidth);

  @override
  State<StatefulWidget> createState() => _AlbumPageState();
}

class _AlbumPageState extends State<AlbumPage> {
  List<ImageHolder> imageHolders;
  PhotoService photoService = PhotoService();
  ProfileBloc bloc;
  int month = 0;
  bool disposed = false;
  bool locked;
  BehaviorSubject<bool> disableImage = BehaviorSubject();
  List<Widget> lockedHolders;

  @override
  void dispose() {
    disposed = true;
    disableImage.close();
    super.dispose();
  }

  @override
  void initState() {
    locked = false;
    disableImage.listen((disable) =>
        mounted ? (setState(() => locked = (disable ?? false))) : null);
    bloc = BlocProvider.of<ProfileBloc>(context);
    String childId = bloc.child.value.id;
    month = monthCounter(
      end: DateTime.now(),
      start: bloc.child.value.birthDate,
    );
    month = month.clamp(0, 12);

    //new
    double width = widget.screenWidth;
    double miniSpace = 16; // margin from left and right
    double miniGap = 6; // gap between widgets
    // 12 - all borders width
    double miniWidth = (width - (miniSpace * 2) - (miniGap * 2) - 12) / 3;
    double mainWidth = width - 72;
    lockedHolders = [
      lockedImageHolder(Size(mainWidth, mainWidth)),
      lockedImageHolder(Size(miniWidth, miniWidth))
    ];
    imageHolders = List.generate(13, (index) => index)
        .map((index) => ImageHolder(
              month: index,
              size: index == month
                  ? Size(mainWidth, mainWidth)
                  : Size(miniWidth, miniWidth),
              image: null,
              noImageHolder: noImageText(index),
              onPressed: () => onPressed(getIndex(index)),
              active: index <= month,
            ))
        .toList();
    imageHolders.add(imageHolders.removeAt(month));
    photoService.getLinksFromDatabase(childId).then((links) {
      for (int _month in links.keys) {
        //print("MONTH $_month: WE GONNNA DOWNLOAD IT, BOI");
        photoService.getImage(childId, _month, links[_month]).then((bytes) {
          if (disposed) {
            return;
          }
          if (bytes != null) {
            setState(() {
              imageHolders[getIndex(_month)] =
                  imageHolders[getIndex(_month)].withImage(MemoryImage(bytes));
            });
          }
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //print('START BUILD');
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Фотоальбом'),
      ),
      body: SingleChildScrollView(
        child: photos(context),
      ),
    );
  }

  Widget noImageText(int month) {
    String text = 'месяцев';
    if (month == 1) {
      text = 'месяц';
    } else if (month >= 2 && month <= 4) {
      text = 'месяца';
    }
    return Center(
      child: Text(
        '$month\n$text',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w600,
          color: Color(0xFF444444),
        ),
      ),
    );
  }

  void onPressed(int index) async {
    //print('PRESSED ON $index');
    if (imageHolders[index].image == null) {
      await change(index);
      var event = AnalyticEvent.addPhotoEvent(month: index);
      analytics.logEvent(name: event.name, parameters: event.parameters);
    } else {
      showDialog(
        context: context,
        builder: (_) => WatchImageDialog(
          imageHolders[index].image,
          (action) async {
            if (action == ImageActionType.CHANGE) {
              await change(index);
              var event = AnalyticEvent.changePhotoEvent(month: index);
              analytics.logEvent(
                  name: event.name, parameters: event.parameters);
            } else if (action == ImageActionType.DELETE) {
              await delete(index);
              var event = AnalyticEvent.deletePhotoEvent(month: index);
              analytics.logEvent(
                  name: event.name, parameters: event.parameters);
            } else {
              await setMain(index);
              var event = AnalyticEvent.setMainPhotoEvent(month: index);
              analytics.logEvent(
                  name: event.name, parameters: event.parameters);
            }
          },
        ),
      );
    }
  }

  // hard logic
  // such wow
  int getIndex(int index) {
    if (index == month) {
      return 12;
    } else if (index == 12) {
      return month;
    } else {
      return index;
    }
  }

  Future setMain(int index) async {
    Child child = bloc.child.value;
    child.mainImageId = getIndex(index);
    bloc.child.add(child);
  }

  Future delete(int index) async {
    imageHolders[index] = imageHolders[index].withImage(null);
    await photoService.deleteImage(bloc.child.value.id, getIndex(index));
    Child child = bloc.child.value;
    if (child.mainImageId == index || child.mainImageId == null) {
      // check the latest photo
      int imageId;
      imageHolders.forEach((image) {
        if (image.image != null) {
          imageId = getIndex(image.month);
        }
      });
      child.mainImageId = imageId;
      bloc.child.add(child);
    }
    setState(() {});
  }

  Future change(int index) async {
    await showDialog(
      context: context,
      builder: (_) => LoadImageDialog(
        onPressed: (source) async {
          File image;
          try {
            image = await ImagePicker.pickImage(
                source: source,
                maxHeight: 1000,
                maxWidth: 1000,
                imageQuality: 80);
            if (image == null) {
              image = (await ImagePicker.retrieveLostData())?.file;
            }
            if (image == null) {
              return;
            }
          } on PlatformException catch (e) {
            if (e.code == 'camera_access_denied' ||
                e.code == 'photo_access_denied') {
              await showDialog(
                context: context,
                builder: (context) => NoPermissionDialog(
                  reason: e.code == 'camera_access_denied'
                      ? NoPermissionReason.CAMERA
                      : NoPermissionReason.PHOTOS,
                ),
              );
              return;
            } else {
              //print('PlatformException on getting photo: $e');
              return;
            }
          } catch (e) {
            //print('Error on getting photo: ${e.toString()}');
            return;
          }
          File croppedFile = await ImageCropper.cropImage(
              sourcePath: image.path,
              aspectRatioPresets: [
                CropAspectRatioPreset.square,
              ],
              aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
              androidUiSettings: AndroidUiSettings(
                toolbarTitle: 'Обработка фотографии',
                toolbarColor: ColorStyle.tint,
                toolbarWidgetColor: Colors.white,
                initAspectRatio: CropAspectRatioPreset.square,
                lockAspectRatio: true,
                hideBottomControls: true,
              ),
              iosUiSettings: IOSUiSettings(
                minimumAspectRatio: 1.0,
              ));
          image.writeAsBytesSync(croppedFile.readAsBytesSync());
          setState(() => imageHolders[index] = imageHolders[index]
              .withImage(MemoryImage(image.readAsBytesSync())));
          await photoService.saveImage(
            bloc.child.value.id,
            getIndex(index),
            imageHolders[index].image.bytes,
          );
          Child child = bloc.child.value;
          if (getIndex(index) == child.mainImageId) {
            bloc.child.add(child);
          } else if (getIndex(index) == month) {
            child.mainImageId = getIndex(index);
            bloc.child.add(child);
          } else if (child.mainImageId == null ||
              child.mainImageId < getIndex(index)) {
            int imageId;
            imageHolders.forEach((image) {
              if (image.image != null) {
                imageId = getIndex(image.month);
              }
            });
            child.mainImageId = imageId;
            bloc.child.add(child);
          }
        },
      ),
    );
  }

  Widget photos(BuildContext context) {
    double miniSpace = 16; // margin from left and right
    double miniGap = 6; // gap between
    return Column(
      children: [
        Container(height: 16),

        /// Main
        locked ? lockedHolders[0] : imageHolders[12],
        /*ImageHolder(
          size: Size(mainWidth, mainWidth),
          image: images[0],
          noImageHolder: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(height: mainWidth * 0.0529),
              Text(
                'Малыш родился!',
                style: TextStyle(
                  fontSize: mainWidth * 0.1324,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor,
                  fontFamily: 'AmaticSC',
                ),
              ),
              SvgPicture.asset(
                'assets/vectors/baby_sitting.svg',
                height: mainWidth * 0.48,
              ),
              Container(
                height: 0.158 * mainWidth,
                width: mainWidth - 32,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Theme.of(context).primaryColor,
                ),
                child: Center(
                  child: Text(
                    'Добавьте ваше первое фото',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(height: mainWidth * 0.0529),
            ],
          ),
          onPressed: () => onPressed(0),
        ),*/
        Container(height: 32),

        /// 1 -- 3 month
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(width: miniSpace),
            locked ? lockedHolders[1] : imageHolders[0],
            locked ? lockedHolders[1] : imageHolders[1],
            locked ? lockedHolders[1] : imageHolders[2],
            Container(width: miniSpace),
          ],
        ),
        Container(height: miniGap),

        /// 4 -- 6 month
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(width: miniSpace),
            locked ? lockedHolders[1] : imageHolders[3],
            locked ? lockedHolders[1] : imageHolders[4],
            locked ? lockedHolders[1] : imageHolders[5],
            Container(width: miniSpace),
          ],
        ),
        Container(height: miniGap),

        /// 7 -- 9 month
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(width: miniSpace),
            locked ? lockedHolders[1] : imageHolders[6],
            locked ? lockedHolders[1] : imageHolders[7],
            locked ? lockedHolders[1] : imageHolders[8],
            Container(width: miniSpace),
          ],
        ),
        Container(height: miniGap),

        /// 10 -- 12 month
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(width: miniSpace),
            locked ? lockedHolders[1] : imageHolders[9],
            locked ? lockedHolders[1] : imageHolders[10],
            locked ? lockedHolders[1] : imageHolders[11],
            Container(width: miniSpace),
          ],
        ),
        Container(height: 24),
        CupertinoButton.filled(
            child: Text('Составить коллаж'),
            onPressed: () async {
              var images = imageHolders.map((holder) => holder.image).toList();
              images.insert(month, images.removeAt(12));

              var event =
                  AnalyticEvent.openCreateCollageDialogEvent(month: month);
              analytics.logEvent(
                  name: event.name, parameters: event.parameters);

              await showDialog(
                context: context,
                builder: (_) =>
                    CollageDialog(images, bloc.child.value, bloc, disableImage),
              );
//              setState(() {
//                locked = false;
//              });
            }),
        Container(height: 24),
      ],
    );
  }

  lockedImageHolder(Size size) {
    return Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            border: Border.all(
              // color: Theme.of(context).primaryColor,
              width: 2,
            )));
  }
}
