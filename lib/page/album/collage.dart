import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/album/widgets/collage_builder.dart';
import 'package:happy_mama/util/month_counter.dart';
import 'package:screenshot/screenshot.dart';

GlobalKey watermarkCollageKey = GlobalKey();
GlobalKey fullCollageKey = GlobalKey();
ScreenshotController watermarkController = ScreenshotController();
ScreenshotController noWatermarkController = ScreenshotController();

class Collage extends StatelessWidget {
  final List<ImageProvider> images;
  final ProfileBloc bloc;
  final Color color;
  final Child child;
  final bool withWatermark;

  const Collage(this.images, this.child, this.bloc,
      {this.color = blueColor, this.withWatermark = true});

  @override
  Widget build(BuildContext context) {
    //print(child);
    int height;
    double weight;
    if (bloc.heights.value != null && bloc.heights.value.isNotEmpty) {
      height = bloc.heights.value.first.height;
    }
    if (bloc.weights.value != null && bloc.weights.value.isNotEmpty) {
      weight = bloc.weights.value.first.weight;
    }
    // count width for collage, and height
    // 26 - double padding
    // 48 - also padding
    // 4 - borders
    double _width = MediaQuery.of(context).size.width - 26 - 48 - 4;
    //print("_width = $_width");
    _width = double.parse(_width.toStringAsFixed(1));
    //print("_width = $_width");
    double _height = _width * (351 / 301);
    //print("_height = $_height");
    _height = double.parse(_height.toStringAsFixed(1));
    //print("_height = $_height");

    int month;
    if (child.birthDate != null) {
      month = monthCounter(end: DateTime.now(), start: child.birthDate)
          .clamp(0, 12);
    }
    Widget collage = CollageBuilder(
      isBoy: child.gender == 'male',
      inputHeight: _height,
      name: child.name,
      height: height,
      blue: color == blueColor,
      birthdate: child.birthDate,
      month: month,
      weight: weight,
    );

    // gather photos
    month ??= 0;
    var _images = images.toList();
    _images.insert(0, _images.removeAt(month));
    return Screenshot(
      key: watermarkCollageKey,
      controller: watermarkController,
      child: Container(
        // default: width 301, height 351
        height: _height,
        width: _width,
        color: color,
        child: Stack(
          children: <Widget>[
            Screenshot(
              key: fullCollageKey,
              controller: noWatermarkController,
              child: Container(
                color: color,
                child: CustomMultiChildLayout(
                  delegate: MyDelegate(),
                  children: [
                    //photo month = id - 1
                    LayoutId(
                      id: 0,
                      key: Key('image0'),
                      child: collage,
                    ),
                    LayoutId(
                      id: 1,
                      key: Key('image1'),
                      child: image(_images[0]),
                    ),
                    LayoutId(
                      id: 2,
                      key: Key('image2'),
                      child: image(_images[1], angle: 1.5),
                    ),
                    LayoutId(
                      id: 3,
                      key: Key('image3'),
                      child: image(_images[2], angle: 10),
                    ),
                    LayoutId(
                      id: 4,
                      key: Key('image4'),
                      child: image(_images[3]),
                    ),
                    LayoutId(
                      id: 5,
                      key: Key('image5'),
                      child: image(_images[4], angle: -4),
                    ),
                    LayoutId(
                      id: 6,
                      key: Key('image6'),
                      child: image(_images[5], angle: 2.6),
                    ),
                    LayoutId(
                      id: 7,
                      key: Key('image7'),
                      child: image(_images[6], angle: 5),
                    ),
                    LayoutId(
                      id: 8,
                      key: Key('image8'),
                      child: image(_images[7], angle: 0.5),
                    ),
                    LayoutId(
                      id: 9,
                      key: Key('image9'),
                      child: image(_images[8], angle: -12.75),
                    ),
                    LayoutId(
                      id: 10,
                      key: Key('image10'),
                      child: image(_images[9]),
                    ),
                    LayoutId(
                      id: 11,
                      key: Key('image11'),
                      child: image(_images[10], angle: -4.15),
                    ),
                    LayoutId(
                      id: 12,
                      key: Key('image12'),
                      child: image(_images[11], angle: 4),
                    ),
                    LayoutId(
                      id: 13,
                      key: Key('image13'),
                      child: image(_images[12], angle: -5.5),
                    ),
                    //watermark
                    /*LayoutId(
                      id: 14,
                      child: Image.asset('assets/images/watermark.png'),
                    ),*/
                  ],
                ),
              ),
            ),
            if (withWatermark)
              Positioned(
                top: 38 / 351 * _height,
                child: Image.asset(
                  'assets/images/watermark.png',
                  width: 301 / 301 * _width,
                  height: 301 / 351 * _height,
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget image(ImageProvider image, {double angle = 0}) {
    if (image == null) {
      return Container();
    }
    return Transform.rotate(
      angle: radians(angle),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: image,
          ),
        ),
      ),
    );
  }

  double radians(double angle) => angle * pi / 180;
}

class MyDelegate extends MultiChildLayoutDelegate {
  // size of collage
  final double defaultWidth = 301;
  final double defaultHeight = 351;
  final double ratio = 351 / 301;

  // size of min picture
  final double minW = 64.66;
  final double minH = 64.68;

  // size of big picture
  final double maxW = 112.07;
  final double maxH = 112.11;

  double curWidth;
  double curHeight;
  double curDxOffset;

  double getW(double width) => width / defaultWidth * curWidth;

  double getH(double height) => height / defaultHeight * curHeight;

  Offset getOffset(
    double dx,
    double dy, {
    bool img = true,
  }) =>
      Offset(dx + curDxOffset + (img ? 0.8 : 0), dy + (img ? 0.8 : 0));

  @override
  void performLayout(Size size) {
    // get current size of widget
    curWidth = size.width; // / ratio;
    curHeight = size.height;
    // calculate offset for x
    curDxOffset = 0; //(size.width / 2) - (curWidth / 2);
    // calculate width and height for small photos
    double _minW = getW(minW) - getW(0.8 * 2);
    double _minH = getH(minH) - getH(0.8 * 2);
    //print("SIZE: $size");
    //print("NEW SIZE:${Size(curWidth, curHeight)}");
    // place collage
    layoutChild(
      0,
      BoxConstraints.tight(Size(curWidth, curHeight)),
    );
    positionChild(0, getOffset(0, 0, img: false));

    // place 0 picture
    layoutChild(1, BoxConstraints.tight(Size(getW(maxW), getH(maxH))));
    positionChild(1, getOffset(getW(97.41), getH(119.88)));

    // place 1 picture
    layoutChild(2, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(2, getOffset(getW(20.44), getH(50.38)));

    // place 2 picture
    layoutChild(3, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(3, getOffset(getW(6.75), getH(111.65)));

    // place 3 picture
    layoutChild(4, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(4, getOffset(getW(29.31), getH(169.03)));

    // place 4 picture
    layoutChild(5, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(5, getOffset(getW(15.06), getH(233.97)));

    // place 5 picture
    layoutChild(6, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(6, getOffset(getW(85.16), getH(236.74)));

    // place 6 picture
    layoutChild(7, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(7, getOffset(getW(157.37), getH(229.71)));

    // place 7 picture
    layoutChild(8, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(8, getOffset(getW(228.8), getH(237.69)));

    // place 8 picture
    layoutChild(9, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(9, getOffset(getW(228.06), getH(172.46)));

    // place 9 picture
    layoutChild(10, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(10, getOffset(getW(217.24), getH(117.29)));

    // place 10 picture
    layoutChild(11, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(11, getOffset(getW(229.25), getH(61.89)));

    // place 11 picture
    layoutChild(12, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(12, getOffset(getW(161.37), getH(49.61)));

    // place 12 picture
    layoutChild(13, BoxConstraints.tight(Size(_minW, _minH)));
    positionChild(13, getOffset(getW(89.82), getH(59.81)));

    // place watermark
    /*layoutChild(14, BoxConstraints.tight(Size(getW(301), getH(301))));
    positionChild(14, getOffset(0, getH(38)));*/
  }

  @override
  bool shouldRelayout(MultiChildLayoutDelegate oldDelegate) => true;
}
