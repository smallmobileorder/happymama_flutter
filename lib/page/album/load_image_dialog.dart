import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';
import 'package:image_picker/image_picker.dart';

class LoadImageDialog extends StatelessWidget {
  final Function(ImageSource) onPressed;

  const LoadImageDialog({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(height: 24),
            Text(
              'Фотография',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
            Container(height: 16),
            SizedBox(
              width: double.infinity,
              height: 36,
              child: FlatButton(
                child: Text(
                  'Загрузить с устройства',
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
                ),
                onPressed: () => onPressed == null
                    ? null
                    : () async {
                        await onPressed(ImageSource.gallery);
                        Navigator.pop(context);
                      }(),
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 36,
              child: FlatButton(
                child: Text('Сделать снимок',
                    style:
                        TextStyle(fontWeight: FontWeight.normal, fontSize: 16)),
                onPressed: () => onPressed == null
                    ? null
                    : () async {
                        await onPressed(ImageSource.camera);
                        Navigator.pop(context);
                      }(),
              ),
            ),
            Container(height: 24),
          ],
        ),
      ),
    );
  }
}

class NoPermissionDialog extends StatelessWidget {
  final NoPermissionReason reason;

  const NoPermissionDialog({Key key, @required this.reason}) : super(key: key);

  static final String _photos = 'Чтобы Вы могли загружать фотографии, '
      'HappyMama нужен доступ к галерее.\n'
      'Вы можете включить доступ в настройках приложения.';

  static final String _camera = 'Чтобы Вы могли снимать фотографии, '
      'HappyMama нужен доступ к камере.\n'
      'Вы можете включить доступ в настройках приложения.';

  @override
  Widget build(BuildContext context) {
    String text;
    switch (reason) {
      case NoPermissionReason.CAMERA:
        text = _camera;
        break;
      case NoPermissionReason.PHOTOS:
        text = _photos;
        break;
    }
    return CustomCupertinoAlertDialog(
      title: Text('Разрешите доступ'),
      content: Text(text),
      actions: <Widget>[
        CustomCupertinoDialogAction(
          child: Text('Не сейчас'),
          isDefaultAction: true,
          onPressed: () => Navigator.pop(context),
        ),
        CustomCupertinoDialogAction(
          child: Text('Настройки'),
          onPressed: () {
            Platform.isAndroid
                ? AppSettings.openAppSettings()
                : AppSettings.openNotificationSettings();
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}

enum NoPermissionReason {
  CAMERA,
  PHOTOS,
}
