import 'dart:async';
import 'dart:io';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:happy_mama/page/album/collage.dart';
import 'package:happy_mama/page/premium/ads_service.dart';
import 'package:happy_mama/service/firebase/analytics/events.dart';
import 'package:happy_mama/widgets/loading_container.dart';
import 'package:rxdart/rxdart.dart';
import 'package:screenshot/screenshot.dart';

import '../../application.dart';
import '../home.dart';

class CollageShareDialog extends StatefulWidget {
  const CollageShareDialog({Key key, this.disableImage}) : super(key: key);

  @override
  State<CollageShareDialog> createState() => _CollageShareDialogState();
  final BehaviorSubject<bool> disableImage;
}

class _CollageShareDialogState extends State<CollageShareDialog> {
  bool loading = false;
  StreamSubscription subscription;
  AdmobInterstitial interstitialAd;

  @override
  void dispose() {
    subscription?.cancel();
    interstitialAd?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    interstitialAd = AdmobInterstitial(
        adUnitId: AdsService.getInterstitialAdUnitId(),
        listener: (event, args) {
          if (event == AdmobAdEvent.closed) {
            ScreenshotController controller = watermarkController;
            subscription =
                Future.wait([_share(controller)]).asStream().listen((_) {
              setState(() => loading = false);
            });
          }
        });
    interstitialAd.load();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (loading) return false;
        return true;
      },
      child: Stack(
        children: <Widget>[
          Dialog(
              elevation: 0,
              backgroundColor: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                      color: Theme.of(context).primaryColor, width: 2),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(height: 24),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        'Коллаж с водяным знаком',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(height: 8),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 26),
                      child: Text(
                        'Для того, чтобы открыть доступ ко всем статьям, поделиться коллажем без водяного знака и отключить рекламу - приобретите премиум доступ к приложению',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(height: 8),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      child: CupertinoButton.filled(
                        padding: EdgeInsets.all(16),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            'Купить премиум за ${premiumService.premiumPrice} рублей',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        onPressed: loading ? null : () => buyPremium(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      child: CupertinoButton.filled(
                        padding: EdgeInsets.all(16),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            'Продолжить',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        onPressed: loading ? null : () => share(),
                      ),
                    ),
                    Container(height: 16),
                  ],
                ),
              )),
          loading ? LoadingContainer() : Container(),
        ],
      ),
    );
  }

  buyPremium() async {
    Navigator.pop(context);
    await premiumService.buyPremium();
    var event = AnalyticEvent.createStartPurchaseEvent(section: 'Фото');
    analytics.logEvent(name: event.name, parameters: event.parameters);
  }

  share({bool withWatermark = true}) async {
    setState(() => loading = true);
    if (withWatermark) {
      if (await interstitialAd.isLoaded) {
        interstitialAd.show();
      } else {
        ScreenshotController controller =
            withWatermark ? watermarkController : noWatermarkController;
        subscription = Future.wait([_share(controller)]).asStream().listen((_) {
          setState(() => loading = false);
        });
      }
    }
    /*RenderRepaintBoundary boundary = withWatermark
        ? watermarkCollageKey.currentContext.findRenderObject()
        : fullCollageKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage(pixelRatio: 4);
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    Share.file('title', 'name', pngBytes, 'image/png');*/
  }

  Future _share(ScreenshotController controller) async {
    widget.disableImage.add(true);
    //enableShareReceiving();
    File file = await controller.capture(
      pixelRatio: 4,
      delay: Duration(seconds: 3),
    );
//    //print(file.path);
//    //print(file.exists());
//    //print(file.readAsBytesSync());
    //await Future.delayed(Duration(seconds: 2));
    widget.disableImage.add(false);
    await Share.file('Поделиться коллажем', 'collage.png',
        file.readAsBytesSync(), 'image/*');
    var event = AnalyticEvent.shareCollageEvent(
        havePremium: premiumService.isPremium());
    analytics.logEvent(name: event.name, parameters: event.parameters);
    Navigator.pop(context);
    Navigator.pop(context);
    /*Share sh = Share.image(path: '${file.path}', title: 'title', text:'text',  mimeType: ShareType.TYPE_IMAGE);
    await sh.share();
    await Share.file(
      'Поделиться коллажем',
      'Коллаж',
      file.readAsBytesSync(),
      'image/png',
    );*/
  }

/* @override
  void receiveShare(Share shared) {
    debugPrint("Share received - $shared");
  }*/
}
