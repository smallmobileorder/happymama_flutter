import 'dart:io';
import 'dart:math';

import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/page/auth/email_register_page.dart';
import 'package:happy_mama/page/auth/email_reset_password_page.dart';
import 'package:happy_mama/page/home.dart';
import 'package:happy_mama/page/welcome/welcome_page.dart';
import 'package:happy_mama/service/firebase/auth/auth_error.dart';
import 'package:happy_mama/service/firebase/auth/auth_service.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/cast.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/brand_button.dart';
import 'package:happy_mama/widgets/brand_raw_button.dart';
import 'package:happy_mama/widgets/brand_text_field.dart';

import 'package:happy_mama/widgets/gradient_container.dart';
import 'package:happy_mama/widgets/loading_container.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _authService = AuthService();
  final _userService = UserService();

  final _emailTextController = TextEditingController();
  final _passwordTextController = TextEditingController();

  var isLoading = false;
  bool supportsAppleSignIn = false;

  @override
  void initState() {
    super.initState();
// this bool will be true if apple sign in is enabled
    checkAppleAuthSupport();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPage(context),
    );
  }

  Widget _buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        final height = constraints.biggest.height;
        return Stack(
          children: [
            Positioned.fill(
                child: Container(
              color: Color(0xFFF9F7F0),
            )),
            Positioned.fill(
                child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromRGBO(34, 202, 222, 0.54),
                    Colors.transparent
                  ],
                ),
              ),
            )),
            Positioned(
              left: 0,
              top: 0,
              right: 0,
              child: SvgPicture.asset('assets/vectors/toys.svg'),
            ),
            isLoading
                ? Center(child: LoadingContainer(color: ColorStyle.cyan))
                : Positioned.fill(
                    child: SingleChildScrollView(
                      padding: EdgeInsets.only(
                          top: max(0, min(232, height - (478 + 40 + 14))),
                          left: 28,
                          right: 28,
                          bottom: 40),
                      child: Column(
                        children: [
                          Center(
                            child: Stack(
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.only(top: 164, bottom: 12),
                                  child: GradientContainer(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: 30, left: 18, right: 18),
                                          child: Column(
                                            children: [
                                              BrandTextField(
                                                cursorColor: ColorStyle.cyan,
                                                borderColor: ColorStyle.cyan,
                                                maxLength: 50,
                                                controller:
                                                    _emailTextController,
                                                placeholder: 'E-mail',
                                              ),
                                              Container(height: 12),
                                              BrandTextField(
                                                cursorColor: ColorStyle.cyan,
                                                borderColor: ColorStyle.cyan,
                                                maxLength: 80,
                                                obscureText: true,
                                                controller:
                                                    _passwordTextController,
                                                placeholder: 'Пароль',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 16,
                                              left: 38,
                                              right: 38,
                                              bottom:
                                                  supportsAppleSignIn ? 8 : 48),
                                          child: SizedBox(
                                            height: 42,
                                            child: BrandButton(
                                                color: ColorStyle.cyan,
                                                onPressed: () {
                                                  final email =
                                                      _emailTextController.text
                                                          .trim();
                                                  final password =
                                                      _passwordTextController
                                                          .text
                                                          .trim();
                                                  if (email.isEmpty ||
                                                      password.isEmpty) {
                                                    showDialog(
                                                        context: context,
                                                        builder: (context) =>
                                                            InfoDialog(
                                                              title:
                                                                  'Поля не заполнены',
                                                              borderColor:
                                                                  ColorStyle
                                                                      .cyan,
                                                            ));
                                                    return;
                                                  }
                                                  _handleAuth(
                                                      _authService.signIn(
                                                          email: email,
                                                          password: password));
                                                },
                                                text: 'Войти'),
                                          ),
                                        ),
                                        if (supportsAppleSignIn)
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 8,
                                                left: 38,
                                                right: 38,
                                                bottom: 48),
                                            child: SizedBox(
                                              height: 42,
                                              child: AppleSignInButton(
                                                style: ButtonStyle.black,
                                                type: ButtonType.continueButton,
                                                onPressed: () => _handleAuth(
                                                    _authService.appleSignIn()),
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  child: Center(
                                    child: SvgPicture.asset(
                                      'assets/vectors/baby_sitting.svg',
                                      height: 189,
                                      width: 121,
                                    ),
                                  ),
                                ),
                                Positioned(
                                  left: 0,
                                  bottom: 0,
                                  right: 0,
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Expanded(child: Container()),
                                      Expanded(
                                        child: BrandRawButton(
                                          onPressed: () => _handleAuth(
                                              _authService.facebookSignIn()),
                                          color: Colors.transparent,
                                          child: SvgPicture.asset(
                                              'assets/vectors/fb_sign_in.svg'),
                                        ),
                                      ),
                                      Expanded(
                                        child: BrandRawButton(
                                          onPressed: () => _handleAuth(
                                              _authService.googleSignIn()),
                                          color: Colors.transparent,
                                          child: SvgPicture.asset(
                                              'assets/vectors/google_sign_in.svg'),
                                        ),
                                      ),
                                      Expanded(
                                        child: BrandRawButton(
                                          onPressed: () => _handleAuth(
                                              _authService.vkSignIn()),
                                          color: Colors.transparent,
                                          child: SvgPicture.asset(
                                              'assets/vectors/vk_sign_in.svg'),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          BrandButton(
                            onPressed: () => Navigator.of(context).push(
                                CupertinoPageRoute(
                                    builder: (_) => EmailRegisterPage(),
                                    settings:
                                        RouteSettings(name: 'Регистрация'))),
                            text: 'Регистрация',
                            textColor: ColorStyle.cyan,
                            color: Colors.transparent,
                          ),
                          BrandButton(
                            onPressed: () => Navigator.of(context).push(
                              CupertinoPageRoute(
                                  builder: (_) => EmailResetPasswordPage(),
                                  settings: RouteSettings(
                                      name: 'Восстановление пароля')),
                            ),
                            text: 'Забыли пароль?',
                            textColor: Colors.black.withOpacity(0.5),
                            color: Colors.transparent,
                          )
                        ],
                      ),
                    ),
                  )
          ],
        );
      }),
    );
  }

  _handleAuth(Future<FirebaseUser> future) {
    setState(() => isLoading = true);
    future.then((value) async {
      //print('*********** ${value.email}');
      final user = await _userService.fetchInfo();
      if (user.kids.isEmpty) {
        Navigator.of(context).pushReplacement(CupertinoPageRoute(
            builder: (context) => WelcomePage(parent: user),
            settings: RouteSettings(name: 'Экран приветствия')));
      } else {
        final bloc = BlocProvider.of<ProfileBloc>(context);
        await bloc.init(user);
        Navigator.of(context).pushReplacement(CupertinoPageRoute(
            builder: (context) => HomePage(),
            settings: RouteSettings(name: 'Домашняя страница')));
      }
    }, onError: (error) {
      final authError = cast<AuthError>(error);
      if (authError != null) {
        showDialog(
            context: context,
            builder: (context) => InfoDialog(
                  title: authError.message,
                  borderColor: ColorStyle.cyan,
                ));
        //print('*********** get auth error ${authError.message}');
      } else {
        //print('*********** get error $error');
      }
      setState(() => isLoading = false);
    });
  }

  Future<void> checkAppleAuthSupport() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var version = iosInfo.systemVersion;

      if (version.contains('13') == true) {
        setState(() => supportsAppleSignIn = true);
      }
    }
  }
}
