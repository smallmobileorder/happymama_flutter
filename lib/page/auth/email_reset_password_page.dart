import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/page/welcome/welcome_page.dart';
import 'package:happy_mama/service/firebase/auth/auth_error.dart';
import 'package:happy_mama/service/firebase/auth/auth_service.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/cast.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/brand_button.dart';
import 'package:happy_mama/widgets/brand_raw_button.dart';
import 'package:happy_mama/widgets/brand_text_field.dart';

import 'package:happy_mama/widgets/gradient_container.dart';
import 'package:happy_mama/widgets/loading_container.dart';

class EmailResetPasswordPage extends StatefulWidget {
  EmailResetPasswordPage({Key key}) : super(key: key);

  @override
  _EmailResetPasswordPageState createState() => _EmailResetPasswordPageState();
}

class _EmailResetPasswordPageState extends State<EmailResetPasswordPage> {
  final _authService = AuthService();

  final _emailTextController = TextEditingController();

  var isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPage(context),
    );
  }

  Widget _buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        final height = constraints.biggest.height;
        return Stack(
          children: [
            Positioned.fill(
                child: Container(
              color: Color(0xFFF9F7F0),
            )),
            Positioned.fill(
                child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromRGBO(34, 202, 222, 0.54),
                    Colors.transparent
                  ],
                ),
              ),
            )),
            Positioned(
              left: 0,
              top: 0,
              right: 0,
              child: SvgPicture.asset('assets/vectors/toys.svg'),
            ),
            isLoading
                ? Center(
                    child: LoadingContainer(
                    color: ColorStyle.cyan,
                  ))
                : Positioned.fill(
                    child: SingleChildScrollView(
                      padding: EdgeInsets.only(
                          top: max(0, min(100, height - 340 - 164 - 20)),
                          bottom: 0),
                      child: Column(
                        children: [
                          Center(
                            child: Stack(
                              children: [
                                Container(
                                  padding: EdgeInsets.only(left: 18, right: 18),
                                  child: Stack(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 164, bottom: 20),
                                        child: GradientContainer(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              Row(
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 22, top: 19),
                                                    child: SizedBox(
                                                      width: 12,
                                                      height: 20,
                                                      child: BrandRawButton(
                                                        onPressed: () =>
                                                            Navigator.of(
                                                                    context)
                                                                .maybePop(),
                                                        color:
                                                            Colors.transparent,
                                                        child: SvgPicture.asset(
                                                            'assets/vectors/left_chevron.svg'),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    top: 12,
                                                    left: 18,
                                                    right: 18),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 16,
                                                              vertical: 8),
                                                      child: Text(
                                                        'Введите вашу почту',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color: ColorStyle
                                                                .dirtyBlack),
                                                      ),
                                                    ),
                                                    BrandTextField(
                                                      maxLength: 50,
                                                      controller:
                                                          _emailTextController,
                                                      placeholder: 'E-mail',
                                                      borderColor:
                                                          ColorStyle.cyan,
                                                      cursorColor:
                                                          ColorStyle.cyan,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    top: 16,
                                                    left: 38,
                                                    right: 38,
                                                    bottom: 16),
                                                child: SizedBox(
                                                  height: 42,
                                                  child: BrandButton(
                                                    onPressed: () {
                                                      final email =
                                                          _emailTextController
                                                              .text;
                                                      if (email.isEmpty) {
                                                        return;
                                                      }

                                                      _handleReset(_authService
                                                          .resetPassword(
                                                              email: email));
                                                    },
                                                    text: 'Сбросить пароль',
                                                    color: ColorStyle.cyan,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: 0,
                                        left: 0,
                                        right: 0,
                                        child: Center(
                                          child: SvgPicture.asset(
                                            'assets/vectors/baby_sitting.svg',
                                            height: 189,
                                            width: 121,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
          ],
        );
      }),
    );
  }

  _handleReset(Future<void> future) {
    setState(() => isLoading = true);
    future.catchError((error) {
      print('ERROR ON PASSWORD RESET: $error');
    }).whenComplete(() {
      showDialog(
          context: context,
          builder: (context) => InfoDialog(
                title:
                    'Ссылка для сброса пароля отправлена на указанный e-mail',
                borderColor: ColorStyle.cyan,
              )).then((_) {
        Navigator.of(context).pop();
      });
    });
  }
}
