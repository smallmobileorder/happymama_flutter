import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/notification_bloc.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/deeplink_navigator.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';
import 'package:happy_mama/widgets/rate_dialog.dart';
import 'package:package_info/package_info.dart';
import 'package:rxdart/rxdart.dart';
import 'package:url_launcher/url_launcher.dart';

import 'baby/baby.dart';
import 'knowledge/knowledge_page.dart';
import 'calendar/calendar.page.dart';
import 'notification/notifications_list.dart';
import 'premium/premium_service.dart';
import 'premium/widgets/banner.dart';

final CupertinoTabController tabBarController =
    CupertinoTabController(initialIndex: 0);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

final DeeplinkNavigator deeplinkNavigator = DeeplinkNavigator();

final babyTab = GlobalKey<NavigatorState>();
final notificationTab = GlobalKey<NavigatorState>();
final knowledgeTab = GlobalKey<NavigatorState>();
final calendarTab = GlobalKey<NavigatorState>();
final premiumService = PremiumService();

final banner = AdsBanner();

final APP_STORE = 'http://itunes.apple.com/app/id1488980532';

final PLAY_MARKET =
    'https://play.google.com/store/apps/details?id=com.mummy.happy_mama';

final tabsName = [
  'Домашняя страница',
  'Уведомления',
  'База знаний',
  'Календарь'
];

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with RouteAware {

  final FirebaseAnalyticsObserver observer1 =
  FirebaseAnalyticsObserver(analytics: analytics);
  final FirebaseAnalyticsObserver observer2 =
  FirebaseAnalyticsObserver(analytics: analytics);
  final FirebaseAnalyticsObserver observer3 =
  FirebaseAnalyticsObserver(analytics: analytics);
  final FirebaseAnalyticsObserver observer4 =
  FirebaseAnalyticsObserver(analytics: analytics);
  final StreamController<bool> isBabyPageHidden = StreamController();
  final UserService userService = UserService();
  PublishSubject<bool> knowledgePageIsClose;
  NotificationBloc bloc;
  ProfileBloc profileBloc;
  int _pushAmount;
  StreamSubscription rateSubscription;
  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void initState() {
    super.initState();
    tabBarController.addListener(_sendCurrentTabToAnalytics);
    WidgetsBinding.instance.addPostFrameCallback((c) async {
      //print('CALLBACK');
      await checkVersion(context);

      profileBloc = BlocProvider.of<ProfileBloc>(context);
      bool rated =
          await userService.checkIfRated(profileBloc.parent.identifier);
      if (rated) {
        //print('ALREADY REATED, SKIP RATING');
        return;
      }
      //print('NOT RATED, CHECKING LAST RATE TIME');
      // get last rate time
      DateTime last = await StorageService.getLastRateTime();
      if (last == null) {
        StorageService.saveLastRateTime();
        return;
      } else {
        //print(
        //"LAST = ${last.toIso8601String()}, NOW = ${DateTime.now().toIso8601String()}");
        // if last rating was less than 48 hours then dont show rate dialog
        //print('DIFFERENCE IS ' +
        //DateTime.now().difference(last).inHours.toString());
        if (DateTime.now().difference(last).inHours < 48) {
          //print('DIFFERENCE IN RATE IS LESS THAN WE NEED, NO SHOW');

          /// CHECK IF WE NEED TO SHOW LINK TO MARKETS
          last = await StorageService.getLastMarketRateTime();
          if (last != null && DateTime.now().difference(last).inDays < 30) {
            //print('DIFFERENCE IN MARKET RATE IS LESS, NO SHOW');
            return;
          }
          //print('SHOW MARKET RATE DIALOG');
          showDialog(
            context: context,
            builder: (context) => CustomCupertinoAlertDialog(
              title: Text('Нравится HappyMama?'),
              content: Text(
                  'Найдите время оставить ваш отзыв в ${Platform.isIOS ? "AppStore" : "PlayMarket"}'),
              actions: <Widget>[
                CustomCupertinoDialogAction(
                  child: Text('Позже'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                CustomCupertinoDialogAction(
                  child: Text('Оценить'),
                  isDefaultAction: true,
                  onPressed: () {
                    _launchURL(Platform.isIOS ? APP_STORE : PLAY_MARKET);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
          StorageService.setLastMarketRateTime();
          return;
        }
        //print('DIFFERENCE IN RATE TIME IS BIGGER THAN WE NEED, '
        //'SHOW RATE DIALOG');
      }
      // else show rate dialog and save time
      rateSubscription =
          Future.delayed(Duration(seconds: 3)).asStream().listen((_) {
        showDialog(context: context, builder: (context) => RateDialog());
        StorageService.saveLastRateTime();
      });
    });

    _pushAmount = 0;
    knowledgePageIsClose = PublishSubject();

    initBloc();
    initFlutterLocalNotification();
    initPremiumService();
    //tabBarController.addListener(returnToFirstPage)
  }

  @override
  dispose() async {
    super.dispose();
    isBabyPageHidden?.close();
    knowledgePageIsClose?.close();
    rateSubscription?.cancel();
    _purchaseUpdatedSubscription?.cancel();
    _purchaseUpdatedSubscription = null;
    _purchaseErrorSubscription?.cancel();
    _purchaseErrorSubscription = null;
    observer.unsubscribe(this);
    await premiumService?.dispose();
  }

  @override
  void didPush() {
    print('DID PUSH');
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    print('DID POP NEXT');
    _sendCurrentTabToAnalytics();
  }

  initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context).notifyBloc;
  }

  initFlutterLocalNotification() {
    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        /* onDidReceiveLocalNotification: _onDidReceiveLocalNotification*/);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onSelectNotification);
    flutterLocalNotificationsPlugin
        .getNotificationAppLaunchDetails()
        .then((v) async {
      ////print('status ${bloc.wasOpenedFromPush}');
      if (v.didNotificationLaunchApp && !bloc.wasOpenedFromPush) {
        bloc.wasOpenedFromPush = true;
        tabBarController.index = 1;
      } else {
        tabBarController.index = 0;
      }
      ////print('status3 ${bloc.wasOpenedFromPush}');
    });
  }

  @override
  Widget build(BuildContext context) {
    banner.init(context);
    return WillPopScope(
      onWillPop: () async {
        //print('home will pop');
//        if (history.isNotEmpty) {
//          //print(history);
//          int tab = history.removeLast();
//          if (tabBarController.index == 2) {
//            deeplinkNavigator.path;
//            if (knowledgeTab?.currentState?.canPop() ?? false) {
//              knowledgeTab.currentState.popUntil((route) => route.isFirst);
//            }
//          }
//          if (tab == 2) {
//            if (knowledgeTab?.currentState?.canPop() ?? false) {
//              knowledgeTab.currentState.popUntil((route) => route.isFirst);
//            }
//          }
//          tabBarController.index = tab;
//          return Future.value(false);
//        }
        switch (tabBarController.index) {
          case 0:
            if (history.isNotEmpty) {
              int tab = history.last;
              history.removeLast();
              deeplinkNavigator.setKnowledgePathIfNecessary(tab);
              tabBarController.index = tab;
              return Future.value(false);
            }
            return !await babyTab.currentState.maybePop();
            break;
          case 1:
            if (notificationTab.currentState.canPop()) {
              return !await notificationTab.currentState.maybePop();
            } else {
              tabBarController.index = 0;
              return Future.value(false);
            }
            break;
          case 2:
            if (knowledgeTab.currentState.canPop()) {
              return !await knowledgeTab.currentState.maybePop();
            } else {
              tabBarController.index = 0;
              return Future.value(false);
            }
            break;
          case 3:
            if (calendarTab.currentState.canPop()) {
              return !await calendarTab.currentState.maybePop();
            } else {
              if (history.isNotEmpty) {
                int tab = history.last;
                history.removeLast();
                deeplinkNavigator.setKnowledgePathIfNecessary(tab);
                tabBarController.index = tab;
                return Future.value(false);
              }
              tabBarController.index = 0;
              return Future.value(false);
            }
            break;
        }
        return await Future<bool>.value(true);
      },
      child: CupertinoTabScaffold(
          controller: tabBarController,
          resizeToAvoidBottomInset: false,
          tabBar: CupertinoTabBar(
            onTap: (index) => returnToFirstPage(index: index),
            iconSize: 29.0,
            backgroundColor: Color(0xFFFFFFFF),
            items: [
              BottomNavigationBarItem(
                activeIcon: SvgPicture.asset(
                  'assets/vectors/baby.svg',
                  color: Theme.of(context).primaryColor,
                ),
                icon: SvgPicture.asset(
                  'assets/vectors/baby.svg',
                ),
              ),
              BottomNavigationBarItem(
                activeIcon: notificationIcon(
                  color: Theme.of(context).primaryColor,
                ),
                icon: notificationIcon(),
              ),
              BottomNavigationBarItem(
                activeIcon: SvgPicture.asset(
                  'assets/vectors/book.svg',
                  color: Theme.of(context).primaryColor,
                ),
                icon: SvgPicture.asset('assets/vectors/book.svg'),
              ),
              BottomNavigationBarItem(
                activeIcon: SvgPicture.asset(
                  'assets/vectors/calendar.svg',
                  color: Theme.of(context).primaryColor,
                ),
                icon: SvgPicture.asset('assets/vectors/calendar.svg'),
              ),
            ],
          ),
          tabBuilder: (BuildContext context, int index) {
            assert(index >= 0 && index <= 3);
            if (index != 0) {
              isBabyPageHidden.add(true);
            }
            //print('history: $history');
            switch (index) {
              case 0:
                return Theme(
                    // Create a unique theme with "ThemeData"
                    data: Platform.isIOS ? iosTheme() : androidTheme(),
                    child: CupertinoTabView(
                        navigatorKey: babyTab,
                        navigatorObservers: <NavigatorObserver>[observer1],
                        builder: (BuildContext context) => BabyPage(
                              isPageHidden: isBabyPageHidden.stream,
                            )));
                break;
              case 1:
                return Theme(
                    // Create a unique theme with "ThemeData"
                    data: Platform.isIOS ? iosTheme() : androidTheme(),
                    child: CupertinoTabView(
                      navigatorKey: notificationTab,
                      navigatorObservers: <NavigatorObserver>[observer2],
                      builder: (BuildContext context) => NotificationsList(),
                    ));
                break;
              case 2:
                return Theme(
                    // Create a unique theme with "ThemeData"
                    data: Platform.isIOS ? iosTheme() : androidTheme(),
                    child: CupertinoTabView(
                      navigatorKey: knowledgeTab,
                      navigatorObservers: <NavigatorObserver>[observer3],
                      builder: (BuildContext context) => KnowledgePage(
                        tabChangeStream: knowledgePageIsClose.stream,
                      ),
                    ));
                break;
              case 3:
                return Theme(
                    // Create a unique theme with "ThemeData"
                    data: Platform.isIOS ? iosTheme() : androidTheme(),
                    child: CupertinoTabView(
                      navigatorKey: calendarTab,
                      navigatorObservers: <NavigatorObserver>[observer4],
                      builder: (BuildContext context) => CalendarPage(
                        tabChanged: knowledgePageIsClose.stream,
                      ),
                    ));
                break;
            }
            return null;
          }),
    );
  }

  Widget notificationIcon({Color color}) {
    return StreamBuilder(
        initialData: 0,
        builder: (context, snapshot) {
          return Container(
            height: 33,
            width: 33,
            //alignment: Alignment.center,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 29,
                  width: 29,
                  child: SvgPicture.asset(
                    'assets/vectors/envelope.svg',
                    color: color,
                  ),
                  alignment: Alignment.bottomCenter,
                ),
                notificationAmount()
              ],
            ),
          );
        });
  }

  Widget notificationAmount() {
    return StreamBuilder(
        stream: bloc?.unreadAmount ?? null,
        key: ValueKey(_pushAmount),
        initialData: _pushAmount,
        builder: (context, snapshot) {
          if (snapshot.hasData) _pushAmount = snapshot.data;
          return snapshot.hasData && snapshot.data > 0
              ? Positioned(
                  right: 0.0,
                  top: 0.0,
                  key: ValueKey(snapshot.data),
                  child: Container(
                    height: 15,
                    width: 15,
                    //padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                    child: Text(
                      '${snapshot.data}',
                      style: TextStyle(color: Colors.white, fontSize: 11),
                    ),
                    alignment: Alignment.center,
                  ),
                )
              : Container();
        });
  }

  Future _onSelectNotification(String payload) async {
    if (notificationTab?.currentState?.canPop() ?? false) {
      await notificationTab?.currentState?.maybePop();
    }
    tabBarController.index = 1;
  }

  void returnToFirstPage({int index}) {
    //switch(tabBarController.index){
    //case 0:
    //print(index);
    //print('CLEAR HISTORY OF DEEPLINK NAVIGATOR');
    history.clear();
    deeplinkNavigator.clearKnowledgePaths();
    deeplinkNavigator.path;
    knowledgePageIsClose.add(true);
    //print('why close?');
    isBabyPageHidden.add(true);
    if (babyTab?.currentState?.canPop() ?? false) {
      babyTab.currentState.popUntil((route) => route.isFirst);
    }
    //break;
    //case 1:
    if (notificationTab?.currentState?.canPop() ?? false) {
      notificationTab.currentState.popUntil((route) => route.isFirst);
    }
    //break;
    //case 2:
    if (knowledgeTab?.currentState?.canPop() ?? false) {
      knowledgeTab.currentState.popUntil((route) => route.isFirst);
    }
    //break;
    //case 3:
    if (calendarTab?.currentState?.canPop() ?? false) {
      calendarTab.currentState.popUntil((route) => route.isFirst);
    }
    //break;

    //}
  }

  /// Version checking
  Future checkVersion(BuildContext context) async {
    //Get Current installed version of app
    final PackageInfo info = await PackageInfo.fromPlatform();
    //print(
    //"INFO: ${info.version}, ${info.buildNumber}, ${info.appName}, ${info.packageName}");
    int currentVersion = int.parse(info.version.trim().replaceAll(".", ""));
    //print("CURRENT VERSION IS $currentVersion");

    //Get Latest version info from firebase config
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    //Get version for current platform
    String platform;
    if (Platform.isIOS) {
      platform = 'ios_version';
    } else if (Platform.isAndroid) {
      platform = 'android_version';
    } else {
      //print('unknown platform');
    }

    try {
      // Using default duration to force fetching from remote server.
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      String version = remoteConfig.getString(platform);

      // get minimal and current version
      int configCurrentVersion;
      int configMinimalVersion;
      try {
        Map<String, dynamic> json = jsonDecode(version);
        json = json.cast<String, String>();
        configCurrentVersion =
            int.parse(json['current'].trim().replaceAll('.', ''));
        configMinimalVersion =
            int.parse(json['minimal'].trim().replaceAll('.', ''));
      } catch (e) {
        //print('Error on parsing json from config. '
        //'Config: "$version", Errror: $e');
      }
      //print("CONFIG MINIMAL VERSION IS $configMinimalVersion");
      //print("CONFIG CURRENT VERSION IS $configCurrentVersion");
      if (configMinimalVersion > currentVersion) {
        await showNewVersionDialog(context, forced: true);
      } else if (configCurrentVersion > currentVersion) {
        await showNewVersionDialog(context);
      }
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      //print(exception);
    } catch (exception) {
      //print('Unable to fetch remote config. '
      //'Cached or default values will be used');
    }
  }

  showNewVersionDialog(BuildContext context, {bool forced = false}) async {
    if (forced) {
      await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => WillPopScope(
          onWillPop: () async => false,
          child: CustomCupertinoAlertDialog(
            title: Text(
                'Версия вашего приложения устарела. Для работы требуется его обновить.'),
            actions: <Widget>[
              CustomCupertinoDialogAction(
                child: Text('Обновить'),
                isDefaultAction: true,
                onPressed: () {
                  _launchURL(Platform.isIOS ? APP_STORE : PLAY_MARKET);
                },
              ),
            ],
          ),
        ),
      );
    } else {
      await showDialog(
        context: context,
        builder: (context) => CustomCupertinoAlertDialog(
          title: Text(
              'Версия вашего приложения устарела. Для работы требуется его обновить.'),
          actions: <Widget>[
            CustomCupertinoDialogAction(
              child: Text('Позже'),
              isDefaultAction: false,
              onPressed: () {
                //print('UPDATE LATER');
                Navigator.pop(context);
              },
            ),
            CustomCupertinoDialogAction(
              child: Text('Обновить'),
              isDefaultAction: true,
              onPressed: () {
                _launchURL(Platform.isIOS ? APP_STORE : PLAY_MARKET);
              },
            ),
          ],
        ),
      );
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      //print('Could not launch $url');
    }
  }

  void initPremiumService() async {
    await premiumService
        .initPlatformState()
        .whenComplete(() => setState(() {}));
    _purchaseUpdatedSubscription =
        FlutterInappPurchase.purchaseUpdated.listen((productItem) async {
      //print('purchase update');
      await FlutterInappPurchase.instance.finishTransaction(productItem,
          isConsumable: false,
          developerPayloadAndroid: productItem.developerPayloadAndroid);
      if (productItem.productId == PremiumService.premiumId) {
        await premiumService.getPurchases();
        setState(() {});
        showDialog(
            context: context,
            builder: (_) => CustomCupertinoAlertDialog(
                  title: Text('Поздравляем с покупкой премиума!'),
                  actions: <Widget>[
                    CustomCupertinoDialogAction(
                      isDefaultAction: true,
                      child: Text('ОК'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                ));
      }
    });
    _purchaseErrorSubscription =
        FlutterInappPurchase.purchaseError.listen((purchaseError) {
      showDialog(
          context: context,
          builder: (_) => CustomCupertinoAlertDialog(
                title: Text('При покупке премиума произошла ошибка'),
                actions: <Widget>[
                  CustomCupertinoDialogAction(
                    isDefaultAction: true,
                    child: Text('ОК'),
                    onPressed: () => Navigator.pop(context),
                  )
                ],
              ));
    });
  }

  void _sendCurrentTabToAnalytics() {
    //print('SEND TAB ${tabBarController.index}');
    observer.analytics.setCurrentScreen(
      screenName: tabsName[tabBarController?.index ?? 0],
    );
  }
}

ThemeData androidTheme() {
  return ThemeData(
    fontFamily: 'Montserrat',
    scaffoldBackgroundColor: Colors.white,
    accentColor: themeMainColor,
    buttonColor: themeMainColor,
    textSelectionHandleColor: themeMainColor,
    primaryColor: themeMainColor,
    cupertinoOverrideTheme: CupertinoThemeData(
      scaffoldBackgroundColor: Colors.white,
      primaryColor: themeMainColor,
      barBackgroundColor: Colors.white,
      textTheme: CupertinoTextThemeData(
        navTitleTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
        navActionTextStyle: TextStyle(
          color: themeMainColor,
        ),
      ),
    ),
    buttonTheme: ButtonThemeData(
        disabledColor: ColorStyle.disabled,
        buttonColor: themeMainColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        textTheme: ButtonTextTheme.normal),
    pageTransitionsTheme: PageTransitionsTheme(builders: {
      TargetPlatform.android: CupertinoPageTransitionsBuilder(),
    }),
  );
}

ThemeData iosTheme({
  bool transparentCanvas = false,
}) {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    cupertinoOverrideTheme: CupertinoThemeData(
      scaffoldBackgroundColor: Colors.white,
      primaryColor: themeMainColor,
      barBackgroundColor: Colors.white,
    ),
    fontFamily: 'Montserrat',
    accentColor: themeMainColor,
    buttonColor: themeMainColor,
    textSelectionHandleColor: themeMainColor,
    primaryColor: themeMainColor,
    canvasColor: transparentCanvas ? Colors.transparent : null,
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
        elevation: 0.0, highlightElevation: 0.0, hoverElevation: 0.0),
    buttonTheme: ButtonThemeData(
        disabledColor: ColorStyle.disabled,
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        buttonColor: themeMainColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        textTheme: ButtonTextTheme.normal),
    pageTransitionsTheme: PageTransitionsTheme(builders: {
      TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
    }),
  );
}
