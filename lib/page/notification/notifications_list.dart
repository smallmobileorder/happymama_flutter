import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/notification_bloc.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/push_notification.dart';
import 'package:happy_mama/page/premium/ads_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/empty_screen.dart';
import 'package:happy_mama/widgets/loading_container.dart';
import 'package:rxdart/rxdart.dart';

import '../home.dart';
import 'bookmarks_page.dart';
import 'notification_page.dart';
import 'widgets/notification_tile.dart';

class NotificationsList extends StatefulWidget {
  NotificationsList({Key key}) : super(key: key);

  @override
  _NotificationsListState createState() => _NotificationsListState();
}

class _NotificationsListState extends State<NotificationsList> {
  NotificationBloc bloc;

  BehaviorSubject<int> tab = BehaviorSubject();
  int currentTab = 0;

  PageController pageController;
  AdmobInterstitial pushInterstitialAd;

  @override
  void initState() {
    super.initState();
    if (!premiumService.isPremium()) {
      pushInterstitialAd = AdmobInterstitial(
          adUnitId: AdsService.getInterstitialAdUnitId(),
          listener: (event, args) {
            if (event == AdmobAdEvent.loaded) {
              pushInterstitialAd.show();
            }
            if (event == AdmobAdEvent.closed) {}
          });
      pushInterstitialAd.load();
    }
    tab.add(currentTab);
    pageController = PageController(initialPage: currentTab);
    initBloc();
  }

  @override
  void dispose() {
    tab.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //TODO USE PAGE_WISE INSTEAD OF STANDARD LIST_VIEW
    return StreamBuilder<bool>(
        stream: bloc.firstLoad,
        //initialData: false,
        builder: (context, snapshotLoad) {
          if (snapshotLoad?.data ?? false)
            return Center(
              child: LoadingContainer(),
            );
          return SafeArea(
            child: StreamBuilder<List<PushNotification>>(
                stream: bloc.notifications,
                initialData: null,
                builder: (context, snapshot) {
                  return StreamBuilder<int>(
                      stream: tab,
                      initialData: 0,
                      builder: (context, tabData) {
                        return Scaffold(
                            appBar: (!snapshot.hasData ||
                                        snapshot.hasData &&
                                            (snapshot?.data?.length == 0) ??
                                    true)
                                ? null
                                : AppBar(
                                    backgroundColor: Colors.white,
                                    elevation: 0.0,
                                    title: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        CupertinoButton(
                                          padding: EdgeInsets.only(right: 16),
                                          child: Text(
                                            'Все',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600,
                                                color: tabData.data == 0
                                                    ? ColorStyle.tint
                                                    : ColorStyle.codGray,
                                                fontFamily: 'Montserrat'),
                                          ),
                                          onPressed: () => changeTab(0),
                                        ),
                                        CupertinoButton(
                                          padding: EdgeInsets.only(left: 10),
                                          child: Text(
                                            'Непрочитанные',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600,
                                                color: tabData.data == 1
                                                    ? ColorStyle.tint
                                                    : ColorStyle.codGray,
                                                fontFamily: 'Montserrat'),
                                          ),
                                          onPressed: () => changeTab(1),
                                        )
                                      ],
                                    ),
                                    actions: <Widget>[
                                      CupertinoButton(
                                        padding: EdgeInsets.only(right: 16),
                                        child: Text(
                                          'Закладки',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600,
                                              color: ColorStyle.codGray,
                                              fontFamily: 'Montserrat'),
                                        ),
                                        onPressed: () => goToBookmarkPage(),
                                      )
                                    ],
                                  ),
                            body: Stack(children: <Widget>[
                              Positioned(
                                  bottom: 0,
                                  left: 0,
                                  right: 0,
                                  child: premiumService.isPremium()
                                      ? Container()
                                      : banner.getWidget()),
                              Positioned(
                                  left: 0,
                                  right: 0,
                                  top: 0,
                                  bottom: premiumService.isPremium() ? 0 : 60,
                                  child: notificationBody(snapshot, tabData))
                            ]));
                      });
                }),
          );
        });
  }

  void initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context).notifyBloc;
  }

  openNotificationPage(PushNotification push) {
    bloc.readNotification(push);
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => NotificationPage(
              notification: push,
              addToBookmark: () => bloc.addToBookmark(push),
            ),
        settings: RouteSettings(name: 'Уведомление. ${push.title}')));
  }

  notificationBody(AsyncSnapshot<List<PushNotification>> snapshot,
      AsyncSnapshot<int> tabData) {
    if (snapshot.hasData) {
      if (snapshot.data.length == 0) {
        return EmptyScreen(
          textBeforePic: 'У вас пока нет\nуведомлений',
        );
      }
      List<PushNotification> unreadNote = bloc.getUnreadNotifications();
      return PageView(
        children: <Widget>[
          ListView(
            padding: EdgeInsets.only(left: 16, right: 16),
            children: List.generate(
              snapshot.data.length,
              (index) => Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: NotificationTile(
                  notification: snapshot.data[index],
                  onTap: () => openNotificationPage(snapshot.data[index]),
                ),
              ),
            ),
          ),
          ListView(
            padding: EdgeInsets.only(left: 16, right: 16),
            children: List.generate(
              unreadNote.length,
              (index) => Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: NotificationTile(
                  notification: unreadNote[index],
                  onTap: () => openNotificationPage(unreadNote[index]),
                ),
              ),
            ),
          )
        ],
        controller: pageController,
        onPageChanged: (page) => changeTab(page),
      );
    }
    return EmptyScreen(
      textBeforePic: 'У вас пока нет\nуведомлений',
    );
  }

  changeTab(int newTab) {
    if (pageController == null && !pageController.hasClients) {
      return;
    }
    if ((newTab == 0 || newTab == 1) && currentTab != newTab) {
      currentTab = newTab;
      pageController.animateToPage(currentTab,
          duration: Duration(milliseconds: 400), curve: Curves.easeInOut);
      tab.add(currentTab);
    }
  }

  goToBookmarkPage() {
    Navigator.of(context)
        .push(CupertinoPageRoute(builder: (_) => BookmarksPage()));
  }
}
