import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/entity/push_notification.dart';
import 'package:happy_mama/service/firebase/analytics/events.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/widgets/brand_button.dart';

import '../../application.dart';
import '../home.dart';

class NotificationPage extends StatefulWidget {
  final PushNotification notification;
  final Function addToBookmark;

  const NotificationPage({Key key, this.notification, this.addToBookmark})
      : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  String get getButtonLabel =>
      'Перейти в раздел ${widget.notification.id} ${Formatter.monthSuffix(widget.notification.id)} и отметить достижения';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CupertinoNavigationBar(
        padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
        middle: Text(widget.notification.title),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            CupertinoButton(
              child: SvgPicture.asset(
                'assets/vectors/share.svg',
                color: ColorStyle.tint,
              ),
              padding: EdgeInsets.zero,
              onPressed: shareNotification,
              //color: color5,
            ),
            CupertinoButton(
              child: SvgPicture.asset(
                'assets/vectors/star_${widget.notification.bookmark ? 'shadow' : 'empty'}.svg',
                color: ColorStyle.tint,
              ),
              padding: EdgeInsets.zero,
              onPressed: () {
                if (widget.notification.bookmark == true) {
                  var event = AnalyticEvent.createBookmarkEvent(
                      section: 'Уведомления',
                      category: widget.notification.category,
                      objectTitle: widget.notification.title);
                  analytics.logEvent(
                      name: event.name, parameters: event.parameters);
                }
                widget.addToBookmark();
                setState(() {});
              },
              //color: color5,
            )
          ],
        ),
        backgroundColor: Colors.white,
        border: Border(
            bottom: BorderSide(
                color: ColorStyle.tint, width: 0.0, style: BorderStyle.solid)),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 24, left: 32, right: 32),
        child: Column(
          children: <Widget>[
            if (widget.notification.category == 'birthday')
              SvgPicture.asset('assets/vectors/balloons.svg'),
            if (widget.notification.category == 'attention')
              Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: SvgPicture.asset('assets/vectors/attention_big.svg')),
            Text(widget.notification.body,
                // textAlign: TextAlign.center,
                style: TextStyle(color: ColorStyle.tundora, fontSize: 17)),
//            Container(height: 48,),
//            if (widget.notification.category == 'birthday')
//              BrandButton.unfilled(
//                margin: EdgeInsets.only(top: 32),
//                onPressed: openSection,
//                text: getButtonLabel,
//              ),
            if (widget.notification.buttonPath != null &&
                widget.notification.buttonPath.isNotEmpty)
              BrandButton.unfilled(
                  margin: EdgeInsets.only(top: 32, bottom: 8),
                  onPressed: openSection,
                  text: widget.notification.buttonName ?? 'Подробнее'),
          ],
        ),
      ),
    );
  }

  shareNotification() async {
    try {
      String text = 'Из приложения HappyMama: ' +
          widget.notification.title +
          '\n\n' +
          widget.notification.body +
          '\n\nСкачать приложение можно по ссылке:' +
          '\nAndroid: $PLAY_MARKET' +
          '\niOS: $APP_STORE';
      Clipboard.setData(ClipboardData(text: text));
      Share.text('Поделиться', text, 'text/plain');
      var event = AnalyticEvent.createShareEvent(
          section: 'Уведомления', objectTitle: widget.notification.title);
      analytics.logEvent(name: event.name, parameters: event.parameters);
    } catch (e) {
      //print('error: $e');
    }
  }

  openSection() {
    deeplinkNavigator.parsePathAndNavigate(widget.notification.buttonPath,
        prevTab: 1)();
  }
}
