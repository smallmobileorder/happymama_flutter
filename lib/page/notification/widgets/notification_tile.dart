import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/entity/push_notification.dart';
import 'package:happy_mama/style/color_style.dart';

class NotificationTile extends StatelessWidget {
  final PushNotification notification;
  final Function onTap;

  const NotificationTile({Key key, this.notification, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(8),
      child: Ink(
        //margin: EdgeInsets.only(top: 8, bottom: 8),
        padding: EdgeInsets.all(16),

        decoration: BoxDecoration(
            color: notification.notRead
                ? ColorStyle.tint
                : notification.backgroundColor,
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 0.5),
                  color: Colors.black.withOpacity(0.3),
                  blurRadius: 5)
            ],
            borderRadius: BorderRadius.circular(8)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                if (notification.category == 'everyday')
                  SvgPicture.asset(
                    'assets/vectors/clock.svg',
                    color: notification.notRead
                        ? Colors.white
                        : notification.iconColor,
                    height: 22,
                    width: 22,
                  ),
                if (notification.category == 'birthday')
                  SvgPicture.asset(
                    'assets/vectors/gift.svg',
                    height: 22,
                    width: 22,
                  ),
                if (notification.category == 'attention')
                  SvgPicture.asset(
                    'assets/vectors/attention_small.svg',
                    height: 22,
                    width: 22,
                    color: notification.notRead
                        ? Colors.white
                        : notification.iconColor,
                  ),
                if (notification.category != 'standard')
                  Container(
                    width: 10,
                  ),
                Text(
                  notification.title,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: notification.notRead
                          ? Colors.white
                          : notification.titleTextColor),
                ),
                Expanded(
                  child: Container(),
                ),
                if (notification.bookmark)
                  SvgPicture.asset(
                    'assets/vectors/star_filled.svg',
                    color: ColorStyle.tint,
                    height: 22,
                    width: 22,
                  ),
              ],
            ),
            Container(
              height: 6,
            ),
            Text(
              notification.body,
              style: TextStyle(
                  fontSize: 17,
                  color: notification.notRead
                      ? Colors.white
                      : notification.bodyTextColor),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            )
          ],
        ),
      ),
    );
  }
}
