import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/notification_bloc.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/push_notification.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/empty_screen.dart';

import 'notification_page.dart';
import 'widgets/notification_tile.dart';

class BookmarksPage extends StatefulWidget {
  @override
  _BookmarksPageState createState() => _BookmarksPageState();
}

class _BookmarksPageState extends State<BookmarksPage> {
  NotificationBloc bloc;

  @override
  void initState() {
    super.initState();
    initBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Закладки'),
        backgroundColor: Colors.white,
        border: Border(
            bottom: BorderSide(
                color: ColorStyle.tint, width: 0.0, style: BorderStyle.solid)),
      ),
      body: StreamBuilder<List<PushNotification>>(
          stream: bloc.notifications,
          initialData: [],
          builder: (context, snapshot) {
            if (snapshot.data == null || snapshot.data.isEmpty)
              return EmptyScreen(
                textBeforePic: 'У вас пока нет закладок',
                textAfterPic: '',
              );
            var bookmarksList =
                snapshot.data.where((f) => f.bookmark)?.toList() ?? [];
            if (bookmarksList.isEmpty)
              return EmptyScreen(
                textBeforePic: 'У вас пока нет закладок',
                textAfterPic: '',
              );
            return ListView(
              padding: EdgeInsets.only(left: 16, right: 16),
              children: List.generate(
                bookmarksList.length,
                (index) => Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: NotificationTile(
                    notification: bookmarksList[index],
                    onTap: () => openNotificationPage(bookmarksList[index]),
                  ),
                ),
              ),
            );
          }),
    );
  }

  void initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context).notifyBloc;
  }

  openNotificationPage(PushNotification push) {
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => NotificationPage(
              notification: push,
              addToBookmark: () => bloc.addToBookmark(push),
            ),
        settings: RouteSettings(name: 'Уведомление. ${push.title}')));
  }
}
