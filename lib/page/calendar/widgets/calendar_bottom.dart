import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/page/calendar/calendar.page.dart';

class CalendarBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getH(139, context),
      child: Stack(
        children: <Widget>[
          Center(
            child: SvgPicture.asset(
              'assets/vectors/baby_crawling.svg',
              height: getH(139, context),
              width: getW(154, context),
            ),
          ),

          /// Bottom left
          Positioned(
            left: getW(18, context),
            bottom: getH(41, context),
            child: Transform.rotate(
              angle: -18.46,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                height: 38,
              ),
            ),
          ),

          /// Bottom left 2
          Positioned(
            left: getW(75, context),
            bottom: getH(25, context),
            child: Transform.rotate(
              angle: -18.46,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                height: 16,
              ),
            ),
          ),

          /// right star (3)
          Positioned(
            right: getW(17, context),
            bottom: getH(28, context),
            child: Transform.rotate(
              angle: 30.63,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                width: 31,
              ),
            ),
          ),

          /// right star (2)
          Positioned(
            right: getW(41, context),
            bottom: getH(66, context),
            child: Transform.rotate(
              angle: 30.63,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                width: 17,
              ),
            ),
          ),

          /// right star (1)
          Positioned(
            right: getW(76, context),
            bottom: getH(49, context),
            child: Transform.rotate(
              angle: 30.63,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                width: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
