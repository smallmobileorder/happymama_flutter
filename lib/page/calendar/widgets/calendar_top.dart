import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/page/calendar/calendar.page.dart';

class CalendarTop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getH(100, context),
      child: Stack(
        children: <Widget>[
          Center(
              child: Text(
            'Развитие по\nмесяцам',
            style: TextStyle(
              fontSize: getW(22, context),
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          )),

          /// Bottom right
          Positioned(
            right: getW(18, context),
            bottom: getH(31, context),
            child: Transform.rotate(
              angle: 15.46,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                height: 38,
              ),
            ),
          ),

          /// Bottom right 2
          Positioned(
            right: getW(75, context),
            bottom: getH(15, context),
            child: Transform.rotate(
              angle: 15.46,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                height: 16,
              ),
            ),
          ),

          /// left star (3)
          Positioned(
            left: getW(17, context),
            bottom: getH(18, context),
            child: Transform.rotate(
              angle: 15.63,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                width: 31,
              ),
            ),
          ),

          /// left star (2)
          Positioned(
            left: getW(41, context),
            bottom: getH(66, context),
            child: Transform.rotate(
              angle: -15.63,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                width: 17,
              ),
            ),
          ),

          /// left star (1)
          Positioned(
            left: getW(76, context),
            bottom: getH(49, context),
            child: Transform.rotate(
              angle: -15.63,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Theme.of(context).primaryColor,
                width: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
