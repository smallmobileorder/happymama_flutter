import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../calendar.page.dart';

class CalendarTopMonth extends StatelessWidget {
  final int month;
  final Function onPressed;
  final double height = 80;

  const CalendarTopMonth({@required this.month, this.onPressed});

  @override
  Widget build(BuildContext context) {
    final offset = getH(20, context);
    return Container(
      height: getH(height, context) + offset,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            top: offset,
            child: Container(
              height: getH(height, context),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 4,
                    color: Color.fromRGBO(0, 0, 0, 0.25),
                    offset: Offset(0, 2),
                  ),
                ],
              ),
            ),
          ),

          /// Text
          Positioned(
            left: 0,
            right: 0,
            top: offset,
            bottom: 0,
            child: Center(
                child: Text(
              'Календарь',
              style: TextStyle(
                fontSize: getW(22, context),
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            )),
          ),

          /// left bottom star
          ///

          Positioned(
            left: getW(12, context),
            bottom: getH(13, context) + offset,
            height: 11,
            width: 11,
            child: Transform.rotate(
              angle: -18.46,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Colors.white,
              ),
            ),
          ),

          /// right top star
          Positioned(
            right: getW(22, context),
            top: getH(6, context) + offset,
            child: Transform.rotate(
              angle: -52.91,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Colors.white,
                width: 10,
                height: 10,
              ),
            ),
          ),

          /// right bottom star
          Positioned(
            right: getW(6, context),
            top: getH(14, context) + offset,
            child: Transform.rotate(
              angle: -18.46,
              child: SvgPicture.asset(
                'assets/vectors/star_filled.svg',
                color: Colors.white,
                width: 12,
                height: 12,
              ),
            ),
          ),
          Positioned(
              left: getW(30, context),
              top: getH(16, context) + offset,
              height: getH(30, context),
              width: getW(35, context),
              child: ClipOval(
                child: Container(
                  color: Colors.white,
                ),
              )),
          Positioned(
              right: getW(30, context),
              top: getH(16, context) + offset,
              height: getH(30, context),
              width: getW(35, context),
              child: ClipOval(
                child: Container(
                  color: Colors.white,
                ),
              )),
          Positioned(
            right: getW(33, context),
            bottom: getH(35, context),
            height: getH(63, context),
            width: getW(28, context),
            child: SvgPicture.asset('assets/vectors/calendar/stick.svg'),
          ),

          Positioned(
            left: getW(33, context),
            bottom: getH(35, context),
            height: getH(63, context),
            width: getW(28, context),
            child: SvgPicture.asset('assets/vectors/calendar/stick.svg'),
          )
        ],
      ),
    );
  }
}
