import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/page/calendar/calendar.page.dart';
import 'package:happy_mama/style/color_style.dart';

import 'calendar_top_month.dart';

class Calendar extends StatelessWidget {
  final int month;
  final Function(int) onPressed;
  final CalendarTopMonth top;
  final bool female;

  //final double height = 341.0;

  const Calendar(
      {@required this.month, this.onPressed, this.top, this.female = false});

  @override
  Widget build(BuildContext context) {
    var list = List.generate(
            13, (index) => CalendarValue(month: index, active: index < month))
        .where((value) => value.month != month)
        .toList();
    String title = 'месяцев';
    if (month == 1) {
      title = 'месяц';
    } else if (month >= 2 && month <= 4) {
      title = 'месяца';
    }
    return Column(
      children: <Widget>[
        /// TOP PART
        top ?? Container(),

        /// BOTTOM PART
        Container(
          height: getH(350, context),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            color: Color(0xFFF9F7F0),
            boxShadow: [
              BoxShadow(
                blurRadius: 4,
                color: Color.fromRGBO(0, 0, 0, 0.25),
                offset: Offset(0, 2),
              ),
            ],
          ),
          padding: EdgeInsets.fromLTRB(
            getW(28, context),
            getH(24, context),
            getW(28, context),
            getH(29, context),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //Container(width: getW(24, context),),
                    Expanded(
                      child: Text(
                        female ? 'малышке' : 'малышу',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: getW(22, context),
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: CalendarValueBuilder(
                          CalendarValue(month: month, active: true),
                          onPressed: onPressed),
                    ),
                    Expanded(
                      child: Text(
                        title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: getW(22, context),
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    // Container(width: getW(24, context),),
                  ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CalendarValueBuilder(list[0], onPressed: onPressed),
                  CalendarValueBuilder(list[1], onPressed: onPressed),
                  CalendarValueBuilder(list[2], onPressed: onPressed),
                  CalendarValueBuilder(list[3], onPressed: onPressed),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CalendarValueBuilder(list[4], onPressed: onPressed),
                  CalendarValueBuilder(list[5], onPressed: onPressed),
                  CalendarValueBuilder(list[6], onPressed: onPressed),
                  CalendarValueBuilder(list[7], onPressed: onPressed),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CalendarValueBuilder(list[8], onPressed: onPressed),
                  CalendarValueBuilder(list[9], onPressed: onPressed),
                  CalendarValueBuilder(list[10], onPressed: onPressed),
                  CalendarValueBuilder(list[11], onPressed: onPressed),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class CalendarValueBuilder extends StatelessWidget {
  final CalendarValue value;
  final Function(int) onPressed;

  const CalendarValueBuilder(this.value, {this.onPressed});

  @override
  Widget build(BuildContext context) {
    final double size = 56 / layoutWidth * MediaQuery.of(context).size.width;
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular((size / 2).ceilToDouble()),
        color:
            value.active ? Theme.of(context).primaryColor : ColorStyle.inactive,
      ),
      child: FlatButton(
        padding: EdgeInsets.zero,
        shape: CircleBorder(),
        onPressed: () => onPressed(value.month),
        child: Center(
          child: Text(
            value.month.toString(),
            style: TextStyle(
              fontSize: 30 / layoutWidth * MediaQuery.of(context).size.width,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}

class CalendarValue {
  final int month;
  final bool active;

  CalendarValue({
    @required this.month,
    this.active = false,
  });
}
