import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/page/calendar/calendar_month/calendar_month.page.dart';
import 'package:happy_mama/page/calendar/widgets/calendar.dart';

import 'package:happy_mama/page/calendar/widgets/calendar_bottom.dart';
import 'package:happy_mama/page/calendar/widgets/calendar_top_month.dart';
import 'package:happy_mama/util/month_counter.dart';

import '../home.dart';
import 'widgets/calendar_top.dart';

//final double layoutHeight = 790;
double layoutHeight = 812; //812; //675; //798.21; //all, no bars, old
double layoutWidth = 375;

double getH(double height, BuildContext context) =>
    height / layoutHeight * MediaQuery.of(context).size.height;

double getW(double width, BuildContext context) =>
    width / layoutWidth * MediaQuery.of(context).size.width;

class CalendarPage extends StatefulWidget {
  final Stream<bool> tabChanged;

  CalendarPage({this.tabChanged});

  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  ProfileBloc bloc;
  int month = 0;
  bool female = false;

  StreamSubscription subs;
  StreamSubscription tabChangedSub;
  Widget monthWidget;

  @override
  void dispose() {
    subs?.cancel();
    tabChangedSub?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    subs = bloc.child.listen((child) {
      month = monthCounter(
          start: child?.birthDate ?? DateTime.now(), end: DateTime.now());
      female = (child?.gender ?? '') == 'female';
      setState(() {});
    });
    tabChangedSub = widget.tabChanged.listen((_) {
      monthWidget = null;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (monthWidget == null) {
      monthWidget = checkDeeplink();
    }
    if (monthWidget != null) {
      return monthWidget;
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CalendarTop(),

              /// calendar
              Padding(
                padding: EdgeInsets.fromLTRB(
                  getW(10, context),
                  0,
                  getW(10, context),
                  getH(10, context),
                ),
                child: Calendar(
                  female: female,
                  top: CalendarTopMonth(
                    month: month,
                    onPressed: () => openMonth(context, month),
                  ),
                  month: month,
                  onPressed: (i) => openMonth(context, i),
                ),
              ),

              /// Bottom
              CalendarBottom(),
            ],
          ),
        ),
      ),
    );
  }

  checkDeeplink() {
    if (ModalRoute.of(context).isCurrent && tabBarController.index == 3) {
      String path = deeplinkNavigator.path;
      ////print('path = $path');
      List<String> temp = (path.trim().split('/'));
      temp.removeWhere((f) => f.isEmpty || f == null);
      if (temp.contains('calendar')) {
        //print('hop');
        if (temp.length != 1) {
          var month = int.tryParse(temp[1]);
          if (month != null && month >= 0 && month <= 12) {
            return CalendarMonthPage(
                month: month, overrideBackButton: true, female: female);
          }
        }
      }
    }
  }

  void openMonth(BuildContext context, int index) {
    Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) =>
                CalendarMonthPage(month: index, female: female),
            settings: RouteSettings(name: 'Календарь. $index месяц')));
  }
}
