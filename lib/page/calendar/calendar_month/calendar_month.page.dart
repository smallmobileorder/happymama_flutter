import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/entity/advice.dart';
import 'package:happy_mama/service/firebase/baby/calendar_service.dart';
import 'package:happy_mama/util/deeplink_navigator.dart';
import 'package:happy_mama/util/loading_image_widget.dart';
import 'package:happy_mama/widgets/error_page.dart';
import 'package:happy_mama/widgets/loading_container.dart';

import '../../home.dart';

class CalendarMonthPage extends StatefulWidget {
  final int month;
  final bool overrideBackButton;
  final bool female;

  const CalendarMonthPage({
    @required this.month,
    this.overrideBackButton = false,
    this.female = false,
  });

  @override
  State<StatefulWidget> createState() => _CalendarMonthPageState();
}

class _CalendarMonthPageState extends State<CalendarMonthPage> {
  String title;
  List<Advice> advices = [];
  bool isLoading;
  bool isError;
  bool disposed = false;

  @override
  void initState() {
    disposed = false;
    // get title
    String text = 'месяцев';
    if (widget.month == 1) {
      text = 'месяц';
    } else if (widget.month >= 2 && widget.month <= 4) {
      text = 'месяца';
    }
    title = 'Малыш${widget.female ? 'ке' : 'у'} ${widget.month} $text';
    isLoading = true;
    isError = false;

    loadAdvices()
        .whenComplete(() => setState(() {
              if (!disposed) {
                isLoading = false;
              }
            }))
        .catchError((e) {
      //print(e);
      isLoading = false;
      if (!disposed) {
        setState(() => isError = true);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    disposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // generate advices
    var adviceList = advices
        ?.map((advice) => InkWell(
              onTap: () => goToPath(advice),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 18),
                child: AdviceBuilder(advice: advice),
              ),
            ))
        ?.expand((elem) => [
              elem,
              Divider(
                indent: 16,
                endIndent: 16,
                color: Theme.of(context).primaryColor,
                height: 0,
              )
            ])
        ?.toList();
    adviceList.add(Container(height: 24));
    adviceList.add(Padding(
      padding: const EdgeInsets.symmetric(horizontal: 48.0),
      child: CupertinoButton.filled(
        padding: EdgeInsets.all(16),
        child: Text(
          'Загрузить фото и обновить данные',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
        ),
        onPressed: () {
          deeplinkNavigator.parsePathAndNavigate('/main', prevTab: 3)();
        },
      ),
    ));
    adviceList.add(Container(height: 24));
    return Scaffold(
        appBar: CupertinoNavigationBar(
          leading: widget.overrideBackButton
              ? CupertinoNavigationBarBackButton(
                  onPressed: () {
                    if (history.isNotEmpty) {
                      int tab = history.removeLast();
                      deeplinkNavigator.setKnowledgePathIfNecessary(tab);
                      tabBarController.index = tab;
                    }
                  },
                )
              : null,
          middle: Text(title),
          border: Border(
            bottom: BorderSide(color: Theme.of(context).primaryColor),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: premiumService.isPremium()
                    ? Container()
                    : banner.getWidget()),
            Positioned(
              right: 0,
              left: 0,
              top: premiumService.isPremium() ? 0 : 60,
              bottom: 0,
              child: Builder(builder: (_) {
                if ((isLoading || isError) == false)
                  return ListView(
                    children: adviceList,
                  );
                if (isError) return ErrorPage();
                if (isLoading) return LoadingContainer();
              }),
            )
          ],
        ));
  }

  loadAdvices() async {
    var service = CalendarService();
    advices = await service.fetchAdvices(widget?.month ?? 0);
  }

  goToPath(Advice advice) {
    //print(advice.path);
    deeplinkNavigator.parsePathAndNavigate(advice?.path, prevTab: 3)();
  }
}

class AdviceBuilder extends StatelessWidget {
  final Advice advice;

  const AdviceBuilder({@required this.advice});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            width: 50,
            child: LoadingImageWidget(url: advice?.image ?? '') ?? Container()),
        Container(width: 16),
        Expanded(child: Text(advice.text)),
      ],
    );
  }
}
