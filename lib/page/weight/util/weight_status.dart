import 'dart:math';

enum WeightStatus { BAD, NORMAL, FATAL }

//weightStatus(double weight, int age) {
//  if (age > 12) return WeightStatus.NORMAL;
//  if (_belongToRange(weight, orangeDown[age], orangeUp[age])) {
//    return WeightStatus.NORMAL;
//  } else if (_belongToRange(weight, orangeUp[age], redUp[age]) ||
//      _belongToRange(weight, orangeDown[age], redDown[age])) {
//    return WeightStatus.BAD;
//  } else {
//    return WeightStatus.FATAL;
//  }
//}

weightStatusByAprox(double weight, double age, {bool girl = false}) {
  if (age > 12) return WeightStatus.NORMAL;
  if (_upperThanLine1(weight, age, girl)) return WeightStatus.FATAL;
  //if (_upperThanLine2(weight, age)) return WeightStatus.BAD;
  //if (_upperThanLine3(weight, age)) return WeightStatus.NORMAL;
  if (_upperThanLine4(weight, age, girl))
    return WeightStatus.NORMAL; //WeightStatus.BAD;
  return WeightStatus.FATAL;
}

_upperThanLine1(double weight, double age, bool girl) {
  ///girl
  ///-0.000279289 x^4 + 0.0107299 x^3 - 0.167103 x^2 + 1.64259 x + 4.80084 (quartic)
  ///boy
  ///-0.000413352 x^4 + 0.0155285 x^3 - 0.222834 x^2 + 1.83508 x + 5.00172 (quartic)
  //print('line1');
  if (girl) {
    return weight >
        (-0.000279289 * pow(age, 4) +
            0.0107299 * pow(age, 3) -
            0.167103 * pow(age, 2) +
            1.64259 * age +
            4.80084);
  } else {
    return weight >
        (-0.000411709 * pow(age, 4) +
            0.0153184 * pow(age, 3) -
            0.221198 * pow(age, 2) +
            1.8557 * age +
            5.05094);
  }
//  if (age <= 2.5) {
//    //print(-2.9 * age + 2.5 * weight - 10.5);
//    return (-2.9 * age + 2.5 * weight - 10.5) > 0;
//  }
//  if (age > 2.5 && age <= 5.4) {
//    //print(1.9 * age - 2.9 * weight + 15.84);
//    return (1.9 * age - 2.9 * weight + 15.84) < 0;
//  }
//  if (age > 5.4) {
//    //print(-2.4 * age + 6.6 * weight - 46.44);
//    return (-2.4 * age + 6.6 * weight - 46.44) > 0;
//  }
}

_upperThanLine2(double weight, double age) {
  //print('line2');
  if (age <= 2) {
    //print(-2.3 * age + 2 * weight - 7.4);
    return (-2.3 * age + 2 * weight - 7.4) > 0;
  }
  if (age > 2 && age <= 6) {
    //print(2.4 * age - 4 * weight + 19.2);
    return (2.4 * age - 4 * weight + 19.2) < 0;
  }
  if (age > 6) {
    //print(-1.9 * age + 6 * weight - 39);
    return (-1.9 * age + 6 * weight - 39) > 0;
  }
}

_upperThanLine3(double weight, double age) {
  //print('line3');
  if (age <= 3) {
    //print(-2.5 * age + 3 * weight - 8.4);
    return (-2.5 * age + 3 * weight - 8.4) > 0;
  }
  if (age > 3 && age <= 7) {
    //print(1.5 * age - 4 * weight + 16.7);
    return (1.5 * age - 4 * weight + 16.7) < 0;
  }
  if (age > 7) {
    //print(-1.2 * age + 5 * weight - 25.6);
    return (-1.2 * age + 5 * weight - 25.6) > 0;
  }
}

_upperThanLine4(double weight, double age, bool girl) {
  ///girl
  ///-0.000144501 x^4 + 0.00556321 x^3 - 0.0903427 x^2 + 0.891357 x + 1.99764 (quartic)
  ///boy
  ///-0.000241826 x^4 + 0.00906986 x^3 - 0.131932 x^2 + 1.09187 x + 2.19438 (quartic)
  //print('line4');
  if (girl) {
    return weight >
        (-0.000144501 * pow(age, 4) +
            0.00556321 * pow(age, 3) -
            0.0903427 * pow(age, 2) +
            0.891357 * age +
            1.99764);
  } else {
    return weight >
        (-0.000241826 * pow(age, 4) +
            0.00906986 * pow(age, 3) -
            0.131932 * pow(age, 2) +
            1.09187 * age +
            2.19438);
  }
//  if (age <= 3) {
//    //print(-3.4 * age + 3 * weight - 7.2);
//    return (-3.4 * age + 3 * weight - 7.2) > 0;
//  }
//  if (age > 3 && age <= 8) {
//    //print(0.7 * age - 5 * weight + 26.9);
//    return (0.7 * age - 5 * weight + 26.9) < 0;
//  }
//  if (age > 8) {
//    //print(-0.7 * age + 4 * weight - 20.4);
//    return (-0.7 * age + 4 * weight - 20.4) > 0;
//  }
}

bool _belongToRange(double figure, double start, double end) {
  if (figure >= start && figure < end || figure >= end && figure < start)
    return true;
  return false;
}

const List<double> redUp = [
  4.2,
  5.5,
  6.6,
  7.4,
  8.2,
  8,
  8,
  9.2,
  9.7,
  10.1,
  10.4,
  10.9,
  11.2,
  11.4
];
const List<double> orangeUp = [
  3.8,
  4.9,
  5.9,
  6.7,
  7.4,
  7.9,
  8.4,
  8.8,
  9.1,
  9.4,
  9.7,
  10.0,
  10.2
];
const List<double> green = [
  3.2,
  4.2,
  5.2,
  5.9,
  6.5,
  6.9,
  7.4,
  7.7,
  8.0,
  8.3,
  8.5,
  8.8,
  9.0
];
const List<double> orangeDown = [
  2.8,
  3.7,
  4.5,
  5.2,
  5.7,
  6.1,
  6.5,
  6.8,
  7.1,
  7.3,
  7.5,
  7.7,
  8.0
];
const List<double> redDown = [
  2.4,
  3.2,
  4.0,
  4.6,
  5.1,
  5.5,
  5.8,
  6.2,
  6.4,
  6.6,
  6.8,
  7.0,
  7.2
];
