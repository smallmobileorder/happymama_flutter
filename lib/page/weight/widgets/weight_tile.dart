import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/entity/weight_entity.dart';
import 'package:happy_mama/style/color_style.dart';

class WeightTile extends StatelessWidget {
  final WeightEntity entity;
  final Function onEdit;
  final Function onDelete;

  const WeightTile({Key key, this.entity, this.onEdit, this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              '${entity.weight} кг',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: ColorStyle.tundora),
            ),
          ),
          Expanded(
            child: Text(
              entity.date,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  color: ColorStyle.tundora),
            ),
          ),
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CupertinoButton(
                  padding: EdgeInsets.zero,
                  child: SvgPicture.asset(
                    'assets/vectors/pen.svg',
                    width: 20,
                    height: 20,
                  ),
                  onPressed: onEdit,
                ),
                CupertinoButton(
                  padding: EdgeInsets.zero,
                  child: SvgPicture.asset(
                    'assets/vectors/bucket.svg',
                    width: 30,
                    height: 30,
                  ),
                  onPressed: onDelete,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
