import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/weight_entity.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/util/month_counter.dart';
import 'package:happy_mama/widgets/confirm_dialog.dart';
import 'package:numberpicker/numberpicker.dart';

class WeightDialog extends StatefulWidget {
  final WeightEntity entity;

  final DateTime birthDay;

  final ProfileBloc bloc;

  const WeightDialog({Key key, this.entity, this.birthDay, this.bloc})
      : super(key: key);

  createState() => _WeightDialogState();
}

class _WeightDialogState extends State<WeightDialog> {
  int id;

  double currentWeight;

  String date;

  double weight;

  bool pickerDialog = false;

  DateTime birthDay;

  @override
  void initState() {
    super.initState();
    id = widget?.entity?.id ?? -1;
    weight = widget?.entity?.weight ?? -1;
    currentWeight = weight == -1 ? 5.5 : weight;
    date = widget?.entity?.date ?? '';
    birthDay = widget?.birthDay ?? DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    var data = MediaQuery.of(context)
        .removeViewInsets(removeLeft: true, removeRight: true);
    double horizontalPadding = data.size.width / 360 * 45;
    return Container(
      //margin: EdgeInsets.only(left: 20, right: 20),
      child: Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(color: ColorStyle.tint, width: 2)),

          //backgroundColor: Colors.transparent,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            child: pickerDialog
                ? picker()
                : Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 24,
                      horizontal: horizontalPadding,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        CupertinoButton(
                          onPressed: selectDate,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                date.isEmpty ? 'ДД.ММ.ГГГГ' : date,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 17,
                                    color: ColorStyle.codGray),
                              ),
                              SvgPicture.asset(
                                'assets/vectors/calendar.svg',
                                color: ColorStyle.tint,
                              )
                            ],
                          ),
                        ),
                        CupertinoButton(
                          onPressed: selectWeight,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('${weight == -1 ? 'XX' : weight} кг',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17,
                                      color: ColorStyle.codGray)),
                              SvgPicture.asset(
                                'assets/vectors/solid-baby_icon.svg',
                                color: ColorStyle.tint,
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 44,
                          margin: EdgeInsets.symmetric(horizontal: 24),
                          child: FlatButton(
                            disabledColor: ColorStyle.inactive,
                            padding: EdgeInsets.zero,
                            color: Theme.of(context).primaryColor,
                            child: Text(
                              'Сохранить',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            onPressed: canSave() ? saveWeight : null,
                          ),
                        ),
                      ],
                    ),
                  ),
          )),
    );
  }

  void saveWeight() async {
    Navigator.of(context, rootNavigator: true).pop();
    List<WeightEntity> tmp = widget?.bloc?.weights?.value ?? [];
    bool result = true;
    List<String> ages =
        widget?.bloc?.weights?.value?.map((f) => f.date)?.toList() ?? [];
    if (((widget?.entity?.date ?? null) != null &&
            widget.entity.date != date &&
            ages.toList().contains(date)) ||
        (((widget?.entity?.date ?? null) == null && ages.contains(date)))) {
      result = await showDialog(
          context: context,
          builder: (_) => ConfirmDialog(
              destructive: true,
              subtitle:
                  'Информация о весе ребенка $date уже заполнена. Удалить прошлую запись?'));
    }
    if (result != true) return;
    try {
      tmp.removeWhere((f) => f.date == date);
    } catch (e) {
      //print(e);
    }
    tmp.add(WeightEntity(id: id, date: date, weight: weight));

    tmp.sort((a, b) => Formatter.parseFormattedStringDate(a.date)
                .compareTo(Formatter.parseFormattedStringDate(b.date)) ==
            -1
        ? 1
        : -1);
    widget.bloc.weights.add(tmp);
  }

  void selectDate() async {
    DateTime tmp = await showDatePicker(
      context: context,
      initialDate: date.isEmpty
          ? DateTime.now()
          : Formatter.parseFormattedStringDate(date),
      firstDate: birthDay,
      lastDate: DateTime.now(),
    );
    if (tmp != null) {
      setState(() => date = Formatter.formatDate(tmp));
      id = monthCounter(start: birthDay, end: tmp);
    }
  }

  void selectWeight() {
    setState(() => pickerDialog = true);
    return;
  }

  Widget picker() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                NumberPicker.decimal(
                    initialValue: currentWeight,
                    minValue: 0,
                    maxValue: 15,
                    onChanged: (value) =>
                        setState(() => currentWeight = value)),
                Text(
                  'кг',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                      color: ColorStyle.codGray),
                )
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 44,
              margin: EdgeInsets.symmetric(horizontal: 48),
              child: FlatButton(
                padding: EdgeInsets.zero,
                color: Theme.of(context).primaryColor,
                child: Text(
                  'Выбрать',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                onPressed: () => setState(() {
                  weight = currentWeight;
                  pickerDialog = false;
                }),
              ),
            ),
          ],
        ));
  }

  bool canSave() {
    if (weight != -1 && date.isNotEmpty) {
      return true;
    }
    return false;
  }
}
