import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/service/firebase/baby/baby_register_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/widgets/brand_raw_button.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/loading_container.dart';
import 'package:intl/intl.dart';

class NewProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NewProfileState();
}

class _NewProfileState extends State<NewProfile> {
  bool loading = false;
  DateTime date;
  String _gender = '';
  TextEditingController controller = TextEditingController();
  BabyInfoService service = BabyInfoService();
  ProfileBloc bloc;

  TextStyle titleStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
  );

  OutlineInputBorder border;

  @override
  void initState() {
    border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(
        width: 2,
        color: ColorStyle.tint, //Theme.of(context).primaryColor,
      ),
    );
    bloc = BlocProvider.of<ProfileBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Добавление ребенка'),
        border:
            Border(bottom: BorderSide(color: Theme.of(context).primaryColor)),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Padding(
        padding: const EdgeInsets.fromLTRB(24, 0, 24, 24),
        child: CupertinoButton(
          padding: EdgeInsets.zero,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Theme.of(context).primaryColor,
            ),
            alignment: Alignment.center,
            height: 48,
            width: MediaQuery.of(context).size.width,
            child: Text(
              'Добавить',
              style: TextStyle(
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: Colors.white,
              ),
            ),
          ),
          onPressed: loading ? null : save,
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(24),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Дата рождения малыша?', style: titleStyle),
                  Container(height: 16),
                  _DatePicker(date, onTap: pickDate),
                  Container(height: 16),
                  Text('У вас мальчик или девочка?', style: titleStyle),
                  Container(height: 12),
                  CheckboxGroup(
                    padding: EdgeInsets.zero,
                    labels: ["Мальчик", "Девочка"],
                    onSelected: (selection) => setState(() =>
                        _gender = selection.length == 0 ? '' : selection.last),
                    checked: [_gender],
                    itemBuilder: (checkbox, text, index) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: index == 0 ? 12 : 0),
                        child: SizedBox(
                          height: 16,
                          child: Row(
                            children: [
                              Container(
                                height: 16,
                                width: 16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: ColorStyle.tint, width: 1),
                                    borderRadius: BorderRadius.circular(2)),
                                child: Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor: Colors.transparent,
                                  ),
                                  child: Checkbox(
                                    onChanged: (value) => setState(() => value
                                        ? _gender = text.data
                                        : _gender = ''),
                                    value: _gender.contains(text.data),
                                    checkColor: ColorStyle.tint,
                                    activeColor: Colors.transparent,
                                  ),
                                ),
                              ),
                              Container(width: 8),
                              BrandRawButton(
                                onPressed: () =>
                                    setState(() => _gender = text.data),
                                color: Colors.transparent,
                                child: Text(
                                  text.data,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 12,
                                      color: ColorStyle.dirtyBlack),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  Container(height: 16),
                  Text('Как зовут кроху?', style: titleStyle),
                  Container(height: 12),
                  TextField(
                    controller: controller,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(16, 11, 16, 11),
                      enabledBorder: border,
                      focusedBorder: border,
                      hintText: 'Имя крохи',
                      hintStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFFC4C4C4),
                        height: 1.25,
                      ),
                    ),
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                    scrollPadding: EdgeInsets.all(148),
                  ),
                  Container(height: 80),
                ],
              ),
            ),
          ),
          loading ? LoadingContainer() : Container(),
        ],
      ),
    );
  }

  void pickDate() async {
    DateTime now = DateTime.now();
    DateTime _date = await showDatePicker(
      context: context,
      initialDate: now,
      firstDate: now.subtract(Duration(days: 365)),
      lastDate: now,
    );
    if (_date == null) {
      return;
    }
    setState(() => date = _date);
  }

  void save() async {
    setState(() => loading = true);
    String name = controller.text;
    if (date == null) {
      showError('Выберите дату рождения');
      setState(() => loading = false);
      return;
    }
    if (_gender.isEmpty) {
      showError('Выберите пол ребенка');
      setState(() => loading = false);
      return;
    }
    if (name.isEmpty) {
      showError('Введите имя ребенка');
      setState(() => loading = false);
      return;
    }
    name = controller.text[0].toUpperCase() + controller.text.substring(1);
    var _date = DateFormat("dd.MM.yyyy").format(date);
    //print(
        //'CONDITION: ${bloc.children?.length == 1 && bloc.child.value.isEmpty()}');
    if (bloc.children?.length == 1 && bloc.child.value.isEmpty()) {
      //print('POST NEW CHILD');
      await service
          .postInfo(
              bloc.parent.kids[0],
              name,
              _gender == 'Мальчик' ? 'male' : 'female',
              Formatter.formatDate(date))
          .whenComplete(() {
        bloc.changeChildInfo(
            name, _gender == 'Мальчик' ? 'male' : 'female', true, date);
        setState(() => loading = false);
        Navigator.of(context).pop(bloc.parent.kids[0]);
      });
    } else {
      BabyRegisterService registerService = BabyRegisterService();
      String newBabyId = await registerService.register(bloc.parent.identifier,
          _date, _gender == 'Мальчик' ? 'male' : 'female', name);
      //print("NEW BABY ID IS $newBabyId");
      bloc.children.add(await service.fetchInfo(newBabyId));
      // sort kids by birthdate
      final now = DateTime.now();
      bloc.children.sort((left, right) =>
          left.birthDate?.compareTo(right.birthDate ?? now) ?? 1);
      //print("ADDED IT TO BLOC");
      setState(() => loading = false);
      Navigator.of(context).pop(newBabyId);
    }
  }

  void showError(String text) {
    showDialog(
      context: context,
      builder: (_) => InfoDialog(
        title: text,
      ),
    );
  }
}

class _DatePicker extends StatelessWidget {
  final DateTime date;
  final Function onTap;

  const _DatePicker(this.date, {this.onTap});

  @override
  Widget build(BuildContext context) {
    String dateText;
    if (date == null) {
      dateText = 'ДД.ММ.ГГГГ';
    } else {
      dateText = Formatter.formatDate(date);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        InkWell(
          onTap: onTap,
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Row(
            children: <Widget>[
              Text(
                dateText,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Container(width: 16),
              SvgPicture.asset(
                'assets/vectors/calendar.svg',
                color: Theme.of(context).primaryColor,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
