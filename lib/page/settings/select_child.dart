import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/service/firebase/photo/photo_service.dart';
import 'package:happy_mama/widgets/loading_container.dart';

class SelectChild extends StatefulWidget {
  final String buttonText;
  final Future Function(Child, int) onPressed;
  final List<Future<Uint8List>> images;

  const SelectChild({
    Key key,
    this.images,
    this.buttonText = '',
    this.onPressed,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SelectChildState();
}

class _SelectChildState extends State<SelectChild> {
  ProfileBloc bloc;
  int selected;
  bool loading = false;
  PhotoService photoService = PhotoService();
  List<Future<Uint8List>> images;

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    selected = bloc.children.indexOf(bloc.child.value);
    this.images = widget.images;
    if (images == null) {
      images = List.filled(bloc.children.length, null);
      for (int i = 0; i < bloc.children.length; i++) {
        if (bloc.children[i].mainImageId != null) {
          images[i] = photoService.getImageByMonth(
              bloc.children[i].id, bloc.children[i].mainImageId);
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if ((bloc?.children?.length ?? 0) == 0) {
      return Container();
    }
    List<Widget> children = [];
    for (int i = 0; i < bloc.children.length; i++) {
      Child child = bloc.children[i];
      children.add(Padding(
        padding: const EdgeInsets.fromLTRB(24, 8, 24, 8),
        child: child.mainImageId == null
            ? ChildTileBuilder(
                name: child.name,
                boy: child.gender == 'male',
                value: i,
                groupValue: selected,
                onChanged: (value) {
                  selected = value;
                  setState(() {});
                },
              )
            : FutureBuilder(
                future: images[i],
                builder: (context, snapshot) {
                  return ChildTileBuilder(
                    image: snapshot.data == null
                        ? null
                        : MemoryImage(snapshot.data),
                    name: child.name,
                    boy: child.gender == 'male',
                    value: i,
                    groupValue: selected,
                    onChanged: (value) {
                      selected = value;
                      setState(() {});
                    },
                  );
                },
              ),
      ));
    }
    // detector to hide stack
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: 350,
            ),
            child: Dialog(
              backgroundColor: Colors.transparent,
              elevation: 0,
              child: GestureDetector(
                onTap: () {}, //override prev detector
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(
                        color: Theme.of(context).primaryColor, width: 2),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxHeight: 200,
                          ),
                          child: Scrollbar(
                            child: SingleChildScrollView(
                              child: Column(
                                children: children,
                              ),
                            ),
                          ),
                        ),
                        CupertinoButton.filled(
                          child: Text(widget.buttonText),
                          onPressed: loading
                              ? null
                              : () {
                                  ////print('Selected $selected child: ${bloc.children[selected]}');
                                  setState(() => loading = true);
                                  widget
                                      .onPressed(
                                          bloc.children[selected], selected)
                                      .then((_) {
                                    //print('Now pop');
                                    setState(() => loading = false);
                                    Navigator.pop(context, true);
                                  });
                                },
                        ),
                        Container(height: 24),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          loading ? LoadingContainer() : Container(),
        ],
      ),
    );
  }
}

class ChildTileBuilder extends StatelessWidget {
  final MemoryImage image;
  final String name;
  final bool boy;
  final int value;
  final int groupValue;
  final Function(int) onChanged;

  const ChildTileBuilder({
    @required this.name,
    this.image,
    this.boy = true,
    @required this.value,
    @required this.groupValue,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            image == null
                ? SvgPicture.asset(
                    'assets/vectors/baby_face.svg',
                  )
                : Container(
                    width: 42,
                    height: 42,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: image, fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(21),
                    ),
                  ),
            Container(width: 10),
            Text(
              name,
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w500,
                color: boy ? blueColor : pinkColor,
              ),
            ),
          ],
        ),
        Radio<int>(
          value: value,
          groupValue: groupValue,
          onChanged: onChanged,
          activeColor: boy ? blueColor : pinkColor,
        ),
      ],
    );
  }
}
