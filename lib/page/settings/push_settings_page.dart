import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/event_name_selector.dart';
//import 'package:open_settings/open_settings.dart';

import 'widgets/custom_cupertino_switch.dart';
import 'widgets/time_picker.dart';

const SOUNDS_NAME = [
  'Системный',
  'Thrown away',
  'Who',
  'Results',
  'Enough',
  'Concise',
  'Strum',
  'Tasty',
  'Slow Spring Board',
];

class PushSettingsPage extends StatefulWidget {
  @override
  _PushSettingsPageState createState() => _PushSettingsPageState();
}

class _PushSettingsPageState extends State<PushSettingsPage> {
  ProfileBloc bloc;
  String pushSoundValue;
  String pushTimeValue;
  bool isPushEnable;
  AudioCache player;
  bool needUpdate;

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    int soundIndex = 0;
    try {
      soundIndex = SOUNDS_NAME.indexOf(SOUNDS_NAME.firstWhere((f) =>
          f
              .toLowerCase()
              .replaceAll('(по умолчанию)', '')
              .trim()
              .replaceAll(' ', '_') ==
          bloc.pushSettings['sound']));
    } catch (e) {
      //print('error when get sound $e');
    }
    pushSoundValue = SOUNDS_NAME[soundIndex].replaceAll('(по умолчанию)', '');
    pushTimeValue = '${bloc.pushSettings['time'][0]}' +
        ':' +
        '${bloc.pushSettings['time'][1] < 10 ? '0' : ''}${bloc.pushSettings['time'][1]}';
    isPushEnable = bloc.pushSettings['enable'];
    needUpdate = false;
    player = AudioCache(prefix: 'sounds/');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (needUpdate)
          bloc.updatePushSettings(
              hour: int.parse(pushTimeValue.split(':')[0]),
              minute: int.parse(pushTimeValue.split(':')[1]),
              silent: !isPushEnable,
              sound: pushSoundValue.toLowerCase().trim().replaceAll(' ', '_'));

        return Future.value(true);
      },
      child: Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Уведомления'),
          backgroundColor: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _SettingsButton(
                text: 'Выключить уведомления',
                /*value: isPushEnable,*/
                onPressed: () => Platform.isAndroid
                    ? AppSettings.openAppSettings()
                    : AppSettings.openNotificationSettings(),
                /*onValue: switchNotificationStatus*/
              ),
              smallDivider(),
              _SettingsSwitch(
                  text: 'Включить звук',
                  value: isPushEnable,
                  onValue: switchNotificationStatus),
              smallDivider(),
              AnimatedContainer(
                duration: Duration(milliseconds: 300),
                height: isPushEnable ? 60 : 0,
                child: _SettingsButton(
                  text: 'Звук',
                  currentValue: pushSoundValue,
                  onPressed: choosePushSound,
                ),
              ),
              if (isPushEnable) smallDivider(),
              _SettingsButton(
                text: 'Время',
                currentValue: pushTimeValue,
                onPressed: choosePushTime,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget smallDivider() {
    return Divider(
      height: 0,
      thickness: 1,
      color: Theme.of(context).primaryColor.withOpacity(0.25),
    );
  }

  switchNotificationStatus(bool value) {
    needUpdate = true;
    setState(() => isPushEnable = value);
    //print(value);
  }

  choosePushSound() {
    showDialog<String>(
      context: context,
      builder: (_) => EventNameSelector(
        text: 'Выбор звука',
        ownButtonText: 'Выбор звука',
        initValue: pushSoundValue,
        showChosen: true,
        names: SOUNDS_NAME,
        overrideTapBottomFunction: (String name) {
          String temp = name?.replaceAll(('(по умолчанию)'), '')?.trim() ?? '';
          if (temp != pushSoundValue && temp != null && temp.isNotEmpty) {
            needUpdate = true;
            setState(() => pushSoundValue =
                name.replaceAll(('(по умолчанию)'), '').trim());
          }
        },
        overrideTapTileFunction: playSound,
      ),
    );
  }

  choosePushTime() async {
    String temp = await showDialog(
        context: context,
        builder: (_) => TimePicker(
              hour: int.parse(pushTimeValue.split(':')[0]),
              minute: int.parse(pushTimeValue.split(':')[1]),
            ));
    if (temp != pushTimeValue && temp != null && temp.isNotEmpty) {
      needUpdate = true;
      setState(() => pushTimeValue = temp);
    }
  }

  playSound(String name) async {
    try {
      await player
          .play(name.toLowerCase().trim().replaceAll(' ', '_') + '.mp3');
    } catch (e) {
      //print('PLAY SOUND ERROR $e');
    }
  }
}

class _SettingsButton extends StatelessWidget {
  final String text;
  final String currentValue;
  final Function onPressed;

  const _SettingsButton({
    this.text = '',
    this.onPressed,
    this.currentValue = '',
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: onPressed,
      padding: EdgeInsets.fromLTRB(24, 12, 24, 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              fontSize: 16,
              color: Color(0xFF3F3F3F),
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w500,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                currentValue,
                style: TextStyle(
                  fontSize: 16,
                  color: Theme.of(context).primaryColor.withOpacity(0.5),
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w500,
                ),
              ),
              Container(
                width: 10,
              ),
              Icon(
                Icons.chevron_right,
                color: Theme.of(context).primaryColor.withOpacity(0.5),
                size: 32,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _SettingsSwitch extends StatelessWidget {
  final String text;
  final bool value;
  final Function(bool) onValue;

  const _SettingsSwitch({
    Key key,
    this.text = '',
    @required this.value,
    this.onValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget widgetText = Text(
      text,
      style: TextStyle(
        fontSize: 16,
        color: Color(0xFF3F3F3F),
        fontFamily: 'Montserrat',
        fontWeight: FontWeight.w500,
      ),
    );
    return Padding(
      padding: EdgeInsets.fromLTRB(24, 12, 24, 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          widgetText,
          CustomCupertinoSwitch(
            onChanged: onValue,
            value: value,
            activeColor: Theme.of(context).primaryColor,
            inactiveColor: ColorStyle.doveGray.withOpacity(0.3),
          ),
        ],
      ),
    );
  }
}
