import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:numberpicker/numberpicker.dart';

class TimePicker extends StatefulWidget {
  final int hour;
  final int minute;

  const TimePicker({
    Key key,
    this.hour = 11,
    this.minute = 00,
  }) : super(key: key);

  createState() => _MonthPickerState();
}

class _MonthPickerState extends State<TimePicker> {
  int currentHour;
  int currentMinute;
  int maxMonth;
  double check;

  @override
  void initState() {
    currentHour = widget?.hour ?? 11;
    currentMinute = widget?.minute ?? 00;
    check = 00;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        //margin: EdgeInsets.only(left: 20, right: 20),
        child: Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
                side: BorderSide(color: ColorStyle.tint, width: 2)),

            //backgroundColor: Colors.transparent,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        NumberPicker.integer(
                            initialValue: currentHour,
                            minValue: 0,
                            maxValue: 23,
                            infiniteLoop: true,
                            onChanged: (value) =>
                                setState(() => currentHour = value)),
                        Text(
                          ':',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: ColorStyle.codGray),
                        ),
                        NumberPicker.integer(
                            initialValue: currentMinute,
                            minValue: 0,
                            maxValue: 59,
                            infiniteLoop: true,
                            onChanged: (value) =>
                                setState(() => currentMinute = value)),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 44,
                      margin: EdgeInsets.symmetric(horizontal: 48),
                      child: FlatButton(
                        padding: EdgeInsets.zero,
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          'Выбрать',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () => setState(() {
                          Navigator.of(context).pop(
                              '$currentHour:${currentMinute < 10 ? '0' : ''}$currentMinute');
                        }),
                      ),
                    ),
                  ],
                ))));
  }
}
