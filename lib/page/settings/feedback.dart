import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/brand_checkbox.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/loading_container.dart';

class FeedbackPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  ProfileBloc bloc;
  TextEditingController emailController;
  TextEditingController textController;
  OutlineInputBorder border;
  bool useEmail;
  UserService userService;
  bool loading = false;

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    userService = UserService();
    useEmail = false;
    emailController = TextEditingController();
    textController = TextEditingController();
    border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(
        width: 2,
        color: ColorStyle.tint, //Theme.of(context).primaryColor,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Обратная связь'),
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
        body: Center(child: LoadingContainer()),
      );
    }
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Обратная связь'),
        border: Border(
          bottom: BorderSide(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(48, 0, 48, 24),
        child: CupertinoButton.filled(
          child: Text('Отправить'),
          onPressed: () async {
            if (emailController.text.isEmpty) {
              showDialog(
                context: context,
                builder: (_) => InfoDialog(
                  title: 'Введите E-mail',
                ),
              );
              return;
            }
            if (textController.text.isEmpty) {
              showDialog(
                context: context,
                builder: (_) => InfoDialog(
                  title: 'Введите ваше сообщение',
                ),
              );
              return;
            }
            loading = true;
            setState(() {});
            await userService.postFeedback(
              bloc.parent.identifier,
              emailController.text,
              textController.text,
            );
            Navigator.pop(context);
            showDialog(
              context: context,
              builder: (context) => InfoDialog(
                title: 'Отзыв отправлен',
              ),
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Почта, на которую придет ответ',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(height: 16),
              TextField(
                controller: emailController,
                maxLines: 1,
                maxLength: 500,
                enabled: !useEmail,
                decoration: InputDecoration(
                  enabledBorder: border,
                  focusedBorder: border,
                  disabledBorder: border,
                  hintText: 'E-mail',
                  contentPadding: EdgeInsets.all(16),
                  counter: Container(),
                ),
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              Container(height: 12),
              BrandCheckbox(
                forSettings: true,
                text: 'Использовать почту, указанную при регистрации',
                //onChanged: (val) => setState(() => useEmail = val),
                value: useEmail,
                onChecked: (val) => setState(() {
                  useEmail = val;
                  emailController.text = bloc.parent.email;
                }),
              ),
              Container(height: 16),
              TextField(
                controller: textController,
                maxLines: 10,
                maxLength: 500,
                decoration: InputDecoration(
                  enabledBorder: border,
                  focusedBorder: border,
                  hintText: 'Ваше сообщение',
                  contentPadding: EdgeInsets.all(16),
                  counter: Container(),
                ),
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
