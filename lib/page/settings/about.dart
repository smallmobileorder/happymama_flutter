import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var text = RichText(
      text: TextSpan(
        style: TextStyle(
          fontSize: 15,
          color: Color(0xFF444444),
          fontFamily: 'Montserrat',
        ),
        children: <TextSpan>[
          TextSpan(
            text: 'P.S. ',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Color(0xFF444444),
            ),
          ),
          TextSpan(
            text:
                'Если вам понравилось приложение HappyMama, вдохновите нас на создание продолжения для более старших деток – оставьте отзыв в магазине приложений!',
          ),
        ],
      ),
    );
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('О приложении'),
        border: Border(
          bottom: BorderSide(color: Theme.of(context).primaryColor),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(32, 0, 24, 0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 24,
              ),
              Text(
                'HappyMama –  самое важное о первом годе жизни малыша в одном приложении!',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF444444),
                ),
              ),
              Container(height: 16),
              Text(
                'Мы не знаем, что лучше для вашего ребенка. Мы вовремя даем современную информацию, которая поможет вам каждый день спокойно принимать верные решения',
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF444444),
                ),
              ),
              Container(
                height: 20,
              ),
              Text(
                'Ежедневные мини-статьи о том, что актуально в текущей неделе и месяце',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF444444),
                ),
              ),
              Container(height: 16),
              Text(
                'Каждый день получайте уведомления, в которых вы найдете:\n- актуальные советы по уходу за ребенком, привязанные к возрасту вашего карапуза;\n- напоминания мамам о необходимости находить время для себя и отдыхать;\n- информацию о психологических и физиологических особенностях малышей на том или ином этапе развития.',
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF444444),
                ),
              ),
              Container(
                height: 20,
              ),
              Text(
                'Наглядные данные о развитии и росте вашего карапуза',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF444444),
                ),
              ),
              Container(height: 16),
              Text(
                'Заполняйте профиль ребенка, следите за ростом, весом, зубами, двигательными навыками вашего карапуза и будьте уверены в том, что малыш развивается в рамках реальных (а не устаревших!) норм.\nДобавляйте фото, формируйте коллаж и делитесь им.',
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF444444),
                ),
              ),
              Container(
                height: 20,
              ),
              Text(
                'Актуальная информация о важных темах первого года',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF444444),
                ),
              ),
              Container(height: 16),
              Text(
                'Погружайтесь подробно в интересующие вас темы (сон, ГВ, прикорм, развитиеи т.д.). Мы собрали для вас современную актуальную информацию. Здесь вы не найдете «бабушкиных» советов, только данные на основе рекомендаций отсовременных врачей и авторитетных мировых организаций.',
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF444444),
                ),
              ),
              Container(
                height: 20,
              ),
              Text(
                'Самая важная информация о развитии по месяцам',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF444444),
                ),
              ),
              Container(height: 16),
              Text(
                'Освежайте знания каждый месяц, ведь малыш растет и меняется. Для этого читайте «выжимку» о самом важном за каждый новый месяц жизни вашего крохи.',
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF444444),
                ),
              ),
              Container(
                height: 20,
              ),
              Text(
                'Что еще?',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF444444),
                ),
              ),
              Container(height: 16),
              Text(
                'Следите за скачками развития малыша, ведите записи о сделанных прививкахи посещенных врачах... Будьте спокойными и уверенными родителями каждый день!',
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xFF444444),
                ),
              ),
              Container(
                height: 24,
              ),
              text,
              Container(
                height: 24,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
