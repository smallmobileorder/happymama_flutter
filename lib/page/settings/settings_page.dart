import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/settings/about.dart';
import 'package:happy_mama/page/settings/feedback.dart';
import 'package:happy_mama/page/settings/new_profile.dart';
import 'package:happy_mama/page/settings/privacy_policy.dart';
import 'package:happy_mama/page/settings/select_child.dart';
import 'package:happy_mama/page/settings/widgets/custom_cupertino_switch.dart';
import 'package:happy_mama/service/firebase/photo/photo_service.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/widgets/confirm_dialog.dart';

import 'push_settings_page.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  ProfileBloc bloc;
  List<Future<Uint8List>> profilePics;
  PhotoService photoService = PhotoService();

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    bloc?.children?.forEach(print);
    //print(bloc?.parent?.identifier);
    //print(bloc?.children?.runtimeType);
    profilePics = List.filled(bloc.children.length, null);
    for (int i = 0; i < bloc.children.length; i++) {
      if (bloc.children[i].mainImageId != null) {
        profilePics[i] = photoService.getImageByMonth(
            bloc.children[i].id, bloc.children[i].mainImageId);
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Настройки'),
        backgroundColor: Colors.white,
        border: Border(
          bottom: BorderSide(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            (bloc.children?.length ?? 0) < 3
                ? _SettingsButton(
                    text:
                        bloc.children?.length == 1 && bloc.child.value.isEmpty()
                            ? 'Заполнить профиль ребенка'
                            : 'Добавить профиль ребенка',
                    onPressed: openNewChildDialog,
                  )
                : Container(),
            (bloc.children?.length ?? 1) > 1 ? smallDivider() : Container(),
            (bloc.children?.length ?? 1) > 1
                ? _SettingsButton(
                    text: 'Сменить профиль ребенка',
                    onPressed: openChooseChildDialog,
                  )
                : Container(),
            (bloc.children?.length ?? 1) > 1 ? smallDivider() : Container(),
            (bloc.children?.length ?? 1) > 1
                ? _SettingsButton(
                    text: 'Удалить профиль ребенка',
                    onPressed: openDeleteChildDialog,
                  )
                : Container(),
            bigDivider(),
            _SettingsSwitch(
              text: 'Цвет фона:',
              forColors: true,
              value: mainThemeIsPink.value,
              onValue: (value) {
                mainThemeIsPink.add(value);
                String id = bloc.child.value.id;
                StorageService.saveColor(id, value);
                setState(() {});
              },
            ),
            bigDivider(),
            StreamBuilder<Child>(
                stream: bloc.child,
                builder: (context, snapshot) {
                  if (snapshot?.data?.isEmpty() ?? true) return Container();
                  return _SettingsButton(
                    text: 'Уведомления',
                    onPressed: () => Navigator.of(context).push(
                        CupertinoPageRoute(
                            builder: (context) => PushSettingsPage(),
                            settings:
                                RouteSettings(name: 'Настройка уведомлений'))),
                  );
                }),
            StreamBuilder<Child>(
                stream: bloc.child,
                builder: (context, snapshot) {
                  if (snapshot?.data?.isEmpty() ?? true) return Container();
                  return smallDivider();
                }),
            _SettingsButton(
              text: 'Обратная связь',
              onPressed: () => Navigator.of(context).push(CupertinoPageRoute(
                  builder: (context) => FeedbackPage(),
                  settings: RouteSettings(name: 'Обратная связь'))),
            ),
            bigDivider(),
            _SettingsButton(
              text: 'О приложении',
              onPressed: () => Navigator.of(context).push(
                CupertinoPageRoute(
                    builder: (_) => AboutPage(),
                    settings: RouteSettings(name: 'О приложении')),
              ),
            ),
            smallDivider(),
            _SettingsButton(
              text: 'Политика конфиденциальности',
              onPressed: () => Navigator.of(context).push(
                CupertinoPageRoute(
                    builder: (_) => PrivacyPolicyPage(),
                    settings:
                        RouteSettings(name: 'Политика конфиденциальности')),
              ),
            ),
//            bigDivider(),
//            _SettingsButton(
//              text: 'Восстановление данных',
//              onPressed: () => showSnackBar(notAvailableSnackBar()),
//            ),
          ],
        ),
      ),
    );
  }

  openNewChildDialog() {
    Navigator.of(context)
        .push<String>(CupertinoPageRoute(
      builder: (context) => NewProfile(),
    ))
        .then((id) async {
      if (id != null) {
        await bloc.changeChild(id);
        setState(() {});
        Navigator.pop(context);
      }
    });
  }

  Future openChooseChildDialog() async {
    bool res = await showDialog(
      context: context,
      builder: (context) => SelectChild(
        images: profilePics,
        buttonText: 'Выбрать',
        onPressed: (child, index) async {
          await bloc.changeChild(child.id);
          setState(() {});
        },
      ),
    );
    // Pop on main window
    if (res == true) {
      Navigator.of(context).pop();
    }
  }

  Future openDeleteChildDialog() async {
    showDialog(
      context: context,
      builder: (context) => SelectChild(
        images: profilePics,
        buttonText: 'Удалить',
        onPressed: (child, index) async {
          await showDialog(
            context: context,
            builder: (context) => ConfirmDialog(
              destructive: true,
              title: 'Вы действительно хотите удалить этот профиль?',
            ),
          ).then((delete) async {
            if (delete == null || delete == false) {
              return;
            }

            await bloc.deleteChild(child);
            //bloc.children.removeAt(index);
//            bloc.child.add(bloc.children[0]);
//            String res = bloc.children[0].skills.join(', ');
//            if (res.isNotEmpty) {
//              res = 'Умеет $res';
//            }
//            bloc.skills.add(res);
//            bloc.childJaw.add(bloc.children[0].jaw);
            //mainThemeIsPink.add(bloc.child.value.gender == 'female');
            setState(() {});
          });
        },
      ),
    );
  }

  Widget smallDivider() {
    return Divider(
      height: 0,
      thickness: 1,
      color: Theme.of(context).primaryColor.withOpacity(0.25),
    );
  }

  Widget bigDivider() {
    return Divider(
      height: 8,
      thickness: 8,
      color: Theme.of(context).primaryColor.withOpacity(0.25),
    );
  }
}

class _SettingsButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  const _SettingsButton({
    this.text = '',
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: onPressed,
      padding: EdgeInsets.fromLTRB(24, 12, 24, 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              fontSize: 16,
              color: Color(0xFF3F3F3F),
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w500,
            ),
          ),
          Icon(
            Icons.chevron_right,
            color: Theme.of(context).primaryColor.withOpacity(0.5),
            size: 32,
          ),
        ],
      ),
    );
  }
}

class _SettingsSwitch extends StatelessWidget {
  final String text;
  final bool value;
  final Function(bool) onValue;
  final bool forColors;

  const _SettingsSwitch({
    Key key,
    this.text = '',
    @required this.value,
    this.onValue,
    this.forColors = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget widgetText;
    if (forColors) {
      String _colorText = ' Голубой';
      Color _color = blueColor;
      if (value) {
        _colorText = ' Розовый';
        _color = pinkColor;
      }
      widgetText = Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              fontSize: 16,
              color: Color(0xFF3F3F3F),
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            _colorText,
            style: TextStyle(
              fontSize: 16,
              color: _color,
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      );
    } else {
      widgetText = Text(
        text,
        style: TextStyle(
          fontSize: 16,
          color: Color(0xFF3F3F3F),
          fontFamily: 'Montserrat',
          fontWeight: FontWeight.w500,
        ),
      );
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(24, 12, 24, 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          widgetText,
          CustomCupertinoSwitch(
            onChanged: onValue,
            value: value,
            activeColor: pinkColor,
            inactiveColor: blueColor,
          ),
        ],
      ),
    );
  }
}
