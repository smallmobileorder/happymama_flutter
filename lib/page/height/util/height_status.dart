import 'dart:math';

enum HeightStatus { BAD, NORMAL, FATAL }

//heightStatus(int height, int age) {
//  if (age > 12) return HeightStatus.NORMAL;
//  if (_belongToRange(height, orangeDown[age], orangeUp[age])) {
//    return HeightStatus.NORMAL;
//  } else if (_belongToRange(height, orangeUp[age], redUp[age]) ||
//      _belongToRange(height, orangeDown[age], redDown[age])) {
//    return HeightStatus.BAD;
//  } else {
//    return HeightStatus.FATAL;
//  }
//}

heightStatusByAprox(double height, double age, {bool girl = false}) {
  if (age > 12) return HeightStatus.NORMAL;

  if (_upperThanLine1(height, age, girl)) return HeightStatus.FATAL;
  // if (_upperThanLine2(height, age)) return HeightStatus.BAD;
  // if (_upperThanLine3(height, age)) return HeightStatus.NORMAL;
  if (_upperThanLine4(height, age, girl)) return HeightStatus.BAD;
  return HeightStatus.FATAL;
}

_upperThanLine1(double height, double age, bool girl) {
  ///girl
  ///-0.00140534 x^4 + 0.0482829 x^3 - 0.637512 x^2 + 5.36896 x + 52.07 (quartic)
  ///boy
  ///-0.00134218 x^4 + 0.0475694 x^3 - 0.650468 x^2 + 5.59547 x + 55.6729 (quartic)
  if (girl) {
    return height >
        ( -0.00140534 * pow(age, 4) +
            0.0482829 * pow(age, 3) -
            0.637512 * pow(age, 2) +
            5.36896 * age +
            52.07);
  } else {
    return height >
        (-0.00134218 * pow(age, 4) +
            0.0475694 * pow(age, 3) -
            0.650468 * pow(age, 2) +
            5.59547 * age +
            55.6729);
  }
//  if (age <= 2) {
//    return (-8.5 * age + 2 * height - 110) > 0;
//  }
//  if (age > 2 && age <= 6) {
//    return (9 * age - 4 * height + 236) < 0;
//  }
//  if (age > 6) {
//    return (-9 * age + 6 * height - 381) > 0;
//  }
}

_upperThanLine2(double height, double age) {
  if (age <= 2) {
    return (-8.5 * age + 2 * height - 166) > 0;
  }
  if (age > 2 && age <= 6) {
    return (8.5 * age - 4 * height + 229) < 0;
  }
  if (age > 6) {
    return (-9 * age + 6 * height - 366) > 0;
  }
}

_upperThanLine3(double height, double age) {
  if (age <= 2) {
    return (-7 * age + 2 * height - 92) > 0;
  }
  if (age > 2 && age <= 6) {
    return (8.5 * age - 4 * height + 195) < 0;
  }
  if (age > 6) {
    return (-6.1 * age + 6 * height - 332.4) > 0;
  }
}

_upperThanLine4(double height, double age, girl) {
  ///girl
  ///-0.00152276 x^4 + 0.0484166 x^3 - 0.604556 x^2 + 4.86594 x + 45.5615 (quartic)
  ///boy
  ///-0.00107621 x^4 + 0.0398845 x^3 - 0.582266 x^2 + 5.17022 x + 44.1996 (quartic)
  if (girl) {
    return height >
        (-0.00152276 * pow(age, 4) +
            0.0484166 * pow(age, 3) -
            0.604556 * pow(age, 2) +
            4.86594 * age +
            45.5615);
  } else {
    return height >
        (-0.00107621 * pow(age, 4) +
            0.0398845 * pow(age, 3) -
            0.582266 * pow(age, 2) +
            5.17022 * age +
            44.1996);
  }
//  if (age <= 2) {
//    return (-7.5 * age + 2 * height - 87) > 0;
//  }
//  if (age > 2 && age <= 6) {
//    return (8.2 * age - 4 * height + 187.6) < 0;
//  }
//  if (age > 6) {
//    return (-6.8 * age + 6 * height - 314.4) > 0;
//  }
}

bool _belongToRange(int figure, int start, int end) {
  if (figure >= start && figure < end || figure >= end && figure < start)
    return true;
  return false;
}

const List<int> redUp = [
  55,
  59,
  63,
  66,
  68,
  71,
  72,
  74,
  76,
  77,
  79,
  80,
  82,
];
const List<int> orangeUp = [53, 58, 61, 64, 66, 68, 70, 72, 73, 75, 76, 78, 79];
const List<int> orangeDown = [
  46,
  50,
  53,
  56,
  58,
  59,
  61,
  63,
  64,
  65,
  66,
  68,
  69
];
const List<int> redDown = [
  44,
  48,
  51,
  54,
  56,
  57,
  59,
  60,
  62,
  63,
  64,
  65,
  66,
];
