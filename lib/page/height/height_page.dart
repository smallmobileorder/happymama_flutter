import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/height_entity.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/brand_button.dart';
import 'package:happy_mama/widgets/confirm_dialog.dart';

import '../home.dart';
import 'widgets/graphic.dart';
import 'widgets/height_dialog.dart';
import 'widgets/height_tile.dart';

class HeightPage extends StatefulWidget {
  @override
  _HeightPageState createState() => _HeightPageState();
}

class _HeightPageState extends State<HeightPage> {
  List<HeightEntity> temp = [];
  ProfileBloc bloc;
  String gender = '';

  @override
  void initState() {
    super.initState();
    initBloc();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //print(width);
    return WillPopScope(
      onWillPop: () async {
        bloc.loadHeightsToServer();
        return Future.value(true);
      },
      child: Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Рост'),
          backgroundColor: Colors.white,
          border: Border(
              bottom: BorderSide(
                  color: ColorStyle.tint,
                  width: 0.0,
                  style: BorderStyle.solid)),
        ),
        body: StreamBuilder<List<HeightEntity>>(
            stream: bloc.heights,
            builder: (context, snapshot) {
              temp = snapshot?.data ?? [];
              //print(snapshot.hasData);
              return SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Рост $gender\nс рождения до года',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                color: ColorStyle.tundora),
                          ),
                          Container(
                            width: 5,
                          ),
                          Image.asset(
                            'assets/images/voz.png',
                            height: 36,
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.all(10),
                        width: width,
                        height: width / 343 * 389,
                        child: Graphic(
                          heights: temp,
                          girl: gender=='девочек',
                          birthDay: bloc.child.value.birthDate,
                        )),
                    Container(
                      height: 16,
                    ),
                    BrandButton.unfilled(
                        text: 'Подробнее про рост',
                        onPressed: goToHeightKnowledgePage),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 44,
                      margin: EdgeInsets.symmetric(horizontal: 48),
                      child: FlatButton(
                        padding: EdgeInsets.zero,
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          'Добавить новое значение роста',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () => addNewValue(HeightEntity()),
                      ),
                    ),
                    Container(
                      height: 16,
                    ),
                    //show if have weight list
                    if (temp.isNotEmpty)
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            height: 48,
                            alignment: Alignment.center,
                            child: Text(
                              'Рост',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: ColorStyle.tundora),
                            ),
                          )),
                          Expanded(
                              child: Container(
                            height: 48,
                            alignment: Alignment.center,
                            child: Text(
                              'Дата',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: ColorStyle.tundora),
                            ),
                          )),
                          Expanded(
                            child: Container(),
                          )
                        ],
                      ),
                    if (temp.isNotEmpty)
                      Column(
                        children: List.generate(
                            temp.length,
                            (index) => HeightTile(
                                  entity: temp[index],
                                  onDelete: () => deleteTile(temp[index]),
                                  onEdit: () => addNewValue(temp[index]),
                                )),
                      ),
                  ],
                ),
              );
            }),
      ),
    );
  }

  goToHeightKnowledgePage() {
    deeplinkNavigator.parsePathAndNavigate('/knowledge/4/', prevTab: 0)();
  }

  addNewValue(HeightEntity editable) {
    showDialog(
        context: context,
        builder: (_) => HeightDialog(
              bloc: bloc,
              entity: editable,
              birthDay: bloc.child.value.birthDate,
            ));
    //print('oh, boy! STOP THIS!');
  }

  void initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    gender = bloc.child.value.gender == 'female' ? 'девочек' : 'мальчиков';
  }

  deleteTile(HeightEntity tmp) async {
    bool result = true;
    result = await showDialog(
        context: context,
        builder: (_) => ConfirmDialog(
            destructive: true,
            title: 'Вы действительно хотите удалить это значение?'));
    if (result != true) return;
    try {
      temp.removeWhere((f) => f.date == tmp.date);
    } catch (e) {}

    //TODO NEED TO CHECK
    bloc.heights.add(temp);
  }
}
