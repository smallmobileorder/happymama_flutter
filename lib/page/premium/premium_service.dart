import 'dart:async';
import 'dart:io';

import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:happy_mama/storage/storage_service.dart';

class PremiumService {
  bool _isPremium = true;

  static String premiumId = 'com.mummy.happy_mama.premium_item';
  final List<String> _productLists = ['com.mummy.happy_mama.premium_item'];

  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];

  String _premiumPrice;

  String get premiumPrice {
    if (_premiumPrice == null) return Platform.isAndroid ? '149' : '149';
    return _premiumPrice;
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    await _checkStorage();

    ///END INIT IF WE HAVE PREMIUM
    if (_isPremium) return;
    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    //print('result: $result');

    ///FOR TESTS:
    ///AT ANDROID THIS OPERATION CONSUME YOUR PURCHASE AND YOU LOST YOUR PREMIUM
//    try {
//      await FlutterInappPurchase.instance.consumeAllItems;
//    } catch (e) {
//      //print('ERROR OCCURED WHEN CONSUME ITEMS');
//    }
    await _getProduct();
    await getPurchases();
    //await _getPurchaseHistory();
  }

  _requestPurchase(IAPItem item) async {
    await FlutterInappPurchase.instance.requestPurchase(item.productId);
  }

  Future _getProduct() async {
    List<IAPItem> items =
        await FlutterInappPurchase.instance.getProducts(_productLists);
    //print('PRODUCTS ${items.length}');
    for (var item in items) {
      //print('${item.toString()}');
      List<String> temp = item?.price?.split('.') ?? [];
      if (temp.isNotEmpty) {
        if (temp.length > 1 && temp[1].startsWith('0')) {
          _premiumPrice = temp[0];
        } else {
          _premiumPrice = item.price;
        }
      }

      this._items.add(item);
    }
  }

  Future getPurchases() async {
    List<PurchasedItem> items =
        await FlutterInappPurchase.instance.getAvailablePurchases();
    for (var item in items) {
      //print('${item.toString()}');
      this._purchases.add(item);
    }
  }

  Future _getPurchaseHistory() async {
    List<PurchasedItem> items =
        await FlutterInappPurchase.instance.getPurchaseHistory();
    for (var item in items) {
      //print('${item.toString()}');
      this._purchases.add(item);
    }
  }

  bool isPremium() {
    //_isPremium = false;
    if (_purchases.isNotEmpty) {
      _isPremium = true;
    }
    return _isPremium;
  }

  buyPremium() async {
    //print('buy premium func');
    var temp =
        _items.firstWhere((i) => i.productId == premiumId, orElse: () => null);
    if (temp != null) {
      //print('buy premium: items is not empty');
      //print(temp);
      await _requestPurchase(temp);
      await getPurchases();
      //await _getPurchaseHistory();
    }
  }

  _checkStorage() async {
    ///GET PREMIUM FROM STORAGE
    ///
    _isPremium = await StorageService.getPremiumStatus() ?? false;
  }

  dispose() async {
    await FlutterInappPurchase.instance.endConnection;
  }
}
