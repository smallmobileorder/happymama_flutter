import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/service/firebase/analytics/events.dart';

import '../../home.dart';

class BuyPremiumDialog extends StatelessWidget {
  final int type;

  const BuyPremiumDialog({Key key, @required this.type}) : super(key: key);

  static const String firstTitle = 'Купить премиум?';
  static const String secondTitle = 'Коллаж с водяным знаком';
  static const String thirdTitle = 'Эта статья заблокирована';
  static const String fourthTitle = 'Этот раздел заблокирован';

  static const fromWhereOpened = [
    'Домашняя страница',
    'Фото',
    'База знаний',
    'База знаний',
    'Предложение о покупке'
  ];

  static const String body =
      'Для того, чтобы открыть доступ ко всем статьям, поделиться коллажем без водяного знака и отключить рекламу - приобретите премиум доступ к приложению';

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(height: 24),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                [
                  firstTitle,
                  secondTitle,
                  thirdTitle,
                  fourthTitle,
                ][(type ?? 0) > 3 ? 0 : type],
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Container(height: 8),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 26),
              child: Text(
                body,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Container(height: 8),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: CupertinoButton.filled(
                  padding: EdgeInsets.all(16),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'Купить премиум за ${premiumService.premiumPrice} рублей',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  onPressed: () => buyPremium(context)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: CupertinoButton.filled(
                padding: EdgeInsets.all(16),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    type == 0 ? 'Закрыть' : 'Продолжить',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                onPressed: () => Navigator.pop(context, false),
              ),
            ),
            Container(height: 16),
          ],
        ),
      ),
    );
  }

  buyPremium(BuildContext context) async {
    Navigator.pop(context);
    var event =
        AnalyticEvent.createStartPurchaseEvent(section: fromWhereOpened[type]);
    analytics.logEvent(name: event.name, parameters: event.parameters);
    await premiumService.buyPremium();
  }
}
