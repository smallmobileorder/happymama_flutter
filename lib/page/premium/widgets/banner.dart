import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';

import '../ads_service.dart';

class AdsBanner {
  int _height = 60;
  int _width = 320;
  Widget _banner;
  Widget _largeBanner;

  init(BuildContext context) {
    if (_banner == null)
      _banner = AdmobBanner(
        key: Key('blocked'),
        adUnitId: AdsService.getBannerAdUnitId(),
        adSize: AdmobBannerSize(
            width: MediaQuery?.of(context)?.size?.width?.toInt() ?? _width,
            height: 56,
            name: 'CustomSize'),
        listener: (AdmobAdEvent event, Map<String, dynamic> args) {
          AdsService.handleEvent(event, args, 'Banner');
        },
      );
    if (_largeBanner == null)
      _largeBanner = AdmobBanner(
       // key: Key('blockedLarge'),
        adUnitId: AdsService.getBannerAdUnitId(),
        adSize: AdmobBannerSize(
            width: MediaQuery?.of(context)?.size?.width?.toInt() ?? _width,
            height: 115,
            name: 'CustomSize'),
        listener: (AdmobAdEvent event, Map<String, dynamic> args) {
          AdsService.handleEvent(event, args, 'LargeBanner');
        },
      );
  }

  Widget getWidget() {
    return _banner ?? Container();
  }

  Widget getLargeWidget(){
    return _largeBanner ?? Container();
  }
}
