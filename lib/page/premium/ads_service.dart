import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';

class AdsService {
  static void handleEvent(AdmobAdEvent event, Map<String, dynamic> args,
      String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        //print('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        //print('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        //print('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        ////print('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        ////print('Reward callback fired. Thanks Andrew!');
        ////print('Type: ${args['type']}');
        ////print('Amount: ${args['amount']}');
        break;
      default:
    }
  }

  static String getBannerAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-5867360842288606/8946949323';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-5867360842288606/6530020619';
    }
    return null;
  }

  static String getInterstitialAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-5867360842288606/3874230822';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-5867360842288606/9427646697';
    }
    return null;
  }

}
