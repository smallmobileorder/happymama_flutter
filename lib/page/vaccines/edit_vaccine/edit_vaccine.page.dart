import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/vaccine.dart';
import 'package:happy_mama/page/vaccines/vaccine_names.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/event_date_builder.dart';
import 'package:happy_mama/widgets/event_name_selector.dart';

class EditVaccinePage extends StatefulWidget {
  final Vaccine vaccine;

  const EditVaccinePage({this.vaccine});

  @override
  State<StatefulWidget> createState() => _EditVaccinePageState();
}

class _EditVaccinePageState extends State<EditVaccinePage> {
  Vaccine vaccine;
  ProfileBloc bloc;
  final TextEditingController commentCon = TextEditingController();
  final TextEditingController nameCon = TextEditingController();
  final String ownVaccineName = 'Добавить прививку вручную';
  String title;
  String vaccineNameButtonValue;

  OutlineInputBorder border;
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    title = widget.vaccine == null ? 'Добавить прививку' : widget.vaccine.name;
    vaccine = widget.vaccine ?? Vaccine(name: null, date: null);
    if (vaccine.name != null) {
      if (vaccineNames.contains(vaccine.name)) {
        vaccineNameButtonValue = vaccine.name;
      } else {
        nameCon.text = vaccine.name;
        vaccineNameButtonValue = ownVaccineName;
      }
    } else {
      vaccineNameButtonValue = '';
    }
    commentCon.text = vaccine.comment;
    commentCon.addListener(() {
      scrollController.jumpTo(0);
    });

    /// Init border
    border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(
        width: 2,
        color:  ColorStyle.tint, //Theme.of(context).primaryColor,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    nameCon.dispose();
    commentCon.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              controller: scrollController,
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(24, 16, 24, 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    EventDateBuilder(vaccine.date, onTap: selectDate),
                    Container(height: 16),

                    /// Button with vaccine name
                    vaccine.name == null
                        ? Container(
                      width: MediaQuery.of(context).size.width,
                      child: CupertinoButton.filled(
                        padding: EdgeInsets.all(12),
                        child: Text(
                          'Выбрать прививку',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: selectVaccine,
                      ),
                    )
                        : Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                          width: 2,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      child: CupertinoButton(
                        padding: EdgeInsets.all(12),
                        child: Text(
                          vaccineNameButtonValue,
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.black,
                            fontWeight:
                            vaccineNameButtonValue == ownVaccineName
                                ? FontWeight.w500
                                : FontWeight.w400,
                          ),
                        ),
                        onPressed: selectVaccine,
                      ),
                    ),

                    /// Button with vaccine name if we selected own name
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: vaccineNameButtonValue == ownVaccineName
                          ? TextField(
                        controller: nameCon,
                        decoration: InputDecoration(
                          enabledBorder: border,
                          focusedBorder: border,
                          hintText: 'Название прививки',
                          contentPadding:
                          EdgeInsets.fromLTRB(16, 17, 16, 18),
                          counter: Container(),
                        ),
                        style: TextStyle(
                          fontSize: 13,
                        ),
                        maxLength: 150,
                        onChanged: changeName,
                      )
                          : Container(),
                    ),
                    Container(height: 16),
                    TextField(
                      controller: commentCon,
                      maxLines: 6,
                      maxLength: 500,
                      decoration: InputDecoration(
                        enabledBorder: border,
                        focusedBorder: border,
                        hintText: 'Ваш комментарий',
                        contentPadding: EdgeInsets.all(16),
                        counter: Container(),
                      ),
                      style: TextStyle(
                        fontSize: 13,
                      ),
                      onChanged: changeComment,
                    ),
                    Container(height: 42),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: Center(
                child: CupertinoButton.filled(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                  child: Text('Сохранить'),
                  onPressed:
                  (vaccine.date == null) || (vaccine?.name?.isEmpty ?? true)
                      ? null
                      : saveVaccine,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void saveVaccine() {
    bool dateEmpty = vaccine.date == null;
    bool nameEmpty = vaccine?.name?.isEmpty ?? true;
    if (!nameEmpty && !dateEmpty) {
      FocusScope.of(context).requestFocus(FocusNode());
      Navigator.pop(context, vaccine);
    } else {
      String text;
      if (nameEmpty) {
        if (vaccineNameButtonValue == ownVaccineName) {
          text = 'Введите название прививки';
        } else {
          text = 'Выберите прививку';
        }
      } else if (dateEmpty) {
        text = 'Выберите дату прививки';
      }
      showDialog(
        context: context,
        builder: (_) => InfoDialog(
          title: text,
        ),
      );
    }
  }

  void selectVaccine() {
    showDialog<String>(
      context: context,
      builder: (_) => EventNameSelector(
        text: 'Выбор прививки',
        ownButtonText: ownVaccineName,
        names: vaccineNames,
      ),
    ).then((name) {
      if (name == null) {
        return;
      }
      setState(() {
        vaccineNameButtonValue = name;
        if (name != ownVaccineName) {
          vaccine = vaccine.copyWith(name: name);
        } else {
          vaccine = vaccine.copyWith(name: '');
        }
      });
    });
  }

  void selectDate() async {
    DateTime now = DateTime.now();
    DateTime initDate = vaccine.date == null
        ? DateTime.utc(now.year, now.month, now.day)
        : DateTime.utc(
            vaccine.date.year,
            vaccine.date.month,
            vaccine.date.day,
          );
    DateTime date = await showDatePicker(
      context: context,
      initialDate: initDate,
      firstDate: bloc.child.value.birthDate,
      lastDate: DateTime.utc(now.year, now.month, now.day, 11, 59),
    );
    if (date == null) {
      return;
    }
    date = DateTime.utc(
      date.year,
      date.month,
      date.day,
    );
    changeDate(date);
  }

  void changeDate(DateTime date) {
    setState(() => vaccine = vaccine.copyWith(date: date));
  }

  void changeName(String name) {
    setState(() => vaccine = vaccine.copyWith(name: name));
  }

  void changeComment(String comment) {
    setState(() => vaccine = vaccine.copyWith(comment: comment));
  }
}
