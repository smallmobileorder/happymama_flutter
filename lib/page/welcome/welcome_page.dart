import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/user.dart';
import 'package:happy_mama/page/home.dart';
import 'package:happy_mama/service/firebase/baby/baby_register_service.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/brand_button.dart';
import 'package:happy_mama/widgets/brand_checkbox.dart';
import 'package:happy_mama/widgets/brand_raw_button.dart';
import 'package:happy_mama/widgets/brand_text_field.dart';
import 'package:happy_mama/widgets/loading_container.dart';
import 'package:intl/intl.dart';

import 'package:happy_mama/widgets/gradient_container.dart';

class WelcomePage extends StatefulWidget {
  final User parent;

  WelcomePage({Key key, this.parent}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final _registerService = BabyRegisterService();
  final _userService = UserService();
  final ScrollController scrollController = ScrollController();

  String _date;
  int _step = -1;
  bool _notBornYet = false;
  String _gender = '';
  var isLoading = false;
  bool privacyPolicyAccepted = false;

  final _nameController = TextEditingController();

  @override
  void initState() {
    _nameController.addListener(() async {
      //print('ee');
      await scrollController.animateTo(0.0,
          duration: Duration(milliseconds: 200), curve: Curves.easeOut);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        child: _buildFragment(),
      ),
    );
  }

  Widget _buildFragment() {
    Widget child;
    if (_step == -1) {
      child = _buildPrivacyPolicy();
    } else if (_step == 0) {
      child = _buildWelcomeFragment();
    } else if (_step == 1) {
      child = _buildQuestionsFragment();
    } else if (_step == 2) {
      child = _buildFinishFragment();
    } else if (_step == 3) {
      child = _buildBabyNotBorn();
    } else if (_step == 4) {
      child = _buildAlert();
    }

    return Stack(children: [
      Positioned.fill(
          child: Container(
        color: Color(0xFFF9F7F0),
      )),
      Positioned.fill(
          child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color.fromRGBO(34, 202, 222, 0.54), Colors.transparent],
          ),
        ),
      )),
      Positioned(
        left: 0,
        top: 0,
        right: 0,
        child: SvgPicture.asset('assets/vectors/toys.svg'),
      ),
      isLoading
          ? Center(
              child: LoadingContainer(
              color: ColorStyle.cyan,
            ))
          : Positioned(
              left: 28,
              top: 28,
              right: 28,
              bottom: 28,
              child: SafeArea(
                child: Center(
                  child: GradientContainer(
                    child: SingleChildScrollView(
                        controller: scrollController,
                        reverse: true,
                        child: Padding(
                            padding: EdgeInsets.only(
                                left: 24, top: 24, right: 24, bottom: 16),
                            child: child)),
                  ),
                ),
              ),
            )
    ]);
  }

  Text head(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color(0xFF444444),
      ),
    );
  }

  Text body(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 15,
        color: Color(0xFF444444),
      ),
    );
  }

  Widget paragraph(Widget number, Widget text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        number,
        Container(width: 8),
        Expanded(
          child: text,
        ),
      ],
    );
  }

  Widget _buildPrivacyPolicy() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          head('Пользовательское Соглашение и Политика конфиденциальности'),
          Container(height: 16),
          paragraph(
            body('1.'),
            body(
              'Настоящее Пользовательское Соглашение (далее Соглашение) регулирует отношения между владельцем мобильного приложения HappyMama (Администрация) с одной стороны и пользователем приложения с другой.',
            ),
          ),
          Container(height: 16),
          paragraph(
            body('2.'),
            body(
              'Пользователь подтверждает, что отметка им чекбокса “Согласен с пользовательским соглашением” и использование приложения означает полное согласие Пользователя со всеми положениями настоящего Соглашения.',
            ),
          ),
          Container(height: 16),
          paragraph(
            body('3.'),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                head('Права и обязанности сторон'),
                paragraph(
                  body('3.1.'),
                  head(
                    'Пользователь имеет право:',
                  ),
                ),
                body(
                  '''
- использовать информацию из приложения в личных некоммерческих целях;
- заполнять данные о ребенке, менять и удалять их.''',
                ),
                paragraph(
                  body('3.2.'),
                  head(
                    'Администрация имеет право:',
                  ),
                ),
                body(
                  '''
- по своему усмотрению и необходимости создавать, изменять, отменять данные правила;
- создавать, изменять, удалять информацию в мобильном приложении.''',
                ),
                paragraph(
                  body('3.3.'),
                  head(
                    'Пользователь обязуется:',
                  ),
                ),
                body(
                  '''
- не использовать информацию из приложения в коммерческих целях;
- не копировать и не распространять материалы из приложения без указания прямой явной ссылки на мобильное приложение;
- не использовать информацию, связанную с медициной и здоровьем ребенка или взрослого, в качестве замены консультации с медицинским специалистом;
- в любых ситуациях, которые могут касаться здоровья ребенка или взрослого, консультироваться с лечащим врачом.''',
                ),
                paragraph(
                  body('3.4.'),
                  head(
                    'Администрация обязуется:',
                  ),
                ),
                body(
                  '''
- поддерживать работоспособность мобильного приложения за исключением случаев, когда это невозможно по независящим от Администрации причинам.''',
                ),
              ],
            ),
          ),
          Container(height: 16),
          paragraph(
            body('4.'),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                head('Ответственность сторон'),
                body(
                  '4.1. Администрация не несет ответственности за актуальность медицинской информации, предоставленной в приложении в связи с быстрыми изменениями в медицине и смежных областях.',
                ),
                body(
                  '4.2. Вся информация, предоставляемая в приложении, носит сугубо информационный характер и не должна истолковываться как индивидуальная рекомендация, указание к действию или замена консультации с медицинским сотрудником.',
                ),
                body(
                  '4.3. Администрация не несет ответственности за последствия применения указанной в приложении информации в связи с ее общим характером и наличием индивидуальных особенностей здоровья у каждого ребенка или взрослого.',
                ),
              ],
            ),
          ),
          Container(height: 16),
          paragraph(
            body('5.'),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                head('Персональные данные и Политика конфиденциальности'),
                body(
                  '''
5.1 Для полноценного функционирования приложения Пользователь соглашается предоставить и дает согласие на обработку персональных данных в соответствии с ФЗ 27.07.2006 №152-ФЗ «О персональных данных». Под «персональными данными» понимаются данные, необходимые для входа в приложение, а также данные, заполняемые Пользователем и включающие в себя:
- имя, дату рождения и пол ребенка
- информацию о росте, весе, зубах, поставленных прививках и посещенных врачах
- фотографии''',
                ),
                body(
                  '5.2 Администрация гарантирует конфиденциальность в отношении персональных данных и предоставляет к ним доступ только тем сотрудникам, которым эта информация нужна для обеспечения полноценного функционирования приложения.',
                ),
                body(
                  '5.3 Полученная Администрацией информация (персональные данные) не подлежит разглашению, за исключением случаев, когда ее раскрытие является обязательным по законодательству Российской Федерации или необходимо для работы приложения и его функций.',
                ),
              ],
            ),
          ),
          Container(height: 16),
          paragraph(
            body('6.'),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                head('Условия действия Соглашения'),
                body(
                  '6.1 Данное Соглашение вступает в силу в момент первого входа в мобильное приложение и установки чекбокса о согласии с Соглашением.',
                ),
                body(
                  '6.2 Соглашение перестает действовать при появлении его новой версии.',
                ),
                body(
                  '6.3 Администрация оставляет за собой право в одностороннем порядке изменять данное соглашение по своему усмотрению.',
                ),
                body(
                  '6.4 Администрация не оповещает пользователей об изменении в Соглашении.',
                ),
              ],
            ),
          ),
          Container(height: 8),
          Row(
            children: <Widget>[
              Checkbox(
                onChanged: (value) =>
                    setState(() => privacyPolicyAccepted = value),
                value: privacyPolicyAccepted,
                checkColor: ColorStyle.cyan,
                activeColor: Colors.transparent,
              ),
              Container(width: 8),
              Expanded(
                child: Text(
                  'Я согласен с условиями пользовательского соглашения',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Color(0xFF444444),
                  ),
                ),
              ),
            ],
          ),
          Container(height: 8),
          Center(
            child: CupertinoButton.filled(
              child: Text('Далее'),
              onPressed: privacyPolicyAccepted
                  ? () {
                      setState(() => _step = 0);
                    }
                  : null,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildWelcomeFragment() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          'Дорогие родители!',
          maxLines: 2,
          style: TextStyle(
              color: ColorStyle.dirtyBlack,
              fontSize: 18,
              fontWeight: FontWeight.bold),
        ),
        Padding(
          padding: EdgeInsets.only(top: 24),
          child: Text(
            """
В вашей жизни произошло чудо – появился (или вот-вот появится) маленький человек!

Первый год бывает не только счастливым, но и, порой, сложным, тревожным. Малышу постоянно нужны вы – ваше время, любовь, спокойствие, мудрость. Иногда может не хватать сил, терпения, знаний, простого доброго слова.

И мы надеемся, что в такие моменты наше приложение сможет вам помочь – подбодрит, даст нужную информацию, заставит улыбнуться

Для того чтобы оно заработало, нам нужно познакомиться немного с вашим крохой! Ответьте, пожалуйста, на несколько вопросов.

Поехали?
""",
            style: TextStyle(
                color: ColorStyle.dirtyBlack,
                fontSize: 12,
                fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 24),
          child: SizedBox(
            height: 36,
            child: Row(
              children: [
                Expanded(
                  child: BrandButton(
                    onPressed: () => setState(() => _step = 4),
                    text: 'Пропустить',
                    color: ColorStyle.inactive,
                    borderRadius: 10,
                  ),
                ),
                Container(width: 24),
                Expanded(
                  child: BrandButton(
                    color: ColorStyle.cyan,
                    onPressed: () => setState(() => _step = 1),
                    text: 'Начать',
                    borderRadius: 10,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildQuestionsFragment() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _notBornYet
            ? Container()
            : Text(
                'Дата рождения малыша?',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: ColorStyle.dirtyBlack),
              ),
        _notBornYet
            ? Container()
            : BrandRawButton(
                onPressed: () => _pickDate(),
                color: Colors.transparent,
                child: Row(
                  children: [
                    Text(
                      (_notBornYet) ? 'ДД.ММ.ГГГГ' : _date ?? 'ДД.ММ.ГГГГ',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                          color: ColorStyle.dirtyBlack),
                    ),
                    Container(width: 16),
                    SizedBox(
                      width: 24,
                      height: 24,
                      child:
                          SvgPicture.asset('assets/vectors/smallCalendar.svg'),
                    )
                  ],
                ),
              ),
        Container(height: 8),
        BrandCheckbox(
            key: ValueKey(_notBornYet),
            onChecked: (value) {
              setState(() {
                _notBornYet = value;
                //_date = null;
                //_nameController.text = '';
                //_gender = '';
              });
            },
            value: _notBornYet,
            text: 'Малыш ещё не родился'),
        ((_notBornYet) || _date == null)
            ? Container()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(height: 24),
                  Text(
                    'У вас мальчик или девочка?',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: ColorStyle.dirtyBlack),
                  ),
                  Container(height: 12),
                  CheckboxGroup(
                    padding: EdgeInsets.zero,
                    labels: ["Мальчик", "Девочка"],
                    onSelected: (selection) => setState(() =>
                        _gender = selection.length == 0 ? '' : selection.last),
                    checked: [_gender],
                    itemBuilder: (checkbox, text, index) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: index == 0 ? 12 : 0),
                        child: SizedBox(
                          height: 16,
                          child: Row(
                            children: [
                              Container(
                                height: 16,
                                width: 16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: ColorStyle.cyan, width: 1),
                                    borderRadius: BorderRadius.circular(2)),
                                child: Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor: Colors.transparent,
                                  ),
                                  child: Checkbox(
                                    onChanged: (value) => setState(() => value
                                        ? _gender = text.data
                                        : _gender = ''),
                                    value: _gender.contains(text.data),
                                    checkColor: ColorStyle.cyan,
                                    activeColor: Colors.transparent,
                                  ),
                                ),
                              ),
                              Container(width: 16),
                              InkWell(
                                onTap: () =>
                                    setState(() => _gender = text.data),
                                child: Text(
                                  text.data,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: ColorStyle.dirtyBlack,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  Container(height: 8),
                  Text(
                      'Если у вас два и более детей до года, вы сможете позже добавить отдельный профиль на каждого ребенка в разделе Настройки',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 9,
                          color: ColorStyle.dirtyBlack)),
                  (_gender.isEmpty)
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(height: 24),
                            Text(
                              'Как зовут кроху?',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: ColorStyle.dirtyBlack),
                            ),
                            Container(height: 12),
                            BrandTextField(
                              maxLength: 30,
                              controller: _nameController,
                              placeholder: 'Имя крохи',
                            )
                          ],
                        )
                ],
              ),
        (_notBornYet ||
                (!_notBornYet &&
                    _gender.isNotEmpty &&
                    (_nameController.text ?? '').isNotEmpty))
            ? Padding(
                padding: EdgeInsets.only(top: 24),
                child: Center(
                  child: SizedBox(
                    width: 124,
                    height: 36,
                    child: BrandButton(
                      color: ColorStyle.cyan,
                      onPressed: () =>
                          setState(() => _step = (_notBornYet) ? 3 : 2),
                      text: 'Сохранить',
                      borderRadius: 10,
                    ),
                  ),
                ),
              )
            : Container()
      ],
    );
  }

  Widget _buildFinishFragment() {
    return Column(
      children: [
        Text(
          'Приятно познакомиться! ${(_nameController?.text[0]?.toUpperCase() ?? '') + (_nameController?.text?.substring(1) ?? '')} - наверняка ${_gender == 'Мальчик' ? 'чудесный' : 'чудесная'} ${_gender == 'Мальчик' ? 'малыш' : 'малышка'}! Добавьте фото и расскажите о ${_gender == 'Мальчик' ? 'нем' : 'ней'} больше в разделе «Профиль»',
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 12),
        ),
        Padding(
          padding: EdgeInsets.only(top: 24),
          child: Center(
            child: SizedBox(
              width: 124,
              height: 36,
              child: BrandButton(
                color: ColorStyle.cyan,
                onPressed: () => _register(),
                text: 'Готово',
                borderRadius: 10,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildBabyNotBorn() {
    return Column(
      children: [
        Text(
          'В таком случае отложим знакомство до рождения крохи! Часть функций приложения не будет пока доступной. Рекомендуем прочитать полезные статьи в разделе «База знаний»',
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 12),
        ),
        Padding(
          padding: EdgeInsets.only(top: 24),
          child: Center(
            child: SizedBox(
              width: 124,
              height: 36,
              child: BrandButton(
                color: ColorStyle.cyan,
                onPressed: () => _register(),
                text: 'ОК',
                borderRadius: 10,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildAlert() {
    return Column(
      children: [
        Text(
          """
Приложение не может полноценно работать без информации о малыше. Рекомендуем ввести как минимум дату рождения.

Вы можете сделать это в разделе «Профиль».
""",
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 12),
        ),
        Padding(
          padding: EdgeInsets.only(top: 24),
          child: Center(
            child: SizedBox(
              width: 124,
              height: 36,
              child: BrandButton(
                color: ColorStyle.cyan,
                onPressed: () => _register(),
                text: 'ОК',
                borderRadius: 10,
              ),
            ),
          ),
        )
      ],
    );
  }

  _pickDate() async {
    final now = DateTime.now();
    final date = await showDatePicker(
      context: context,
      initialDate: now,
      firstDate: now.subtract(Duration(days: 365)),
      lastDate: now,
    );
    if (date != null) {
      setState(() {
        _date = DateFormat("dd.MM.yyyy").format(date);
        _notBornYet = false;
      });
    }
  }

  _register() {
    setState(() => isLoading = true);
    if (_notBornYet) {
      _registerService
          .register(widget.parent.identifier, '', '', '')
          .then((_) => _userService.fetchInfo())
          .then((user) async {
        final bloc = BlocProvider.of<ProfileBloc>(context);
        await bloc.init(user);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => HomePage(),
                settings: RouteSettings(name: 'Домашняя страница')),
            (_) => false);
      }).whenComplete(() => setState(() => isLoading = true));
      return;
    }
    String childName = '';
    try {
      childName = (_nameController?.text[0]?.toUpperCase() ?? '') +
          (_nameController?.text?.substring(1) ?? '');
    } on RangeError {
      //print(childName);
      childName = '';
    }
    String _genderNew = '';
    if (_gender == 'Мальчик') {
      _genderNew = 'male';
    } else if (_gender == 'Девочка') {
      _genderNew = 'female';
    }
    _registerService
        .register(widget.parent.identifier, _date, _genderNew, childName)
        .then((_) => _userService.fetchInfo())
        .then((user) async {
      final bloc = BlocProvider.of<ProfileBloc>(context);
      await bloc.init(user);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => HomePage(),
              settings: RouteSettings(name: 'Домашняя страница')),
          (_) => false);
    }).whenComplete(() => setState(() => isLoading = true));
  }
}
