import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_mama/page/album/custom_dialog.dart' as kk;
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:numberpicker/numberpicker.dart';

class SetImageDateDialog extends StatefulWidget {
  final File image;
  final Function onPressed;
  final int maxMonth;

  SetImageDateDialog({this.image, this.onPressed, this.maxMonth});

  @override
  State<StatefulWidget> createState() => _SetImageDateDialogState();
}

class _SetImageDateDialogState extends State<SetImageDateDialog> {
  int month;
  int currentMonth = 0;

  @override
  void initState() {
    if ((widget?.maxMonth ?? 0) == 0) month = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double mainWidth = width - 72;
    //print('hello');
    return kk.CustomDialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(width: 2, color: Theme.of(context).primaryColor),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: mainWidth,
              height: mainWidth,
              margin: EdgeInsets.only(top: 24),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              child: Image(
                fit: BoxFit.cover,
                image: FileImage(widget.image),
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(24, 16, 24, 10),
                child: Text(
                  'Сколько месяцев было ребёнку во время этой фотографии?',
                  style: TextStyle(
                      color: ColorStyle.codGray,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                )),
            CupertinoButton(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 10),
              onPressed: (widget?.maxMonth ?? 0) == 0 ? null : selectDate,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    month == null
                        ? 'XX месяцев'
                        : '$month ${Formatter.monthSuffix(month)}',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: ColorStyle.tundora),
                    textAlign: TextAlign.start,
                  ),
                  Container(
                    width: 10,
                  ),
                  SvgPicture.asset(
                    'assets/vectors/calendar.svg',
                    color: ColorStyle.tint,
                  )
                ],
              ),
            ),
            Container(
              height: 10,
            ),
            SizedBox(
              width: mainWidth / 2.5,
              height: 36,
              child: CupertinoButton.filled(
                padding: EdgeInsets.all(0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'Установить',
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                ),
                onPressed: month == null
                    ? null
                    : () async {
                        await widget.onPressed(widget.image, month);
                        //print('load to server');
                        Navigator.pop(context);
                      },
              ),
            ),
            Container(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  void selectDate() async {
    month = await showDialog(
        context: context,
        builder: (_) => MonthPicker(
              month: month,
              maxMonth: widget?.maxMonth ?? 0,
            ));
    setState(() {});
  }
}

class MonthPicker extends StatefulWidget {
  final int month;
  final int maxMonth;

  const MonthPicker({Key key, this.month = 0, this.maxMonth = 0})
      : super(key: key);

  createState() => _MonthPickerState();
}

class _MonthPickerState extends State<MonthPicker> {
  int currentMonth;
  int maxMonth;

  @override
  void initState() {
    currentMonth = widget?.month ?? 0;
    maxMonth = widget?.maxMonth ?? 0;
    maxMonth = maxMonth > 12 ? 12 : maxMonth;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        //margin: EdgeInsets.only(left: 20, right: 20),
        child: Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
                side: BorderSide(color: ColorStyle.tint, width: 2)),

            //backgroundColor: Colors.transparent,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 24, horizontal: 0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        NumberPicker.integer(
                            initialValue: currentMonth,
                            minValue: 0,
                            maxValue: maxMonth,
                            onChanged: (value) =>
                                setState(() => currentMonth = value)),
                        Text(
                          'мес',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: ColorStyle.codGray),
                        )
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 44,
                      margin: EdgeInsets.symmetric(horizontal: 48),
                      child: FlatButton(
                        padding: EdgeInsets.zero,
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          'Выбрать',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onPressed: () => setState(() {
                          Navigator.of(context).pop(currentMonth);
                        }),
                      ),
                    ),
                  ],
                ))));
  }
}
