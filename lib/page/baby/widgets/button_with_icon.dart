import 'package:flutter/material.dart';

class ButtonWithIcon extends StatelessWidget {
  final bool disableButtons;

  final Function onTapIfActive;

  final Function onTapIfDisabled;

  final String iconPath;

  final String title;

  final Widget label;

  ButtonWithIcon(
      {Key key,
      this.disableButtons = false,
      @required this.onTapIfActive,
      this.onTapIfDisabled,
      this.title='',
      @required this.iconPath,
      this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      onPressed: disableButtons ? onTapIfDisabled : onTapIfActive,
      disabledColor: Theme.of(context).disabledColor,
      label: Expanded(
        child: label??Text(
          title??'',
          style: TextStyle(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
        ),
      ),
      icon: Container(
        width: 26,
        height: 26,
        child: Image.asset(
          iconPath??'',
          color: Colors.white,
        ),
      ),
      color: disableButtons
          ? Theme.of(context).disabledColor
          : Theme.of(context).buttonColor,
    );
  }
}
