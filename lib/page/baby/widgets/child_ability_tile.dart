import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/baby/motor_skills/motor_skills.page.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/baby_info_dialog.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';

class ChildAbilityTile extends StatefulWidget {
  final Function onTap;
  final StreamSink<bool> showSettingsSurface;

  const ChildAbilityTile(this.showSettingsSurface, {Key key, this.onTap})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChildAbilityTileState();
}

class _ChildAbilityTileState extends State<ChildAbilityTile> {
  ProfileBloc bloc;

  @override
  void initState() {
    super.initState();
    initBloc();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        if (bloc.child.value.birthDate == null) {
          widget.showSettingsSurface.add(false);
          showDialog(
            context: context,
            builder: (context) => CustomCupertinoAlertDialog(
              title: Text('Этот раздел недоступен'),
              content: Text(
                  'Заполните профиль ребенка, чтобы пользоваться полным функционалом'),
              actions: <Widget>[
                CustomCupertinoDialogAction(
                  child: Text('Позже'),
                  isDefaultAction: true,
                  onPressed: () => Navigator.of(context).pop(),
                ),
                CustomCupertinoDialogAction(
                  child: Text('Ок'),
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.of(context).pop();
                    showInfoDialog();
                  },
                ),
              ],
            ),
          );
          return;
        }
        Navigator.of(context).push(CupertinoPageRoute(
          builder: (_) => MotorSkillsPage(),
          settings: RouteSettings(name: 'Двигательные навыки'),
        ));
      },
      child: StreamBuilder<Child>(
          initialData: Child(),
          stream: bloc.child,
          builder: (context, childSnap) {
            return StreamBuilder<String>(
              stream: bloc.skills,
              builder: (context, snapshot) {
                return Container(
                  width: width - 40,
                  // height: (width - 40) / 320 * 80,
                  margin: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: stateColor(childSnap?.data?.birthDate != null),
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(15)),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset(
                        'assets/vectors/crawlingBaby.svg',
                        color: stateColor(childSnap?.data?.birthDate != null),
                      ),
                      Container(
                        width: 21,
                      ),
                      Expanded(
                        child: Text(
                          childSnap?.data?.birthDate != null
                              ? (snapshot.hasData && snapshot.data.isNotEmpty
                                  ? snapshot.data
                                  : 'Расскажите, какими двигательными навыками уже владеет ${childSnap?.data?.name ?? 'малыш'}')
                              : 'Расскажите, какими двигательными навыками уже владеет ваш ребенок',
                          maxLines: 3,
                          style: TextStyle(
                            fontSize: 12,
                            color: ColorStyle.codGray,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }),
    );
  }

  Color stateColor(bool hasData) {
    return hasData ? ColorStyle.tint : ColorStyle.inactive;
  }

  initBloc() async {
    bloc = BlocProvider.of<ProfileBloc>(context);
  }

  void showInfoDialog() {
    widget.showSettingsSurface.add(false);
    showDialog(
      context: context,
      builder: (_) => BabyInfoDialog(),
    );
  }
}
