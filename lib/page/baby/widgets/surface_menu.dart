import 'package:flutter/material.dart';

class SurfaceMenu extends StatelessWidget {
  final Function buyPremium;
  final Function chooseFotoTap;
  final Function fillBabyInfoTap;
  final Function settingsTap;
  final Function exit;
  final Function changeProfile;

  final Stream showStream;

  final bool initialData;

  const SurfaceMenu(
      {Key key,
      this.chooseFotoTap,
      this.fillBabyInfoTap,
      this.settingsTap,
      this.exit,
      this.showStream,
      this.changeProfile,
      this.initialData,
      this.buyPremium})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 800),
      child: StreamBuilder<bool>(
          stream: showStream,
          initialData: initialData,
          builder: (context, snapshot) {
            if (!snapshot.data)
              return Container(
                width: 10,
                height: 10,
              );
            return Column(
              children: <Widget>[
                if (buyPremium != null)
                  FlatButton(
                      child: Text(
                        'Купить премиум',
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).primaryColor),
                      ),
                      onPressed: buyPremium),
                if (chooseFotoTap != null)
                  FlatButton(
                      child: Text(
                        'Выбрать фото',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      onPressed: chooseFotoTap),
                if (fillBabyInfoTap != null)
                  FlatButton(
                    child: Text(
                      'Анкета ребенка',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    onPressed: fillBabyInfoTap,
                  ),
                if (changeProfile != null)
                  FlatButton(
                    child: Text(
                      'Сменить профиль',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    onPressed: changeProfile,
                  ),
                if (settingsTap != null)
                  FlatButton(
                    child: Text(
                      'Настройки',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    onPressed: settingsTap,
                  ),
                if (exit != null)
                  FlatButton(
                    child: Text(
                      'Выйти из аккаунта',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    onPressed: exit,
                  ),
              ],
            );
          }),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          width: 2,
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}
