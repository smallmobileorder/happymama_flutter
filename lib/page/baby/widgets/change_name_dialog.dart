import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';
import 'package:happy_mama/widgets/info_dialog.dart';
import 'package:happy_mama/widgets/loading_container.dart';

class ChangeNameDialog extends StatefulWidget {
  createState() => _ChangeNameDialogState();
}

class _ChangeNameDialogState extends State<ChangeNameDialog> {
  BabyInfoService service;
  ProfileBloc bloc;
  TextEditingController controller;
  bool buttonPressed;

  @override
  void initState() {
    controller = TextEditingController();
    service = BabyInfoService();
    initBloc();
    buttonPressed = false;
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomCupertinoAlertDialog(
      title: buttonPressed
          ? Container()
          : Text('Как зовут кроху?',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: ColorStyle.codGray,
              )),
      content: Padding(
        padding: EdgeInsets.only(top: 10),
        child: buttonPressed
            ? LoadingContainer()
            : TextField(
                controller: controller,
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(
                  hintText: 'Имя крохи',
                  hintStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 12),
                  contentPadding: EdgeInsets.fromLTRB(16, 11, 16, 11),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      width: 2,
                      color: ColorStyle.tint, //Theme.of(context).primaryColor,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      width: 2,
                      color: ColorStyle.tint, //Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ),
      ),
      actions: <Widget>[
        if (!buttonPressed)
          CustomCupertinoDialogAction(
            child: Text('Отмена'),
            onPressed: () => Navigator.of(context).pop(),
          ),
        if (!buttonPressed)
          CustomCupertinoDialogAction(
            child: Text('Сменить'),
            isDefaultAction: true,
            onPressed: save,
          ),
      ],
    );
  }

  void save() {
    if (buttonPressed) return;
    buttonPressed = true;
    String name = controller.text;
    if (name.isEmpty) {
      showError('Введите имя ребенка');
      buttonPressed = false;
      return;
    }
    name = controller.text[0].toUpperCase() +
        (controller?.text?.substring(1) ?? '');
    service
        .postInfo(bloc.child.value.id, name, bloc.child.value.gender,
            Formatter.formatDate(bloc.child.value.birthDate))
        .whenComplete(() {
      bloc.updateName(name);
      buttonPressed = false;
      Navigator.of(context).pop();
    }).catchError((e) {
      showError(e);
      buttonPressed = false;
      Navigator.of(context).pop();
    });
  }

  void showError(String text) {
    showDialog(
      context: context,
      builder: (_) => InfoDialog(
        title: text,
      ),
    );
  }

  void initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context);
  }
}
