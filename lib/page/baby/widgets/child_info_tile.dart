import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:date_util/date_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/entity/height_entity.dart';
import 'package:happy_mama/entity/weight_entity.dart';
import 'package:happy_mama/page/baby/util/step_qualifier.dart';
import 'package:happy_mama/page/height/height_page.dart';
import 'package:happy_mama/page/height/util/height_status.dart';
import 'package:happy_mama/page/weight/util/weight_status.dart';
import 'package:happy_mama/service/firebase/baby/change_date.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/util/month_counter.dart';
import 'package:happy_mama/util/next_month_birdthday.dart';
import 'package:happy_mama/widgets/baby_info_dialog.dart';
import 'package:happy_mama/widgets/confirm_dialog.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';

import '../../home.dart';
import '../evolution_steps.dart';
import 'package:happy_mama/page/weight/weight_page.dart';

import 'change_name_dialog.dart';

class ChildInfoTile extends StatefulWidget {
  final Function tapToWeight;
  final Function tapToHeight;
  final Function tapToMonths;
  final Function tapToWeeks;
  final StreamSink<bool> showSettingsSurface;

  const ChildInfoTile(this.showSettingsSurface,
      {Key key,
      this.tapToWeight,
      this.tapToHeight,
      this.tapToMonths,
      this.tapToWeeks})
      : super(key: key);

  @override
  createState() => _ChildInfoTileState();
}

class _ChildInfoTileState extends State<ChildInfoTile> {
  ProfileBloc bloc;
  ChangeDateService changeDateService = ChangeDateService();

  @override
  void initState() {
    super.initState();
    initBloc();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return StreamBuilder<Child>(
        stream: bloc.child,
        initialData: Child(),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Container();
          }
          return Container(
            width: width - 40,
            //height: (width - 40) / 320 * 88,
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.25),
                      offset: Offset(0, -1),
                      blurRadius: 4)
                ],
                borderRadius: BorderRadius.circular(15)),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    if ((snapshot?.data?.isEmpty() ?? true)) {
                      showFillInfoDialog();
                      return;
                    }
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (_) => HeightPage()));
                  },
                  child: StreamBuilder<List<HeightEntity>>(
                      stream: bloc.heights,
                      initialData: [],
                      builder: (context, height) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.string(_getRulerSvgString(
                                (height.data.isNotEmpty
                                    ? height.data.first
                                    : null))),
                            Text(
                              'Рост',
                              style: TextStyle(
                                color: ColorStyle.codGray.withOpacity(0.65),
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              (height?.data?.isEmpty ?? true)
                                  ? '...'
                                  : '${height.data.first.height} см',
                              style: TextStyle(
                                color: ColorStyle.codGray.withOpacity(0.65),
                                fontSize: 10,
                                fontWeight: FontWeight.w600,
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        );
                      }),
                ),
                Container(width: 5),
                Expanded(
                  child: (snapshot.data.name != null &&
                          snapshot.data.birthDate != null)
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            InkWell(
                              child: AutoSizeText(
                                snapshot.data.name == null
                                    ? ''
                                    : '${snapshot.data.name[0].toUpperCase()}${snapshot.data.name.substring(1).toLowerCase()}',
                                minFontSize: 12,
                                maxFontSize: 18,
                                maxLines: 2,
                                style: TextStyle(
                                  color: Colors.black.withOpacity(0.65),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              onTap: showChangeNameDialog,
                            ),
                            InkWell(
                              onTap: () async {
                                if (snapshot.data == null ||
                                    snapshot.data.birthDate == null) {
                                  showFillInfoDialog();
                                  return;
                                }
                                bool change = await showDialog<bool>(
                                  context: context,
                                  builder: (_) => ConfirmDialog(
                                    title:
                                        'Вы действительно хотите изменить дату рождения?',
                                    destructiveIsBlue: true,
                                  ),
                                );
                                if (change == null || !change) {
                                  return;
                                }
                                final now = DateTime.now();
                                final date = await showDatePicker(
                                  context: context,
                                  initialDate: snapshot.data.birthDate,
                                  firstDate: snapshot.data.birthDate
                                      .subtract(Duration(days: 365)),
                                  lastDate: now,
                                );
                                if (date == null) {
                                  return;
                                }
                                await bloc.notifyBloc.disableNotifications();
                                await bloc.notifyBloc
                                    .deleteById(snapshot.data.id);
                                await changeDateService.changeDate(
                                  snapshot.data.id,
                                  date,
                                  snapshot.data.birthDate,
                                  snapshot.data.mainImageId,
                                );
                                bloc.notifyBloc.disableNotifications();
                                await bloc.notifyBloc
                                    .deleteById(snapshot.data.id);
                                await bloc.init(bloc.parent,
                                    mainBabyBirthday: date);
                                /*bloc.notifyBloc.disableNotifications();
                                await bloc.notifyBloc
                                    .deleteById(snapshot.data.id);
                                await bloc.getTeeth();
                                bloc.changeChildInfo(
                                  snapshot.data.name,
                                  snapshot.data.gender,
                                  snapshot.data.wasBorn,
                                  date,
                                  mainImageId: newMainImageId,
                                  setMainImageToNull: true,
                                );*/
                              },
                              child: Text(
                                snapshot.data.birthDate == null
                                    ? ''
                                    : '${Formatter.formatDate(snapshot.data.birthDate)}',
                                style: TextStyle(
                                  color: Colors.black.withOpacity(0.65),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                if (snapshot.data.birthDate == null) {
                                  showFillInfoDialog();
                                  return;
                                } else {
                                  Navigator.of(context).push(
                                    CupertinoPageRoute(
                                        builder: (_) => EvolutionStepsPage(),
                                        settings: RouteSettings(
                                            name: 'Этапы развития')),
                                  );
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    snapshot.data.birthDate == null
                                        ? ''
                                        : '${(DateTime.now().difference(snapshot.data.birthDate).inDays / 7).floor() + 1}-${Formatter.weekSuffix((DateTime.now().difference(snapshot.data.birthDate).inDays / 7).floor() + 1)}',
                                    style: TextStyle(
                                      color:
                                          ColorStyle.codGray.withOpacity(0.65),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Container(
                                    width: 5,
                                  ),
                                  SvgPicture.asset(
                                    'assets/vectors/steps/${getCurrentEvolutionStep((DateTime.now().difference(snapshot?.data?.birthDate ?? DateTime.now()).inDays / 7).floor() + 1).toString().split('.').last.toLowerCase()}.svg',
                                    color: ColorStyle.tint,
                                    height: 20,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: 2,
                            ),
                            InkWell(
                              onTap: () {
                                if (snapshot.data.birthDate == null) {
                                  showFillInfoDialog();
                                  return;
                                } else {
                                  deeplinkNavigator.parsePathAndNavigate(
                                      '/calendar/${monthCounter(
                                        start: snapshot.data.birthDate,
                                        end: DateTime.now(),
                                      )}',
                                      prevTab: 0)();
                                  /*Navigator.of(context).push(CupertinoPageRoute(
                                      builder: (_) => MonthPage()));*/
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    snapshot.data.birthDate == null
                                        ? ''
                                        : '${monthCounter(end: DateTime.now(), start: snapshot.data.birthDate)} ${Formatter.monthSuffix(monthCounter(end: DateTime.now(), start: snapshot.data.birthDate))}',
                                    style: TextStyle(
                                      color:
                                          ColorStyle.codGray.withOpacity(0.65),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Container(
                                    width: 5,
                                  ),
                                  SvgPicture.asset(
                                    'assets/vectors/smallCalendar.svg',
                                    color: ColorStyle.tint,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                      : Center(child: Text('Нет данных')),
                ),
                Container(width: 5),
                InkWell(
                  onTap: () {
                    if ((snapshot?.data?.isEmpty() ?? true)) {
                      showFillInfoDialog();
                      return;
                    }
                    Navigator.of(context)
                        .push(CupertinoPageRoute(builder: (_) => WeightPage()));
                  },
                  child: StreamBuilder<List<WeightEntity>>(
                      stream: bloc.weights,
                      initialData: [],
                      builder: (context, weight) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.string(_getScalesSvgString(
                                weight.data.isNotEmpty
                                    ? weight.data.first
                                    : null)),
                            Text(
                              'Вес',
                              style: TextStyle(
                                color: ColorStyle.codGray.withOpacity(0.65),
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              (weight?.data?.isEmpty ?? true)
                                  ? '...'
                                  : '${weight.data.first.weight} кг',
                              style: TextStyle(
                                color: ColorStyle.codGray.withOpacity(0.65),
                                fontSize: 10,
                                fontWeight: FontWeight.w600,
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }

  initBloc() async {
    bloc = BlocProvider.of<ProfileBloc>(context);
  }

  String _getRulerSvgString(HeightEntity height) {
    return '''
          <svg width="27" height="35" viewBox="0 0 27 35" fill="none" xmlns="http://www.w3.org/2000/svg">
          <g clip-path="url(#clip0)">
          <rect x="18.501" y="0.623047" width="8.11997" height="34.3395" transform="rotate(31.7072 18.501 0.623047)" fill="${_getHeightColor(height)}"/>
          <path d="M18.0241 0.52153C17.3885 1.01366 0 28.6752 0 29.2084C0 29.885 0.451115 30.2541 3.54741 32.2226C6.60269 34.1706 7.23835 34.3142 8.07907 33.2274C8.34564 32.8583 9.86303 30.5207 11.4214 28.0396C12.9798 25.5584 16.8348 19.5094 19.9516 14.5881C23.0684 9.66687 25.6316 5.44279 25.6316 5.19673C25.6316 4.35601 25.242 4.00742 22.2892 2.12094C19.3775 0.275467 18.6803 0.00889863 18.0241 0.52153ZM21.6125 2.63357C24.4423 4.41753 24.8114 4.7046 24.8114 5.13521C24.8114 5.38127 23.499 7.59584 22.3302 9.33879C22.2482 9.44131 21.6946 9.17474 21.0589 8.76464C20.4232 8.35453 19.8081 8.06746 19.685 8.14948C19.357 8.35453 19.7671 8.76464 20.8538 9.42081C21.346 9.68738 21.7356 10.0155 21.7356 10.1385C21.7356 10.241 21.551 10.6101 21.305 10.9382L20.8948 11.5328L19.7671 10.7742C18.9263 10.2205 18.5778 10.0975 18.4137 10.2615C18.2907 10.3846 18.2087 10.5281 18.2702 10.5691C18.3112 10.6101 18.8238 10.9587 19.3775 11.3278C19.9516 11.6969 20.4437 12.0455 20.4847 12.066C20.5258 12.107 20.3412 12.4556 20.0951 12.8452L19.6645 13.5834L18.5983 12.8657C18.0241 12.4761 17.4295 12.148 17.2859 12.148C16.6913 12.148 17.0809 12.7016 18.1471 13.3783C18.7623 13.7679 19.2749 14.137 19.2749 14.1985C19.2749 14.26 19.0494 14.5676 18.7828 14.8957L18.3112 15.5109L16.4452 14.3216C15.4199 13.6654 14.4767 13.1938 14.3537 13.2758C14.0051 13.4808 14.2922 13.7474 16.2197 14.9777L18.0241 16.1465L17.614 16.8232C17.3679 17.1718 17.1219 17.4794 17.0604 17.4794C16.9783 17.4794 16.5067 17.1923 15.9941 16.8642C15.0303 16.2286 14.6613 16.126 14.4767 16.4336C14.4152 16.5361 14.8663 16.9462 15.4815 17.3563C16.0966 17.7664 16.6093 18.1765 16.6093 18.2586C16.6093 18.3816 16.1991 19.0788 15.8711 19.5094C15.8301 19.5504 15.3379 19.2838 14.7638 18.9352C12.9183 17.8075 12.4672 18.2176 14.2511 19.4069C14.8663 19.817 15.3789 20.2271 15.3789 20.3091C15.3789 20.3911 15.1739 20.6987 14.9073 21.0063L14.4562 21.5804L13.3284 20.8832C12.3031 20.2271 11.893 20.1245 11.893 20.5347C11.893 20.6372 12.4057 21.0473 13.0208 21.4574C14.2101 22.2366 14.2306 22.3186 13.4925 23.2619C13.2464 23.5899 13.0618 23.5284 11.2779 22.4006C9.49393 21.2933 9.02231 21.1088 9.02231 21.5804C9.02231 21.6829 9.86303 22.2776 10.8678 22.9338C11.893 23.5694 12.7133 24.2051 12.7133 24.3281C12.7133 24.4512 12.5492 24.7997 12.3442 25.1073L11.9956 25.6405L10.8063 24.9228C9.96555 24.4101 9.53494 24.2666 9.3914 24.4101C9.24787 24.5537 9.53494 24.8613 10.3346 25.3944C10.9703 25.8045 11.4829 26.2351 11.4829 26.3376C11.4829 26.6452 10.8268 27.5269 10.5807 27.5269C10.4577 27.5269 9.94505 27.2399 9.43241 26.9118C8.44816 26.2556 7.99705 26.1326 7.99705 26.5632C7.99705 26.7067 8.50968 27.1168 9.12484 27.4859C9.73999 27.855 10.2526 28.2241 10.2526 28.2856C10.2526 28.5727 7.25886 33.0634 6.99229 33.1659C6.82825 33.2274 5.41339 32.4482 3.81398 31.4434C1.39436 29.9261 0.922736 29.557 0.943241 29.1469C0.943241 28.8598 4.53166 22.9953 9.20686 15.6339C13.759 8.47757 17.696 2.26448 17.9831 1.83387C18.2497 1.42376 18.6188 1.07517 18.8033 1.07517C18.9879 1.07517 20.2592 1.77235 21.6125 2.63357Z" fill="#696969"/>
          <path d="M19.808 3.86391C19.1109 4.54059 19.726 5.99646 20.7103 5.99646C21.2639 5.99646 21.9406 5.27878 21.9406 4.70463C21.9406 3.69987 20.5052 3.14623 19.808 3.86391ZM21.0794 4.70463C21.0384 4.90968 20.8743 5.07372 20.7103 5.07372C20.5462 5.07372 20.3822 4.90968 20.3412 4.70463C20.3002 4.45857 20.4027 4.35604 20.7103 4.35604C21.0179 4.35604 21.1204 4.45857 21.0794 4.70463Z" fill="#696969"/>
          </g>
          <defs>
          <clipPath id="clip0">
          <rect width="26.0417" height="34.0387" fill="white" transform="translate(0 0.291626)"/>
          </clipPath>
          </defs>
          </svg>
          ''';
  }

  _getScalesSvgString(WeightEntity weight) {
    return '''
           <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.5 8C0.5 5.23858 2.73858 3 5.5 3H27.8C30.5614 3 32.8 5.23858 32.8 8V28C32.8 30.7614 30.5614 33 27.8 33H4.5C2.29086 33 0.5 31.2091 0.5 29V8Z" fill="${_getWeightColor(weight)}"/>
            <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="11" y="-1" width="12" height="4">
            <rect x="11" y="-1" width="12" height="4" fill="#C4C4C4"/>
            </mask>
            <g mask="url(#mask0)">
            <ellipse cx="16.854" cy="6.18194" rx="5.6813" ry="6" transform="rotate(-1.67277 16.854 6.18194)" fill="${_getWeightColor(weight)}"/>
            </g>
            <path d="M31.7553 4.30167C29.3877 1.62254 25.151 2.68173 21.9734 2.61943C20.9142 1.1241 19.1073 0.251827 16.8643 0.00260548C14.746 -0.0596998 12.6899 0.99949 11.5061 2.74404C9.2631 2.74404 7.02011 2.55712 4.77712 2.74404C3.53101 2.86865 2.34721 3.11787 1.53724 4.05245C0.727276 5.04933 0.353444 6.42005 0.291138 7.66616C0.166528 9.59762 0.166528 11.5291 0.104222 13.4606C0.0419172 15.6412 0.166528 17.7596 0.166528 19.878C0.104222 22.3702 -0.0826934 24.8624 0.0419172 27.3546C0.166528 29.4107 0.415749 32.2144 2.78335 32.7129C6.77089 33.5852 11.3815 33.0867 15.4313 33.0244C19.7304 32.9621 24.0918 33.2736 28.3908 32.9621C31.6307 32.7129 33.0014 31.9029 33.0637 28.4761C33.0637 23.9901 33.0637 19.4419 33.126 14.9559C33.1883 11.716 34.0606 6.9808 31.7553 4.30167ZM16.8643 0.937185C19.7304 1.24871 21.9734 2.74404 22.098 5.797C22.2226 8.91226 20.0419 11.716 16.7397 11.6537C13.8737 11.6537 11.2569 9.16148 11.2569 6.23314C11.2569 3.24248 13.936 0.812574 16.8643 0.937185ZM32.3161 13.3359C32.2538 16.7627 32.2538 20.1272 32.2538 23.554C32.2538 25.1116 32.2538 26.6693 32.2538 28.2269C32.2538 28.9123 32.3784 29.9092 32.0668 30.5945C31.3815 32.2144 29.5123 32.1521 28.0793 32.2144C24.8394 32.4014 21.5996 32.2768 18.3597 32.2144C15.1198 32.2144 11.8176 32.3391 8.57774 32.2768C7.02011 32.2144 5.40017 32.2768 3.90485 32.0275C1.84877 31.716 1.28802 30.3453 1.0388 28.6007C0.54036 25.1116 1.10111 21.4979 1.10111 17.9465C1.0388 14.6444 1.10111 11.2799 1.22572 7.97768C1.35033 5.17395 2.78335 3.80323 5.58709 3.67862C7.39394 3.61631 9.2631 3.67862 11.07 3.67862C10.7584 4.36398 10.5092 5.11164 10.5092 5.92161C10.26 9.41071 13.3129 12.6506 16.802 12.6506C20.3534 12.6506 23.0326 9.59762 22.9703 6.17083C22.9703 5.17395 22.7834 4.30167 22.4095 3.55401C23.4687 3.55401 24.5279 3.4917 25.5871 3.4917C26.9578 3.4917 28.827 3.24248 30.0731 3.99014C33.0014 5.797 32.3161 10.3453 32.3161 13.3359Z" fill="#696969"/>
            <path d="M15.9299 6.98071C16.4283 7.35455 17.1137 7.41685 17.4875 6.8561C17.799 6.41997 17.799 5.92152 17.4875 5.54769C17.4875 5.48539 17.4875 5.42308 17.4252 5.42308C17.6744 4.80003 17.8613 4.11467 18.1105 3.49162C18.2975 2.99318 17.4875 2.74395 17.3006 3.2424C17.1137 3.86545 16.8644 4.4885 16.6152 5.11156C15.6806 5.11156 14.933 6.23305 15.9299 6.98071ZM16.6775 5.92152C16.7398 5.92152 16.7398 5.98383 16.8021 5.98383C17.0514 6.17074 16.6152 6.66919 16.366 6.23305C16.2414 5.98383 16.4906 5.92152 16.6775 5.92152Z" fill="#696969"/>
            <path d="M29.45 17.5104C29.5123 16.1397 29.7615 14.2082 29.3254 12.8998C29.0138 11.9652 28.2662 11.4668 27.3939 11.3422C26.3347 11.2176 24.2163 11.0929 23.2194 11.5914C22.721 11.8406 22.3472 12.526 21.9733 12.8998C21.1011 13.7098 19.9796 14.2082 18.8581 14.5197C15.992 15.3297 13.2506 14.3328 11.1322 12.4637C9.88612 11.3422 6.39702 11.2799 4.964 12.0275C3.03253 12.9621 3.71789 15.6412 3.71789 17.3858C3.7802 20.6257 3.40637 24.1771 3.96711 27.3546C4.21633 28.85 4.90169 29.6599 6.39702 29.9092C8.26618 30.283 10.5092 30.2207 12.4406 30.283C14.4344 30.3453 16.4282 30.283 18.4219 30.1584C20.2911 30.0338 22.098 29.9715 23.9671 29.9092C25.4624 29.9092 26.8332 30.2207 28.1416 29.4107C30.2599 28.04 29.5123 25.2986 29.45 23.1179C29.3254 21.311 29.3877 19.4419 29.45 17.5104ZM28.4531 27.7285C27.8923 29.2238 26.5216 29.0992 25.0886 29.0992C21.5372 29.2238 17.9858 29.473 14.4344 29.473C12.6899 29.473 11.0076 29.2861 9.26306 29.1615C7.70543 29.0369 5.52475 29.5976 4.90169 27.7285C4.46556 26.3577 4.65247 24.5509 4.65247 23.1179C4.65247 21.4356 4.65247 19.8157 4.59017 18.1334C4.52786 16.5135 3.7802 13.5852 5.71166 12.7129C6.70855 12.2767 9.63689 12.3391 10.4469 13.0867C12.3783 14.769 14.8082 15.8905 17.4251 15.6412C18.6712 15.5166 19.9173 15.1428 20.9765 14.5197C22.098 13.8967 22.9079 12.5883 24.154 12.2144C24.6525 12.0275 25.774 12.0898 26.3347 12.1521C27.4562 11.9029 28.1416 12.4637 28.5154 13.8344C28.5154 14.2082 28.5777 14.6443 28.5777 15.0182C28.64 16.7627 28.4531 18.445 28.4531 20.1895C28.4531 21.8718 28.5777 23.4917 28.5777 25.1116C28.64 26.1085 28.7646 26.9185 28.4531 27.7285Z" fill="#696969"/>
            <path d="M13.0638 5.85928C13.1261 4.80009 13.6868 3.7409 14.6214 3.18015C15.1199 2.93093 14.6837 2.18326 14.1853 2.43248C13.0015 3.11784 12.3161 4.48856 12.1915 5.85928C12.1292 6.42002 13.0015 6.42002 13.0638 5.85928Z" fill="#696969"/>
            <path d="M12.5032 6.73157C11.9422 6.73157 11.9422 7.60429 12.5032 7.60429C13.0642 7.60429 13.0642 6.73157 12.5032 6.73157Z" fill="#696969"/>
            </svg>
        ''';
  }

  _getWeightColor(WeightEntity weight) {
    String color;
    if (weight == null) return 'none';
    var dateUtil = DateUtil();
    var birthDay = bloc?.child?.value?.birthDate ?? DateTime.now();
    int month = monthCounter(
        end: Formatter.parseFormattedStringDate(weight.date), start: birthDay);
    var current = month == 0
        ? birthDay
        : getNextMonthBirthday(birthDay,
            monthNumber: birthDay.month + month, year: birthDay.year);
    double drob = Formatter.parseFormattedStringDate(weight.date)
            .difference(current)
            .inDays /
        dateUtil.daysInMonth(
            Formatter.parseFormattedStringDate(weight.date).month,
            Formatter.parseFormattedStringDate(weight.date).year);
    switch (weightStatusByAprox(weight.weight, (month + drob),
        girl: bloc?.child?.value?.gender == 'female')) {
      case WeightStatus.NORMAL:
        color = '#B9E462';
        break;
      case WeightStatus.BAD:
        color = '#B9E462';
        //color = '#FFA73F';
        break;
      case WeightStatus.FATAL:
        color = '#FF6D3F';
        break;
      default:
        color = 'none';
        break;
    }
    return color;
  }

  _getHeightColor(HeightEntity height) {
    String color;
    if (height == null) return 'none';
    var dateUtil = DateUtil();
    var birthDay = bloc?.child?.value?.birthDate ?? DateTime.now();
    int month = monthCounter(
        end: Formatter.parseFormattedStringDate(height.date), start: birthDay);
    var current = month == 0
        ? birthDay
        : getNextMonthBirthday(birthDay,
            monthNumber: birthDay.month + month, year: birthDay.year);
    double drob = Formatter.parseFormattedStringDate(height.date)
            .difference(current)
            .inDays /
        dateUtil.daysInMonth(
            Formatter.parseFormattedStringDate(height.date).month,
            Formatter.parseFormattedStringDate(height.date).year);
    switch (heightStatusByAprox(height.height * 1.0, (month + drob),
        girl: bloc?.child?.value?.gender == 'female')) {
      case HeightStatus.NORMAL:
        color = '#B9E462';
        break;
      case HeightStatus.BAD:
        color = '#B9E462';
        //color = '#FFA73F';
        break;
      case HeightStatus.FATAL:
        color = '#FF6D3F';
        break;
      default:
        color = 'none';
        break;
    }
    return color;
  }

  showChangeNameDialog() {
    showDialog(context: context, builder: (_) => ChangeNameDialog());
  }

  void showFillInfoDialog() {
    widget.showSettingsSurface.add(false);
    showDialog(
      context: context,
      builder: (context) => CustomCupertinoAlertDialog(
        title: Text('Этот раздел недоступен'),
        content: Text(
            'Заполните профиль ребенка, чтобы пользоваться полным функционалом'),
        actions: <Widget>[
          CustomCupertinoDialogAction(
            child: Text('Позже'),
            isDefaultAction: true,
            onPressed: () => Navigator.of(context).pop(),
          ),
          CustomCupertinoDialogAction(
            child: Text('Ок'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.of(context).pop();
              showInfoDialog();
            },
          ),
        ],
      ),
    );
  }

  void showInfoDialog() {
    widget.showSettingsSurface.add(false);
    showDialog(
      context: context,
      builder: (_) => BabyInfoDialog(),
    );
  }
}
