import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/application.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/album/album.page.dart';
import 'package:happy_mama/page/album/load_image_dialog.dart';
import 'package:happy_mama/page/doctors/all_doctors/all_doctors.page.dart';
import 'package:happy_mama/page/jaw/jaw.dart';
import 'package:happy_mama/page/premium/widgets/buy_premium_dialog.dart';
import 'package:happy_mama/page/settings/select_child.dart';
import 'package:happy_mama/page/settings/settings_page.dart';
import 'package:happy_mama/page/splash/splash_page.dart';
import 'package:happy_mama/page/vaccines/all_vaccines/all_vaccines.page.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/service/firebase/photo/photo_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';
import 'package:happy_mama/util/month_counter.dart';
import 'package:happy_mama/widgets/baby_info_dialog.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/rxdart.dart';

import '../home.dart';
import 'widgets/button_with_icon.dart';
import 'widgets/child_ability_tile.dart';
import 'widgets/child_info_tile.dart';
import 'widgets/set_image_date_dialog.dart';
import 'widgets/surface_menu.dart';

class BabyPage extends StatefulWidget {
  final Stream<bool> isPageHidden;

  BabyPage({Key key, this.isPageHidden}) : super(key: key);

  @override
  _BabyPageState createState() => _BabyPageState();
}

class _BabyPageState extends State<BabyPage> {
  ProfileBloc bloc;
  bool disableButtons = true;
  BehaviorSubject<bool> showSettingsSurface = BehaviorSubject();
  bool surfaceOpened = false;
  PhotoService service = PhotoService();
  BabyInfoService babyInfoService = BabyInfoService();
  MemoryImage mainImage;
  bool disposed = false;
  List<Future<Uint8List>> profilePics;
  PhotoService photoService = PhotoService();

  @override
  void initState() {
    super.initState();
    initBloc();
    // subscribe to children updates for updating profile pics
    bloc.childrenUpdated.listen((_) {
      profilePics = List.filled(bloc.children.length, null);
      for (int i = 0; i < bloc.children.length; i++) {
        if (bloc.children[i].mainImageId != null) {
          profilePics[i] = photoService.getImageByMonth(
              bloc.children[i].id, bloc.children[i].mainImageId);
        }
      }
    });
    // subscribe to child
    bloc.child.listen((child) async {
      if (child == null) {
        return;
      }
      //print('MAIN IMAGE ID ${child.mainImageId}');
      await babyInfoService.postInfo(child.id, child.name, child.gender,
          Formatter.formatDate(child.birthDate),
          mainImageId: child.mainImageId);
      if (child.mainImageId != null) {
        if (disposed) {
          return;
        }
        var links = await service.getLinksFromDatabase(child.id);
        //print('LINKS: $links');
        var bytes = await service.getImage(
            child.id, child.mainImageId, links[child.mainImageId]);
        //print('GOT BYTES: $bytes');
        if (bytes != null) {
          mainImage = MemoryImage(bytes);
          setState(() {});
        } else {
          mainImage = null;
          setState(() {});
        }
      } else {
        if (disposed) {
          return;
        }
        mainImage = null;
        setState(() {});
      }
    });
    widget.isPageHidden?.listen((_) {
      surfaceOpened = false;
      showSettingsSurface.add(surfaceOpened);
    });
  }

  initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context);
  }

  @override
  void dispose() {
    disposed = true;
    showSettingsSurface.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        //print('go');
        surfaceOpened = false;
        showSettingsSurface.add(surfaceOpened);
      },
      child: SafeArea(
        child: Stack(
          children: <Widget>[
            Scaffold(
              backgroundColor: Colors.transparent,
              resizeToAvoidBottomInset: false,
              body: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    height: 16,
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        child: mainImage == null
                            ? SvgPicture.asset(
                                'assets/vectors/baby_sitting.svg',
                                fit: BoxFit.fitHeight)
                            : ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                child: Image(
                                  image: mainImage,
                                  fit: BoxFit.cover,
                                ),
                              ),
                      ),
                    ),
                  ),
                  ChildInfoTile(showSettingsSurface.sink),
                  ChildAbilityTile(showSettingsSurface.sink),
                  StreamBuilder<Child>(
                      stream: bloc.child,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          snapshot.data.birthDate == null
                              ? disableButtons = true
                              : disableButtons = false;
                        }
                        return GridView.count(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 15, bottom: 10),
                            mainAxisSpacing: 15,
                            crossAxisSpacing: 10,
                            crossAxisCount: 2,
                            childAspectRatio: 3.3,
                            children: [
                              ButtonWithIcon(
                                disableButtons: disableButtons,
                                onTapIfActive: goToDoctors,
                                onTapIfDisabled: showFillInfoDialog,
                                title: 'Врачи',
                                iconPath: 'assets/images/doctors.png',
                              ),
                              ButtonWithIcon(
                                disableButtons: disableButtons,
                                onTapIfActive: goToVaccinations,
                                onTapIfDisabled: showFillInfoDialog,
                                title: 'Прививки',
                                iconPath: 'assets/images/vaccine.png',
                              ),
                              ButtonWithIcon(
                                disableButtons: disableButtons,
                                onTapIfActive: goToTeeth,
                                onTapIfDisabled: showFillInfoDialog,
                                title: 'Прививки',
                                iconPath: 'assets/images/tooth.png',
                                label: StreamBuilder<int>(
                                    stream: bloc.teethAmount,
                                    builder: (context, snapshot) {
                                      String n = '';
                                      if (snapshot.hasData &&
                                          snapshot.data != 0) {
                                        n = snapshot.data.toString();
                                      }
                                      return Text(
                                        'Зубы $n',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600),
                                      );
                                    }),
                              ),
                              ButtonWithIcon(
                                disableButtons: disableButtons,
                                onTapIfActive: goToPhoto,
                                onTapIfDisabled: showFillInfoDialog,
                                title: 'Фото',
                                iconPath: 'assets/images/photo.png',
                              ),
                            ]);
                      }),
                ],
              ),
            ),
            Positioned(
                top: 36,
                right: 36,
                child: StreamBuilder<Child>(
                    stream: bloc.child,
                    builder: (context, snapshot) {
                      return SurfaceMenu(
                        showStream: showSettingsSurface,
                        initialData: surfaceOpened,
                        buyPremium: (premiumService.isPremium() ?? false)
                            ? null
                            : openBuyPremiumDialog,
                        chooseFotoTap: (snapshot?.data?.isEmpty() ?? true)
                            ? null
                            : openFotoDialog,
                        fillBabyInfoTap: snapshot?.data?.birthDate != null
                            ? null
                            : showInfoDialog,
                        changeProfile: (bloc.children?.length ?? 1) > 1
                            ? changeProfile
                            : null,
                        settingsTap: goToSettings,
                        exit: logout,
                      );
                    })),
            Positioned(
              top: 16,
              right: 16,
              child: SizedBox(
                height: 40,
                width: 40,
                child: FloatingActionButton(
                  elevation: 0.0,
                  child: StreamBuilder<bool>(
                      stream: showSettingsSurface,
                      initialData: surfaceOpened,
                      builder: (context, snapshot) {
                        return Icon(snapshot.data ? Icons.close : Icons.dehaze,
                            color: Colors.white);
                      }),
                  onPressed: tapToMenuButton,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future changeProfile() async {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    await showDialog(
      context: context,
      builder: (context) => SelectChild(
        images: profilePics,
        buttonText: 'Выбрать',
        onPressed: (child, index) async {
          await bloc.changeChild(child.id);
          setState(() {});
        },
      ),
    );
  }

  void goToVaccinations() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    Navigator.of(context).push(
      CupertinoPageRoute(
          builder: (_) => AllVaccinesPage(),
          settings: RouteSettings(name: 'Прививки')),
    );
  }

  void goToDoctors() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    Navigator.of(context).push(
      CupertinoPageRoute(
          builder: (_) => AllDoctorsPage(),
          settings: RouteSettings(name: 'Врачи')),
    );
  }

  void goToTeeth() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => Jaw(), settings: RouteSettings(name: 'Зубы')));
  }

  void goToPhoto() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    // for rate dialog testing
    /*showDialog(
      context: context,
      builder: (context) => RateDialog(),
    );*/
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => AlbumPage(MediaQuery.of(context).size.width),
        settings: RouteSettings(name: 'Фото')));
  }

  ///FOR SURFACE MENU
  ///
  void tapToMenuButton() {
    surfaceOpened = !surfaceOpened;
    showSettingsSurface.add(surfaceOpened);
  }

  void openFotoDialog() async {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    var imag;
    await showDialog(
      context: context,
      builder: (_) => LoadImageDialog(
        onPressed: (source) async {
          File image;
          try {
            image = await ImagePicker.pickImage(
                source: source,
                maxHeight: 1000,
                maxWidth: 1000,
                imageQuality: 80);
            if (image == null) {
              image = (await ImagePicker.retrieveLostData())?.file;
            }
            if (image == null) {
              return;
            }
          } on PlatformException catch (e) {
            if (e.code == 'camera_access_denied' ||
                e.code == 'photo_access_denied') {
              await showDialog(
                context: context,
                builder: (context) => NoPermissionDialog(
                  reason: e.code == 'camera_access_denied'
                      ? NoPermissionReason.CAMERA
                      : NoPermissionReason.PHOTOS,
                ),
              );
              return;
            } else {
              //print('PlatformException on getting photo: $e');
              return;
            }
          } catch (e) {
            //print('Error on getting photo: ${e.toString()}');
            return;
          }
          File croppedFile = await ImageCropper.cropImage(
              sourcePath: image.path,
              aspectRatioPresets: [
                CropAspectRatioPreset.square,
              ],
              aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
              androidUiSettings: AndroidUiSettings(
                toolbarTitle: 'Обработка фотографии',
                toolbarColor: ColorStyle.tint,
                toolbarWidgetColor: Colors.white,
                initAspectRatio: CropAspectRatioPreset.square,
                lockAspectRatio: true,
                hideBottomControls: true,
              ),
              iosUiSettings: IOSUiSettings(
                minimumAspectRatio: 1.0,
              ));
          //image.writeAsBytesSync(croppedFile.readAsBytesSync());
          imag = croppedFile;
          //showImageDateDialog(image, context);
          // service.savePhoto(bloc?.child?.value?.id ?? 'test_id', index, image);
        },
      ),
    );
    if (imag != null) {
      showImageDateDialog(imag);
    }
  }

  void goToSettings() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    Navigator.of(context, rootNavigator: true).push(CupertinoPageRoute(
        builder: (_) => SettingsPage(),
        settings: RouteSettings(name: 'Настройки')));
  }

  void logout() async {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    mainThemeIsPink.add(false);
    await bloc.exitFromProfile();
    Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
        CupertinoPageRoute(builder: (_) => SplashPage()), (_) => false);
  }

  void showFillInfoDialog() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    showDialog(
      context: context,
      builder: (context) => CustomCupertinoAlertDialog(
        title: Text('Этот раздел недоступен'),
        content: Text(
            'Заполните профиль ребенка, чтобы пользоваться полным функционалом'),
        actions: <Widget>[
          CustomCupertinoDialogAction(
            child: Text('Позже'),
            isDefaultAction: true,
            onPressed: () => Navigator.of(context).pop(),
          ),
          CustomCupertinoDialogAction(
            child: Text('Ок'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.of(context).pop();
              showInfoDialog();
            },
          ),
        ],
      ),
    );
  }

  void showInfoDialog() {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    showDialog(
      context: context,
      builder: (_) => BabyInfoDialog(),
    );
  }

  void showImageDateDialog(File image) {
    showDialog(
      context: context,
      builder: (_) => SetImageDateDialog(
        maxMonth: monthCounter(
            end: DateTime.now(), start: bloc.child.value.birthDate),
        image: image,
        onPressed: setMain,
      ),
    );
  }

  Future setMain(File image, int month) async {
    //print('month $month');
    await service.saveImage(
        bloc?.child?.value?.id ?? 'test_id', month, image.readAsBytesSync());
    Child child = bloc.child.value;
    child.mainImageId = month;
    /*monthCounter(start: child.birthDate, end: DateTime.now()) == month
            ? 0
            : month;*/
    //mainImage = MemoryImage(image.readAsBytesSync());
    bloc.child.add(child);
    //setState(() {});
  }

  openBuyPremiumDialog() async {
    surfaceOpened = false;
    showSettingsSurface.add(surfaceOpened);
    bool status = await showDialog(
      context: context,
      builder: (_) => BuyPremiumDialog(
        type: 0,
      ),
    );
    //print('status $status');
  }
}
