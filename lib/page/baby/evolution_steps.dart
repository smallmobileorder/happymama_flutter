import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/brand_button.dart';

import '../home.dart';
import 'util/step_qualifier.dart';

class EvolutionStepsPage extends StatefulWidget {
  const EvolutionStepsPage({
    Key key,
  }) : super(key: key);

  @override
  _EvolutionStepsPageState createState() => _EvolutionStepsPageState();
}

class _EvolutionStepsPageState extends State<EvolutionStepsPage> {
  ProfileBloc bloc;
  int weeks;

  @override
  void initState() {
    super.initState();
    initBloc();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //print(width);
    return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Скачки развития'),
          backgroundColor: Colors.white,
          border: Border(
              bottom: BorderSide(
                  color: ColorStyle.tint,
                  width: 0.0,
                  style: BorderStyle.solid)),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            stepsTable(),
            info(),
            BrandButton.unfilled(
                text: 'Подробнее про скачки развития',
                onPressed: goToKnowledgeBase),
          ]),
        ));
  }

  stepsTable() {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: width * 1.32,
      padding: EdgeInsets.all(13),
      child: Column(children: [
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 1, index + 1 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 10, index + 10 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 19, index + 19 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 28, index + 28 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 37, index + 37 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 46, index + 46 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 55, index + 55 == weeks))),
        ),
        Expanded(
          child: Row(
              children: List.generate(
                  9, (index) => stepTile(index + 64, index + 64 == weeks))),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: (width - 32) / 9 * 1.5),
            child: Row(
                children: List.generate(
                    6, (index) => stepTile(index + 73, index == 38))),
          ),
        ),
      ]),
    );
  }

  info() {
    return Container(
      padding: EdgeInsets.only(left: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            height: 8,
          ),
          infoLine(EvolutionStep.NICE, 'Прекрасное настроение'),
          Container(
            height: 8,
          ),
          infoLine(EvolutionStep.CALM, 'Спокойный период'),
          Container(
            height: 8,
          ),
          infoLine(EvolutionStep.SAD, 'Капризный период'),
          Container(
            height: 8,
          ),
          infoLine(EvolutionStep.CRANKY, 'Пик капризности'),
          Container(
            height: 8,
          ),
          infoLine(null, 'Текущая неделя'),
          Container(
            height: 8,
          ),
        ],
      ),
    );
  }

  infoLine(EvolutionStep step, String text) {
    //print(step.toString());
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 24,
          width: 24,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: step == null ? Border.all(color: ColorStyle.tint) : null,
              color: step == null ? null : getStepColor(step)),
        ),
        Container(
          width: 14,
        ),
        step != null
            ? SvgPicture.asset(
                'assets/vectors/steps/${step.toString().split('.').last.toLowerCase()}.svg')
            : Container(
                width: 24,
              ),
        Container(
          width: 14,
        ),
        Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 12,
              color: ColorStyle.codGray),
        )
      ],
    );
  }

  stepTile(int week, bool current) {
    int month;
    if (week == 9) month = 2;
    if (week == 18) month = 4;
    if (week == 25) month = 6;
    if (week == 35) month = 8;
    if (week == 44) month = 10;
    if (week == 53) month = 12;
    if (week == 62) month = 14;
    if (week == 73) month = 16;
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (month != null)
              Text(
                '${month == 12 ? '1 год' : '$month мес'}',
                style: TextStyle(fontSize: 8),
              ),
            if (month == null)
              Text(
                '',
                style: TextStyle(fontSize: 8),
              ),
            Container(
              height: 1,
            ),
            AspectRatio(
              aspectRatio: 1,
              child: Stack(children: [
                Container(
                  decoration: current
                      ? BoxDecoration(
                          color: Color(0xFFB9E462).withOpacity(0.3),
                          borderRadius: BorderRadius.circular(13),
                          border: Border.all(color: ColorStyle.tint, width: 2))
                      : null,
                ),
                Positioned(
                  top: 4.5,
                  bottom: 4.5,
                  left: 4.5,
                  right: 4.5,
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: getStepColor(getCurrentEvolutionStep(week)),
                          borderRadius: BorderRadius.circular(8)),
                      child: Text(
                        week.toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }

  getStepColor(EvolutionStep step) {
    switch (step) {
      case EvolutionStep.NICE:
        return ColorStyle.conifer;
        break;
      case EvolutionStep.CALM:
        return ColorStyle.cyan;
        break;
      case EvolutionStep.SAD:
        return ColorStyle.sunglow;
        break;
      case EvolutionStep.CRANKY:
        return ColorStyle.pink;
        break;
      default:
        return ColorStyle.cyan;
        break;
    }
  }

  goToKnowledgeBase() {
    deeplinkNavigator.parsePathAndNavigate('/knowledge/5/', prevTab: 0)();
  }

  void initBloc() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    weeks = (DateTime.now()
                .difference(bloc?.child?.value?.birthDate ?? DateTime.now())
                .inDays /
            7)
        .floor()+1;
  }
}
