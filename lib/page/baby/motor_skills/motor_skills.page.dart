import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/entity/child.dart';
import 'package:happy_mama/page/baby/motor_skills/skill.dart';
import 'package:happy_mama/page/baby/motor_skills/skills_list.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/month_counter.dart';
import 'package:happy_mama/widgets/confirm_dialog.dart';

import '../../home.dart';
import 'motor_skills_graph.dart';

class MotorSkillsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MotorSkillsPageState();
}

class _MotorSkillsPageState extends State<MotorSkillsPage> {
  // skill id : date
  Map<int, DateTime> skills;
  BabyInfoService service = BabyInfoService();
  ProfileBloc bloc;
  bool loadingSkills = true;
  String identifier;
  Child child;

  @override
  void initState() {
    bloc = BlocProvider.of<ProfileBloc>(context);
    child = bloc.child.value;
    identifier = child.id;
    //print('id = $identifier');
    skills = {};
    service.fetchSkills(identifier).then((res) => setState(() {
          skills = res ?? {};
          loadingSkills = false;
        }));
    super.initState();
  }

  @override
  void dispose() {
    service.postSkills(identifier, skills);
    String res = skills.entries
        .where((entry) => entry.value != null)
        .fold<List<MapEntry<int, DateTime>>>([], (list, skill) {
          list.add(skill);
          list.sort((a, b) {
            int compare = b.value.compareTo(a.value);
            if (compare == 0) {
              return b.key.compareTo(a.key);
            }
            return compare;
          });
          return list;
        })
        .map((entry) => baseSkills[entry.key])
        .map((skill) => skill.shortName)
        .take(6)
        .join(', ');
    //print('RES = $res');
    if (res.isNotEmpty) {
      res = 'Умеет $res';
    }
    bloc.skills.add(res);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Create skill list or circular indicator
    Widget skillsList;
    if (loadingSkills) {
      skillsList = Center(child: CircularProgressIndicator());
    } else {
      skillsList = SkillsList(
        name: child.name ?? 'Малыш',
        skills: skills.map((k, v) => MapEntry(k,
            v == null ? null : monthCounter(end: v, start: child.birthDate))),
        onDelete: (i) {
          showDialog<bool>(
            context: context,
            builder: (_) => ConfirmDialog(
              title: 'Вы действительно хотите удалить это значение?',
              destructive: true,
            ),
          ).then((delete) {
            if (delete != null && delete) {
              setState(() => skills[i] = null);
            }
          });
        },
        onEdit: (i) {
          showDatePicker(
            context: context,
            firstDate: child.birthDate,
            initialDate: skills[i] == null ? DateTime.now() : skills[i],
            lastDate: DateTime.now(),
          ).then((date) {
            if (date == null) {
              return;
            }
            setState(() => skills[i] = date);
          });
        },
      );
    }
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Двигательные навыки'),
        border: Border(
          bottom: BorderSide(color: Theme.of(context).primaryColor),
        ),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(left: 16, right: 16, top: 10),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Окна возможностей\n6 основных двигательных навыков',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: ColorStyle.codGray),
                    textAlign: TextAlign.center,
                  ),
                ),
                Image.asset(
                  'assets/images/voz.png',
                  height: 36,
                ),
              ],
            ),
          ),
          MotorSkillsGraph(
            skills: skills.map((k, v) => MapEntry(
                k,
                v == null
                    ? null
                    : monthCounter(end: v, start: child.birthDate))),
            birthDate: child.birthDate,
          ),
          Container(height: 16),
          _InfoButton(
            onPressed: () {
              deeplinkNavigator.parsePathAndNavigate('/knowledge/2/',
                  prevTab: 0)();
            },
          ),
          Container(height: 16),
          skillsList,
        ],
      ),
    );
  }
}

class _InfoButton extends StatelessWidget {
  final Function onPressed;

  const _InfoButton({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 48),
      height: 52,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          width: 2,
          color: Theme.of(context).primaryColor,
        ),
      ),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        padding: EdgeInsets.zero,
        child: Text(
          'Подробнее про\nдвигательное развитие',
          textAlign: TextAlign.center,
          //style: TextStyle(fontSize: 14),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
