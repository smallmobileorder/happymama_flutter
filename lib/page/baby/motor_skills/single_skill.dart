import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/util/formatter.dart';

import 'skill.dart';

class SingleSkill extends StatelessWidget {
  final Skill skill;
  final Function onEdit;
  final Function onDelete;

  const SingleSkill({
    @required this.skill,
    this.onEdit,
    this.onDelete,
  });

  final TextStyle style = const TextStyle(fontSize: 9, color: Colors.black);
  final TextStyle disabledStyle =
  const TextStyle(fontSize: 9, color: Color(0xFF696969));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 11),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            fit: FlexFit.tight,
            child: Text(skill.name,
                style: skill.achieved == null ? disabledStyle : style),
          ),
          Container(width: 16),
          Flexible(
            fit: FlexFit.tight,
            child: Text(getFrom(),
                style: skill.achieved == null ? disabledStyle : style),
          ),
          Container(width: 16),
          Flexible(
            fit: FlexFit.tight,
            child: skill.achieved == null
                ? Container()
                : Text(getAchieved(), style: style),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              CupertinoButton(
                minSize: 35,
                padding: EdgeInsets.zero,
                child: SvgPicture.asset('assets/vectors/pen.svg', width: 18),
                onPressed: onEdit,
              ),
              skill.achieved == null
                  ? Container(width: 35)
                  : CupertinoButton(
                minSize: 35,
                padding: EdgeInsets.zero,
                child: SvgPicture.asset('assets/vectors/bucket.svg',
                    width: 26),
                onPressed: onDelete,
              ),
            ],
          ),
        ],
      ),
    );
  }

  String getFrom() {
    return 'С '
        '${Formatter.doubleToString(skill.from)}-'
        '${Formatter.doubleToString(skill.to)} месяцев';
  }

  String getAchieved() {
    String postfix = 'месяцев';
    if (skill.achieved == 1) {
      postfix = 'месяца';
    }
    return 'С ${skill.achieved} $postfix';
  }
}