import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/page/baby/motor_skills/single_skill.dart';
import 'package:happy_mama/page/baby/motor_skills/skill.dart';

class SkillsList extends StatelessWidget {
  final String name;
  final Function(int) onEdit;
  final Function(int) onDelete;

  // which skill is enabled and from which month
  final Map<int, int> skills;

  const SkillsList(
      {@required this.name, @required this.skills, this.onEdit, this.onDelete});

  @override
  Widget build(BuildContext context) {
    List<Widget> skillsWidgets = [];
    for (int i = 0; i < baseSkills.length; i++) {
      Skill skill = baseSkills[i];
      if (skill == null) {
        continue;
      }
      if (skills.containsKey(i)) {
        skill = skill.copyWithAchieved(skills[i]);
      }
      skillsWidgets.add(Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: SingleSkill(
          skill: skill,
          onEdit: onEdit == null ? null : () => onEdit(i),
          onDelete: onDelete == null ? null : () => onDelete(i),
        ),
      ));
      skillsWidgets.add(Divider(
        indent: 16,
        endIndent: 16,
        thickness: 1.0,
        color: skill.achieved != null
            ? Theme.of(context).primaryColor
            : Theme.of(context).primaryColor.withOpacity(0.25),
      ));
    }
    return Column(
      children: [
        getTitle(),
        ...skillsWidgets,
      ],
    );
  }

  /// TITLE

  final TextStyle style = const TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.bold,
  );

  Widget getTitle() {
    return Padding(
      padding: EdgeInsets.only(left: 11),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            fit: FlexFit.tight,
            child: Container(
              child: Text('Навык', style: style),
            ),
          ),
          Container(width: 16),
          Flexible(
            fit: FlexFit.tight,
            child: Container(
              child: Text('В среднем дети могут', style: style),
            ),
          ),
          Container(width: 16),
          Flexible(
            fit: FlexFit.tight,
            child: Container(
              child: Text('$name может', style: style),
            ),
          ),
          Container(width: 70),
        ],
      ),
    );
  }
}
