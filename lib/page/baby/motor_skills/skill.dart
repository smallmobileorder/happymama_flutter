import 'package:flutter/foundation.dart';

class Skill {
  final String name;
  final String shortName;

  // From and to on list tile
  final double from;
  final double to;

  // From and to on graph (can be null if skill is not main)
  final double fromOnGraph;
  final double toOnGraph;

  // When skill is achieved (can be null)
  final int achieved;

  const Skill({
    @required this.name,
    @required this.shortName,
    @required this.from,
    @required this.to,
    this.fromOnGraph,
    this.toOnGraph,
    this.achieved,
  });

  Skill copyWithAchieved(int achieved) {
    return Skill(
      name: this.name,
      shortName: this.shortName,
      from: this.from,
      to: this.to,
      achieved: achieved,
    );
  }
}

/// shortName is name for main screen
/// it is added for the case when name on screen is in other wording
const List<Skill> baseSkills = [
  Skill(
    name: 'Удерживать голову от 10 секунд',
    shortName: 'удерживать голову',
    from: 1,
    to: 2,
  ),
  Skill(
    name: 'Переворачиваться со спины на живот',
    shortName: 'переворачиваться со спины на живот',
    from: 3,
    to: 3.5,
  ),
  Skill(
    name: 'Опираться на прямые руки, лежа',
    shortName: 'опираться на прямые руки',
    from: 4,
    to: 5.5,
  ),
  Skill(
    name: 'Ползать по-пластунски',
    shortName: 'ползать по-пластунски',
    from: 5,
    to: 7,
  ),
  Skill(
    name: 'Сидеть без поддержки',
    shortName: 'сидеть без поддержки',
    from: 6,
    to: 8,
    fromOnGraph: 4,
    toOnGraph: 9,
  ),
  Skill(
    name: 'Ползать на четвереньках',
    shortName: 'ползать на четвереньках',
    from: 6,
    to: 8,
    fromOnGraph: 5.5,
    toOnGraph: 13.5,
  ),
  Skill(
    name: 'Стоять с поддержкой',
    shortName: 'стоять с поддержкой',
    from: 8,
    to: 9,
    fromOnGraph: 5,
    toOnGraph: 11.5,
  ),
  Skill(
    name: 'Ходить с поддержкой',
    shortName: 'ходить с поддержкой',
    from: 9,
    to: 10,
    fromOnGraph: 5.5,
    toOnGraph: 13.5,
  ),
  Skill(
    name: 'Стоять без поддержки',
    shortName: 'стоять без поддержки',
    from: 11,
    to: 16,
    fromOnGraph: 7,
    toOnGraph: 16.5,
  ),
  Skill(
    name: 'Ходить без поддержки',
    shortName: 'ходить без поддержки',
    from: 12,
    to: 18,
    fromOnGraph: 8,
    toOnGraph: 17.5,
  ),
];

/// Skills on graph are not in the same order as in list
/// so we need this list
const List<int> mainBaseSkills = [4, 6, 5, 7, 8, 9];
