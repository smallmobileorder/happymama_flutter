enum EvolutionStep { NICE, CALM, SAD, CRANKY }

getCurrentEvolutionStep(int week) {
  if (week == null) week = 0;
  if (_niceList.contains(week)) {
    return EvolutionStep.NICE;
  } else if (_sadList.contains(week)) {
    return EvolutionStep.SAD;
  } else if (_crankyList.contains(week)) {
    return EvolutionStep.CRANKY;
  } else {
    return EvolutionStep.CALM;
  }
}

var _niceList = [
  1,
  2,
  3,
  4,
  7,
  11,
  14,
  20,
  22,
  27,
  28,
  32,
  33,
  38,
  40,
  41,
  47,
  48,
  50,
  56,
  57,
  59,
  65,
  67,
  68,
  69,
  70,
  71,
  77,
  78
];

var _calmList = [6, 10, 13, 21, 31, 39, 49, 58, 66, 76];

var _sadList = [
  9,
  15,
  16,
  18,
  19,
  23,
  24,
  25,
  29,
  30,
  37,
  42,
  43,
  45,
  46,
  51,
  52,
  53,
  55,
  60,
  61,
  63,
  64,
  72,
  74,
  75
];

var _crankyList = [5, 8, 12, 17, 26, 36, 44, 54, 62, 73];
