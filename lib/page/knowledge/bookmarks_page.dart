import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/empty_screen.dart';

import 'section/section_page.dart';
import 'widgets/main_section_tile.dart';

class BookmarksPage extends StatefulWidget {
  final List<KnowledgeSectionEntity> sections;

  const BookmarksPage({Key key, this.sections = const []}) : super(key: key);

  @override
  _BookmarksPageState createState() => _BookmarksPageState();
}

class _BookmarksPageState extends State<BookmarksPage> {
  Map<int, List<int>> bookmarks = {};

  @override
  void initState() {
    super.initState();
    getBookmarks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Закладки'),
          backgroundColor: Colors.white,
          border: Border(
              bottom: BorderSide(
                  color: ColorStyle.tint,
                  width: 0.0,
                  style: BorderStyle.solid)),
        ),
        body: Builder(builder: (context) {
          if (widget?.sections?.isEmpty ?? true)
            return EmptyScreen(
              textBeforePic: 'У вас пока нет закладок',
              textAfterPic: '',
            );
          ;
          var tiles = generateSectionTiles();
          if (tiles == null || tiles.isEmpty)
            return EmptyScreen(
              textBeforePic: 'У вас пока нет закладок',
              textAfterPic: '',
            );
          ;
          return ListView.separated(
              shrinkWrap: true,
              itemBuilder: (_, i) => tiles[i],
              separatorBuilder: (_, i) => Divider(
                    height: 0,
                    thickness: 0,
                    color: ColorStyle.tint,
                    indent: 16,
                    endIndent: 16,
                  ),
              itemCount: tiles.length);
        }));
  }

  List<MainSectionTile> generateSectionTiles() {
    List<MainSectionTile> tiles = [];
    if (bookmarks != null && bookmarks.isNotEmpty) {
      bookmarks.forEach((k, v) {
        for (int i in v) {
          ////print(i);
          tiles.add(MainSectionTile(
            onTap: openSectionPage,
            id: i,
            entity: widget.sections[k],
          ));
        }
      });
      //print(tiles.length);
    }

    return tiles;
  }

  openSectionPage(KnowledgeSectionEntity entity, int id) async {
    await Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => SectionPage(section: entity, id: id),
        settings: RouteSettings(name: 'База знаний. ${entity.name}')));
    getBookmarks();
  }

  void getBookmarks() async {
    bookmarks = await StorageService.readKnowledgeBookmarks();
    if (bookmarks != null && bookmarks.isNotEmpty) {
      setState(() {});
    }
  }
}
