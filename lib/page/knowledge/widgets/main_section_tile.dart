import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';
import 'package:happy_mama/page/premium/widgets/buy_premium_dialog.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/loading_image_widget.dart';

class MainSectionTile extends StatelessWidget {
  final KnowledgeSectionEntity entity;
  final int id;
  final Function onTap;
  final bool premiumStatus;

  const MainSectionTile(
      {Key key, this.entity, this.onTap, this.id, this.premiumStatus = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool withId = id != null && id != -1;
    bool premium = false;

    ///type
    ///3 - article blocked
    ///4 - section blocked
    int type;
    if (entity.premium && !premiumStatus) {
      premium = true;
      type = 3;
    }
    if (!premium &&
        !premiumStatus &&
        withId &&
        (entity?.articles
                ?.firstWhere((art) => (art?.id ?? -1) == id)
                ?.premium ??
            false)) {
      premium = true;
      type = 2;
    }
    return InkWell(
      onTap: () {
        if (premium) {
          openBuyPremiumDialog(context, type);
        } else {
          onTap(entity, id);
        }
      },
      child: Ink(
        height: 94,
        width: double.infinity,
        padding: EdgeInsets.only(left: 27, right: 16, bottom: 17, top: 17),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                width: 50,
                child: LoadingImageWidget(
                        key: Key(entity?.url ?? ''), url: entity?.url ?? '') ??
                    Container()),
            Container(
              width: 17,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                width: double.infinity,
                height: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                decoration: BoxDecoration(
                    color: premium
                        ? ColorStyle.inactive.withOpacity(0.35)
                        : (withId ? Colors.white : ColorStyle.tint),
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: ColorStyle.tint, width: 2)),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: withId
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: AutoSizeText(
                                    entity.name,
                                    style: TextStyle(
                                      color: ColorStyle.codGray,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: AutoSizeText(
                                    entity.articles
                                        .firstWhere((f) => f.id == id)
                                        .name,
                                    style: TextStyle(
                                      color: ColorStyle.codGray,
                                      //fontSize: 12,
                                    ),
                                    minFontSize: 10,
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            )
                          : AutoSizeText(
                              entity.name,
                              style: TextStyle(
                                color:
                                    premium ? ColorStyle.codGray : Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                              maxLines: 3,
                              textAlign: TextAlign.center,
                            ),
                    ),
                    if (premium)
                      Container(
                        width: 10,
                      ),
                    if (premium)
                      SvgPicture.asset('assets/vectors/knowledge/padlock.svg'),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  openBuyPremiumDialog(BuildContext context, int type) async {
    bool status = await showDialog(
      context: context,
      builder: (_) => BuyPremiumDialog(
        type: type,
      ),
    );
    //print('status $status');
  }
}
