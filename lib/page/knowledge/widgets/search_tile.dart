import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';

class SearchTile extends StatefulWidget {
  final Function searchFunction;
  final Stream<bool> clearSearchStream;

  const SearchTile({Key key, this.searchFunction, this.clearSearchStream})
      : super(key: key);

  @override
  createState() => _SearchTileState();
}

class _SearchTileState extends State<SearchTile> {
  FocusNode focusNode;
  bool searchIsActive;

  TextEditingController searchController;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    searchIsActive = false;
    focusNode = FocusNode();
    focusNode.addListener(focusChecker);
    searchController = TextEditingController();
    subscription = widget?.clearSearchStream?.listen((close) {
      //print('wtf bro $close');
      closeSearch();
    });
  }

  @override
  void dispose() {
    searchController?.dispose();
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      height: 36,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: 16,
          ),
          Expanded(
            child: SizedBox(
              height: 36,
              width: double.infinity,
              child: TextField(
                controller: searchController,
                focusNode: focusNode,
                onChanged: (value) {
                  widget.searchFunction(value);
                },
                decoration: InputDecoration(
                    // fillColor: ,
                    fillColor: Color.fromRGBO(118, 118, 128, 0.12),
                    filled: true,
                    border: UnderlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        width: 0,
                        style: BorderStyle.none,
                      ),
                    ),
                    hintText: "Поиск",
                    contentPadding:
                        EdgeInsets.only(left: 30, top: 0, bottom: 11)),
                style: TextStyle(
                  fontSize: 17,
                  color: ColorStyle.codGray,
                ),
              ),
            ),
          ),
          if (searchIsActive == false)
            Container(
              width: 16,
            ),
          if (searchIsActive || (searchController?.text?.isNotEmpty ?? false))
            CupertinoButton(
              padding: EdgeInsets.symmetric(horizontal: 7, vertical: 0),
              child: Text(
                'Закрыть',
                style: TextStyle(
                    color: ColorStyle.tint,
                    fontFamily: 'Montserrat',
                    fontSize: 17),
                textAlign: TextAlign.center,
              ),
              onPressed: closeSearch,
            )
        ],
      ),
    );
  }

  void focusChecker() {
    setState(() {
      searchIsActive = focusNode.hasFocus;
    });
  }

  void closeSearch() {
    //print('close search');
    setState(() {
      focusNode?.unfocus();
      searchIsActive = false;
      searchController?.clear();
    });
    widget?.searchFunction('');
  }
}
