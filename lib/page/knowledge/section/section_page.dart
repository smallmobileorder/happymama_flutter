import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';
import 'package:happy_mama/page/home.dart';
import 'package:happy_mama/page/knowledge/util/parse_body_text.dart';
import 'package:happy_mama/service/firebase/analytics/events.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/deeplink_navigator.dart';
import 'package:happy_mama/widgets/custom_scrollbar.dart';
import 'package:happy_mama/widgets/empty_screen.dart';

import '../../../application.dart';
import 'widgets/article_tile.dart';

class SectionPage extends StatefulWidget {
  final KnowledgeSectionEntity section;

  // final Function addToBookmark;
  final int id;

  final bool overrideBackButton;

  const SectionPage({
    Key key,
    this.section,
    this.id,
    this.overrideBackButton = false,
  }) : super(key: key);

  @override
  createState() => _SectionPageState();
}

class _SectionPageState extends State<SectionPage> {
  bool sectionIsEmpty;
  var controller = ItemScrollController();
  Map<int, List<int>> knowledgeBookmarks = {};
  bool jumpWasMade;

  @override
  void initState() {
    super.initState();
    jumpWasMade = false;
    getBookmarks();
    sectionIsEmpty =
        widget.section.articles == null || widget.section.articles.isEmpty;
    ////print('images ${widget.section.buttons}');
    if (widget.id != null && widget.id != -1) {
      initJump();
    }
  }

  initJump() async {
    await Future.delayed(Duration(milliseconds: 300));
    try {
      controller.jumpTo(index: widget.id + 1);
      jumpWasMade = true;
    } catch (e) {
      //print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        //print('section will pop');
        if (jumpWasMade == true) {
          //print('jump');
          controller.jumpTo(index: 0);
          jumpWasMade = false;
          return Future.value(false);
        }
        deeplinkNavigator.path;
        if (history.isNotEmpty) {
          deeplinkNavigator.path;
          //print(history);
          int tab = history.removeLast();
          deeplinkNavigator.setKnowledgePathIfNecessary(tab);
          tabBarController.index = tab;
          if (knowledgeTab?.currentState?.canPop() ?? false) {
            knowledgeTab.currentState.popUntil((route) => route.isFirst);
          }
          return Future.value(false);
        }

//        if (knowledgeTab?.currentState?.canPop() ?? false) {
//          knowledgeTab.currentState.popUntil((route) => route.isFirst);
//        }
        //deeplinkNavigator.path;
        //if (history.isNotEmpty) {
        //if(knowledgeTab.currentState.canPop()) knowledgeTab.currentState.pop();
        //}
        deeplinkNavigator.clearKnowledgePaths();
        return Future.value(true);
      },
      child: Scaffold(
          appBar: CupertinoNavigationBar(
            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
//            leading: widget.overrideBackButton
//                ? CupertinoNavigationBarBackButton(
//                    onPressed: () {
//                      if (jumpWasMade) {
//                        controller.jumpTo(index: 0);
//                        jumpWasMade = false;
//                        return;
//                      }
//                      if (history.isNotEmpty) {
//                        deeplinkNavigator.path;
//                        //print(history);
//                        int tab = history.removeLast();
//                        tabBarController.index = tab;
//                      }
//                    },
//                  )
//                : null,
            middle: Text(
              widget.section.name,
              textAlign: TextAlign.center,
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ///Share button
//                if (sectionIsEmpty == false)
//                  CupertinoButton(
//                    child: SvgPicture.asset(
//                      'assets/vectors/share.svg',
//                      color: ColorStyle.tint,
//                    ),
//                    padding: EdgeInsets.zero,
//                    onPressed: shareSection,
//                    //color: color5,
//                  ),
                if (sectionIsEmpty == false)
                  CupertinoButton(
                    child: SvgPicture.asset(
                      'assets/vectors/star_${widget.section.bookmark ? 'shadow' : 'empty'}.svg',
                      color: ColorStyle.tint,
                    ),
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      setState(() {
                        widget.section.bookmark = !widget.section.bookmark;
                      });
                      addSectionToBookmark();
                    },
                    //color: color5,
                  )
              ],
            ),
            backgroundColor: Colors.white,
            border: Border(
                bottom: BorderSide(
                    color: ColorStyle.tint,
                    width: 0.0,
                    style: BorderStyle.solid)),
          ),
          body: sectionIsEmpty
              ? EmptyScreen(
                  textBeforePic: 'У вас пока нет\nстатей',
                )
              : articlesList()),
    );
  }

  articlesList() {
    return CustomScrollbar(
      child: ScrollablePositionedList.builder(
        itemScrollController: controller,
        itemCount: widget.section.articles.length + 1,
        itemBuilder: (_, i) => i == 0
            ? firstTile()
            : ArticleTile(
                addToBookmark: () =>
                    addArticleToBookmark(widget.section.articles[i - 1].id),
                article: widget.section.articles[i - 1],
                last: i == widget.section.articles.length,
                addBanner:
                    !premiumService.isPremium() && (i > 0 && (i - 1) % 3 == 0),
              ),
      ),
    );
//    return IndexedListView.separated(
//      controller: controller,
//      itemBuilder: (_, i) => Padding(
//          padding: EdgeInsets.only(left: 16, right: 16),
//          child: i == 0
//              ? firstTile()
//              : ArticleTile(
//                  article: widget.section.articles[i - 1],
//                )),
//      separatorBuilder: (_, i) => Divider(
//        height: 0,
//        thickness: 0,
//        color: ColorStyle.tint,
//        indent: 11,
//        endIndent: 11,
//      ),
//      minItemCount: 0,
//      maxItemCount: widget.section.articles.length,
//      emptyItemBuilder: (_, int) => null,
//    );
  }

  firstTile() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: titlesBuilder()),
        Column(
          children: parseBodyText(
            context,
            text: widget?.section?.text ?? '',
            images: widget?.section?.images ?? [],
          ),
        ),
        Container(
          height: 8,
        ),
        Divider(
          height: 0,
          thickness: 0,
          color: ColorStyle.tint.withOpacity(0.25),
          indent: 11,
          endIndent: 11,
        ),
      ],
    );
  }

  Widget titlesBuilder() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: List.generate(
          widget.section.articles.length,
          (index) => Ink(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: InkWell(
                  child: Text(
                    '${widget.section.articles[index].name}',
                    style: TextStyle(
                        color: widget.section.articles[index].premium &&
                                !premiumService.isPremium()
                            ? ColorStyle.inactive
                            : ColorStyle.tint,
                        fontWeight: FontWeight.w600,
                        fontSize: 15),
                  ),
                  onTap: () {
                    controller.jumpTo(index: index + 1);
                    jumpWasMade = true;
                  },
                ),
              )),
    );
  }

  shareSection() async {
    try {
      String text = 'Из приложения HappyMama: ' +
          (widget.section?.name ?? '') +
          '\n\n' +
          (widget.section?.text ?? '') +
          '\n\n\n' +
          formatArticleTextForShare() +
          '\n\n' +
          '\n\nСкачать приложение можно по ссылке:' +
          '\nAndroid: $PLAY_MARKET' +
          '\niOS: $APP_STORE';
      Clipboard.setData(ClipboardData(text: text));
      Share.text('Поделиться', text, 'text/plain');
    } catch (e) {
      //print('error: $e');
    }
  }

  String formatArticleTextForShare() {
    return widget.section.articles
        .map((article) => article.format())
        .join('\n\n\n');
  }

  void getBookmarks() async {
    knowledgeBookmarks = await StorageService.readKnowledgeBookmarks() ?? {};
  }

  void addSectionToBookmark() async {
    if (widget.section.bookmark) {
      if (knowledgeBookmarks.keys?.contains(widget.section.id) ?? false) {
        if (!(knowledgeBookmarks[widget.section.id]?.contains(-1) ?? false)) {
          knowledgeBookmarks[widget.section.id].addAll([-1]);
        }
      } else {
        knowledgeBookmarks[widget.section.id] = [-1];
      }
      var event = AnalyticEvent.createBookmarkEvent(
          section: 'База знаний',
          category: widget.section.name,
          objectTitle: widget.section.name);
      analytics.logEvent(name: event.name, parameters: event.parameters);
    } else {
      if (knowledgeBookmarks.keys?.contains(widget.section.id) ?? false) {
        if (knowledgeBookmarks[widget.section.id]?.contains(-1) ?? false) {
          knowledgeBookmarks[widget.section.id].remove(-1);
        }
      }
    }
    ////print(knowledgeBookmarks.keys);
    ////print(knowledgeBookmarks);
    await StorageService.writeKnowledgeBookmarks(knowledgeBookmarks);
  }

  addArticleToBookmark(int id) async {
    if (widget.section.articles.firstWhere((f) => f.id == id).bookmark) {
      ////print('step 1');
      if (knowledgeBookmarks.keys?.contains(widget.section.id) ?? false) {
        ////print('step 2');
        if (!(knowledgeBookmarks[widget.section.id]?.contains(id) ?? false)) {
          ////print('step 3');
          knowledgeBookmarks[widget.section.id].add(id);
        }
      } else {
        ////print('step 4');
        knowledgeBookmarks[widget.section.id] = [id];
      }
      var event = AnalyticEvent.createBookmarkEvent(
          section: 'База знаний',
          category: widget.section.name,
          objectTitle:
              widget.section.articles.firstWhere((f) => f.id == id).name);
      analytics.logEvent(name: event.name, parameters: event.parameters);
    } else {
      ////print('step 5');
      if (knowledgeBookmarks.keys?.contains(widget.section.id) ?? false) {
        ////print('step 6');
        if (knowledgeBookmarks[widget.section.id]?.contains(id) ?? false) {
          ////print('step 7');
          knowledgeBookmarks[widget.section.id].remove(id);
        }
      }
    }
    await StorageService.writeKnowledgeBookmarks(knowledgeBookmarks);
  }
}
