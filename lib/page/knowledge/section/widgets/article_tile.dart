import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';
import 'package:happy_mama/page/knowledge/util/parse_body_text.dart';
import 'package:happy_mama/page/premium/widgets/buy_premium_dialog.dart';
import 'package:happy_mama/style/color_style.dart';

import '../../../home.dart';

class ArticleTile extends StatefulWidget {
  final ArticleEntity article;
  final Function addToBookmark;
  final bool last;
  final EdgeInsets padding;
  final bool addBanner;

  const ArticleTile(
      {Key key,
      this.article,
      this.addToBookmark,
      this.last = false,
      this.padding = const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      this.addBanner = false})
      : super(key: key);

  createState() => _ArticleTileState();
}

class _ArticleTileState extends State<ArticleTile> {
  @override
  Widget build(BuildContext context) {
    return (widget?.article?.premium ?? false) && !premiumService.isPremium()
        ? blockedArticle()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 8,
              ),
              Container(
                padding: widget.padding,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        '${widget.article.name}',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: ColorStyle.dirtyBlack),
                      ),
                    ),
                    CupertinoButton(
                      child: SvgPicture.asset(
                        'assets/vectors/star_${widget.article.bookmark ? 'shadow' : 'empty'}.svg',
                        color: ColorStyle.tint,
                      ),
                      padding: EdgeInsets.zero,
                      onPressed: () {
                        setState(() {
                          widget.article.bookmark = !widget.article.bookmark;
                        });
                        widget.addToBookmark();
                      },
                      //color: color5,
                    ),
///Share button
//                    CupertinoButton(
//                      child: SvgPicture.asset(
//                        'assets/vectors/share.svg',
//                        color: ColorStyle.tint,
//                      ),
//                      padding: EdgeInsets.zero,
//                      onPressed: shareArticle,
//                      //color: color5,
//                    ),
                  ],
                ),
              ),
              Column(
                  children: parseBodyText(context,
                      images: widget?.article?.images ?? [],
                      text: widget?.article?.text ?? '',
                      widgetPadding: widget.padding,
                      articleId: widget?.article?.id ?? -1)),
              Container(
                height: 16,
              ),
              if (widget.last == false)
                Divider(
                  height: 0,
                  thickness: 0,
                  color: ColorStyle.tint.withOpacity(0.25),
                  indent: 11,
                  endIndent: 11,
                ),
              if (widget.addBanner) banner.getLargeWidget(),
            ],
          );
  }

  void shareArticle() {
    try {
      String text = 'Из приложения HappyMama: ' +
          widget.article.name +
          '\n\n' +
          widget.article.format() +
          '\n\n' +
          '\n\nСкачать приложение можно по ссылке:' +
          '\nAndroid: $PLAY_MARKET' +
          '\niOS: $APP_STORE';
      Clipboard.setData(ClipboardData(text: text));
      Share.text('Поделиться', text, 'text/plain');
    } catch (e) {
      //print('error: $e');
    }
  }

  blockedArticle() {
    return Column(
      children: <Widget>[
        Ink(
          color: ColorStyle.inactive,
          child: InkWell(
            onTap: openBuyPremiumDialog,
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 8),
              padding: widget.padding,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      '${widget.article.name}',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: ColorStyle.dirtyBlack),
                    ),
                  ),
                  SvgPicture.asset(
                    'assets/vectors/knowledge/padlock.svg',
                  ),
                ],
              ),
            ),
          ),
        ),
        if (widget.addBanner) banner.getLargeWidget(),
      ],
    );
  }

  openBuyPremiumDialog() async {
    bool status = await showDialog(
      context: context,
      builder: (_) => BuyPremiumDialog(
        type: 2,
      ),
    );
    //print('status $status');
  }
}
