import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/loading_image_widget.dart';
import 'package:happy_mama/widgets/brand_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'dart:math' as math;

import '../../home.dart';

parseBodyText(BuildContext context,
    {String text,
    List<String> images,
    EdgeInsets widgetPadding = const EdgeInsets.symmetric(
      horizontal: 16,
    ),
    int articleId = -1}) {
  List<Widget> widgets = [];
  /*if (images?.isNotEmpty ?? false) {
    List<String> texts = text.split(RegExp(r'<image:.?image_\d\d?>'));
    for (var i = 0; i < texts.length; i++) {
      widgets.add(Padding(
        padding: widgetPadding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
              parseButtons(context, texts[i], articleId, images, widgetPadding),
        ),
      ));
      try {
        widgets.add(Container(
            padding: widgetPadding,
            width: MediaQuery.of(context).size.width,
            child: LoadingImageWidget(
              enablePhotoView: true,
              url: images[i],
            )));
      } catch (e) {}
    }
    return widgets;
  } else {
    widgets.add(Padding(
      padding: widgetPadding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: parseButtons(context, text, articleId, images, widgetPadding),
      ),
    ));
  }*/
  widgets.add(Padding(
    padding: widgetPadding,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: parseButtons(context, text, articleId, images),
    ),
  ));
  return widgets;
}

List<Widget> parseButtons(
    BuildContext context, String text, int articleId, List<String> images) {
  List<Widget> widgets = [];
  text.splitMapJoin(
    RegExp(r"<button='.*?'>.*?<\/button>"),
    onMatch: (match) {
      String temp = match.group(0);
      temp = temp.replaceAll('<button=\'', '');
      temp = temp.replaceAll(r'</button>', '');
      var tokens = temp.split('\'>');
      if (tokens.length != 2) {
        return '';
      }
      widgets.add(BrandButton.unfilled(
          text: tokens[1],
          margin: EdgeInsets.only(left: 48, right: 48, bottom: 16, top: 16),
          onPressed: () {
            deeplinkNavigator.addArticleIdToKnowledgePath(articleId);
            deeplinkNavigator.parsePathAndNavigate(
              tokens[0],
              prevTab: 2,
            )();
          }));
      return '';
    },
    onNonMatch: (match) {
      widgets.addAll(parseVideos(context, match, articleId, images));
      //widgets.add(parseLinksInText(match, articleId));
      return '';
    },
  );
  return widgets;
}

List<Widget> parseVideos(
    BuildContext context, String text, int articleId, List<String> images) {
  List<Widget> widgets = [];
  text.splitMapJoin(
    RegExp(r"<video='.*?'\/>"),
    onMatch: (match) {
      String temp = match.group(0);
      temp = temp.replaceAll('<video=\'', '');
      temp = temp.replaceAll(r'/>', '');
      YoutubePlayerController controller = YoutubePlayerController(
          initialVideoId: YoutubePlayer.convertUrlToId(temp ?? ''),
          flags: YoutubePlayerFlags(
            autoPlay: false,
            mute: false,
          ));
      tabBarController?.addListener(() {
        if (tabBarController?.index != 2) {
          try {
            controller?.pause();
          } catch (e) {
            //print('PARSE_BODY: EXCEPTION $e');
          }
        }
      });
      widgets.add(
        YoutubePlayer(
          controller: controller,
          showVideoProgressIndicator: true,
          bottomActions: <Widget>[
            CurrentPosition(),
            SizedBox(width: 10.0),
            ProgressBar(isExpanded: true),
            SizedBox(width: 10.0),
            RemainingDuration(),
          ],
        ),
      );
      return '';
    },
    onNonMatch: (match) {
      widgets.addAll(parseLinkedImages(context, match, articleId, images));
      return '';
    },
  );
  return widgets;
}

List<Widget> parseLinkedImages(
    BuildContext context, String text, int articleId, List<String> images) {
  List<Widget> res = [];
  text.splitMapJoin(
    RegExp(r"<a href='.*?'><image:.?image_\d\d?><\/a>"),
    onMatch: (match) {
      String temp = match.group(0);
      temp = temp.replaceAll(RegExp(r"<a href='"), '');
      temp = temp.replaceAll(RegExp(r"><\/a>"), '');
      List<String> tokens = temp.split(RegExp("'><image:.?image_"));
      if (tokens.length != 2) {
        return '';
      }
      int index = int.tryParse(tokens[1]);
      if (index == null) {
        return '';
      }
      Function goToLink = () async {
        var link = tokens[0];
        bool can = await canLaunch(link);
        if (can) {
          await launch(link);
        } else {
          deeplinkNavigator.addArticleIdToKnowledgePath(articleId);
          deeplinkNavigator.parsePathAndNavigate(link, prevTab: 2)();
        }
      };
      res.add(
        GestureDetector(
          onTap: goToLink,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: ColorStyle.tint, width: 2),
            ),
            width: MediaQuery.of(context).size.width,
            child: ClipPath(
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: Stack(
                children: <Widget>[
                  LoadingImageWidget(
                    enablePhotoView: false,
                    url: images[index],
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: Container(
                      width: 36,
                      height: 36,
                      decoration: BoxDecoration(
                        color: ColorStyle.tint,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                        ),
                      ),
                      child: Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.rotationY(math.pi),
                        child: Icon(Icons.reply, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      return '';
    },
    onNonMatch: (nonMatch) {
      res.addAll(parseImages(context, nonMatch, articleId, images));
      return '';
    },
  );
  return res;
}

List<Widget> parseImages(
    BuildContext context, String text, int articleId, List<String> images) {
  List<Widget> res = [];
  text.splitMapJoin(
    RegExp(r"<image:.?image_\d\d?>"),
    onMatch: (match) {
      String temp = match.group(0);
      temp = temp.replaceAll(RegExp(r'<image:.?image_'), '');
      temp = temp.replaceAll(r'>', '');
      int index = int.tryParse(temp);
      if (index == null) {
        return '';
      }
      res.add(
        Container(
          width: MediaQuery.of(context).size.width,
          child: LoadingImageWidget(enablePhotoView: true, url: images[index]),
        ),
      );
      return '';
    },
    onNonMatch: (nonMatch) {
      res.addAll(parseLinksInText(context, nonMatch, articleId));
      return '';
    },
  );
  return res;
}

List<Widget> parseLinksInText(
    BuildContext context, String text, int articleId) {
  List<TextSpan> spans = [];
  text.splitMapJoin(
    RegExp(r"<a href='.*?'>.*?<\/a>"),
    onMatch: (match) {
      String temp = match.group(0);
      temp = temp.replaceAll('<a href=\'', '');
      temp = temp.replaceAll(r'</a>', '');
      var tokens = temp.split('\'>');
      //print('tokens $tokens');
      if (tokens.length != 2) {
        return '';
      }
      Function goToLink = () async {
        var link = tokens[0];

        bool can = await canLaunch(link);
        if (can) {
          await launch(link);
        } else {
          deeplinkNavigator.addArticleIdToKnowledgePath(articleId);
          deeplinkNavigator.parsePathAndNavigate(link, prevTab: 2)();
        }
      };
      spans.add(TextSpan(
        text: tokens[1],
        style: TextStyle(
          fontSize: 15,
          color: ColorStyle.tint,
          fontFamily: 'Montserrat',
        ),
        recognizer: TapGestureRecognizer()..onTap = goToLink,
      ));
      return '';
    },
    onNonMatch: (match) {
      spans.addAll(parseBoldText(match));
      return '';
    },
  );
  return [
    RichText(
      text: TextSpan(children: spans),
    )
  ];
}

List<TextSpan> parseBoldText(String text) {
  List<TextSpan> spans = [];
  text.splitMapJoin(
    RegExp(r'<b>(.|\n)*?</b>'),
    onMatch: (match) {
      String temp = match.group(0);
      temp = temp.replaceAll('<b>', '');
      temp = temp.replaceAll(r'</b>', '');
      spans.add(TextSpan(
        text: temp,
        style: TextStyle(
          fontSize: 15,
          color: Color(0xFF3F3F3F),
          fontWeight: FontWeight.bold,
          fontFamily: 'Montserrat',
        ),
      ));
      return '';
    },
    onNonMatch: (match) {
      spans.add(TextSpan(
        text: match,
        style: TextStyle(
          fontSize: 15,
          color: Color(0xFF3F3F3F),
          fontFamily: 'Montserrat',
        ),
      ));
      return '';
    },
  );
  return spans;
}
