import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';
import 'package:happy_mama/page/premium/widgets/buy_premium_dialog.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:rxdart/rxdart.dart';

import '../home.dart';
import 'bookmarks_page.dart';
import 'section/section_page.dart';
import 'widgets/main_section_tile.dart';
import 'widgets/search_tile.dart';

class SectionsListView extends StatefulWidget {
  final List<KnowledgeSectionEntity> sections;
  final Stream<bool> tabChangeStream;

  const SectionsListView({
    Key key,
    @required this.sections,
    this.tabChangeStream,
  }) : super(key: key);

  createState() => _SectionsListViewState();
}

class _SectionsListViewState extends State<SectionsListView> {
  Map<KnowledgeSectionEntity, List<int>> _sections;

  BehaviorSubject<Map<KnowledgeSectionEntity, List<int>>> sections =
      BehaviorSubject();

  StreamSubscription subscription;

  @override
  void initState() {
    _sections = {};
    widget.sections.forEach((f) => _sections.putIfAbsent(f, () => <int>[-1]));
    sections.add(_sections);
    subscription = widget.tabChangeStream.listen((_) {
      setState(() {});
    });
    // WidgetsBinding.instance.addPostFrameCallback(checkDeeplink());
    super.initState();
  }

  @override
  void dispose() {
    subscription?.cancel();
    sections.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //print('BUILD: deeplink = ${deeplinkNavigator.pathNoCleaning}');
    checkDeeplink();
//    var test = checkDeeplink();
//    if (test != null) {
//      return test;
//    }
    return sectionsList();
  }

  sectionsList() {
    return Scaffold(
        //resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          actions: <Widget>[
            CupertinoButton(
              padding: EdgeInsets.only(right: 5),
              child: Text(
                'Закладки',
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: ColorStyle.codGray,
                    fontFamily: 'Montserrat'),
              ),
              onPressed: goToBookmarkPage,
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            SearchTile(
              searchFunction: searchFunction,
              clearSearchStream: widget?.tabChangeStream,
            ),
            Container(
              height: 10,
            ),
            Divider(
              height: 0,
              color: ColorStyle.tint,
            ),
            Expanded(
              child: StreamBuilder<Map<KnowledgeSectionEntity, List<int>>>(
                  stream: sections,
                  builder: (context, snapshot) {
                    if (snapshot.hasData == false || snapshot.data.isEmpty)
                      return Container();
                    var tiles = generateSectionTiles(snapshot.data);
                    return ListView.separated(
                        shrinkWrap: true,
                        itemBuilder: (_, i) => tiles[i],
                        separatorBuilder: (_, i) => Divider(
                              height: 0,
                              thickness: 0,
                              color: ColorStyle.tint,
                              indent: 16,
                              endIndent: 16,
                            ),
                        itemCount: tiles.length);
                  }),
            ),
          ],
        ));
  }

  generateSectionTiles(Map<KnowledgeSectionEntity, List<int>> map) {
    List<MainSectionTile> tiles = [];
    ////print(map.length);
    map.forEach((k, v) {
      ////print(k.name);
      for (int i in v) {
        //print(i);
        tiles.add(MainSectionTile(
          onTap: openSectionPage,
          id: i,
          entity: k,
          premiumStatus: premiumService.isPremium(),
        ));
      }
    });
    ////print(tiles.length);
    return tiles;
  }

  searchFunction(String value) {
    ////print(value);
    if (value.isEmpty) {
      sections.add(_sections);
      return;
    }
    Map<KnowledgeSectionEntity, List<int>> founded = {};
    ////print('dsdq');
    try {
      widget.sections.forEach((sec) {
        List<int> temp = [];
        if (sec.name.toLowerCase().contains(value.toLowerCase().trim())) {
          temp.add(-1);
        } else if (sec?.text
                ?.toLowerCase()
                ?.contains(value.toLowerCase().trim()) ??
            false) {
          temp.add(-1);
        }
        if (sec?.articles?.isNotEmpty ?? false) {
          sec.articles.forEach((art) {
            if (art.name.toLowerCase().contains(value.toLowerCase().trim())) {
              temp.add(art.id);
            } else if (art?.text
                    .toLowerCase()
                    ?.contains(value.toLowerCase().trim()) ??
                false) {
              temp.add(art.id);
            }
          });
        }
        if (temp.isNotEmpty) {
          founded.addAll({sec: temp});
        }
      });
    } catch (e) {
      //print('error at search $e');
    }
    ////print('dsde');
    ////print('dsd');
    //founded.forEach((k, v) => //print(k.name + ' ' + v.toString()));
    sections.add(founded);
  }

  goToBookmarkPage() {
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => BookmarksPage(sections: widget?.sections ?? []),
        settings: RouteSettings(name: 'База знаний. Закладки')));
  }

  openSectionPage(KnowledgeSectionEntity entity, int id) {
//    //print(id);
//    if (entity.id == 0) {
//      //print('dsadasdasd');
//      deeplinkNavigator.parsePathAndNavigate('/main/height')(context);
//    }
//
//    if (entity.id == 1)
//      deeplinkNavigator.parsePathAndNavigate('/main/weight/')(context);
//    if (entity.id == 2)
//      deeplinkNavigator.parsePathAndNavigate('notifications')(context);
    if (entity.premium && !premiumService.isPremium()) {
      openBuyPremiumDialog();
    } else {
      deeplinkNavigator
          .addKnowledgePath('knowledge/${entity.id}/${id == -1 ? '' : '$id'}');
      Navigator.of(context).push(CupertinoPageRoute(
          builder: (_) => SectionPage(section: entity, id: id),
          settings: RouteSettings(name: 'База знаний. ${entity.name}')));
    }
  }

  Future checkDeeplink() async {
    await Future.delayed(Duration.zero);
    if (ModalRoute.of(context).isCurrent && tabBarController.index >= 2) {
      String temp = deeplinkNavigator.pathNoCleaning;
      //print('DIPLINK = $temp');
      ////print('temp $temp');
      if (temp.contains('knowledge')) {
        // parse deeplink and remove 'knowledge' from it
        List<int> parsed = temp
            .trim()
            .split('/')
            .where((f) => f != null && f.isNotEmpty)
            .map((token) => int.tryParse(token))
            .skip(1)
            .toList();
        // get id's of sections
        int sectionId = parsed.isEmpty ? null : parsed.removeAt(0);
        int partId = parsed.isEmpty ? null : parsed.removeAt(0);
        ////print('SECTION ID = $sectionId');
        ////print('PART ID = $partId');
        if (sectionId == null) {
          return;
        }
        // get section by id
        var sec = _sections.keys
            .firstWhere((f) => f.id == sectionId, orElse: () => null);
        if (sec != null) {
          if (sec.premium && !premiumService.isPremium()) {
            openBuyPremiumDialog();
          } else {
            deeplinkNavigator.addKnowledgePath(temp);
            Navigator.push(
                context,
                CupertinoPageRoute(
                    builder: (_) => SectionPage(
                          section: sec,
                          id: partId,
                          overrideBackButton: true,
                        ),
                    settings: RouteSettings(name: 'База знаний. ${sec.name}')));
          }
        }
      }
    }
  }

  openBuyPremiumDialog() {
    Future.delayed(Duration(milliseconds: 100), () async {
      bool status = await showDialog(
        context: context,
        builder: (_) => BuyPremiumDialog(
          type: 2,
        ),
      );
      //print('status $status');
    });
  }
}
