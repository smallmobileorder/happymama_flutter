import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/entity/knoweledge_section_entity.dart';
import 'package:happy_mama/service/firebase/knowledge/knowledge_service.dart';
import 'package:happy_mama/storage/storage_service.dart';
import 'package:happy_mama/widgets/error_page.dart';
import 'package:happy_mama/widgets/loading_container.dart';

import 'sections_list_view.dart';

class KnowledgePage extends StatefulWidget {
  final Stream<bool> tabChangeStream;
  KnowledgePage({Key key, this.tabChangeStream}) : super(key: key);

  @override
  _KnowledgePageState createState() => _KnowledgePageState();
}

class _KnowledgePageState extends State<KnowledgePage> {
  List<KnowledgeSectionEntity> sections = [];
  bool isLoading;
  bool isError;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    isError = false;
    loadKnowledgeBase()
        .whenComplete(() => setState(() => isLoading = false))
        .catchError((e) {
      //print('ERROR IN KNOWLEDGE: $e');
      isLoading = false;
      setState(() => isError = true);
      throw e;
    });
  }

  Future loadKnowledgeBase() async {
    var service =  KnowledgeService();
    sections = await service.fetchSections();
    Map<int, List<int>> bookmarks = await getBookmarks();
    if (bookmarks != null &&
        bookmarks.isNotEmpty &&
        sections != null &&
        sections.isNotEmpty) {
      sections.forEach((s) {
        if (bookmarks?.containsKey(s.id) ?? false) {
          if (bookmarks[s.id]?.contains(-1) ?? false) {
            s.bookmark = true;
          }
          s.articles.forEach((a) {
            if (bookmarks[s.id]?.contains(a.id) ?? false) {
              a.bookmark = true;
            }
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        if ((isLoading || isError) == false)
          SectionsListView(
            sections: sections,
            tabChangeStream: widget?.tabChangeStream,
          ),
        if (isError) ErrorPage(),
        if (isLoading) LoadingContainer(),
      ],
    );
  }

  Future<Map<int, List<int>>> getBookmarks() async {
    return (await StorageService.readKnowledgeBookmarks() ?? {});
  }
}
