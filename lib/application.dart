import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:happy_mama/page/splash/splash_page.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:rxdart/rxdart.dart';
import 'page/home.dart';
import 'package:flutter/foundation.dart';
import 'non_splash_scroll_behavior.dart';

GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

/// Main colors for themes
const blueColor = Color(0xFF22CADE);
const pinkColor = Color(0xFFFF8A9F);
Color themeMainColor;

BehaviorSubject<bool> mainThemeIsPink = BehaviorSubject.seeded(false);
final FirebaseAnalytics analytics = FirebaseAnalytics();
final FirebaseAnalyticsObserver observer =
    FirebaseAnalyticsObserver(analytics: analytics);

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white,
    ));
    ////print(BlocProvider.of<ProfileBloc>(context).notifyBloc.wasOpenedFromPush);
    return StreamBuilder<bool>(
        stream: mainThemeIsPink,
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            themeMainColor = blueColor;
            ColorStyle.tint = blueColor;
          } else {
            themeMainColor = snapshot.data ? pinkColor : blueColor;
            ColorStyle.tint = snapshot.data ? pinkColor : blueColor;
          }
          //print('Color is $themeMainColor');
          return MaterialApp(
            title: 'HappyMama',
            theme: Platform.isIOS ? iosTheme() : androidTheme(),
            themeMode: ThemeMode.light,
            home: SplashPage(),
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              const FallbackCupertinoLocalizationsDelegate(),
              const BackButtonTextDelegate(),
            ],
            supportedLocales: [const Locale('ru', 'RU')],
            builder: (context, child) {
              return ScrollConfiguration(
                behavior: NonSplashScrollBehavior(),
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  resizeToAvoidBottomPadding: true,
                  body: child,
                  key: scaffoldKey,
                ),
              );
            },
            navigatorObservers: <NavigatorObserver>[observer],
          );
        });
  }
}

class BackButtonTextOverride extends DefaultMaterialLocalizations {
  BackButtonTextOverride(Locale locale) : super();

  @override
  String get backButtonTooltip => null;
}

class BackButtonTextDelegate
    extends LocalizationsDelegate<MaterialLocalizations> {
  const BackButtonTextDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<MaterialLocalizations> load(Locale locale) {
    return SynchronousFuture(BackButtonTextOverride(locale));
  }

  @override
  bool shouldReload(BackButtonTextDelegate old) => false;
}

class FallbackCupertinoLocalizationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalizationsDelegate old) => false;
}
