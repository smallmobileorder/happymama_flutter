import 'dart:ui';

class ColorStyle {
  static Color _tint = Color(0xFF22CADE);

  static set tint(Color color) => _tint = color;

  static get tint => _tint;
  static const dirtyWhite = Color(0xFFF7F6E7);
  static const dirtyBlack = Color(0xFF181818);
  static const inactive = Color(0xFFC4C4C4);
  static const conifer = Color(0xFFB9E462);
  static const doveGray = Color(0xFF696969);
  static const tarawera = Color(0xFF0A4065);
  static const codGray = Color(0xFF181818);
  static const tundora = Color(0xFF444444);
  static const disabled = Color(0XFF636366);
  static const sunglow = Color(0XFFFDBD37);
  static const pink = Color(0XFFFE7BA0);
  static const cyan = Color(0xFF22CADE);
}
