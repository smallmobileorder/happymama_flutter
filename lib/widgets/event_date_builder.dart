import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_mama/util/formatter.dart';

/// Using in events: doctors visit and vaccines
class EventDateBuilder extends StatelessWidget {
  final DateTime date;
  final Function onTap;

  const EventDateBuilder(this.date, {this.onTap});

  @override
  Widget build(BuildContext context) {
    String dateText;
    if (date == null) {
      dateText = 'ДД.ММ.ГГГГ';
    } else {
      dateText = Formatter.formatDate(date);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(width: 40),
        InkWell(
          onTap: onTap,
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Row(
            children: <Widget>[
              Text(
                dateText,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Container(width: 16),
              SvgPicture.asset(
                'assets/vectors/calendar.svg',
                color: Theme.of(context).primaryColor,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
