import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';

class BrandCheckbox extends StatefulWidget {
  final ValueKey<bool> key;
  final ValueChanged<bool> onChecked;
  final bool value;
  final String text;
  final bool forSettings;

  BrandCheckbox({
    this.key,
    @required this.onChecked,
    @required this.value,
    @required this.text,
    this.forSettings = false,
  });

  @override
  createState() => _BrandCheckboxState();
}

class _BrandCheckboxState extends State<BrandCheckbox> {
  bool _value;

  @override
  void initState() {
    _value = widget.value ?? false;
    super.initState();
  }

  void _check() {
    final newValue = !_value;
    setState(() => _value = newValue);
    widget.onChecked(newValue);
  }

  @override
  Widget build(BuildContext context) {
    ////print('sadasd');
    if (widget.forSettings) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 16,
            width: 16,
            decoration: BoxDecoration(
                border: Border.all(color: ColorStyle.tint, width: 1),
                borderRadius: BorderRadius.circular(2)),
            child: Theme(
              data: ThemeData(
                unselectedWidgetColor: Colors.transparent,
              ),
              child: Checkbox(
                onChanged: (checked) => _check(),
                value: _value,
                checkColor: ColorStyle.tint,
                activeColor: Colors.transparent,
              ),
            ),
          ),
          Container(width: 8),
          Expanded(
            child: Text(
              widget.text,
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                  color: ColorStyle.dirtyBlack),
            ),
          ),
        ],
      );
    }
    return SizedBox(
      key: widget.key,
      height: 16,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 16,
            width: 16,
            decoration: BoxDecoration(
                border: Border.all(color: ColorStyle.tint, width: 1),
                borderRadius: BorderRadius.circular(2)),
            child: Theme(
              data: ThemeData(
                unselectedWidgetColor: Colors.transparent,
              ),
              child: Checkbox(
                onChanged: (checked) => _check(),
                value: _value,
                checkColor: ColorStyle.tint,
                activeColor: Colors.transparent,
              ),
            ),
          ),
          Container(width: 16),
          InkWell(
            onTap: () => _check(),
            child: Text(
              widget.text,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
                color: ColorStyle.dirtyBlack,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
