import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'custom_cupertino_alert_dialog.dart';

class ConfirmDialog extends StatelessWidget {
  final String title;
  final String subtitle;

  /// Is action for destroying / deleting something
  /// true - for deleting
  /// false - for keeping smth
  final bool destructive;

  final bool destructiveIsBlue;

  const ConfirmDialog({
    this.title = '',
    this.subtitle = '',
    this.destructive = true,
    this.destructiveIsBlue = false,
  });

  @override
  Widget build(BuildContext context) {
    return CustomCupertinoAlertDialog(
      title: Text(title),
      content: Text(subtitle),
      actions: <Widget>[
        CustomCupertinoDialogAction(
          isDefaultAction: destructive ? false : true,
          child: Text(destructive ? 'Отмена' : 'Да'),
          onPressed: () =>
              Navigator.of(context).pop(destructive ? false : true),
        ),
        CustomCupertinoDialogAction(
          isDestructiveAction: destructiveIsBlue ? false : true,
          isDefaultAction: destructiveIsBlue ? true : (destructive ? true : false),
          child: Text(destructive ? 'Да' : 'Отмена'),
          onPressed: () =>
              Navigator.of(context).pop(destructive ? true : false),
        ),
      ],
    );
  }
}
