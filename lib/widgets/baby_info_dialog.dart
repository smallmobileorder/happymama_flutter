import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/service/firebase/baby/baby_info_service.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/util/formatter.dart';

import 'brand_raw_button.dart';
import 'info_dialog.dart';

class BabyInfoDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BabyInfoDialogState();
}

class _BabyInfoDialogState extends State<BabyInfoDialog> {
  DateTime date;
  String _gender = '';
  TextEditingController controller = TextEditingController();
  BabyInfoService service = BabyInfoService();
  ProfileBloc bloc;

  TextStyle titleStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
  );

  OutlineInputBorder border;

  bool buttonPressed;

  @override
  void initState() {
    border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(
        width: 2,
        color: ColorStyle.tint, //Theme.of(context).primaryColor,
      ),
    );
    bloc = BlocProvider.of<ProfileBloc>(context);
    buttonPressed = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Дата рождения малыша?', style: titleStyle),
              Container(height: 16),
              _DatePicker(date, onTap: pickDate),
              Container(height: 16),
              Text('У вас мальчик или девочка?', style: titleStyle),
              Container(height: 12),
              CheckboxGroup(
                padding: EdgeInsets.zero,
                labels: ["Мальчик", "Девочка"],
                onSelected: (selection) => setState(() =>
                    _gender = selection.length == 0 ? '' : selection.last),
                checked: [_gender],
                itemBuilder: (checkbox, text, index) {
                  return Padding(
                    padding: EdgeInsets.only(bottom: index == 0 ? 12 : 0),
                    child: SizedBox(
                      height: 16,
                      child: Row(
                        children: [
                          Container(
                            height: 16,
                            width: 16,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: ColorStyle.tint, width: 1),
                                borderRadius: BorderRadius.circular(2)),
                            child: Theme(
                              data: ThemeData(
                                unselectedWidgetColor: Colors.transparent,
                              ),
                              child: Checkbox(
                                onChanged: (value) => setState(() =>
                                    value ? _gender = text.data : _gender = ''),
                                value: _gender.contains(text.data),
                                checkColor: ColorStyle.tint,
                                activeColor: Colors.transparent,
                              ),
                            ),
                          ),
                          Container(width: 8),
                          BrandRawButton(
                            onPressed: () =>
                                setState(() => _gender = text.data),
                            color: Colors.transparent,
                            child: Text(
                              text.data,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12,
                                  color: ColorStyle.dirtyBlack),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
              Container(height: 9),
              Text(
                'Если у вас два и более детей до года, вы сможете позже добавить отдельный профиль на каждого ребенка в разделе Настройки',
                style: TextStyle(fontSize: 9, color: ColorStyle.dirtyBlack),
              ),
              Container(height: 16),
              Text('Как зовут кроху?', style: titleStyle),
              Container(height: 12),
              TextField(
                scrollPadding: EdgeInsets.only(bottom: 100),
                controller: controller,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(16, 17, 16, 18),
                  enabledBorder: border,
                  focusedBorder: border,
                ),
              ),
              Container(height: 24),
              Center(
                child: CupertinoButton.filled(
                  child: Text('Сохранить'),
                  onPressed: save,
                ),
              ),
              Container(height: 8),
            ],
          ),
        ),
      ),
    );
  }

  void pickDate() async {
    DateTime now = DateTime.now();
    DateTime _date = await showDatePicker(
      context: context,
      initialDate: now,
      firstDate: now.subtract(Duration(days: 365)),
      lastDate: now,
    );
    if (_date == null) {
      return;
    }
    setState(() => date = _date);
  }

  void save() {
    if (buttonPressed) return;
    buttonPressed = true;
    String name = controller.text;
    if (date == null) {
      showError('Выберите дату рождения');
      buttonPressed = false;
      return;
    }
    if (_gender.isEmpty) {
      showError('Выберите пол ребенка');
      buttonPressed = false;
      return;
    }
    if (name.isEmpty) {
      showError('Введите имя ребенка');
      buttonPressed = false;
      return;
    }
    name = controller.text[0].toUpperCase() + controller.text.substring(1);
    String _genderNew = '';
    if (_gender == 'Мальчик') {
      _genderNew = 'male';
    } else if (_gender == 'Девочка') {
      _genderNew = 'female';
    }
    service
        .postInfo(
            bloc.parent.kids[0], name, _genderNew, Formatter.formatDate(date))
        .whenComplete(() {
      bloc.changeChildInfo(
          name, _gender == 'Мальчик' ? 'male' : 'female', true, date);
      buttonPressed = false;
      Navigator.of(context).pop();
    }).catchError((e) {
      showError(e);
      buttonPressed = false;
      Navigator.of(context).pop();
    });
  }

  void showError(String text) {
    showDialog(
      context: context,
      builder: (_) => InfoDialog(
        title: text,
      ),
    );
  }
}

class _DatePicker extends StatelessWidget {
  final DateTime date;
  final Function onTap;

  const _DatePicker(this.date, {this.onTap});

  @override
  Widget build(BuildContext context) {
    String dateText;
    if (date == null) {
      dateText = 'ДД.ММ.ГГГГ';
    } else {
      dateText = Formatter.formatDate(date);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        InkWell(
          onTap: onTap,
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Row(
            children: <Widget>[
              Text(
                dateText,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Container(width: 16),
              SvgPicture.asset(
                'assets/vectors/calendar.svg',
                color: Theme.of(context).primaryColor,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
