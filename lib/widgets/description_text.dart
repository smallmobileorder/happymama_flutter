import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';

class DescriptionTextWidget extends StatefulWidget {
  final String text;
  final TextStyle style;

  DescriptionTextWidget({Key key, @required this.text, @required this.style})
      : super(key: key);

  @override
  _DescriptionTextWidgetState createState() => _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.contains('\n')) {
      int index = widget.text.indexOf('\n');
      firstHalf = widget.text.substring(0, index);
      secondHalf = widget.text.substring(index);
    } else {
      if (widget.text.length > 50) {
        firstHalf = widget.text.substring(0, 50);
        secondHalf = widget.text.substring(50, widget.text.length);
      } else {
        firstHalf = widget.text;
        secondHalf = "";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: secondHalf.isEmpty
          ? Padding(
              padding: EdgeInsets.only(top: 8),
              child: Text(
                firstHalf,
                style: widget.style,
                textAlign: TextAlign.left,
              ),
            )
          : flag
              ? GestureDetector(
                  onTap: () => setState(() => flag = !flag),
                  child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(vertical: 8),
                    color: Colors.transparent,
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: firstHalf + '... ',
                          style: widget.style,
                        ),
                        TextSpan(
                          text: 'ещё',
                          style: widget.style.copyWith(color: ColorStyle.tint),
                        ),
                      ]),
                    ),
                  ),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(height: 8),
                    Text(
                      firstHalf + secondHalf,
                      style: widget.style,
                    ),
                    GestureDetector(
                      onTap: () => setState(() => flag = !flag),
                      child: Container(
                        color: Colors.transparent,
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Скрыть",
                              style: widget.style.copyWith(
                                color: ColorStyle.tint,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
    );
  }
}
