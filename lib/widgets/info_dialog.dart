import 'dart:async';

import 'package:flutter/cupertino.dart';

import 'custom_cupertino_alert_dialog.dart';

class InfoDialog extends StatefulWidget {
  final String title;
  final String message;
  final Duration duration;
  final Color borderColor;

  const InfoDialog({
    Key key,
    @required this.title,
    this.message,
    this.duration = const Duration(seconds: 2),
    this.borderColor,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _InfoDialogState();
}

class _InfoDialogState extends State<InfoDialog> {
  StreamSubscription wait;

  @override
  void initState() {
    wait = Future.delayed(widget.duration)
        .asStream()
        .listen((_) => Navigator.of(context).pop());
    super.initState();
  }

  @override
  void dispose() {
    wait?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomCupertinoAlertDialog(
      title: Text(widget?.title??''),
      content: widget.message == null ? Container() : Text(widget.message),
      borderColor: widget.borderColor,
    );
  }
}
