import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';

class LoadingContainer extends StatelessWidget {
  final double height;
  final double width;
  final Color color;
  final double strokeWidth;

  LoadingContainer(
      {Key key,
      this.height = 70,
      this.width = 50,
      this.color,
      this.strokeWidth = 5.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color _color = color ?? ColorStyle.tint;
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: strokeWidth,
        valueColor: AlwaysStoppedAnimation<Color>(
          _color,
        ),
      ),
    );
  }
}
