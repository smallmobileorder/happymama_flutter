import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';

import 'brand_raw_button.dart';

class BrandButton extends StatefulWidget {
  final Function onPressed;
  final String text;
  final Color textColor;
  final Color color;
  final double borderRadius;

  BrandButton(
      {this.onPressed,
      @required this.text,
      this.textColor,
      this.color,
      this.borderRadius});

  @override
  createState() => _BrandButtonState();

  static Widget unfilled(
      {Function onPressed,
      String text = '',
      EdgeInsets margin = const EdgeInsets.only(left: 48, right: 48, bottom: 16),
      double height = 48}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          //constraints: BoxConstraints(maxHeight: 70, minHeight: 48),
          //height: height,
          width: double.infinity,
          margin: margin,
          child: InkWell(
            borderRadius: BorderRadius.circular(15),
            onTap: onPressed,
            child: Ink(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: ColorStyle.tint, width: 2),
                  borderRadius: BorderRadius.circular(15)),
              child: Center(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 14,
                      color: ColorStyle.codGray,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _BrandButtonState extends State<BrandButton> {
  @override
  Widget build(BuildContext context) {
    return BrandRawButton(
      onPressed: widget.onPressed,
      child: Text(
        widget.text,
        style: TextStyle(
            color: widget.textColor ?? ColorStyle.dirtyWhite, fontSize: 12),
      ),
      color: widget.color,
      borderRadius: widget.borderRadius,
    );
  }
}
