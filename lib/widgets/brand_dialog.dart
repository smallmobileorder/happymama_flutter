
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrandDialog extends StatefulWidget {
  final String title;
  final String message;
  final Widget child;

  const BrandDialog({
    Key key,
    @required this.title,
    this.message, this.child,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BrandDialogState();
}

class _BrandDialogState extends State<BrandDialog> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        //margin: EdgeInsets.fromLTRB(24, 100, 24, 100),
        constraints: BoxConstraints(
          maxHeight: 400,
          minHeight: 200,
        ),
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            width: 2,
            color: Theme.of(context).primaryColor,
          ),
        ),
        child: widget.child,
      ),
    );
  }

  Widget title(String text) {}

  Widget message(String text) {}
}
