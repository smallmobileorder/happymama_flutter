import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

/// Using in doctors and vaccines edit page, like dropdown menu
/// Call it with showDialog()
///
///
///
class EventNameSelector extends StatefulWidget {
  final String text;
  final String ownButtonText;
  final List<String> names;
  final Function overrideTapTileFunction;
  final Function overrideTapBottomFunction;
  final showChosen;
  final initValue;

  EventNameSelector({
    @required this.text,
    @required this.ownButtonText,
    @required this.names,
    this.overrideTapTileFunction,
    this.overrideTapBottomFunction,
    this.showChosen = false,
    this.initValue = '',
  });

  createState() => _EventNameSelectorState();
}

class _EventNameSelectorState extends State<EventNameSelector> {
  String text;
  String ownButtonText;
  List<String> names;
  bool showChosen;
  String initValue;
  String chosenName;

  @override
  void initState() {
    text = widget?.text ?? '';
    ownButtonText = widget?.ownButtonText ?? '';
    names = widget?.names ?? [];
    showChosen = widget?.showChosen ?? false;
    initValue = widget?.initValue ?? '';
    chosenName = initValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List list = names
        .map((name) => FlatButton(
              onPressed: () {
                if (showChosen) setState(() => chosenName = name);
                //print(chosenName);
                if (widget?.overrideTapTileFunction == null) {
                  Navigator.of(context).pop(name);
                  return;
                }
                widget?.overrideTapTileFunction(
                    chosenName.replaceAll('(по умолчанию)', ''));
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                child: Container(
                  child: Center(
                    child: Text(
                      name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
              ),
            ))
        .toList();
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        //margin: EdgeInsets.fromLTRB(24, 100, 24, 100),
        constraints: BoxConstraints(
          maxHeight: 400,
          minHeight: 200,
        ),
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            width: 2,
            color: Theme.of(context).primaryColor,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            /// Top text
            ClipRRect(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(13),
                topLeft: Radius.circular(13),
              ),
              child: Container(
                height: 48,
                color: Theme.of(context).primaryColor,
                child: Center(
                  child: Text(
                    text,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ),

            /// List of event names
            Expanded(
              child: Container(
                color: Colors.white,
                child: ListView(
                  padding: EdgeInsets.symmetric(vertical: 6),
                  physics: BouncingScrollPhysics(),
                  children: list,
                ),
              ),
            ),

            /// Bottom button
            Container(
              height: 48,
              decoration: BoxDecoration(
                color: Colors.transparent,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.25),
                    offset: Offset(0, -1),
                    blurRadius: 4,
                  )
                ],
              ),
              child: ClipRRect(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(13),
                  bottomRight: Radius.circular(13),
                ),
                child: Container(
                  color: Colors.white,
                  child: FlatButton(
                    shape: Border(),
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      if (widget?.overrideTapBottomFunction == null) {
                        Navigator.of(context).pop(ownButtonText);
                        return;
                      }
                      widget?.overrideTapBottomFunction(
                          showChosen ? chosenName : ownButtonText);
                      Navigator.of(context).pop(ownButtonText);
                    },
                    child: Center(
                      child: Text(
                        showChosen
                            ? 'Установить ${chosenName.replaceAll(('(по умолчанию)'), '').trim()}'
                            : ownButtonText,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
