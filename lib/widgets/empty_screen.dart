import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EmptyScreen extends StatelessWidget {
  final String textBeforePic;
  final String textAfterPic;

  const EmptyScreen({Key key, @required this.textBeforePic, this.textAfterPic})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery
        .of(context)
        ?.size
        ?.height ?? 400;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            textBeforePic??'',
            style: TextStyle(
              fontSize: 56,
              fontWeight: FontWeight.bold,
              fontFamily: 'AmaticSC',
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
            textAlign: TextAlign.center,
          ),
          SvgPicture.asset(
            'assets/vectors/BabyFiveWithStar.svg', height: height / 2.5,),
          Text(
            textAfterPic ?? 'Но они обязательно скоро\nпоявятся',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 34,
              fontWeight: FontWeight.bold,
              fontFamily: 'AmaticSC',
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
          ),
        ],
      ),
    );
  }
}