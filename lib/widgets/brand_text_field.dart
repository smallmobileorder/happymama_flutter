import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';
import 'package:happy_mama/widgets/brand_raw_button.dart';

class BrandTextField extends StatefulWidget {
  final TextEditingController controller;
  final String placeholder;
  final Color borderColor;
  final bool obscureText;
  final int maxLength;
  final Color cursorColor;
  final int maxLines;

  BrandTextField({
    this.controller,
    this.placeholder,
    this.borderColor,
    this.obscureText = false,
    this.maxLength,
    this.cursorColor,
    this.maxLines = 1,
  });

  @override
  createState() => _BrandTextFieldState();
}

class _BrandTextFieldState extends State<BrandTextField> {
  bool _obscureText;

  @override
  void initState() {
    _obscureText = widget.obscureText;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(
              color: widget.borderColor ?? ColorStyle.tint, width: 2),
          borderRadius: BorderRadius.circular(15)),
      child: Stack(children: [
        CupertinoTextField(
          maxLines: widget.maxLines,
          maxLength: widget.maxLength,
          obscureText: _obscureText,
          padding: EdgeInsets.only(
            top: 11,
            left: 14,
            bottom: 11,
            right: widget.obscureText ? 32 : 14,
          ),
          decoration: BoxDecoration(),
          cursorColor: widget.cursorColor ?? ColorStyle.tint,
          controller: widget.controller,
          placeholder: widget.placeholder,
          placeholderStyle:
              TextStyle(color: Color.fromRGBO(172, 172, 169, 1), fontSize: 12),
          style: TextStyle(fontSize: 12, color: Theme.of(context).textTheme.body1.color),
        ),
        !widget.obscureText
            ? Container()
            : Positioned(
                top: 0,
                right: 0,
                bottom: 0,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: BrandRawButton(
                    onPressed: () =>
                        setState(() => _obscureText = !_obscureText),
                    color: Colors.transparent,
                    child: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off,
                      color: ColorStyle.inactive,
                      size: 20,
                    ),
                  ),
                ),
              )
      ]),
    );
  }
}
