import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/zoomable.dart';
import 'package:happy_mama/style/color_style.dart';

class ImageViewer extends StatefulWidget {
  final Image image;
  final Object heroTag;

  const ImageViewer({Key key, this.image, this.heroTag}) : super(key: key);

  _ImageViewerState createState() => _ImageViewerState();
}

class _ImageViewerState extends State<ImageViewer> {
  bool _lockController = false;
  double startSwipePosition;
  double swipePosition = 0;

  @override
  Widget build(BuildContext context) {
    Widget buildItem = ZoomableWidget(
      onZoomChanged: (zoom) {
        if (zoom == 1.0) {
          _lockController = false;
        } else {
          _lockController = true;
        }
        setState(() {});
      },
      zoomSteps: 2,
      maxScale: 6,
      minScale: 1,
      autoCenter: true,
      singleFingerPan: true,
      multiFingersPan: true,
      child: Transform(
        transform: Matrix4.translationValues(0, swipePosition, 0),
        child: widget.image,
      ),
    );
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: ColorStyle.tint),
      ),
      body: Hero(
        tag: widget?.heroTag ?? 'default',
        child: Container(
          child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onVerticalDragStart: _lockController
                  ? null
                  : (data) {
                      swipePosition = 0;
                      setState(() {});
                    },
              onVerticalDragUpdate: _lockController
                  ? null
                  : (data) {
                      swipePosition += data.delta.dy;
                      setState(() {});
                    },
              onVerticalDragEnd: _lockController
                  ? null
                  : (data) {
                      if (swipePosition.abs() >
                          MediaQuery.of(context).size.height / 8) {
                        Navigator.pop(context);
                      } else {
                        swipePosition = 0;
                        setState(() {});
                      }
                    },
              child: buildItem),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
