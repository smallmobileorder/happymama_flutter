import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrandRawButton extends StatefulWidget {
  final Function onPressed;
  final Color color;
  final Widget child;
  final double borderRadius;

  BrandRawButton({this.onPressed, this.color, this.child, this.borderRadius});

  @override
  createState() => _BrandRawButtonState();
}

class _BrandRawButtonState extends State<BrandRawButton> {
  @override
  Widget build(BuildContext context) {
    RoundedRectangleBorder shape;
    if (widget.borderRadius != null) {
      shape = RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(widget.borderRadius));
    }
    if (Platform.isIOS) {
      return widget.color == null
          ? CupertinoButton.filled(
              onPressed: widget.onPressed,
              padding: EdgeInsets.zero,
              borderRadius: BorderRadius.circular(widget.borderRadius ?? 15),
              pressedOpacity: 0.7,
              child: widget.child,
            )
          : CupertinoButton(
              onPressed: widget.onPressed,
              padding: EdgeInsets.zero,
              borderRadius: BorderRadius.circular(widget.borderRadius ?? 15),
              pressedOpacity: 0.7,
              color: widget.color,
              child: widget.child,
            );
    } else {
      return FlatButton(
        color: widget.color ?? Theme.of(context).buttonColor,
        onPressed: widget.onPressed,
        padding: EdgeInsets.zero,
        child: widget.child,
        shape: shape,
      );
    }
  }
}
