import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_mama/bloc/bloc_provider.dart';
import 'package:happy_mama/bloc/profile_bloc.dart';
import 'package:happy_mama/service/firebase/user/user_service.dart';
import 'package:happy_mama/widgets/brand_text_field.dart';
import 'package:happy_mama/widgets/custom_cupertino_alert_dialog.dart';
import 'package:happy_mama/widgets/info_dialog.dart';

class RateDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RateDialogState();
}

class _RateDialogState extends State<RateDialog> {
  int stars = 0;
  TextEditingController controller = TextEditingController();
  ScrollController scrollController = ScrollController();
  ProfileBloc bloc;

  @override
  void initState() {
    /*controller.addListener(() {
      //print(scrollController.offset.toString());
      scrollController.animateTo(scrollController.offset + 40,
          duration: Duration(milliseconds: 500), curve: Curves.easeOut);
    });*/
    bloc = BlocProvider.of<ProfileBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          //controller: scrollController,
          child: Container(
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.vertical -
                50,
            child: GestureDetector(
              onTap: () => print('tap'),
              child: CustomCupertinoAlertDialog(
                title: Text('Нравится HappyMama?'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(height: 12),
                    Text('Оцените приложение'),
                    Container(height: 12),
                    buildStars(),
                    Container(height: 16),
                    Text('Поделитесь впечатлениями'),
                    Container(height: 12),
                    BrandTextField(
                      controller: controller,
                      placeholder: 'Ваши впечатления',
                      maxLines: 3,
                    ),
                  ],
                ),
                actions: <Widget>[
                  CustomCupertinoDialogAction(
                    child: Text('Позже'),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  CustomCupertinoDialogAction(
                    child: Text('Оценить'),
                    isDefaultAction: true,
                    onPressed: () {
                      if (stars == 0) {
                        showDialog(
                          context: context,
                          builder: (context) => InfoDialog(
                            title: 'Вы не оценили приложение',
                          ),
                        );
                        return;
                      }

                      /// Send data to firebase
                      //print('SENDING DATA TO USER WITH ID='
                          //'${bloc.parent.identifier}');
                      UserService userService = UserService();
                      userService.rateApp(
                        bloc.parent.identifier,
                        stars,
                        comment: controller.text,
                      );
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildStars() {
    List<Widget> starsList = List.generate(5, (index) => index)
        .map((index) => Container(
              height: 32,
              width: 32,
              child: GestureDetector(
                child: SvgPicture.asset(index < stars
                    ? 'assets/vectors/rate_filled.svg'
                    : 'assets/vectors/rate_empty.svg'),
                onTap: () => setState(() => stars = index + 1),
              ),
            ))
        .toList();
    return Row(
      children: starsList,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }
}
