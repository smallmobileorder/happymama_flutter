import 'package:flutter/material.dart';

class GradientContainer extends StatefulWidget {

  final Widget child;

  GradientContainer({this.child});

  @override
  createState() => _GradientContainerState();
}

class _GradientContainerState extends State<GradientContainer> {

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFFF7F6E7),
            Colors.white
          ],
          stops: [0.4, 1]
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.25),
            blurRadius: 2
          )
        ]
      ),
      child: widget.child,
    );
  }

}
