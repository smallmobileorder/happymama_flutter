import 'package:flutter/material.dart';
import 'package:happy_mama/application.dart';

void showSnackBar(
  SnackBar snackBar, {
  bool hidePrev = true,
}) {
  assert(snackBar != null);
  if (hidePrev) {
    scaffoldKey.currentState.hideCurrentSnackBar();
  }
  scaffoldKey.currentState.showSnackBar(snackBar);
}

TextStyle _normalSnackBarTextStyle = TextStyle();

SnackBar normalSnackBar(
  String text, {
  Duration duration = const Duration(seconds: 2),
}) =>
    SnackBar(
      content: Text(text, style: _normalSnackBarTextStyle),
    );

SnackBar notAvailableSnackBar({
  Duration duration = const Duration(seconds: 2),
}) =>
    SnackBar(
      content: Text(
        'Раздел находится в разработке',
        style: _normalSnackBarTextStyle,
      ),
    );
