import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ErrorPage extends StatelessWidget {
  final String error;
  final String help;

  const ErrorPage({Key key, this.error, this.help}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery
        .of(context)
        ?.size
        ?.height ?? 400;
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          Text(
            error ?? 'Что-то пошло не так',
            style: TextStyle(
              fontSize: 56,
              fontWeight: FontWeight.bold,
              fontFamily: 'AmaticSC',
              color: Theme.of(context).primaryColor,
            ),
            textAlign: TextAlign.center,
          ),
          SvgPicture.asset(
            'assets/vectors/BabyFiveWithStar.svg',
            height: height / 2.5,
          ),
          Text(
            help ?? 'Повторите позже',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 34,
              fontWeight: FontWeight.bold,
              fontFamily: 'AmaticSC',
              color: Theme.of(context).primaryColor,
            ),
          )
        ]));
  }
}
