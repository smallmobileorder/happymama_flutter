import 'package:flutter/material.dart';
import 'package:happy_mama/style/color_style.dart';

class PushNotification {
  final int id;
  final String title;
  final String body;
  final DateTime date;
  final String category;
  final Widget icon;
  final String buttonPath;
  final String buttonName;
  bool notRead;
  bool bookmark;

  ///style
  final Color backgroundColor;
  final Color iconColor;
  final Color titleTextColor;
  final Color bodyTextColor;

  PushNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    this.date,
    this.category = 'everyday',
    this.icon,
    this.notRead = true,
    this.bookmark = false,
    this.backgroundColor = Colors.white,
    this.iconColor = ColorStyle.codGray,
    this.titleTextColor = ColorStyle.codGray,
    this.bodyTextColor = ColorStyle.codGray,
    this.buttonPath = '',
    this.buttonName = '',
  });

  factory PushNotification.fromJson(Map<String, dynamic> jsonMap) {
    return PushNotification(
        id: jsonMap['id'],
        title: jsonMap['title'],
        body: jsonMap['body'],
        date:
            DateTime.parse(jsonMap['date'] ?? DateTime.now().toIso8601String()),
        category: jsonMap['category'],
        notRead: jsonMap['notRead'] ?? true,
        bookmark: jsonMap['bookmark'] ?? false,
        buttonPath: jsonMap['button_path'] ?? '',
        buttonName: jsonMap['button_name'] ?? '');
  }

  toJson() {
    return {
      'id': id,
      'title': title,
      'body': body,
      'date': date.toIso8601String(),
      'category': category,
      'notRead': notRead,
      'bookmark': bookmark,
      'button_path': buttonPath,
      'button_name': buttonName,
    };
  }
}
