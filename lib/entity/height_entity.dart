class HeightEntity {
  final int id;
  final String date;
  final int height;

  HeightEntity({this.id, this.date, this.height});

  factory HeightEntity.fromJson(Map map) {
    if (map == null) return null;
    return HeightEntity(
      id: map['id'],
      date: map['date'],
      height: map['height'],
    );
  }
}
