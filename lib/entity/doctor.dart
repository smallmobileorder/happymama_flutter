import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:happy_mama/util/formatter.dart';

class Doctor {
  final String name;
  final DateTime date;
  final String comment;

  Doctor({
    @required this.name,
    @required this.date,
    this.comment = '',
  });

  Doctor copyWith({String name, DateTime date, String comment, bool deleted}) {
    return Doctor(
      name: name ?? this.name,
      date: date ?? this.date,
      comment: comment ?? this.comment,
    );
  }

  Doctor withNullDate() {
    return Doctor(
      name: this.name,
      date: null,
      comment: this.comment,
    );
  }

  factory Doctor.fromJson(Map<dynamic, dynamic> json) {
    if (json == null || json['date'] == null) {
      return null;
    }
    DateTime parsed = Formatter.parseFormattedStringDate(json['date']);
    DateTime date = DateTime(
      parsed.year,
      parsed.month,
      parsed.day,
    );
    return Doctor(
      name: json['type'],
      date: date,
      comment: json['comment'],
    );
  }

  Map<String, String> toMap() {
    return {
      'type': name,
      'date': Formatter.formatDate(date),
      'comment': comment,
    };
  }
}
