import 'package:flutter/cupertino.dart';

class Advice {
  final String image;
  final String text;
  final String path;

  Advice({
    @required this.image,
    @required this.text,
    this.path,
  });

  factory Advice.fromJson(var json) {
    if (json == null) {
      return null;
    }
    return Advice(
      image: json['image'],
      text: json['text'],
      path: json['path']
    );
  }
}