import 'package:flutter/material.dart';
import 'package:happy_mama/util/formatter.dart';

class Vaccine {
  final String name;
  final DateTime date;
  final String comment;

  Vaccine({
    @required this.name,
    @required this.date,
    this.comment = '',
  });

  Vaccine copyWith({String name, DateTime date, String comment, bool deleted}) {
    return Vaccine(
      name: name ?? this.name,
      date: date ?? this.date,
      comment: comment ?? this.comment,
    );
  }

  Vaccine withNullDate() {
    return Vaccine(
      name: this.name,
      date: null,
      comment: this.comment,
    );
  }

  factory Vaccine.fromJson(Map<dynamic, dynamic> json) {
    if (json == null || json['date'] == null) {
      return null;
    }
    DateTime parsed = Formatter.parseFormattedStringDate(json['date']);
    DateTime date = DateTime(
      parsed.year,
      parsed.month,
      parsed.day,
    );
    return Vaccine(
      name: json['type'],
      date: date,
      comment: json['comment'],
    );
  }

  Map<String, String> toMap() {
    return {
      'type': name,
      'date': Formatter.formatDate(date),
      'comment': comment,
    };
  }
}
