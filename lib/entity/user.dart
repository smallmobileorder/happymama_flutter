import 'package:happy_mama/util/formatter.dart';

class User {
  final String identifier;
  final String email;
  final List<String> kids;
  final DateTime creationDate;

  User({this.identifier, this.email, this.kids, this.creationDate});

  factory User.fromJson(Map<String, dynamic> jsonMap) {
    return User(
        identifier: jsonMap['id'],
        email: jsonMap['email'],
        kids: jsonMap['kids'],
        creationDate: DateTime.parse(jsonMap['creationDate']));
  }

  toJson() {
    return {
      'id': identifier,
      'email': email,
      'kids': kids,
      'creationDate': creationDate.toIso8601String(),
    };
  }
}
