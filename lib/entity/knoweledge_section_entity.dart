import 'package:happy_mama/util/cast.dart';

class KnowledgeSectionEntity {
  final int id;
  final String name;
  final String url;
  final List<ArticleEntity> articles;
  final String text;
  final List<String> images;
  bool bookmark;
  final bool premium;

  KnowledgeSectionEntity(
      {this.premium = false,
      this.images,
      this.id,
      this.name = '',
      this.url,
      this.articles,
      this.text = '',
      this.bookmark = false});

  factory KnowledgeSectionEntity.fromJson(var jsonMap) {
    List<ArticleEntity> list = [];
    if (jsonMap['articles'] != null)
      for (var art in jsonMap['articles']) {
        list.add(ArticleEntity.fromJson(art));
      }

    List<String> images = [];
    ////print(jsonMap['images']);
    List temp = cast(jsonMap['images']);
    temp?.forEach((v) => images.add(v));
    //(jsonMap['images'] as List)?.forEach((v) => images.add(v));
    return KnowledgeSectionEntity(
        id: jsonMap['id'],
        name: cast(jsonMap['name'])??'Bad name',
        url: cast(jsonMap['url'])??'Bad url',
        text: cast(jsonMap['text']),
        premium: cast(jsonMap['premium']) ?? false,
        images: images,
        articles: list);
  }
}

class ArticleEntity {
  final int id;
  final String name;
  final String text;
  final List<String> images;
  final bool premium;
  bool bookmark;

  ArticleEntity(
      {this.premium = false,
      this.id,
      this.name = '',
      this.text,
      this.images,
      this.bookmark = false});

  factory ArticleEntity.fromJson(var jsonMap) {
    List<String> images = [];
    //print(jsonMap['images']);
    List temp = cast(jsonMap['images']);
    temp?.forEach((v) => images.add(v));
    //(jsonMap['images'] as List)?.forEach((v) => images.add(v));
    return ArticleEntity(
      id: jsonMap['id'],
      name: cast(jsonMap['name'])??'Bad name',
      text: cast(jsonMap['text']),
      premium: cast(jsonMap['premium']) ?? false,
      images: images,
    );
  }

  String format() {
    String res = '';
    if (name != null && name.isNotEmpty) {
      res += name + '\n\n';
    }
    if (text != null && text.isNotEmpty) {
      res += formatText();
    }
    return res;
  }

  String formatText() {
    return text
        .replaceAll(RegExp(r'<image:.?image_\d\d?>'), '')
        .replaceAll(RegExp(r"<video='|'/>"), '')
        .replaceAll(RegExp(r"<button='.*?'>.*?<\/button>"), '')
        .replaceAll(RegExp(r"<a href='.*?'><image:.?image_\d\d?><\/a>"), '')
        .replaceAll(RegExp(r"(<a href='.*?'>|</a>)"), '')
        .replaceAll(RegExp(r'(<b>|</b>)'), '');
  }
}
