import 'dart:convert';
import 'package:happy_mama/page/baby/motor_skills/skill.dart';
import 'package:happy_mama/util/formatter.dart';
import 'child_jaw.dart';
import 'doctor.dart';
import 'vaccine.dart';

class Child {
  final String id;
  String photo;
  String name;
  String gender;

  bool get wasBorn => birthDate == null;
  DateTime birthDate;
  List<double> weight;
  List<int> height;
  ChildJaw jaw;
  List<String> photos;
  List<String> skills;
  List<Doctor> doctors;
  List<Vaccine> vaccines;
  int mainImageId;

  Child({
    this.id,
    this.name,
    this.gender,
    this.birthDate,
    this.weight,
    this.height,
    this.skills,
    String photo,
    this.mainImageId,
  });

  bool isEmpty() {
    return birthDate == null || name == null || name == '';
  }

  Child.fromJson(this.id, Map<dynamic, dynamic> json) {
    final birthDateString = json['birth_date'];
    if (birthDateString != null && birthDateString != '') {
      birthDate = Formatter.parseFormattedStringDate(birthDateString);
    }
    var _skills = json['skills'];
    if (_skills != null) {
      var map = _skills as Map<dynamic, dynamic>;

      //TODO AFTER DELETE APP INFO CATCH ERROR HERE AT INIT
      try {
        skills = map.entries
            .map((entry) => MapEntry(int.parse(entry.key.substring(0, 1)),
                Formatter.parseFormattedStringDate(entry.value)))
            .fold<List<MapEntry<int, DateTime>>>([], (list, skill) {
              list.add(skill);
              list.sort((a, b) {
                int compare = b.value.compareTo(a.value);
                if (compare == 0) {
                  return b.key.compareTo(a.key);
                }
                return compare;
              });
              return list;
            })
            .map((entry) => baseSkills[entry.key])
            .map((skill) => skill.shortName)
            .take(6)
            .toList();
      } catch (e) {
        //print(e);
        skills = [];
      }
    } else {
      skills = [];
    }
    gender = json['gender'];
    height = json['height'] == null ? [] : [json['height']];
    name = json['name'];
    photo = json['photo'];
    //skills = json['skills']?.cast<List<String>>() ?? [];
    //weight = json['weight'];
    weight = json['weight'] == null ? [] : [json['weight']];
    mainImageId = json['mainImageId'];
  }

  @override
  String toString() {
    return jsonEncode({
      'id': id,
      'photo': photo,
      'name': name,
      'gender': gender,
      'birthdate': birthDate?.toIso8601String(),
      'weight': weight,
      'height': height,
      'mainImageId': mainImageId,
      'skills': skills,
    });
  }
}
