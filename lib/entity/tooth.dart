import 'dart:math';

class Tooth {
  Point center = Point(0, 0);
  bool marked = false;
  String date = '';

  Tooth(
      this.center,
      this.marked,
      this.date,
      );
}
