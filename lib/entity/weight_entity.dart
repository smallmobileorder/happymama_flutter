import 'package:happy_mama/util/cast.dart';

class WeightEntity {
  final int id;
  final String date;
  final double weight;

  WeightEntity({this.id, this.date, this.weight});

  factory WeightEntity.fromJson(Map map) {
    if (map == null) return null;
    return WeightEntity(
      id: map['id'],
      date: map['date'],
      weight: cast<double>(map['weight']) ?? map['weight'].toDouble(),
    );
  }
}
